# coqdram

This is a Coq framework to design DRAM controllers. Everything is inside framework/DRAM.

The mcsimcoq submodule refers to the experiment described in Chapter 7 of my thesis.
