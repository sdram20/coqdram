Set Printing Projections.
Set Warnings "-notation-overridden,-parsing".

From mathcomp Require Import ssreflect eqtype ssrbool ssrnat fintype div ssrZ zify tuple.
From Coq Require Import Program Equality.
From DRAM Require Export InterfaceSubLayer.

Section TDMimpSL.

	Context {SYS_CFG : System_configuration}.

	Class TDM_configuration := mkTDMCFG {
		SN : nat;
		SN_gt_1 : SN > 1;

		SL : nat; 
		SL_pos 	: SL > 0;

		(* Necessary for TDM guarantees *)

		(* -------------- Intra bank constraints ---------------- *)
		(* SL is big enough to accomodate a PRE-ACT-CAS sequence *)
		SL_enough : T_RP + T_RCD + 1 <= SL;
		SL_WR_to_RD : T_WL + T_BURST + T_WTR_l <= SL;
		SL_RD_to_WR : T_RTW <= SL;
		SL_CAS_to_CAS : T_CCD_l <= SL;
		SL_ACT_to_ACT_SB : T_RRD_l <= SL;

		(* ------------- Inter-bank constraints ------------------ *)
		SL_ACT_to_PRE : T_RAS - 1 <= SL;
		SL_ACT_to_ACT_DB : T_RC - T_RP - 1 <= SL;
		SL_RD_to_PRE : T_RTP - 1 <= SL;
		SL_WR_to_PRE : T_WL + T_BURST + T_WR - 1 <= SL;
	}.

	Context {TDM_CFG : TDM_configuration}.
	
	Definition Slot_t := ordinal_eqType SN.

  #[global] Instance REQUESTOR_CFG : Requestor_configuration := 
  {
    Requestor_t := Slot_t
  }.

	Context {SCH_OPT 	: SchedulingOptions_t}.
	Context {AF : Arrival_function_t}.

	Definition TDM_counter_t := ordinal_eqType SL.

	Definition TDM_internal_state : Type := (Slot_t * TDM_counter_t)%type.
	
	#[global] Instance SCH_ST : SchedulerInternalState := mkSIS TDM_internal_state.

	Definition filter_cmd (slot : Slot_t) := filter (fun cmd =>
		match cmd with
			| CRD r | CRDA r | CWR r | CWRA r | ACT r | PRE r => r.(Requestor) == slot
			| _ => false
		end).

	Definition TDM_schedule
		(map 		: ReqCmdMap_t)
		(SS 		: SystemState_t)
		(TDM_st : TDM_internal_state) : Command_kind_t :=
		let slot := fst TDM_st in
		match seq.ohead (filter_cmd slot map)  with
			| Some cmd => cmd
			| None => NOP
		end.

	Lemma SN_pos : SN > 0.
	Proof. specialize SN_gt_1; lia. Qed.

	Definition OZSlot := Ordinal SN_pos.
	Definition O1Slot := Ordinal SN_gt_1.
	Definition OZCycle := Ordinal SL_pos.

	(* Increment counter for cycle offset (with wrap-arround). *)
	Definition Next_cycle (c : TDM_counter_t) : TDM_counter_t :=
		let nextc := c.+1 < SL in
			(if nextc as X return (nextc = X -> TDM_counter_t) then 
				fun (P : nextc = true) => Ordinal (P : nextc)
				else
				fun _ => OZCycle) Logic.eq_refl.

	Definition Next_slot (s : Slot_t) (c : TDM_counter_t) : Slot_t :=
	 	if (c.+1 < SL) then s else
			let nexts := s.+1 < SN in
			(if nexts as X return (nexts = X -> Slot_t) then 
				fun (P : nexts = true) => Ordinal (P : nexts)
				else fun _ => OZSlot) Logic.eq_refl.

	Definition InitSchState (cmd_map : ReqCmdMap_t) := (OZSlot,OZCycle).

	Definition UpdatSchState (cmd : Command_kind_t) (cmd_map : ReqCmdMap_t) (SS 	: SystemState_t) 
		(TDM_st : TDM_internal_state) : TDM_internal_state :=
		(* let cmd := TDM_schedule cmd_map SS TDM_st in *)
		let '(s,c) := TDM_st in
		((Next_slot s c),(Next_cycle c)).

	#[global] Program Instance TDM : Scheduler_t := 
		mkScheduler TDM_schedule _ _ _ InitSchState UpdatSchState.
	Next Obligation.
		unfold TDM_schedule. 
		destruct hd; simpl;
		try (destruct (eq_op r.(Requestor) (fst SCH_ST0)) eqn:H_req; rewrite H_req //=);
		try (by left); by right.
	Qed.
	Next Obligation.
	Admitted.

	(* The one defined in Interface SubLayer *)
	#[global] Existing Instance ARBITER_CFG.

	#[global]	Existing Instance ImplementationSubLayer.

	Definition TDM_arbitrate t := 
		@mkTestTrace SYS_CFG REQUESTOR_CFG
			((Default_arbitrate t).(Arbiter_Commands))
			((Default_arbitrate t).(Arbiter_Time)).
	
End TDMimpSL.