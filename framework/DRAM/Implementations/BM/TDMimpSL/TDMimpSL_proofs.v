Set Printing Projections.

From Hammer Require Import Hammer.
From mathcomp Require Import ssreflect eqtype ssrbool ssrnat fintype div ssrZ zify tuple.
From Coq Require Import Program List. 
From DRAM Require Import TDMimpSL.

Set Hammer ReconstrLimit 15.

Section TDMimpSL_proofs.

	Context {SYS_CFG : System_configuration}.
	
	(* Defined in InterfaceSubLayer *)
	Existing Instance ARBITER_CFG.
	Existing Instance ImplementationSubLayer.
	
	(* Defined in TDMimpSL *)
	Existing Instance REQUESTOR_CFG.
	Existing Instance SCH_ST.

	Context {SCH_OPT : SchedulingOptions_t}.
	Context {TDM_CFG : TDM_configuration}.
	Context {AF : Arrival_function_t}.

	Notation "# t" := (Default_arbitrate t) (at level 0).

	Fixpoint request_list (cmds : seq.seq Command_kind_t) :=
		match cmds with
		| [::] => [::]
		| hd :: tl =>
			match hd with
			| CRD r | CRDA r | CWR r | CWRA r | ACT r | PRE r => r :: request_list tl
			| _ => request_list tl
			end
		end.

	(* Returns the list of pending requests *)
	Definition TDM_pending t : seq.seq Request_t :=
		request_list 
		(match # (t).(Implementation_State) with
		| Some st => st.(CMap)
		| None => [::]
		end).
	
	Definition get_req_from_cmdkind (cmd : Command_kind_t) :=
		match cmd with
			| CRD r | CRDA r | CWR r | CWRA r | ACT r | PRE r => Some r
			| _ => None
		end.

	(* ------------------------------ PROOFS -------------------------------------- *)
	Lemma request_list_rcons r x a :
		r \in request_list x -> r \in request_list (x ++ [:: a]).
	Proof.
		induction x; simpl; intro H; [ discriminate H | ];
		destruct a0; try by apply IHx in H.
		all: 
			rewrite in_cons in H; move: H => /orP [/eqP H | H];
			try (by subst r; rewrite in_cons eq_refl orTb).
		all: 
			apply IHx in H as IH; clear IHx;
			by rewrite in_cons IH orbT.
	Qed.

	Lemma request_list_id a i : request_list [:: cmdGEN a i] = [:: a].
	Proof.
		simpl; unfold cmdGEN; destruct SCH_OPT.(pP);
		[ unfold cmdGEN_opage | unfold cmdGEN_cpage ];
		set nth_bk := nth _ _ _;
		destruct nth_bk; try reflexivity; [ destruct (_ == r) | ]; try reflexivity;
		destruct a.(Kind); reflexivity.
	Qed.

	Lemma request_in_queue : forall r l tl I,
		r \in l -> r \in request_list (map_arriving_req_to_cmd l tl I).
	Proof.
		intros r l tl i H.
		induction l; [ discriminate H | ].
		rewrite in_cons in H; move: H => /orP [/eqP H | H].
		{ subst r; clear IHl; simpl.
			set ll := map_arriving_req_to_cmd _ _ _.
			induction ll;
			[ by rewrite app_nil_l request_list_id mem_seq1 eq_refl | ].
			simpl; destruct a0; try (by rewrite in_cons IHll orbT);
			assumption.
		}
		apply IHl in H as IH; clear IHl. 
		simpl. set ll := map_arriving_req_to_cmd _ _ _; fold ll in IH.
		by apply request_list_rcons.
	Qed.

	Lemma request_in_queue_rem r ra l cmd :
		get_req_from_cmdkind cmd == Some ra ->
		r <> ra -> r \in request_list l -> r \in request_list (rem cmd l).
	Proof.
		intros Hreq Hdiff H.
		induction l; simpl in *; [ discriminate H | ].
		unfold get_req_from_cmdkind in Hreq.
		destruct a, cmd; simpl; try discriminate Hreq.
		all: rewrite inj_eq in Hreq; [ | exact ssrfun.Some_inj ]; move: Hreq => /eqP Hreq; subst.
		all: try (apply IHl in H; clear IHl); try (by rewrite H).
		all: rewrite in_cons in H; move: H => /orP [/eqP H | H ]; try subst; 
			try (by rewrite in_cons eq_refl orTb).
		all: try (rewrite in_cons; apply IHl in H; clear IHl; by rewrite H orbT).
		all: destruct (_ == _) eqn:Heq; simpl;
			try (move: Heq => /eqP Heq; injection Heq as Hbug; rewrite Hbug in Hdiff; done);
			try (by rewrite in_cons eq_refl orTb);
			try assumption;
			rewrite in_cons; apply IHl in H; by rewrite H orbT.
	Qed.
	
	Lemma request_in_queue_rem_ r ra l :
		r <> ra -> r \in request_list l -> r \in request_list (rem (CRD ra) l).
	Proof.
		intros Hdiff H.
		induction l; simpl in *; [ discriminate H | ].
		destruct a; simpl.
		{ rewrite in_cons in H; move: H => /orP [/eqP H | H]; [ subst r | ].
			{ destruct (_ == _) eqn:Heq;
				[ move: Heq => /eqP Heq; injection Heq as Hbug; rewrite Hbug in Hdiff; done | ].
				simpl; by rewrite in_cons eq_refl orTb.
			}
			apply IHl in H as IH; clear IHl;
			destruct (_ == _) eqn:Heq; [ assumption | ].
			simpl; by rewrite in_cons IH orbT.
		}
		all: 
			try (rewrite in_cons in H; move: H => /orP [/eqP H | H]); 
			try subst r;
			try rewrite in_cons; try (by rewrite eq_refl orTb); apply IHl in H as IH; clear IHl;
			rewrite IH; try rewrite orbT; done.
	Qed.

	Lemma arriving_request_is_not_scheduled ra ta i : 
		let SS := i.(SystemState) in
		let R := Arrival_at ta in 
		let sch_cmd := Schedule 
			(replace_nonrdy_cmds
			(IssueREF (map_arriving_req_to_cmd R (map_running_req_to_cmd i.(CMap) SS) SS) SS) SS) SS i.(SchState) in 
		ra \in R -> get_req_from_cmdkind sch_cmd <> Some ra.
	Proof.
		cbv zeta; intros Hin; unfold get_req_from_cmdkind.
	Admitted.
	
	(* It either goes into the queue OR 
		 starts being processed immediatly  *)

	(* Opt 1 : Change the algorithm such that arriving requests cannot 
		start being processed immediatly ? *)
	Lemma Pending_on_arrival ta ra:
    ra \in Arrival_at ta -> ra \in (TDM_pending ta).
  Proof.
		intros H; unfold TDM_pending. 
		destruct ta; simpl; [ by apply request_in_queue | ].
		unfold Next_SL_state.
		destruct # (ta).(Implementation_State) eqn:HS.
		2: { simpl. admit. (* Invalid state ... *) }
		set SS' := SystemUpdate _ _; destruct SS' eqn:HS'; simpl.
		2: { simpl. admit. (* Invalid state ... *) }
		set sch_cmd := TDM_schedule _ _ _; destruct sch_cmd eqn:Hcmd; simpl.
		5-9:
			unfold IssueREF; destruct SCH_OPT.(pP);
			destruct (_ == _); simpl; [ | destruct (_ == _) | | ]; by apply request_in_queue.
		all:
			apply request_in_queue_rem with (r := ra) (ra := r); 
			[ by unfold get_req_from_cmdkind; rewrite eq_refl | admit | ].
		all:
			unfold IssueREF; destruct SCH_OPT.(pP);
			destruct (_ == _); simpl; [ | destruct (_ == _) | | ]; by apply request_in_queue.
	Admitted.

End TDMimpSL_proofs.