Set Printing Projections.

From mathcomp Require Import ssreflect eqtype ssrbool ssrnat fintype div ssrZ zify tuple.
From Coq Require Import Program List. 
From DRAM Require Import TDMimpSL.

Section TDMimpSL_sim.

	(* Try to reproduce the TDM example from Farouk's paper ? *)
	(* Any advantages to start at 0 ? Yes, the proof of advancement, but quite hard ... *)
	(* Try to do some progress with the advancement proof *)
	(* 
	 
	Program Instance SYS_CFG : System_configuration := {
		BANKGROUPS := 1;
		BANKS := 3;
		T_BURST := 2;
		T_WL    := 2;
		T_RRD_s := 1;
		T_RRD_l := 3;
		T_FAW := 20;
		T_RC  := 3;
		T_RP  := 4;
		T_RCD := 2;
		T_RAS := 4;
		T_RTP := 4;
		T_WR  := 1;
		T_RTW := 10;
		T_WTR_s := 1;
		T_WTR_l := 2;
		T_CCD_s := 1;
		T_CCD_l := 12;
		T_REFI := 100;
		T_RFC := 6
	}. 
	*)
	
	Program Instance SYS_CFG : System_configuration :=
	{
		BANKGROUPS := 4;
		BANKS := 16;

		(* [Figure 105] always the same *)
		T_BURST := 4; (* INTRA/INTER *)

		(* [Figure 105] 1tCK write preamble mode, AL = 0
			-- independent of BC4 or BL8 
			-- depends on write preamble mode (1tCK or 2tCK)
			-- depends on AL (what is AL ?) *)
		T_WL    := 9;
		
		(* ACT to ACT, same and different bank groups, exclusevely inter-bank
			-- depends on page size, which depends on device density (total size) and configuration
			see (https://www.micron.com/-/media/client/global/documents/products/data-sheet/dram/ddr4/16gb_ddr4_sdram.pdf)
		*)
		T_RRD_s := 4; (* [Page 189] Max (4nCK, 5 ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 5 -> 4 cycles at 800MHz*)
		T_RRD_l := 5; (* [Page 189] Max (4nCK, 6 ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 6 -> 4.8 ~ 5 cyles at 800MHz *)

		(* Intra/inter-bank: Four ACT delay window *)
		T_FAW := 20; (* [Page 189] 25ns *)

		(* ACT to ACT, intra bank *) 
		T_RC  := 38; (* [Page 163] 47.5 ns *)

		(* PRE to ACT, intra bank *)
		T_RP  := 10; (* [Page 163] 12.5 ns *)

		(* ACT to CAS, intra bank *)
		T_RCD := 10; (* [Page 163] 12.5 ns *)

		(* ACT to PRE, intra bank *)
		T_RAS := 28; (* [Page 163] 35.0 ns *)

		(* RD to PRE, intra bank *)
		T_RTP := 6; (* [Page 189] Max (4nCK, 7.5ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 7 -> 5.6 ~ 6 cycles at 800MHz *)

		(* Write recovery time: R data to PRE *)
		T_WR  := 12; (* [Page 189] 15 ns *)

		(* Read to write, inter and intra bank *)
		T_RTW := 8; (* [Page 94] T_RTW = RL + BL/2 - WL + 2tCK = 11 + 8/2 - 9 + 2 = 8 *)

		(* End of write operation to read *)
		T_WTR_s := 2; (* [Page 189] Max (2nCK,2.5ns) -> 2nCK = 1/800MHz * 2 = 2.5ns -> max is 2.5ns -> 2 cycles at 800MHz *)
		T_WTR_l := 6; (* [Page 189] Nax (4nCK,7.5ns) -> 4nCK = 5ns -> max is 7.5ns -> 6 cycles at 800MHz *)

		T_CCD_s := 4;
		T_CCD_l := 5;
		
		T_REFI := 2000; (* 6240 [Page 36] Refresh average time*)
		T_RFC := 280; (* 280 [Page 36] *)
	}.

	(* The one defined in TDMimpSL *)
	Existing Instance REQUESTOR_CFG.

	(* The one defined in TDMimpSL *)
	Existing Instance SCH_ST.

	(* The one defined in Interface SubLayer *)
	Existing Instance ARBITER_CFG.
	
	(* Defined in Interface SubLayer*)
	Existing Instance ImplementationSubLayer.

	Instance SCH_OPT : SchedulingOptions_t := mkSCH_OPT opage.
	
	(* There has to be at least two requestors *)
	Program Instance TDM_CFG : TDM_configuration := mkTDMCFG 
		3 (* SN *)
		 _(* SN_gt 1*) 
		27 (* SL *)
		_ (* SL_pos *)
		_ _ _ _ _ _
		_ _ _.
			
	Program Definition A0 := mkReq 
		(Ordinal (_ : 0 < SN)) (* Requestor *) 
		4 (* Date *) 
		RD (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 0) 50).

	Program Definition B0 := mkReq 
		(Ordinal (_ : 1 < SN)) (* Requestor *) 
		50 (* Date *) 
		WR (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 1) 10).

	Program Definition C0 := mkReq 
		(Ordinal (_ : 2 < SN)) (* Requestor *) 
		85 (* Date *) 
		RD (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 2) 20).
	
	Program Definition B1 := mkReq 
		(Ordinal (_ : 1 < SN)) (* Requestor *) 
		145 (* Date *) 
		RD (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 1) 10).

	Program Definition C1 := mkReq 
		(Ordinal (_ : 2 < SN)) (* Requestor *) 
		180 (* Date *) 
		WR (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 2) 30).

	Program Definition A1 := mkReq 
		(Ordinal (_ : 0 < SN)) (* Requestor *) 
		190 (* Date *) 
		RD (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 0) 5).

	Program Definition B2 := mkReq 
		(Ordinal (_ : 1 < SN)) (* Requestor *) 
		220 (* Date *) 
		WR (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 1) 15).

	Definition Input := [:: A0;B0;C0;B1;C1;A1;B2].

	Program Instance AF : Arrival_function_t := Default_arrival_function_t Input.

	(* The one defined in TDMimpSL *)
	(* More like a round robin really ... *)
	(* But it will keep waiting forever if the slot owner has no CAS *)
	Instance TDM_arbiter : TestArbiter_t := mkTestArbiter AF TDM_arbitrate.

	Definition inst := 300.

	Eval vm_compute in (Default_arbitrate inst).(Arbiter_Commands).
	
	(* At 9 see the CRD from Req1 -> Change slot owner to 1 *)
	(* At 11 see the CRD from Req2 -> Change slot owner to 0 *)
	(* At 13 see the CRD from Req1 -> Change slot owner to 1 *)
	(*             A (C)|    A (C)|  |  C                     *)
	(* 1 2 3 4 5 6 7 8  | 9 10 11 |12| 13 14 15 16 17 18 19 20*)
	(* 0 0 0 0 0 0 0 0  | 1  1  1 | 0|  1  0  0  0  0  0  0  0*)
	
	(* Eval vm_compute in
		map (fun '(a,b) => (nat_of_ord a, nat_of_ord b)) 
		(map (fun x => (match (Default_arbitrate x).(Implementation_State) with
		| Some st => (fst st.(SchState),snd st.(SchState))
		| None => (Ordinal (_ : 0 < SN),Ordinal (_ : 0 < SL))
		end)) (seq.iota 1 30)). *)

	Eval vm_compute in
		proj1_sig (match (Default_arbitrate 189).(Implementation_State) with
		| Some st => st.(SystemState).(Banks)
		| None => (exist _ (repeat InitBank BANKS) (repeat_size InitBank BANKS))
		end).

	Eval vm_compute in 
	 (match (Default_arbitrate 4).(Implementation_State) with
		| Some st => st.(CMap)
		| None => [::]
		end).

	Definition get_sys_cnts := 
		match (Default_arbitrate inst).(Implementation_State) with
		| Some st => st.(SystemState).(SysCounters)
		| None => (mkGlobalCounters
			(exist _ (repeat init_val 3) (repeat_size 0 3))
			(exist _ (repeat init_val BANKGROUPS) (repeat_size init_val BANKGROUPS))
			0
			(exist _ (repeat init_val BANKGROUPS) (repeat_size init_val BANKGROUPS))
			0
			(exist _ (repeat init_val BANKGROUPS) (repeat_size init_val BANKGROUPS))
			0)
		end.

	Eval vm_compute in proj1_sig get_sys_cnts.(cACTdb).
	Eval vm_compute in proj1_sig get_sys_cnts.(cACTbgs).
	Eval vm_compute in get_sys_cnts.(cRDdb).

End TDMimpSL_sim.