Set Printing Projections.

From mathcomp Require Import ssreflect eqtype ssrbool ssrnat fintype div ssrZ zify tuple.
From Coq Require Import Program List. 
From DRAM Require Import TDMShelve.

Section TDMShelve_sim.

	Program Instance SYS_CFG : System_configuration :=
	{
		BANKGROUPS := 4;
		BANKS := 4;

		(* [Figure 105] always the same *)
		T_BURST := 4; (* INTRA/INTER *)

		(* [Figure 105] 1tCK write preamble mode, AL = 0
			-- independent of BC4 or BL8 
			-- depends on write preamble mode (1tCK or 2tCK)
			-- depends on AL (what is AL ?) *)
		T_WL    := 9;
		
		(* ACT to ACT, same and different bank groups, exclusevely inter-bank
			-- depends on page size, which depends on device density (total size) and configuration
			see (https://www.micron.com/-/media/client/global/documents/products/data-sheet/dram/ddr4/16gb_ddr4_sdram.pdf)
		*)
		T_RRD_s := 4; (* [Page 189] Max (4nCK, 5 ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 5 -> 4 cycles at 800MHz*)
		T_RRD_l := 5; (* [Page 189] Max (4nCK, 6 ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 6 -> 4.8 ~ 5 cyles at 800MHz *)

		(* Intra/inter-bank: Four ACT delay window *)
		T_FAW := 20; (* [Page 189] 25ns *)

		(* ACT to ACT, intra bank *) 
		T_RC  := 38; (* [Page 163] 47.5 ns *)

		(* PRE to ACT, intra bank *)
		T_RP  := 10; (* [Page 163] 12.5 ns *)
		
		(* ACT to CAS, intra bank *)
		T_RCD := 10; (* [Page 163] 12.5 ns *)
		(* T_RCD := 12; *)

		(* ACT to PRE, intra bank *)
		T_RAS := 28; (* [Page 163] 35.0 ns *)

		(* RD to PRE, intra bank *)
		T_RTP := 6; (* [Page 189] Max (4nCK, 7.5ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 7 -> 5.6 ~ 6 cycles at 800MHz *)

		(* Write recovery time: R data to PRE *)
		T_WR  := 12; (* [Page 189] 15 ns *)

		(* Read to write, inter and intra bank *)
		T_RTW := 8; (* [Page 94] T_RTW = RL + BL/2 - WL + 2tCK = 11 + 8/2 - 9 + 2 = 8 *)

		(* End of write operation to read *)
		T_WTR_s := 2; (* [Page 189] Max (2nCK,2.5ns) -> 2nCK = 1/800MHz * 2 = 2.5ns -> max is 2.5ns -> 2 cycles at 800MHz *)
		T_WTR_l := 6; (* [Page 189] Nax (4nCK,7.5ns) -> 4nCK = 5ns -> max is 7.5ns -> 6 cycles at 800MHz *)

		T_CCD_s := 4;
		T_CCD_l := 5;
		
		T_REFI := 2000; (* 6240 [Page 36] Refresh average time*)
		T_RFC := 280; (* 280 [Page 36] *)
	}.

	(* The one defined in TDMShelve *)
	Existing Instance REQUESTOR_CFG.

	(* The one defined in TDMimpSL *)
	Existing Instance SCH_ST.

	(* The one defined in Interface SubLayer *)
	Existing Instance ARBITER_CFG.
	
	(* Defined in Interface SubLayer*)
	Existing Instance ImplementationSubLayer.

	Instance SCH_OPT : SchedulingOptions_t := mkSCH_OPT opage.
	
	(* There has to be at least two requestors *)
	Program Instance TDM_CFG : TDMShelve_configuration := mkTDMCFG 
		2 (* SN *)
		 _(* SN_gt 1*) 
		27 (* SL *)
		_ (* SL_pos *)
		4 (* Treq*)
		_
		_ _ _ _ _ _
		_ _ _.

	(* 0 1 2 3 *)
	Program Definition fMapping (id : RequestorID_t) : Banks_t :=
		match id with
		| 0 => [:: Nat_to_bank 0]
		| 1 => [:: Nat_to_bank 1]
		| 2 => [:: Nat_to_bank 0; Nat_to_bank 2]
		| 3 => [:: Nat_to_bank 1]
		| _ => False_rect _ _
		end.
	Next Obligation.
		destruct id; simpl in *; lia.
	Defined.
	Next Obligation.
		lia.
	Defined.
	
	Program Instance BMap : BankMap := mkBankMap fMapping _ _.
	Next Obligation.
		unfold fMapping; destruct r; simpl in *.
		unfold isCriticalRequestor in H; simpl in H;
		destruct x; [ simpl; reflexivity | ].
		assert (x = 0); [ lia | ]; subst; simpl; reflexivity.
	Defined.
	Next Obligation.
	Admitted.

	(* Mixing ordinal types and sigma types, not great *)

	Program Definition Slot0 := Some (Ordinal (_ : 0 < SN)).
	Program Definition Slot1 := Some (Ordinal (_ : 1 < SN)).
	
	Program Definition ID0 := exist (fun x => x <= Treq) 0 _.
	Program Definition ID1 := exist (fun x => x <= Treq) 1 _.
	Program Definition ID2 := exist (fun x => x <= Treq) 2 _.
	Program Definition ID3 := exist (fun x => x <= Treq) 3 _.

	Lemma Slot0_matches_ID0 (slt : 'I_2) :
		Slot0 = Some slt -> 0 = slt.
	Proof.
		intro H.
		unfold Slot0 in H; simpl in H.
		move: H => /eqP H; 
		rewrite inj_eq in H; [ | exact ssrfun.Some_inj ]. 
		destruct slt; simpl in *.
		destruct m; simpl in *; [reflexivity | ].
		assert (m = 0); [ lia | ]; subst; simpl.
		contradict H; done.
	Qed.

	Lemma Slot1_matches_ID1 (slt : 'I_2) :
		Slot1 = Some slt -> 1 = slt.
	Proof.
		intro H.
		unfold Slot1 in H; simpl in H.
		move: H => /eqP H; 
		rewrite inj_eq in H; [ | exact ssrfun.Some_inj ]. 
		destruct slt; simpl in *.
		destruct m; simpl in *; [ contradict H; done | ]. 
		assert (m = 0); [ lia | ]; subst; reflexivity.
	Qed.
	
	(* How to enforce that address is coherent with bank mapping ? *)
	Program Definition A1 := mkReq 
		(mkRequestor Slot0 ID0 (Slot0_matches_ID0))
		8 (* Date *) 
		RD (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 0) 0).

	Program Definition B1 := mkReq 
		(mkRequestor Slot1 ID1 (Slot1_matches_ID1))(* Requestor *) 
		24 (* Date *) 
		WR (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 1) 0).

	Program Definition C1 := mkReq 
		(mkRequestor None ID2 _)(* Requestor *) 
		40 (* Date *) 
		WR (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 0) 10).

	Program Definition B2 := mkReq 
		(mkRequestor Slot1 ID1 (Slot1_matches_ID1))(* Requestor *) 
		60 (* Date *) 
		RD (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 1) 0).

	Program Definition D1 := mkReq 
		(mkRequestor None ID3 _)(* Requestor *) 
		100 (* Date *) 
		WR (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 1) 10).

	Program Definition A2 := mkReq 
		(mkRequestor Slot0 ID0 (Slot0_matches_ID0))
		105 (* Date *) 
		WR (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 0) 0).

	Program Definition B3 := mkReq 
		(mkRequestor Slot1 ID1 (Slot1_matches_ID1))(* Requestor *) 
		120 (* Date *) 
		RD (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 1) 0).

	Program Definition C2 := mkReq 
		(mkRequestor None ID2 _)(* Requestor *) 
		130 (* Date *) 
		RD (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 2) 0).
		
	Program Definition D2 := mkReq 
		(mkRequestor None ID3 _)(* Requestor *) 
		175 (* Date *) 
		RD (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 1) 10).

	Program Definition A3 := mkReq 
		(mkRequestor Slot0 ID0 (Slot0_matches_ID0))
		195 (* Date *) 
		RD (* Kind *) 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 0) 0).
	

	Definition Input := [:: A1; B1; C1; B2; D1; A2; B3; C2; D2; A3].

	Program Instance AF : Arrival_function_t := Default_arrival_function_t Input.

	Definition computeGrant (g : Grant_t) : option nat :=
		match g with
		| NoGrant => None
		| SomeGrant r => Some (proj1_sig r)
		| RefreshGrant => None
		end.

	(* Ignore proofs for testing *)
	Record compState : Type := mkcompState {
		C_Slot : nat;
		C_Counter : nat;
		C_Grant : option nat;
		C_Deadlines : seq nat;
		C_Arrivals : seq bool;
		C_Slack : seq nat;
	}.

	(* Ignore proofs for computing *)
	Definition compstate (sch : TDMShelve_internal_state) : compState :=
		mkcompState
			(nat_of_ord sch.(Slot))
			(nat_of_ord sch.(Counter))
			(computeGrant sch.(Grant))
			(proj1_sig sch.(Deadlines))
			(proj1_sig sch.(Arrivals))
			(proj1_sig sch.(Slack)).

	(* The one defined in TDMimpSL *)
	(* More like a round robin really ... *)
	(* But it will keep waiting forever if the slot owner has no CAS *)
	Instance TDMShelve_arbiter : TestArbiter_t := mkTestArbiter AF TDMShelve_arbitrate.

	Eval vm_compute in (Default_arbitrate 270).(Arbiter_Commands).

	Eval vm_compute in 
	(match (Default_arbitrate 220).(Implementation_State) with
		| Some st => compstate st.(SchState)
		| None => mkcompState 0 0 None [::0] [::false] [::0]
		end).

	Eval vm_compute in 
	(match (Default_arbitrate 203).(Implementation_State) with
	 | Some st => st.(CMap)
	 | None => [::]
	 end).

	Eval vm_compute in 
	 (match (Default_arbitrate 213).(Implementation_State) with
		| Some st => replace_nonrdy_cmds st.(CMap) st.(SystemState)
		| None => [::]
		end).
 
	Record compGlobalCounters := mkcompGlobalCounters {
		c_cACTdb : seq nat;
		c_cACTbgs : seq nat;
		c_cRDdb : nat;
		c_cRDbgs : seq nat;
		c_cWRdb : nat;
		c_cWRbgs : seq nat;
		c_cREF : nat 
	}.

	Eval vm_compute in
		(match (Default_arbitrate 212).(Implementation_State) with
		| Some st => Some (mkcompGlobalCounters 
				(` st.(SystemState).(SysCounters).(cACTdb))
				(` st.(SystemState).(SysCounters).(cACTbgs))
				(st.(SystemState).(SysCounters).(cRDdb))
				(` st.(SystemState).(SysCounters).(cRDbgs))
				(st.(SystemState).(SysCounters).(cWRdb))
				(` st.(SystemState).(SysCounters).(cWRbgs))
				(st.(SystemState).(SysCounters).(cREF)))
		| None => None
		end).

	Eval vm_compute in  
		(match (Default_arbitrate 212).(Implementation_State) with
		| Some st =>
			let banks := proj1_sig st.(SystemState).(Banks) in
			Some (seq.map (fun bk =>
				match bk with
				| IDLE lc => lc
				| ACTIVE _ lc => lc
				end 
			) banks)
		| None => None
		end).

End TDMimpSL_sim.