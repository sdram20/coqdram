Set Printing Projections.
Set Warnings "-notation-overridden,-parsing".

From mathcomp Require Import ssreflect eqtype ssrbool ssrnat fintype div ssrZ zify tuple.
From Coq Require Import BinIntDef List Program Equality.
From DRAM Require Export InterfaceSubLayer.
From Hammer Require Import Hammer.

Section TDMShelve.

	Context {SYS_CFG : System_configuration}.

	(* To-do : 
		1) enforce request address with bank mapping (is at actually necessary?)
		2) See if the thing based on u_map is really necessary to track pending requests 
	*)

	Class TDMShelve_configuration := mkTDMCFG {

		SN : nat;
		SN_gt_1 : SN > 1;

		SL : nat; 
		SL_pos 	: SL > 0;

		(* The total number of requestors, including critical and non-critical *)
		Treq : nat;
		Treq_gt_SN : SN <= Treq;

		(* Necessary for TDM guarantees *)
		(* SL is big enough to accomodate a PRE-ACT-CAS sequence *)
		SL_enough : T_RP + T_RCD + 1 + 2 <= SL;

		(* -------------- Inter bank constraints ---------------- *)
		SL_WR_to_RD : T_WL + T_BURST + T_WTR_l <= SL;
		SL_RD_to_WR : T_RTW <= SL;
		SL_CAS_to_CAS : T_CCD_l <= SL;
		SL_ACT_to_ACT_SB : T_RRD_l <= SL;

		(* ------------- Intra-bank constraints ------------------ *)
		SL_ACT_to_PRE : T_RAS - 1 <= SL;
		SL_ACT_to_ACT_DB : T_RC - T_RP - 1 <= SL;
		SL_RD_to_PRE : T_RTP - 1 <= SL;
		SL_WR_to_PRE : T_WL + T_BURST + T_WR - 1 <= SL;
	}.

	Context {TDMShelve_CFG : TDMShelve_configuration}.

	(* ------------------------------------------------------------------ *)
	(* ------------------ Slot and Cycle Counters ----------------------- *)
	(* ------------------------------------------------------------------ *)
	Definition Slot_t := ordinal_eqType SN.

	Lemma SN_pos : SN > 0.
	Proof. specialize SN_gt_1; lia. Qed.

	Definition OZSlot := Ordinal SN_pos.
	Definition O1Slot := Ordinal SN_gt_1.

	Definition TDMShelve_counter_t := ordinal_eqType SL.
	Definition OZCycle := Ordinal SL_pos.

	(* Increment counter for cycle offset (with wrap-arround). *)
	Definition Next_cycle (c : TDMShelve_counter_t) : TDMShelve_counter_t :=
		let nextc := c.+1 < SL in
			(if nextc as X return (nextc = X -> TDMShelve_counter_t) then 
				fun (P : nextc = true) => Ordinal (P : nextc)
				else
				fun _ => OZCycle) Logic.eq_refl.

	Definition Next_slot (s : Slot_t) (c : TDMShelve_counter_t) : Slot_t :=
			if (c.+1 < SL) then s else
			let nexts := s.+1 < SN in
			(if nexts as X return (nexts = X -> Slot_t) then 
				fun (P : nexts = true) => Ordinal (P : nexts)
				else fun _ => OZSlot) Logic.eq_refl.

	Definition Following_slot (s : Slot_t) : Slot_t :=
		let nexts := s.+1 < SN in
			(if nexts as X return (nexts = X -> Slot_t) then 
				fun (P : nexts = true) => Ordinal (P : nexts)
				else
				fun _ => OZSlot) Logic.eq_refl.

	(* ------------------------------------------------------------------ *)
	(* ---------------- Requestor config -------------------------------- *)
	(* ------------------------------------------------------------------ *)
	Definition RequestorID_t := {id : nat | id < Treq}.

	Record TDMShelve_requestor := mkRequestor {
		(* Critical requestors have an associated slot, NC requestors do not *)
		ReqSlot : option Slot_t;
		
		(* ID is useful to differentiate between NC requestors. For CR, it could have been only slots *)
		ID : RequestorID_t;

		(* Requestor ID matches the slot number for critical requestors *)
		Slot_matches_ID : forall slt, ReqSlot = Some slt -> ` ID = nat_of_ord slt 
	}.
	
	Local Definition TDMShelve_requestor_eqdef (a b : TDMShelve_requestor) :=
		(a.(ReqSlot) == b.(ReqSlot)) && (a.(ID) == b.(ID)).

	Lemma TDMShelve_requestor_eqn : Equality.axiom TDMShelve_requestor_eqdef.
	Admitted.

	Canonical TDMShelve_requestor_eqMixin := EqMixin TDMShelve_requestor_eqn.
  Canonical TDMShelve_requestor_eqType := Eval hnf in EqType TDMShelve_requestor TDMShelve_requestor_eqMixin.

  #[global] Instance REQUESTOR_CFG : Requestor_configuration := {
    Requestor_t := TDMShelve_requestor_eqType
  }.

	Definition isCriticalRequestor (r : RequestorID_t) := ` r < SN. 
	Definition isNonCriticalRequestor (r : RequestorID_t) := ` r >= SN. 
	Definition isCriticalReq (r : Request_t) := isSome r.(Requestor).(ReqSlot).

	(* Critical requestors have IDs bounded by SN *)
	Lemma CR_ID_lt_SN : forall r, isCriticalReq r -> ` r.(Requestor).(ID) < SN.
	Proof.
		intro r; unfold isCriticalReq; intro H.
		destruct r.(Requestor); simpl in *.
		destruct ReqSlot0; [ clear H | done ].
		specialize (Slot_matches_ID0 o).
		assert (H : Some o = Some o); 
		[reflexivity | apply Slot_matches_ID0 in H; clear Slot_matches_ID0 ].
		rewrite H; clear H; destruct o; simpl; assumption.
	Qed.

	(* ------------------------------------------------------------------ *)
	(* ------------------- Scheduling ----------------------------------- *)
	(* ------------------------------------------------------------------ *)
	Context {SCH_OPT 	: SchedulingOptions_t}.
	Context {AF : Arrival_function_t}.

	(* The total number of non-critical requestors *)
	Definition NCreq := Treq - SN.

	Inductive Grant_t :=
		| NoGrant : Grant_t 
		| SomeGrant : RequestorID_t -> Grant_t
		| RefreshGrant : Grant_t.

	Record TDMShelve_internal_state := mkTDMShelve_internal_state {
		(* Typical TDM slot and cycle counters *)
		Slot 			 	: Slot_t;
		Counter 	 	: TDMShelve_counter_t;
		(* Who has the grant to issue commands *)
		Grant 			: Grant_t;
		(* Both critical and non-critical have associated deadlines *)
		Deadlines  	: { deadline_cnts : seq.seq nat 	| seq.size deadline_cnts == Treq};
		(* A list used to track the arrival of requests *)
		Arrivals 		: { nc_arrivals  	: seq.seq bool 	| seq.size nc_arrivals == Treq};
		(* Slack associated with critical requestors *)
		Slack 		 	: { slack_cnts 		: seq.seq nat 	| seq.size slack_cnts == SN };
	}.

	(* add another counter to count for cycles elapsed after REF has been issued ? *)

	#[global] Instance SCH_ST : SchedulerInternalState := mkSIS TDMShelve_internal_state.

	(* Pick the first command from a given request *)
	Definition Pick_cmd_from_requestorID 
		(map : ReqCmdMap_t) 
		(req_id : RequestorID_t) : Command_kind_t :=
		let f := filter (fun cmd => 
			match cmd with
				| CRD r | CRDA r | CWR r | CWRA r | ACT r | PRE r => r.(Requestor).(ID) == req_id
				| _ => false
			end) map in seq.head NOP f.

	Definition Pick_refresh_cmd
		(map : ReqCmdMap_t) : Command_kind_t :=
		let f := filter (fun cmd => 
			match cmd with
				| PREA | REF => true
				| _ => false
			end) map in seq.head NOP f.

	Definition TDMShelve_schedule
		(map 		: ReqCmdMap_t)
		(SS 		: SystemState_t)
		(TDM_st : TDMShelve_internal_state) : Command_kind_t :=
		match TDM_st.(Grant) with
		| NoGrant => NOP
		| SomeGrant requestor_id => Pick_cmd_from_requestorID map requestor_id
		| RefreshGrant => Pick_refresh_cmd map
		end.

	(* ------------------------------------------------------------------ *)
	(* ---------------- Bank Mapping ------------------------------------ *)
	(* ------------------------------------------------------------------ *)
	Program Definition Slot_to_RequestorID (s : Slot_t) : RequestorID_t :=
		exist (fun x => x < Treq) (nat_of_ord s) _.
	Next Obligation.
		specialize Treq_gt_SN as H; destruct s; simpl; lia.
	Defined.

	(* Need all elements of l to not be in s **)
	Definition NoIntersection {T : eqType} (l s : seq T) := 
		all (fun x => ~~ in_mem x (mem s)) l.

	Definition AdjacentSlots (s0 s1 : Slot_t) :=
		(s0 == Following_slot s1) || (s1 == Following_slot s0).

	Class BankMap := mkBankMap {
		Mapping : RequestorID_t -> Banks_t;
		(* Critical requestors can be mapped to one bank at most *)
		CMapping : forall r, isCriticalRequestor r -> seq.size (Mapping r) = 1;
		(* Adjacent slots target different banks *)
		AdjacentSlotsDiffBanks : forall s0 s1, AdjacentSlots s0 s1 ->
			let ID_0 := Slot_to_RequestorID s0 in
			let ID_1 := Slot_to_RequestorID s1 in
			NoIntersection (Mapping ID_0) (Mapping ID_1)
	}.

	Context {BK_MAP : BankMap}.

	(* ------------------------------------------------------------------ *)
	(* --------- Useful functions and facts ----------------------------- *)
	(* ------------------------------------------------------------------ *)
	Definition All_requestors 		:= seq.iota 0 Treq.
	Definition All_NC_requestors 	:= seq.iota SN NCreq.
	Definition All_C_requestors 	:= seq.iota 0 SN.

	Program Fixpoint sig_iota n M (H : n <= M) : seq {x : nat | x < M} :=
		match n with
		| 0 => [::]
		| n'.+1 => (exist _ n' _) :: sig_iota n' M _
		end.

	Program Definition All_requestors_sig := seq.rev (sig_iota Treq Treq _).

	(* Returns true if there is pending request belonging to id *)
	Definition checkIfPendingFromID (unfiltered_cmd_map : seq Command_kind_t) (id : nat) : bool :=
			let f := seq.find (fun cmd =>
				match cmd with
				| CRD r | CRDA r | CWR r | CWRA r | ACT r | PRE r => ` r.(Requestor).(ID) == id
				| _ => false
				end
			) unfiltered_cmd_map in ~~ (f == seq.size unfiltered_cmd_map).

	Definition checkIfPendingFromSlot (u_map : seq Command_kind_t) (s : Slot_t) :=
		checkIfPendingFromID u_map (` (Slot_to_RequestorID s)).

	(* Maybe just look at Arrivals to simplify things ?! *)
	Definition PendingReqs u_map (sub_list : seq RequestorID_t) : seq (option RequestorID_t) := 
		seq.map (fun x => 
			if (checkIfPendingFromID u_map (` x)) then (Some x) else None
		) sub_list.
	
	(* Keep requestors that have a pending request to a different bank than the s bank bank 
		OR self *)
	Definition filter_diff_bank_ u_map (s : Slot_t) : seq (option RequestorID_t) :=
		let s_id := (Slot_to_RequestorID s) in
		(* Know that it is just one element -- default case will never happen *)
		let s_bank := @seq.head Bank_t (Nat_to_bank 0) (Mapping s_id) in
		seq.map (fun cmd => 
			match cmd with
			| CRD r | CRDA r | CWR r | CWRA r | ACT r | PRE r => 
				if ((r.(Address).(Bank) != s_bank) || (r.(Requestor).(ID) == s_id)) then (
					Some (r.(Requestor).(ID))
				) else None
			| _ => None
			end
		) u_map.	

	Lemma size_map_iota {T} (f : nat -> T) x y : 
		seq.size (seq.map f (seq.iota x y)) == y.
	Proof.
		by rewrite size_map size_iota eq_refl. 
	Qed.

	Local Notation "l [@ i $ d ]" := (seq.nth d l i) (at level 1).

	Definition getSlackFromID TDM_st (id : nat) :=
		(` TDM_st.(Slack))[@id $0].

	Definition getSlackFromSlot TDM_st (s : Slot_t) :=
		(` TDM_st.(Slack))[@(nat_of_ord s) $0].

	Definition getDeadlineFromID TDM_st (id : nat) :=
		(` TDM_st.(Deadlines))[@id $1000].

	Definition getArrivalFromID TDM_st (id : nat) :=
		(` TDM_st.(Arrivals))[@id $false].

	(* ------------------------------------------------------------------ *)
	(* ----------- Defining the initial state --------------------------- *)
	(* ------------------------------------------------------------------ *)
	Definition InitialDeadlines :=
		exist (fun s => seq.size s == Treq) (seq.map (fun requestor_ID =>
			if (requestor_ID < SN) then (
				((requestor_ID + 1) * SL) - 1
			) else 0) All_requestors) (size_map_iota _ _ _).
	
	Definition InitialSlack :=
		exist (fun (s : seq nat) => seq.size s == SN) (repeat 0 _) (repeat_size 0 SN).

	Definition InitialArrivals :=
		exist (fun (s : seq bool) => seq.size s == Treq) (repeat false _) (repeat_size false Treq).

	Definition InitSchState (cmd_map : ReqCmdMap_t) := mkTDMShelve_internal_state
		OZSlot OZCycle NoGrant InitialDeadlines InitialArrivals InitialSlack.

	(* ------------------------------------------------------------------ *)
	(* ----------- Defining update functions ---------------------------- *)
	(* ------------------------------------------------------------------ *)
	Program Definition EDF 
		(TDM_st : TDMShelve_internal_state) 
		(requestors : seq (option RequestorID_t)) : option RequestorID_t :=
		foldr (fun i s =>
		  match s, i with
			| None, _ => i
			| _, None => s
			| Some s, Some i =>
			  let min_deadline := getDeadlineFromID TDM_st (` s) in
				let cur_deadline := getDeadlineFromID TDM_st (` i) in
				if (isCriticalRequestor i) then ( (* critical requestor *)
					if (cur_deadline <= SL) then Some i
					else (
						if (cur_deadline <= min_deadline) then Some i else Some s
					)
				) else ( (* non-critical requestor *)
					if ( (* s is a critical requestor with an upcoming deadline *)
						(isCriticalRequestor s) && (min_deadline <= SL)) then Some s
						(* (min_deadline <= SL + (SL - TDM_st.(Counter)))) then s *)
					else (
						(* if (cur_deadline == 0) then Some i else( *)
						if (cur_deadline < min_deadline) then Some i else Some s
					)
				)
			end
		) None requestors. 

	Definition BankFiltering_
		(u_map  : ReqCmdMap_t)
		(TDM_st : TDMShelve_internal_state) : option RequestorID_t :=
		let cnt 			:= TDM_st.(Counter) in
		let slack 		:= TDM_st.(Slack) in
		let deadl 		:= TDM_st.(Deadlines) in
		let ns_slot 	:= Following_slot TDM_st.(Slot) in
		match checkIfPendingFromSlot u_map ns_slot with
		| false => (* No request from next (or current) slot -> speculating based on slack *)
			let slack := getSlackFromSlot TDM_st ns_slot in
			if (slack >= SL - (cnt %% SL)) then (
				EDF TDM_st (PendingReqs u_map All_requestors_sig)
			) else (
				EDF TDM_st (filter_diff_bank_ u_map ns_slot)
			)
		| true => 
			let p_req_id := Slot_to_RequestorID ns_slot in
			let p_req_deadline := getDeadlineFromID TDM_st (` p_req_id) in
			if (p_req_deadline >= SL + (SL - TDM_st.(Counter))) then (
				EDF TDM_st (PendingReqs u_map All_requestors_sig)  (* EDF between everyone *)
			) else (
				EDF TDM_st (filter_diff_bank_ u_map ns_slot)
			)
		end.

	Definition BankFiltering  u_map TDM_st :=
		match BankFiltering_ u_map TDM_st with
		| None => NoGrant
		| Some req => SomeGrant req
		end.

	Definition UpdateGrantRunning
		(u_map 		: ReqCmdMap_t)
		(sch_cmd 	: Command_kind_t)
		(TDM_st 	: TDMShelve_internal_state) : Grant_t :=
		let slt 			:= TDM_st.(Slot) in
		let grant 		:= TDM_st.(Grant) in
		let cur_slack := getSlackFromSlot TDM_st slt in
		let cnt 			:= TDM_st.(Counter) in
		if (cnt == OZCycle) then ( (* Beginning of slot -> might abort requests *)
			match grant with
			| NoGrant => if (u_map == [::]) then NoGrant else (BankFiltering u_map TDM_st) (* EDF (implement choice based on slack inside of EDF function) *)
			| SomeGrant requestor_id => (* May be preempted or not *)
				match checkIfPendingFromSlot u_map slt with
				| false => if (isCAS_cmdkind sch_cmd) then NoGrant else (BankFiltering u_map TDM_st)
				| true =>
						let preq := Slot_to_RequestorID slt in
						let preq_deadl := getDeadlineFromID TDM_st (` preq) in
						if (preq_deadl <= SL) 
							then (SomeGrant preq) (* have to abort ! *) (* Rule 1 *)
					 		else (if (isCAS_cmdkind sch_cmd) then NoGrant else (BankFiltering u_map TDM_st))
				end
			| RefreshGrant => RefreshGrant (* should not happen *)
			end
		) else ( (* Middle of the slot - EDF rules when a scheduling decision comes *)
			match grant with (* Rule 4 *)
			| NoGrant => if (u_map == [::]) then NoGrant else (BankFiltering u_map TDM_st)
			| SomeGrant requestor_id => 
				if (isCAS_cmdkind sch_cmd) then NoGrant else (SomeGrant requestor_id)
			| RefreshGrant => RefreshGrant (* should not happen *)
			end
		).

	(* to-do : account for t_RFC cycles ... *)
	(* bank machine signal enter of refresh cycle and end *)
	Definition UpdateGrantRefresh
	(u_map 		: ReqCmdMap_t)
	(sch_cmd 	: Command_kind_t)
	(TDM_st 	: TDMShelve_internal_state) : Grant_t :=
	match TDM_st.(Grant) with
	| NoGrant => NoGrant (* Should not happen *)
	| SomeGrant r => SomeGrant r (* Should not happen *)
	| RefreshGrant => if (isREF_cmdkind sch_cmd) then NoGrant else RefreshGrant
	end.

	Definition checkIfREForPREA (unfiltered_cmd_map : seq Command_kind_t) : bool :=
		let f := seq.find (fun cmd =>
			match cmd with
			| PREA | REF => true
			| _ => false
			end
		) unfiltered_cmd_map in ~~ (f == seq.size unfiltered_cmd_map).
	
	Definition UpdateGrant
		(u_map 		: ReqCmdMap_t)
		(sch_cmd 	: Command_kind_t)
		(TDM_st 	: TDMShelve_internal_state) : Grant_t :=
		match TDM_st.(Grant) with
		| NoGrant | SomeGrant _ => if (checkIfREForPREA u_map) then RefreshGrant else UpdateGrantRunning u_map sch_cmd TDM_st
		| RefreshGrant => UpdateGrantRefresh u_map sch_cmd TDM_st
		end.

	Definition UpdateDeadlines sch_cmd u_map TDM_st :=
		exist (fun s => seq.size s == Treq) (seq.map (fun requestor_id =>
			(* Cycle counter *)
			let cnt := TDM_st.(Counter) in
			(* Dli counter *)
			let deadl_i := getDeadlineFromID TDM_st requestor_id in
			(* Δi counter *)
			let slack_i := getSlackFromID TDM_st requestor_id in
			if (requestor_id < SN) then ( (* Critical requestor *)
				let replenish_cond := (
					match get_req_from_cmdkind sch_cmd with
					| None => false
					| Some req => (isCAS_cmdkind sch_cmd) && (` req.(Requestor).(ID) == requestor_id)
					end
				) in
				if ((deadl_i == 0) || replenish_cond) then (deadl_i + (SN * SL) - 1)
				else (match checkIfPendingFromID u_map requestor_id with
							| false =>	(* No outstanding request *)
								if (deadl_i - slack_i <= SL - 1) then ( (* Unused slot condition *)
									deadl_i + (SN * SL) - 1 (* replenish deadline *)
								) else (deadl_i - 1) (* normal decrement *)
							| true => deadl_i - 1 (* If there is an outstanding request, then just decrement *)
						  end)
			) else ( (* Non critical requestor *)
				match checkIfPendingFromID u_map requestor_id with
				| false => deadl_i
				| true =>
					let arr := getArrivalFromID TDM_st requestor_id in
					if ~~ arr then ( (* req has arrived just now -> update the deadline counter *)
						(SL - (cnt %% SL)) + (SL - 1) - 1
					) else deadl_i - 1
				end
			)
		) All_requestors) (size_map_iota _ _ _).

	Definition UpdateArrivals sch_cmd u_map TDM_st :=
		exist (fun s => seq.size s == Treq) (seq.map (fun requestor_id =>
			let arr := getArrivalFromID TDM_st requestor_id in
			if ~~ arr then (checkIfPendingFromID u_map requestor_id) 
			else (
				match get_req_from_cmdkind sch_cmd with
				| None => true
				| Some req => 
					(* a CAS to requestor_ID has been issued -> put arrival back to false *)
					~~ ((isCAS_cmdkind sch_cmd) && (` req.(Requestor).(ID) == requestor_id))
				end)
		) All_requestors) (size_map_iota _ _ _).
	
	Definition UpdateSlack sch_cmd TDM_st :=
		exist (fun s => seq.size s == SN) (seq.map (fun requestor_id => 
			let deadl_i := (` TDM_st.(Deadlines))[@requestor_id $ 0] in
			let slack_i := (` TDM_st.(Slack))[@requestor_id $ 0] in
			match get_req_from_cmdkind sch_cmd with
			| None => slack_i
			| Some req =>
				let req_completed := (isCAS_cmdkind sch_cmd) && (` req.(Requestor).(ID) == requestor_id) in
				if req_completed then (deadl_i - 1) else slack_i
			end
		) All_C_requestors) (size_map_iota _ _ _).

	Definition UpdatSchState sch_cmd cmd_map (SS : SystemState_t) TDM_st :=
		let next_grant := UpdateGrant cmd_map sch_cmd TDM_st in
		match next_grant with
		| NoGrant | SomeGrant _ =>
			mkTDMShelve_internal_state
				(Next_slot TDM_st.(Slot) TDM_st.(Counter))
				(Next_cycle TDM_st.(Counter))
				(next_grant)
				(UpdateDeadlines sch_cmd cmd_map TDM_st)
				(UpdateArrivals sch_cmd cmd_map TDM_st)
				(UpdateSlack sch_cmd TDM_st)
		| RefreshGrant =>
			mkTDMShelve_internal_state
				(TDM_st.(Slot))
				(TDM_st.(Counter))
				(next_grant)
				(TDM_st.(Deadlines))
				(UpdateArrivals sch_cmd cmd_map TDM_st)
				(TDM_st.(Slack))
		end.

	Lemma eqb_false_Some_inj {T : eqType} (a b : T) :
		(a == b) = false -> Some a <> Some b.
	Proof.
		intro. 
		apply (@contraPnot (eq (eq_op a b) false) (eq (Some a) (Some b)));
		[ | assumption ]; clear H; intro.
		move: H => /eqP H; rewrite inj_eq in H; [ | exact ssrfun.Some_inj];
		rewrite H; done.
	Qed.

	#[global] Program Instance TDMShelve : Scheduler_t := 
		mkScheduler TDMShelve_schedule _ _ _ InitSchState UpdatSchState.
	Next Obligation.
		unfold TDMShelve_schedule.
		destruct SCH_ST0.(Grant); reflexivity.
	Defined.
	Next Obligation.
		unfold TDMShelve_schedule; destruct SCH_ST0.(Grant);
		[ right; reflexivity | | ];
		unfold Pick_cmd_from_requestorID; simpl;
		destruct hd; simpl;
		try destruct (_ == r); simpl; try (by left; reflexivity); by right; reflexivity.
	Defined.
	Next Obligation.
		unfold TDMShelve_schedule; destruct SCH_ST0.(Grant);
		[ left; reflexivity | | ].
		all: unfold Pick_cmd_from_requestorID;
		induction m; simpl; [ left; reflexivity | ];
		destruct IHm as [IHm | IHm];
		destruct a; try destruct (_ == r); simpl;
		try (by left; assumption);
		try (by right; rewrite in_cons eq_refl orTb);
		by right; rewrite in_cons IHm orbT.
	Defined.
	
	(* The one defined in Interface SubLayer *)
	#[global] Existing Instance ARBITER_CFG.

	#[global]	Existing Instance ImplementationSubLayer.

	Definition TDMShelve_arbitrate t := 
		@mkTestTrace SYS_CFG REQUESTOR_CFG
			((Default_arbitrate t).(Arbiter_Commands))
			((Default_arbitrate t).(Arbiter_Time)).
			
End TDMShelve.