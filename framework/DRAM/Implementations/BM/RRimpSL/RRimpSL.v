Set Printing Projections.
Set Warnings "-notation-overridden,-parsing".

From mathcomp Require Import ssreflect eqtype ssrbool ssrnat fintype div ssrZ zify tuple.
From Coq Require Import Program Equality.
From DRAM Require Export InterfaceSubLayer.

Section RRimpSL.

	Context {SYS_CFG : System_configuration}.

	Class RR_configuration := mkRRCFG {
		SN : nat;
		SN_gt_1 : SN > 1;
	}.

	Context {RR_CFG : RR_configuration}.
	
	Definition Slot_t := ordinal_eqType SN.

  #[global] Instance REQUESTOR_CFG : Requestor_configuration := 
  {
    Requestor_t := Slot_t
  }.

	Context {SCH_OPT 	: SchedulingOptions_t}.
	Context {AF : Arrival_function_t}.

	#[global] Instance SCH_ST : SchedulerInternalState := mkSIS Slot_t.

	Definition filter_cmd slot := fun cmd =>
		match cmd with
			| CRD r | CRDA r | CWR r | CWRA r | ACT r | PRE r => r.(Requestor) == slot
			| _ => false
		end.

	Definition RR_schedule
		(map 		: ReqCmdMap_t)
		(SS 		: SystemState_t)
		(slot 	: Slot_t) : Command_kind_t :=
		let cmd := seq.ohead (filter (filter_cmd slot) map) in
		match cmd with
		| Some cmd => cmd
		| None => NOP
		end.

	Lemma SN_pos : SN > 0.
	Proof. specialize SN_gt_1; lia. Qed.

	Definition OZSlot := Ordinal SN_pos.
	Definition O1Slot := Ordinal SN_gt_1.

	(* Wrap around counter *)
	Definition Next_slot (c : Slot_t) :=
    let nextc := c.+1 < SN in
      (if nextc as X return (nextc = X -> Slot_t) then 
        fun (P : nextc = true) => Ordinal (P : nextc)
       else
        fun _ => OZSlot) Logic.eq_refl.

	(* version with auto-precharge should be OK because of assumption that adjacent slots
	target different banks *)

	Definition InitSchState (cmd_map : ReqCmdMap_t) := OZSlot.
	(* Can't update when the CAS is seen on the structure, but rather issued to the device *)
	(* Maybe look at counters to detect a CWR or RD ?! *)
	(* Give up slot if no pending request *)
	Definition UpdatSchState sch_cmd cmd_map (SS 	: SystemState_t) (slot : Slot_t) : Slot_t :=
		(* let sch_cmd := RR_schedule cmd_map SS slot in  *)
		let cond1 := (* no commands belonging to the current slot owner *)
			((find (fun sch_cmd =>
			match sch_cmd with
			| CRD r | CRDA r | CWR r | CWRA r | ACT r | PRE r => r.(Requestor) == slot
			| _ => false
			end) cmd_map) == seq.size cmd_map) in
		let cond2 := (* scheduled command is a CAS *)
			(match sch_cmd with
			| CRD _ | CRDA _ | CWR _ | CWRA _ => true
			| _ => false
			end) in
		if (cond1 || cond2) then Next_slot slot else slot.
		
	
	#[global] Program Instance RR : Scheduler_t := 
		mkScheduler RR_schedule _ _ _ InitSchState UpdatSchState.
	Next Obligation.
		unfold RR_schedule. 
		destruct hd; simpl;
		try (destruct (eq_op r.(Requestor) SCH_ST0) eqn:H_req; rewrite H_req //=);
		try (by left); by right.
	Qed.
	Next Obligation.
	Admitted.

	(* The one defined in Interface SubLayer *)
	#[global] Existing Instance ARBITER_CFG.

	#[global]	Existing Instance ImplementationSubLayer.

	Definition RR_arbitrate t := 
		@mkTestTrace SYS_CFG REQUESTOR_CFG
			((Default_arbitrate t).(Arbiter_Commands))
			((Default_arbitrate t).(Arbiter_Time)).
	
End RRimpSL.