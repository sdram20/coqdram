Set Printing Projections.

From mathcomp Require Import ssreflect eqtype ssrbool ssrnat fintype div ssrZ zify tuple.
(* From Coq Require Import Program Equality. *)
From Coq Require Import Program List. 
From DRAM Require Import RRimpSL.

Section RRimpSL_sim.

	 Program Instance SYS_CFG : System_configuration := {
		BANKGROUPS := 1;
		BANKS := 2;
		T_BURST := 2;
		T_WL    := 2;
		T_RRD_s := 1;
		T_RRD_l := 3;
		T_FAW := 20;
		T_RC  := 3;
		T_RP  := 4;
		T_RCD := 2;
		T_RAS := 4;
		T_RTP := 4;
		T_WR  := 1;
		T_RTW := 10;
		T_WTR_s := 1;
		T_WTR_l := 10;
		T_CCD_s := 1;
		T_CCD_l := 12;
		T_REFI := 100;
		T_RFC := 6
	}.

	(* The one defined in RRimpSL *)
	Existing Instance REQUESTOR_CFG.

	(* The one defined in RRimpSL *)
	Existing Instance SCH_ST.

	(* The one defined in Interface SubLayer *)
	Existing Instance ARBITER_CFG.
	
	(* Defined in Interface SubLayer*)
	Existing Instance ImplementationSubLayer.

	Instance SCH_OPT : SchedulingOptions_t := mkSCH_OPT opage.
	
	(* There has to be at least two requestors *)
	Program Instance RR_CFG : RR_configuration := mkRRCFG 2 _.

	Program Definition Req1 := mkReq (Ordinal (_ : 0 < SN)) 1 RD 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 0) 5).

	Program Definition Req2 := mkReq (Ordinal (_ : 1 < SN)) 1 RD 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 1) 10).

	Definition Input := [:: Req1;Req2].

	Program Instance AF : Arrival_function_t := Default_arrival_function_t Input.

	(* The one defined in RRimpSL *)
	(* More like a round robin really ... *)
	(* But it will keep waiting forever if the slot owner has no CAS *)
	Instance RRarbiter : TestArbiter_t := mkTestArbiter AF RR_arbitrate.

	Definition inst := 5.
	
	(* At 9 see the CRD from Req1 -> Change slot owner to 1 *)
	(* At 11 see the CRD from Req2 -> Change slot owner to 0 *)
	(* At 13 see the CRD from Req1 -> Change slot owner to 1 *)
	(*             A (C)|    A (C)|  |  C                     *)
	(* 1 2 3 4 5 6 7 8  | 9 10 11 |12| 13 14 15 16 17 18 19 20*)
	(* 0 0 0 0 0 0 0 0  | 1  1  1 | 0|  1  0  0  0  0  0  0  0*)
	Eval vm_compute in map (fun x => 
		nat_of_ord (match (Default_arbitrate x).(Implementation_State) with
		| Some st => st.(SchState)
		| None => Ordinal (_ : 0 < SN)
		end)) (seq.iota 1 30).

	(* Still have a problem : Why ACT is only issued at cycle 7 ?! *)
	Eval vm_compute in (Default_arbitrate inst).(Arbiter_Commands).

	Eval vm_compute in
		proj1_sig (match (Default_arbitrate inst).(Implementation_State) with
		| Some st => st.(SystemState).(Banks)
		| None => (exist _ (repeat InitBank BANKS) (repeat_size InitBank BANKS))
		end).

	Eval vm_compute in 
	 (match (Default_arbitrate 4).(Implementation_State) with
		| Some st => st.(CMap)
		| None => [::]
		end).

	Definition get_sys_cnts := 
		match (Default_arbitrate inst).(Implementation_State) with
		| Some st => st.(SystemState).(SysCounters)
		| None => (mkGlobalCounters
			(exist _ (repeat init_val 3) (repeat_size 0 3))
			(exist _ (repeat init_val BANKGROUPS) (repeat_size init_val BANKGROUPS))
			0
			(exist _ (repeat init_val BANKGROUPS) (repeat_size init_val BANKGROUPS))
			0
			(exist _ (repeat init_val BANKGROUPS) (repeat_size init_val BANKGROUPS))
			0)
		end.

	Eval vm_compute in proj1_sig get_sys_cnts.(cACTdb).
	Eval vm_compute in proj1_sig get_sys_cnts.(cACTbgs).
	Eval vm_compute in get_sys_cnts.(cRDdb).

End RRimpSL_sim.