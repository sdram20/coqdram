Set Printing Projections.

From mathcomp Require Import ssreflect eqtype ssrbool ssrnat fintype div ssrZ zify tuple.
(* From Coq Require Import Program Equality. *)
From Coq Require Import Program List. 
From DRAM Require Import FIFOimpSL.

Section FIFOimpSL_sim.

	 Program Instance SYS_CFG : System_configuration := {
		BANKGROUPS := 1;
		BANKS := 2;
		T_BURST := 2;
		T_WL    := 2;
		T_RRD_s := 1;
		T_RRD_l := 3;
		T_FAW := 20;
		T_RC  := 3;
		T_RP  := 4;
		T_RCD := 2;
		T_RAS := 4;
		T_RTP := 4;
		T_WR  := 1;
		T_RTW := 10;
		T_WTR_s := 1;
		T_WTR_l := 10;
		T_CCD_s := 1;
		T_CCD_l := 12;
		T_REFI := 100;
		T_RFC := 6
	}.

	(* The one defined in TDMimpSL *)
	Existing Instance REQUESTOR_CFG.

	(* The one defined in TDMimpSL *)
	Existing Instance SCH_ST.

	(* The one defined in Interface SubLayer *)
	Existing Instance ARBITER_CFG.
	
	(* Defined in Interface SubLayer*)
	Existing Instance ImplementationSubLayer.

	Instance SCH_OPT : SchedulingOptions_t := mkSCH_OPT opage.
	
	Program Definition Req1 := mkReq tt 1 RD 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 0) 5).

	Program Definition Req2 := mkReq tt 2 RD 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 1) 10).

	Program Definition Req3 := mkReq tt 3 WR 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 0) 5).
	
	Program Definition Req4 := mkReq tt 4 WR 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 1) 15).

	Definition Input := [:: Req1;Req2;Req3;Req4].

	Program Instance AF : Arrival_function_t := Default_arrival_function_t Input.

	(* The one defined in TDMimpSL *)
	(* More like a round robin really ... *)
	(* But it will keep waiting forever if the slot owner has no CAS *)
	Instance FIFO_arbiter : TestArbiter_t := mkTestArbiter AF FIFO_arbitrate.

	Definition inst := 30.
	
	(* Still have a problem : Why ACT is only issued at cycle 7 ?! *)
	Eval vm_compute in (Default_arbitrate inst).(Arbiter_Commands).

	Eval vm_compute in
		proj1_sig (match (Default_arbitrate inst).(Implementation_State) with
		| Some st => st.(SystemState).(Banks)
		| None => (exist _ (repeat InitBank BANKS) (repeat_size InitBank BANKS))
		end).

	Eval vm_compute in 
	 (match (Default_arbitrate 4).(Implementation_State) with
		| Some st => st.(CMap)
		| None => [::]
		end).

	Definition get_sys_cnts := 
		match (Default_arbitrate inst).(Implementation_State) with
		| Some st => st.(SystemState).(SysCounters)
		| None => (mkGlobalCounters
			(exist _ (repeat init_val 3) (repeat_size 0 3))
			(exist _ (repeat init_val BANKGROUPS) (repeat_size init_val BANKGROUPS))
			0
			(exist _ (repeat init_val BANKGROUPS) (repeat_size init_val BANKGROUPS))
			0
			(exist _ (repeat init_val BANKGROUPS) (repeat_size init_val BANKGROUPS))
			0)
		end.

	Eval vm_compute in proj1_sig get_sys_cnts.(cACTdb).
	Eval vm_compute in proj1_sig get_sys_cnts.(cACTbgs).
	Eval vm_compute in get_sys_cnts.(cRDdb).

End TDMimpSL_sim.