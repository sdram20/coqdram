Set Printing Projections.
Set Warnings "-notation-overridden,-parsing".

From mathcomp Require Import ssreflect eqtype ssrbool ssrnat fintype div ssrZ zify tuple.
From Coq Require Import Program Equality.
From DRAM Require Export InterfaceSubLayer.

Section FIFOimpSL.

	Context {SYS_CFG : System_configuration}.

  #[global] Instance REQUESTOR_CFG : Requestor_configuration := 
  {
    Requestor_t := unit_eqType
  }.

	Context {SCH_OPT 	: SchedulingOptions_t}.
	Context {AF : Arrival_function_t}.

	Definition FIFO_internal_state := unit.
	
	#[global] Instance SCH_ST : SchedulerInternalState := mkSIS FIFO_internal_state.

	Definition FIFO_schedule
		(map 			: ReqCmdMap_t)
		(SS 			: SystemState_t)
		(FIFO_st 	: FIFO_internal_state) : Command_kind_t :=	
		let cmd := seq.ohead map in
		match cmd with
		| Some cmd => cmd
		| None => NOP
		end.
	
	Definition InitSchState (cmd_map : ReqCmdMap_t) := tt.

	Definition UpdatSchState (sch_cmd : Command_kind_t) (cmd_map : ReqCmdMap_t) (SS : SystemState_t) 
		(FIFO_st : FIFO_internal_state) : FIFO_internal_state := tt.
		
	#[global] Program Instance FIFO : Scheduler_t := 
		mkScheduler FIFO_schedule _ _ _ InitSchState UpdatSchState.
	Admit Obligations.
	
	(* The one defined in Interface SubLayer *)
	#[global] Existing Instance ARBITER_CFG.

	#[global]	Existing Instance ImplementationSubLayer.

	Definition FIFO_arbitrate t := 
		@mkTestTrace SYS_CFG REQUESTOR_CFG
			((Default_arbitrate t).(Arbiter_Commands))
			((Default_arbitrate t).(Arbiter_Time)).
	
End FIFOimpSL.