module App where

import qualified ImplementationInterface
import qualified FIFO
import qualified Arbiter
import qualified Bank
import qualified System
import qualified Commands
import qualified Datatypes
import qualified Requests
import qualified Trace
import qualified Eqtype
import qualified Seq
import qualified Ssrbool
import qualified Ssrnat
import qualified Bool

import qualified ForeignRequest
import qualified ForeignCommand

import Foreign
import Foreign.Storable
import Foreign.Ptr
import Foreign.C.Types

cint2bool :: CInt -> CInt -> Datatypes.Coq_bool
cint2bool a b =
    if (a == b) then Datatypes.Coq_true
    else Datatypes.Coq_false

cint2reflect :: CInt -> CInt -> Bool.Coq_reflect
cint2reflect a b =
    if (a == b) then Bool.ReflectT
    else Bool.ReflectF

convertReq :: ForeignRequest.ForeignRequest_t -> IO (Requests.Request_t)
convertReq r = do
    case (ForeignRequest.requestType r) of
        0 -> return (Requests.Coq_mkReq
            (FIFO.unsafeCoerce(ForeignRequest.requestorID r))
            (fromIntegral $ ForeignRequest.arriveTime r)
            Requests.RD
            (fromIntegral $ ForeignRequest.bank r)
            (fromIntegral $ ForeignRequest.row r))
        otherwise ->
            return (Requests.Coq_mkReq -- write case
            (FIFO.unsafeCoerce(ForeignRequest.requestorID r))
            (fromIntegral $ ForeignRequest.arriveTime r)
            Requests.WR
            (fromIntegral $ ForeignRequest.bank r)
            (fromIntegral $ ForeignRequest.row r))

convertReqList :: [ForeignRequest.ForeignRequest_t] -> IO (Datatypes.Coq_list Requests.Request_t)
convertReqList [] = do return (Datatypes.Coq_nil)
convertReqList (r:rs) = do
    coq_req <- convertReq r
    coq_req_tail <- convertReqList rs
    return (Datatypes.Coq_cons coq_req coq_req_tail)

--------------------- init state ------------------------------
fifo_init_state_genstate :: [ForeignRequest.ForeignRequest_t] -> IO (FIFO.FIFO_state_t)
fifo_init_state_genstate reqlist = do
    coq_list <- convertReqList reqlist
    -- this should be arguments eventually
    -- let bkcfg = Bank.Build_Bank_configuration 8 1 1 1 1 1 1 1 1 1 1 1 1 1
    let syscfg = System.Build_System_configuration 8 4 8 5 24 37 9 9 28 6 12 7 6 4
    let fifocfg = 100
    -- calling coq
    return (FIFO.coq_Init_state syscfg fifocfg coq_list)

foreign export ccall fifo_init_state_wrapper :: CInt -> ForeignRequest.PtrRequest -> IO (StablePtr FIFO.FIFO_state_t)
fifo_init_state_wrapper :: CInt -> ForeignRequest.PtrRequest -> IO (StablePtr FIFO.FIFO_state_t)
fifo_init_state_wrapper size ptr =
    if (size == 0) then do
        reqlist <- return []
        state <- fifo_init_state_genstate reqlist
        newStablePtr state
    else do
        putStr $ "Init function \n"
        putStr $ "---------------------------- \n"
        reqlist <- peekArray (fromIntegral size) ptr
        state <- fifo_init_state_genstate reqlist
        newStablePtr state

--------------------- next state ------------------------------
-- type Next_state_triple = Datatypes.Coq_prod FIFO.FIFO_state_t (Datatypes.Coq_prod Commands.Command_kind_t Requests.Request_t)
-- type Out_Ptr = StablePtr Next_state_triple

data OutputStruct = 
  Coq_mkOutput FIFO.FIFO_state_t Commands.Command_kind_t Requests.Request_t\

type Out_Ptr_Alt = StablePtr OutputStruct

fifo_next_state_genstate :: [ForeignRequest.ForeignRequest_t] -> FIFO.FIFO_state_t ->
  IO (OutputStruct)
fifo_next_state_genstate reqlist as = do
    coq_list <- convertReqList reqlist
    -- this should be arguments eventually
    let syscfg = System.Build_System_configuration 8 4 8 5 24 37 9 9 28 6 12 7 6 4
    -- let reqcfg = FIFO.unsafeCoerce ((Eqtype.Equality__Mixin (cint2bool) cint2reflect))
    let fifocfg = 100
    -- calling coq
    let triple = FIFO.coq_Next_state syscfg fifocfg coq_list as
    let newstate = Datatypes.fst triple
    let pair = Datatypes.snd triple
    let ncmd = Datatypes.fst pair
    let nreq = Datatypes.snd pair
    return (Coq_mkOutput newstate ncmd nreq)

-- fifo_next_state_genstate :: [ForeignRequest.ForeignRequest_t] -> FIFO.FIFO_state_t ->
--   IO (Next_state_triple)
-- fifo_next_state_genstate reqlist as = do
--     coq_list <- convertReqList reqlist
--     -- this should be arguments eventually
--     let syscfg = System.Build_System_configuration 8 4 8 5 24 37 9 9 28 6 12 7 6 4
--     -- let reqcfg = FIFO.unsafeCoerce ((Eqtype.Equality__Mixin (cint2bool) cint2reflect))
--     let fifocfg = 100
--     -- calling coq
--     return (FIFO.coq_Next_state syscfg fifocfg coq_list as)

foreign export ccall fifo_next_state_wrapper :: CInt -> ForeignRequest.PtrRequest -> StablePtr FIFO.FIFO_state_t ->
  IO (Out_Ptr_Alt)
fifo_next_state_wrapper :: CInt -> ForeignRequest.PtrRequest -> StablePtr FIFO.FIFO_state_t ->
  IO (Out_Ptr_Alt)
fifo_next_state_wrapper size ptr as = do
    case size of
        0 -> do
            reqlist <- return []
            oldstate <- deRefStablePtr as
            putStr "old state: \n"
            case oldstate of
              FIFO.IDLE count reqlist -> putStr $ "IDLE. Counter: " ++ show count ++ "\n"
              FIFO.RUNNING count reqlist req -> putStr $ "RUNNING. Counter: " ++ show count ++ ". Request: " ++ req2string (req) ++ "\n"
            triple <- fifo_next_state_genstate reqlist oldstate
            let (Coq_mkOutput nstate ncmd nreq) = triple
            case nstate of
              FIFO.IDLE count reqlist -> putStr $ "IDLE. Counter: " ++ show count ++ "\n"
              FIFO.RUNNING count reqlist req -> putStr $ "RUNNING. Counter: " ++ show count ++ ". Request: " ++ req2string (req) ++ "\n"
            case ncmd of
              Commands.ACT -> putStr $ "ACT \n"
              Commands.PRE -> putStr $ "PRE \n"
              Commands.PREA -> putStr $ "PREA \n"
              Commands.CRD -> putStr $ "CRD \n"
              Commands.CWR -> putStr $ "CWD\n"
              Commands.REF -> putStr $ "REF\n"
              Commands.NOP -> putStr $ "NOP \n"
            putStr $ "---------------------------- \n"
            newStablePtr triple
        otherwise -> do
            reqlist <- peekArray (fromIntegral size) ptr
            oldstate <- deRefStablePtr as
            triple <- fifo_next_state_genstate reqlist oldstate
            newStablePtr triple

-- foreign export ccall fifo_next_state_wrapper :: CInt -> ForeignRequest.PtrRequest -> StablePtr FIFO.FIFO_state_t ->
--   IO (Out_Ptr)
-- fifo_next_state_wrapper :: CInt -> ForeignRequest.PtrRequest -> StablePtr FIFO.FIFO_state_t ->
--   IO (Out_Ptr)
-- fifo_next_state_wrapper size ptr as = do
--     case size of
--         0 -> do
--             reqlist <- return []
--             oldstate <- deRefStablePtr as
--             putStr "old state: \n"
--             case oldstate of
--               FIFO.IDLE count reqlist -> putStr $ "IDLE. Counter: " ++ show count ++ "\n"
--               FIFO.RUNNING count reqlist req -> putStr $ "RUNNING. Counter: " ++ show count ++ ". Request: " ++ req2string (req) ++ "\n"
--             triple <- fifo_next_state_genstate reqlist oldstate
--             let newstate = Datatypes.fst triple
--             putStr "new state: \n"
--             case newstate of
--               FIFO.IDLE count reqlist -> putStr $ "IDLE. Counter: " ++ show count ++ "\n"
--               FIFO.RUNNING count reqlist req -> putStr $ "RUNNING. Counter: " ++ show count ++ ". Request: " ++ req2string (req) ++ "\n"
--             let pair = Datatypes.snd triple 
--             let newcmd = Datatypes.fst pair
--             case newcmd of
--               Commands.ACT -> putStr $ "ACT \n"
--               Commands.PRE -> putStr $ "PRE \n"
--               Commands.PREA -> putStr $ "PREA \n"
--               Commands.CRD -> putStr $ "CRD \n"
--               Commands.CWR -> putStr $ "CWD\n"
--               Commands.REF -> putStr $ "REF\n"
--               Commands.NOP -> putStr $ "NOP \n"
--             putStr $ "---------------------------- \n"
--             let req = Datatypes.snd pair
--             newStablePtr (Datatypes.Coq_pair newstate (Datatypes.Coq_pair newcmd req))
--         otherwise -> do
--             reqlist <- peekArray (fromIntegral size) ptr
--             oldstate <- deRefStablePtr as
--             triple <- fifo_next_state_genstate reqlist oldstate
--             let newstate = Datatypes.fst triple
--             let pair = Datatypes.snd triple 
--             let newcmd = Datatypes.fst pair
--             let req = Datatypes.snd pair
--             newStablePtr (Datatypes.Coq_pair newstate (Datatypes.Coq_pair newcmd req))
--------------------------------------------------------------
req2string :: Requests.Request_t -> String
req2string (Requests.Coq_mkReq requestor date typ bank row) =
    let reqtype = case typ of
            Requests.RD -> "RD"
            Requests.WR -> "WR" in
    "requestor: " ++ show(FIFO.unsafeCoerce(requestor) :: CInt) ++
    ", date: " ++ show(date) ++
    ", type: " ++ reqtype ++
    ", bank: " ++ show(bank) ++
    ", row:  " ++ show(row)

-- foreign export ccall fifo_print_state :: Out_Ptr -> IO()
-- fifo_print_state :: Out_Ptr -> IO ()
-- fifo_print_state state_ptr = do
--   (Datatypes.Coq_pair state (Datatypes.Coq_pair cmd_kind req)) <- deRefStablePtr state_ptr
--   putStr "1: State\n"
--   case state of
--         FIFO.IDLE count reqlist -> putStr $ "IDLE. Counter: " ++ show count ++ "\n"
--         FIFO.RUNNING count reqlist req -> putStr $ "RUNNING. Counter: " ++ show count ++ ". Request: " ++ req2string (req) ++ "\n"
--   putStr "2. Command and Request\n"
--   case cmd_kind of
--         Commands.ACT ->
--             putStr $ "ACT. Request: " ++ req2string (req) ++ "\n"
--         Commands.PRE ->
--             putStr $ "PRE. Request: " ++ req2string (req) ++ "\n"
--         Commands.PREA ->
--             putStr $ "PREA. Request: " ++ req2string (req) ++ "\n"
--         Commands.CRD ->
--             putStr $ "CRD. Request: " ++ req2string (req) ++ "\n"
--         Commands.CWR ->
--             putStr $ "CWD. Request: " ++ req2string (req) ++ "\n"
--         Commands.REF ->
--             putStr $ "REF. Request: " ++ req2string (req) ++ "\n"
--         Commands.NOP ->
--             putStr $ "NOP. Request: " ++ req2string (req) ++ "\n"
--   putStr "--------------------------------------\n"