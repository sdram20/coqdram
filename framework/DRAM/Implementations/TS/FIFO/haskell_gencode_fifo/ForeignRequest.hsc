module ForeignRequest where

import Foreign
import Foreign.StablePtr
import Foreign.Ptr
import Foreign.C.String
import Foreign.C.Types

#include "ForeignRequest_t.h"

data ForeignRequest_t = ForeignRequest_t {
    requestType :: CInt,
    requestorID :: CUInt,
    requestSize :: CUInt,
    address :: CULLong,
    ddata :: StablePtr CInt,
    arriveTime :: CUInt,
    rank :: CUInt,
    bankGroup :: CUInt,
    bank :: CUInt,
    subArray :: CUInt,
    row :: CUInt,
    col :: CUInt,
    addressMap :: CIntPtr
} deriving (Eq)

type PtrRequest = Ptr ForeignRequest_t

instance Storable ForeignRequest_t where
    sizeOf    _ = (#size ForeignRequest_t)
    alignment _ = alignment (undefined :: CInt)
    peek ptr = do
        r_type <- (#peek ForeignRequest_t, requestType) ptr
        r_requestorID <- (#peek ForeignRequest_t, requestorID) ptr
        r_requestSize <- (#peek ForeignRequest_t, requestSize) ptr
        r_address <- (#peek ForeignRequest_t, address) ptr
        r_ddata <- (#peek ForeignRequest_t, ddata) ptr
        r_arriveTime <- (#peek ForeignRequest_t, arriveTime) ptr
        r_rank <- (#peek ForeignRequest_t, rank) ptr
        r_bankGroup <- (#peek ForeignRequest_t, bankGroup) ptr
        r_bank <- (#peek ForeignRequest_t, bank) ptr
        r_subArray <- (#peek ForeignRequest_t, subArray) ptr
        r_row <- (#peek ForeignRequest_t, row) ptr
        r_col <- (#peek ForeignRequest_t, col) ptr
        r_addressMap <- (#peek ForeignRequest_t, addressMap) ptr
        return ForeignRequest_t {
            requestType = r_type,
            requestorID = r_requestorID,
            requestSize = r_requestSize,
            address = r_address,
            ddata = r_ddata,
            arriveTime = r_arriveTime,
            rank = r_rank,
            bankGroup = r_bankGroup,
            bank = r_bank,
            subArray = r_subArray,
            row = r_row,
            col = r_col,
            addressMap = r_addressMap
        }
    poke ptr (ForeignRequest_t requestType requestorID requestSize address ddata arriveTime rank bankGroup bank subArray row col addressMap) = do
        (#poke ForeignRequest_t, requestType) ptr requestType
        (#poke ForeignRequest_t, requestorID) ptr requestorID
        (#poke ForeignRequest_t, requestSize) ptr requestSize
        (#poke ForeignRequest_t, address) ptr address
        (#poke ForeignRequest_t, ddata) ptr ddata
        (#poke ForeignRequest_t, arriveTime) ptr arriveTime
        (#poke ForeignRequest_t, rank) ptr rank
        (#poke ForeignRequest_t, bankGroup) ptr bankGroup
        (#poke ForeignRequest_t, bank) ptr bank
        (#poke ForeignRequest_t, subArray) ptr subArray
        (#poke ForeignRequest_t, row) ptr row
        (#poke ForeignRequest_t, col) ptr col
        (#poke ForeignRequest_t, addressMap) ptr addressMap

-- foreign export ccall showRequest :: PtrRequest -> IO()
-- showRequest :: PtrRequest -> IO()
-- showRequest ptr = do
--     request <- peek ptr
--     print (arriveTime request)