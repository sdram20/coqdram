#ifndef FOREIGNREQUEST_H
#define FOREIGNREQUEST_H

typedef enum{
	DATA_READ,
	DATA_WRITE,
} CoqRequestType;

typedef struct{
	CoqRequestType requestType;
	unsigned int requestorID;
	unsigned int requestSize;
	unsigned long long address;
	void *ddata;
	unsigned int arriveTime;
	unsigned int returnTime;
	unsigned int rank;
	unsigned int bankGroup;
	unsigned int bank;
	unsigned int subArray;
	unsigned int row;
	unsigned int col;
	unsigned int addressMap[4];
} ForeignRequest_t;

#endif /* REQUEST_H */
