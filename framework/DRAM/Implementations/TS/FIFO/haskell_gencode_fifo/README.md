## C++ application

This is a C++ application that uses the haskell-generated code to run the controller.

You need to have ghc installed, with hsc2hs in it.

1) run make on the parent folder, the file FIFO_extraction.v will generate .hs files in this folder
ps: FIFO_extraction.v needs to be included in the _CoqProject file for this, otherwise compile Extraction.v manually

2) run make on this folder to compile the full c++/haskell application
