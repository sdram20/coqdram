Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From mathcomp Require Export fintype div.
Require Import Program.
From DRAM Require Export FIFO_proofs.

Section FIFOsim.

  Program Instance SYS_CFG : System_configuration :=
  {
    BANKGROUPS := 1;
    BANKS := 4;

    T_BURST := 1;
    T_WL    := 1;

    T_RRD_s := 1;
    T_RRD_l := 3;

    T_FAW := 20;

    T_RC  := 3;
    T_RP  := 4;
    T_RCD := 2;
    T_RAS := 4;
    T_RTP := 4;
    T_WR  := 1;

    T_RTW := 10;
    T_WTR_s := 1;
    T_WTR_l := 10;
    T_CCD_s := 1;
    T_CCD_l := 12;
  }.
	FIFO_arbiter
  Program Instance FIFO_CFG : FIFO_configuration :=
  {
    WAIT := 13
  }.

  Existing Instance REQESTOR_CFG.
  Existing Instance FIFO_implementation.
  Existing Instance FIFO_arbiter.

  Program Definition Req1 := mkReq tt 1 RD 0 0 1.
  Program Definition Req2 := mkReq tt 2 RD 0 0 1.

  Definition Input : Requests_t := [:: Req2; Req1].
 
  Program Instance AF : Arrival_function_t := 
    Default_arrival_function_t Input.
  
  Compute Arbitrate 10.
  
End FIFOsim.