Set Warnings "-notation-overridden,-parsing".
Unset Extraction Optimize.

From DRAM Require Export FIFO.
From Coq Require Extraction.

From Coq Require Import Arith.
From Coq Require Import ExtrHaskellNatNum.

Extraction Language Haskell.

Extract Inductive nat => "Prelude.Int" [ "0" "Prelude.succ" ]
  "(\fO fS n -> if n Prelude.== 0 then fO () else fS (n Prelude.- 1))".

Cd "haskell_gencode_fifo".
Recursive Extraction Library FIFO.
Cd "..".
