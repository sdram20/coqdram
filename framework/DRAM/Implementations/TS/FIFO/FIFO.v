Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From mathcomp Require Import ssreflect ssrnat ssrbool eqtype seq zify fintype.
From DRAM Require Export ImplementationInterface Arbiter.

Section FIFO.

  Context {SYS_CFG : System_configuration}.

  Instance REQUESTOR_CFG : Requestor_configuration := {
    Requestor_t := unit_eqType
  }.

  (* #[local] Axiom DDR3 : BANKGROUPS = 1. *)

  Definition ACT_date := T_RP.-1.
  Definition CAS_date := ACT_date + T_RCD.
  
  Class FIFO_configuration :=
  {
    WAIT : nat;

    T_RP_GT_ONE     : 1 < T_RP;
    T_RCD_GT_ONE    : 1 < T_RCD;
    T_RTP_GT_ONE    : 1 < T_RTP;

    WAIT_gt_one     : 0 < WAIT.-1;
    WAIT_pos        : 0 < WAIT;
    WAIT_ACT        : ACT_date < WAIT;
    WAIT_CAS        : CAS_date < WAIT;

    (* Length of the minimum slot (write request): T_RP + T_RCD + T_WL + T_BURST + T_WR *)
    WAIT_END_WRITE  : CAS_date + T_WR + T_WL + T_BURST < WAIT;

    (* Length of the minimum slot (read request): T_RP + T_RCD + T_RTP *)
    WAIT_END_READ   : CAS_date + T_RTP < WAIT;

    RC_WAIT         : T_RC  < WAIT;
    CCD_WAIT        : T_CCD_l < WAIT;
    RRD_WAIT        : T_RRD_l < WAIT;
    RTW_WAIT        : T_RTW < WAIT;
    RAS_WAIT        : T_RP + T_RAS < WAIT;
    WTP_WAIT        : T_WR + T_WL + T_BURST < WAIT;
    WTR_WAIT        : T_WTR_l + T_WL + T_BURST < WAIT;
    FAW_WAIT        : T_FAW < WAIT + WAIT + WAIT;
  }.

	Context {FIFO_CFG : FIFO_configuration}.
  Context {AF : Arrival_function_t}.

	(* ------------------------------------------------------------------------- *)
	(* Helper Lemmas *)

	Lemma FIFO_WAIT_GT_SCAS :
    CAS_date.+1 < WAIT.
  Proof.
    apply ltn_trans with (n := CAS_date + T_RTP); [ | exact WAIT_END_READ ].
		rewrite -ltn_predRL; specialize T_RTP_GT_ONE; lia.
  Qed.

	Lemma FIFO_WAIT_GT_SACT :
    ACT_date.+1 < WAIT.
  Proof.
    apply ltn_trans with (n := CAS_date).
      2: exact WAIT_CAS.
    unfold CAS_date, ACT_date.
    rewrite prednK.
      2: specialize T_RP_GT_ONE as H; apply ltn_trans with (n := 1); done || exact H.
		specialize T_RCD_GT_ONE; lia.
	Qed.
   
  Lemma CAS_neq_ACT:
    (CAS_date == ACT_date) = false.
  Proof.
    rewrite /CAS_date /ACT_date.
    rewrite -{2}[T_RP.-1]addn0 eqn_add2l.
    rewrite eqn0Ngt. apply Bool.negb_false_iff. 
    specialize T_RCD_pos as H. rewrite -lt0n in H; by rewrite H.
  Qed.

	(* ------------------------------------------------------------------------------- *)

  Definition Counter_t := ordinal WAIT.
  
  Variant FIFO_state_t :=
    | IDLE    : Counter_t -> Requests_t -> FIFO_state_t
    | RUNNING : Counter_t -> Requests_t -> Request_t -> FIFO_state_t.

  #[export] Instance ARBITER_CFG : Arbiter_configuration :=
  {
    State_t := FIFO_state_t;
  }.

  Definition OCycle0 := Ordinal WAIT_pos.
  Definition OACT_date := Ordinal WAIT_ACT.
  Definition OCAS_date := Ordinal WAIT_CAS.
  
  (* Increment counter for cycle ofset (with wrap-arround). *)
  Definition Next_cycle (c : Counter_t) :=
    let nextc := c.+1 < WAIT in
      (if nextc as X return (nextc = X -> Counter_t) then 
        fun (P : nextc = true) => Ordinal (P : nextc)
       else
        fun _ => OCycle0) Logic.eq_refl.

  Definition Init_state R :=
    IDLE OCycle0 R.
		
  Definition Next_state R (AS : FIFO_state_t) : (FIFO_state_t * Command_kind_t) :=
    match AS with
      | IDLE c P =>
        let c' := Next_cycle c in
        let P' := Enqueue R P in
        match P with
          | [::]    => (IDLE c' P',NOP)
          | r :: PP => (RUNNING OCycle0 (Enqueue R (Dequeue r P)) r,PRE r)
        end
      | RUNNING c P r =>
        let P' := Enqueue R P in
        let c' := Next_cycle c in
        if nat_of_ord c == OACT_date then (RUNNING c' P' r,ACT r)
        else if nat_of_ord c == OCAS_date then (RUNNING c' P' r,(Kind_of_req r) r)
        else if nat_of_ord c == WAIT.-1 then
          match P with
            | [::] => (IDLE OCycle0 P', NOP)
            | r :: PP => (RUNNING OCycle0 (Enqueue R (Dequeue r P)) r,PRE r)
          end
        else (RUNNING c' P' r,NOP)
    end.

  #[export] Instance FIFO_implementation : Implementation_t := 
    mkImplementation Init_state Next_state.

	(* ------------------------------------------------------------------ *)
  (* --------------------- USEFUL DEFINITIONS ------------------------- *)
  (* ------------------------------------------------------------------ *)
  
  Definition FIFO_counter (AS : State_t) :=
    match AS with
      | IDLE c _       => nat_of_ord c 
      | RUNNING c _ _  => nat_of_ord c
    end.

  Definition isRUNNING (AS : State_t) :=
    match AS with
      | IDLE _ __ => false
      | RUNNING _ _ _ => true
    end.
  
  Definition isIDLE (AS : State_t) :=
    match AS with
      | IDLE _ __ => true
      | RUNNING _ _ _ => false
    end.

  Definition Requestor_slot_start ta :=
    match (Default_arbitrate ta).(Implementation_State) with
      | IDLE c R => ta
      | RUNNING c R r => ta + (WAIT.-1 - c)
    end.

  Definition Slot_start (t :nat) :=
    match (Default_arbitrate t).(Implementation_State) with
      | IDLE c P => 0
      | RUNNING c P r => t - c
    end.

  Definition FIFO_pending (AS : State_t) :=
    match AS with
        | IDLE _ P      => P
        | RUNNING _ P _  => P
    end.

  Definition FIFO_request (AS : State_t ) : option Request_t := 
    match AS with
      | IDLE _ _ => None
      | RUNNING _ _ r => Some r
    end.

End FIFO.