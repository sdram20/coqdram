Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From mathcomp Require Import ssreflect ssrnat ssrbool eqtype seq zify fintype ssrZ zify.
From DRAM Require Export Util FIFO.
From Coq Require Import Program.

Section Proofs.

	Context {SYS_CFG : System_configuration}.
	Context {FIFO_CFG : FIFO_configuration}.
	
	Existing Instance REQUESTOR_CFG.

  Context {AF : Arrival_function_t}.

	Existing Instance ARBITER_CFG.
  Existing Instance FIFO_implementation.

	Notation "# t" := (Default_arbitrate t) (at level 0).

	Ltac destruct_state t :=
		let x := fresh "Hs" in destruct # (t).(Implementation_State) eqn:x; simpl;
		repeat match goal with
		| |- context [match ?r with | [::] => _ | _ => _ end] =>
			let x := fresh "Hr" in destruct r eqn:x
		| |- context [?c == ACT_date] =>
		  let x := fresh "Hact" in destruct (nat_of_ord c == ACT_date) eqn:x
		| |- context [?c == CAS_date] =>
		  let x := fresh "Hcas" in destruct (nat_of_ord c == CAS_date) eqn:x
		| |- context [?c == FIFO_CFG.(WAIT).-1] =>
		  let x := fresh "Hend" in destruct (nat_of_ord c == FIFO_CFG.(WAIT).-1) eqn:x
		end.

	(* ------------------------------------------------------------------ *)
  (* --------------------- COUNTER PROOFS ----------------------------- *)
  (* ------------------------------------------------------------------ *)

	Lemma FIFO_wait_neq_act :
    (WAIT.-1 == OACT_date) = false.
  Proof.
		unfold OACT_date; simpl; specialize FIFO_WAIT_GT_SACT; lia.
	Qed.

  Lemma FIFO_wait_neq_cas :
    (WAIT.-1 == OCAS_date) = false.
  Proof.
		unfold OCAS_date; simpl; specialize FIFO_WAIT_GT_SCAS; lia.
	Qed.
    
	Lemma FIFO_next_cycle_inc (c : Counter_t) : c < FIFO_CFG.(WAIT).-1 ->
		nat_of_ord (Next_cycle c) = c.+1.
	Proof.
		intro; unfold Next_cycle; set (HH := c.+1 < WAIT); dependent destruction HH;
		apply Logic.eq_sym in x; move : Logic.eq_refl; rewrite {2 3} x; simpl; intro; clear e;
		[ reflexivity | lia ].
	Qed.

	Lemma FIFO_next_cycle_cas_eq_casS:
    nat_of_ord (Next_cycle OCAS_date) = CAS_date.+1.
  Proof.
		apply FIFO_next_cycle_inc; simpl; specialize FIFO_WAIT_GT_SCAS; lia.
	Qed.

	Lemma FIFO_nextcycle_act_eq_actS:
    nat_of_ord (Next_cycle OACT_date) = ACT_date.+1.
  Proof.
		apply FIFO_next_cycle_inc; simpl; specialize FIFO_WAIT_GT_SACT; lia.
	Qed.
    
	Lemma FIFO_inc_counter (t : nat) (c : Counter_t):
    isRUNNING (# t).(Implementation_State) -> c < WAIT.-1 ->
    FIFO_counter (# t).(Implementation_State) = c ->
    FIFO_counter # (t.+1).(Implementation_State) = c.+1.
  Proof.
    intros Hrun H Hc; simpl. 
    unfold FIFO_counter, Next_state.
		destruct_state t;
		try (discriminate Hrun); clear Hrun; simpl;
		unfold FIFO_counter in Hc; subst;
		try (move: Hend => /eqP Hend; lia);
		rewrite -Hc; rewrite -Hc in H;
		apply FIFO_next_cycle_inc; assumption.
	Qed.
		
	Lemma FIFO_inc_run (c : Counter_t) t:
    isRUNNING (# t).(Implementation_State) -> 
		FIFO_counter (# t).(Implementation_State) = c ->
    c < WAIT.-1 -> isRUNNING # (t.+1).(Implementation_State).
  Proof.
    intros Hrun Hc Hw.
    apply (FIFO_inc_counter t c) in Hrun as Hc'; try done.
    simpl; unfold Next_state, isRUNNING.
		destruct_state t; simpl; try trivial.
		unfold FIFO_counter in Hc; subst.
		move: Hend => /eqP Hend; lia.
	Qed.
    
	Lemma FIFO_add_counter (c : Counter_t) d t:
    isRUNNING (# t).(Implementation_State) -> (c + d) < WAIT ->
    FIFO_counter (# t).(Implementation_State) = c ->
    isRUNNING (# (t + d)).(Implementation_State) /\  
    FIFO_counter (# (t + d)).(Implementation_State) = c + d.
  Proof.
    intros Hrun Hbound Hc.
    induction d; [ by rewrite !addn0 | ].
    move: Hbound; rewrite !addnS; intros Hbound. 
    apply ltn_trans with (m := c+d) in Hbound as IH; [ | lia ].
    apply IHd in IH as H; destruct H as [Hrun' Hcd].
    split. 
			{ apply FIFO_inc_run with (t := t + d) (c := Ordinal IH) in Hrun' as HH; try assumption.
      rewrite - ltn_predRL in Hbound; simpl; exact Hbound. }
			{ apply FIFO_inc_counter with (t := t + d) (c := Ordinal IH) in Hrun' as HH; simpl; try assumption.
				lia. }
	Qed.

	(* ------------------------------------------------------------------ *)
  (* --------------------- TIMING PROOFS ------------------------------ *)
  (* ------------------------------------------------------------------ *)

	Lemma FIFO_ACT_date t cmd:
    cmd \in (# t).(Arbiter_Commands) -> isACT cmd -> let tcmd := cmd.(CDate) in
    (FIFO_counter (# tcmd).(Implementation_State)) = Next_cycle OACT_date /\ 
    (isRUNNING (# tcmd).(Implementation_State)) /\
    (FIFO_request (# tcmd).(Implementation_State) == get_req cmd).
  Proof.
    induction t; [ done | ]; simpl.
    unfold Next_state.
		destruct_state t; rewrite in_cons; intros Hi Hp; move: Hi => /orP [/eqP Hi | Hi]; 
		try (by rewrite Hi in Hp);
		try (by apply IHt in Hi);
		try (unfold Kind_of_req in Hi; destruct r0.(Kind); by rewrite Hi in Hp);
		rewrite Hi //= /Next_state Hs //= Hact //=;
		split; try (move: Hact => /eqP Hact; unfold Next_cycle; simpl; rewrite Hact //=);
		split; trivial.
	Qed.

	Lemma FIFO_CAS_date t cmd:
		cmd \in (# t).(Arbiter_Commands) -> isCAS cmd -> let tcmd := cmd.(CDate) in
		(FIFO_counter (# tcmd).(Implementation_State)) = Next_cycle OCAS_date /\ 
		(isRUNNING (# tcmd).(Implementation_State)) /\
		(FIFO_request (# tcmd).(Implementation_State) == get_req cmd).
	Proof.
		induction t; [ done | ]; simpl.
		unfold Next_state.
		destruct_state t; rewrite in_cons; intros Hi Hp; move: Hi => /orP [/eqP Hi | Hi];
		try (by rewrite Hi in Hp);
		try (by apply IHt in Hi);
		try (unfold Kind_of_req in Hi; destruct r0.(Kind); by rewrite Hi in Hp);
		rewrite Hi /Kind_of_req; destruct r0.(Kind); rewrite //= /Next_state Hs //= Hact Hcas //=;
		split; try (move: Hcas => /eqP Hcas; unfold Next_cycle; simpl; rewrite Hcas //=);
		split; trivial.
	Qed.

	Lemma FIFO_PRE_date t cmd:
    cmd \in (# t).(Arbiter_Commands) -> isPRE cmd ->
    (FIFO_counter # (cmd.(CDate)).(Implementation_State) = OCycle0) /\ 
    (isRUNNING # (cmd.(CDate)).(Implementation_State)) /\
  	(FIFO_request # (cmd.(CDate)).(Implementation_State) == get_req cmd).
  Proof.
    induction t; [ done | ]; simpl.
		unfold Next_state; destruct_state t; rewrite in_cons; intros Hi Hp; move: Hi => /orP [/eqP Hi | Hi];
		try (by rewrite Hi in Hp);
		try (by apply IHt in Hi);
		try (unfold Kind_of_req in Hi; destruct r0.(Kind); by rewrite Hi in Hp);
		rewrite Hi //= /Next_state Hs //= Hact Hcas //=.
		move: Hend => /eqP Hend; rewrite !Hend eq_refl //=.
  Qed.

	Lemma FIFO_eq_counter t t' d:
    isRUNNING (# t).(Implementation_State) -> t < t' -> 
		FIFO_counter (# t).(Implementation_State) = FIFO_counter (# t').(Implementation_State) ->
    FIFO_counter (# t).(Implementation_State) + d < WAIT -> t + d < t'.
  Proof.
    intros Hrun Hlt Hc Hd.
    induction d; [ lia | ].
    rewrite addnS in Hd.
		assert (FIFO_counter # (t).(Implementation_State) < WAIT) as HH; [ lia | ].
		assert (FIFO_counter # (t).(Implementation_State) + d < WAIT) as IH; [ lia | ].
    apply IHd in IH as H; clear IHd.
    rewrite addnS ltn_neqAle H andbT.
    destruct (_ == t') eqn:He; [ | trivial ].
  	contradict Hc.
    move: He => /eqP He; rewrite -He -addnS.
    apply (FIFO_add_counter (Ordinal HH) (d.+1)) in Hrun as [_ HHH]; try done.
  	2: rewrite addnS; exact Hd.
    simpl in HHH; rewrite HHH; apply /eqP; rewrite neq_ltn; apply /orP; left; lia.
  Qed.

	Lemma FIFO_counter_bounded t:
    FIFO_counter (# t).(Implementation_State) <= WAIT.-1.
  Proof.
		set c := FIFO_counter _.
		unfold FIFO_counter in c.
		destruct (# t).(Implementation_State) eqn:Hs;
		specialize (@ltn_ord WAIT c0); lia.
	Qed.

	Lemma FIFO_counter_bounded_ : forall (c : Counter_t),
    c <= WAIT.-1.
  Proof.
		intro c; destruct c eqn:H; simpl; lia.
	Qed.

	Lemma FIFO_l_counter_border t :
    FIFO_counter # t.(Implementation_State) = WAIT.-1 -> 
    FIFO_counter # (t.+1).(Implementation_State) = 0.
  Proof.
    intros Hco; simpl.
    unfold Next_state;
		destruct_state t; simpl; try reflexivity;
		simpl in Hco; unfold Next_cycle; set (HH := c.+1 < WAIT);
		dependent destruction HH;
		apply Logic.eq_sym in x; move : Logic.eq_refl; rewrite {2 3} x; simpl; intro; clear e;
		try reflexivity;
		rewrite Hco in x; lia.
	Qed.

	Lemma isIDLE_fromstate ta c0 r:
    (# ta).(Implementation_State) = IDLE c0 r  ->
    isIDLE (# ta).(Implementation_State).
  Proof.
    rewrite /isIDLE; intros H; by rewrite H.
  Qed.

	Lemma isIDLE_notRunning ta:
		isIDLE (# ta).(Implementation_State) -> 
		isRUNNING (# ta).(Implementation_State) == false.
	Proof.
		intros; rewrite /isRUNNING; rewrite /isIDLE in H; simpl in H.
		by destruct (# ta).(Implementation_State) eqn:Hs.
	Qed.

	Lemma FIFO_running ta tb (d : Counter_t):
    isIDLE (Default_arbitrate ta).(Implementation_State) 
    -> isRUNNING (Default_arbitrate tb).(Implementation_State)
    -> ta < tb
    -> FIFO_counter (Default_arbitrate ta).(Implementation_State) = 0
		(* Where still idle *)
    -> ((ta + d < tb) && (isRUNNING (Default_arbitrate (ta + d)).(Implementation_State) == false))
		(* There exists a moment r previous to d in which the counter was 0 (at the beginnin of RUNNING) *)  
    \/ (exists r, (ta + r <= tb) && (r <= d) && (isRUNNING # (ta + r).(Implementation_State)) 
    && (FIFO_counter # (ta + r).(Implementation_State) == 0)).
  Proof.
    intros Hidle_a Hrun_b Hlt Hc.
    destruct d as [d Hd].
    induction d;
  	[ apply isIDLE_notRunning in Hidle_a; left; simpl; by rewrite addn0 Hlt andTb Hidle_a | ].
    apply ltn_trans with (m := d) in Hd as Hd'; [ | lia ].
    specialize IHd with (Hd := Hd') as [IH | IH]; simpl in IH.
    { (* we're still idle after D cycles, next cycle could be IDLE, RUNNING or REFRESH *)
      move : IH => /andP [IH Hrun_a'].
      rewrite leq_eqVlt in IH; move : IH => /orP [/eqP IH | IH].
      { (* tb is the cycle after *)
        right. simpl. subst tb. exists (d.+1).
        rewrite ltnSn andbT addnS Hrun_b andbT leqnn andTb /FIFO_counter.
        rewrite /isRUNNING //= /Next_state in Hrun_b Hrun_a' *.
        destruct ((Default_arbitrate (ta + d)).(Implementation_State)) eqn:HS, (r) eqn:HP; simpl; done. }
      { (* now tb is in the future *)
				simpl; rewrite /isIDLE in Hrun_a'.
				destruct ((Default_arbitrate (ta + d)).(Implementation_State)) eqn:HS; simpl.
				{ (* from IDLE to REFRESH 1 *)
					rewrite addnS IH andTb //= /Next_state HS.
					destruct r; simpl; [ by left | ].
					right; exists (d.+1); apply ltnW in IH.
					rewrite addnS IH andTb ltnSn andTb.
					by rewrite /isRUNNING /FIFO_counter //= HS.
				}
				by contradict Hrun_a'.
			}
    }
		right. destruct IH as [r H]; move : H => /andP [/andP [/andP [Hlt' Hrd ]  Hrun_a'] Hc'].
		exists r; simpl.
		rewrite Hlt' andTb; apply /andP; split.
			{ apply /andP. split.
					{ apply leq_trans with (n := d); (exact Hrd || apply ltnW, ltnSn). }
					exact Hrun_a'.
			}
			exact Hc'.
  Qed.

	Lemma FIFO_counter_lt_WAIT t: 
    FIFO_counter (# t).(Implementation_State) < WAIT.
  Proof.
    set (c := FIFO_counter (Default_arbitrate t).(Implementation_State)).
    unfold FIFO_counter in c; destruct_state t; subst c; try done.
	Qed.

	Lemma FIFO_running_at_zero_lt_at_c t t' d:
    isRUNNING (# t).(Implementation_State) -> 
    t < t' -> FIFO_counter (# t).(Implementation_State) = 0 ->
    d < FIFO_counter (# t').(Implementation_State) ->
    t + d < t'.
  Proof.
  	intros Hrun  Hlt HcZ Hd.
  	induction d; [ by rewrite addn0 | ].
   	apply ltn_trans with (m := d) in Hd as IH; [ | lia ].
    apply IHd in IH; clear IHd.
    rewrite addnS ltn_neqAle IH andbT.
    specialize (FIFO_counter_lt_WAIT t') as HW.
    destruct ((t + d).+1 == t') eqn:H.
    apply FIFO_add_counter with (c := OCycle0) (d := d.+1) in Hrun as [_ HcD].
			4: done.
		  3: exact HcZ.
      2: rewrite add0n; apply ltn_trans with (n := FIFO_counter (Default_arbitrate t').(Implementation_State)); exact Hd || exact HW.
		move : H => /eqP H; subst.
		contradict Hd.
		by rewrite -addnS HcD add0n ltnn.
	Qed.

	Lemma isRUNNING_fromstate ta c0 r r0:
    (# ta).(Implementation_State) = RUNNING c0 r r0 ->
    isRUNNING (# ta).(Implementation_State).
  Proof.
    rewrite /isRUNNING; intros H; by rewrite H.
  Qed.

	Lemma FIFO_l_helper_4 ta tb cb:
    ta < tb ->
    FIFO_counter # (ta.+1).(Implementation_State) = 0 ->
    FIFO_counter # tb.(Implementation_State) = cb ->
    cb > 0 -> ta.+1 < tb.
  Proof.
    intros Hlt Hca Hcb Hcb_pos.
    rewrite ltn_neqAle Hlt andbT.
    destruct (_ == tb) eqn:He; [ | done ].
    move: He => /eqP He.
    rewrite -He in Hcb; lia.
  Qed.

	Lemma FIFO_l_helper_3 ta tb d (cb : Counter_t) :
    isRUNNING (# ta).(Implementation_State) ->
    FIFO_counter (# ta).(Implementation_State) = 0 ->
    FIFO_counter (# tb).(Implementation_State) = cb -> 
    d < cb -> ta < tb -> ta + d < tb.
  Proof.
    intros Hrun_a Hca Hcb Hd Hlt.
    induction d; [ lia | ].
    apply ltn_trans with (m:= d) in Hd as Hd'; [ | apply ltnSn ].
    apply IHd in Hd' as H.
    rewrite addnS ltn_neqAle H andbT.
    destruct ( _ == tb) eqn:He; [ | done ].
    move: He => /eqP He.
    apply FIFO_add_counter with (d := d.+1) (c := Ordinal WAIT_pos) in Hrun_a as HH; simpl; try assumption.
    	2: rewrite add0n; apply ltn_trans with (n := cb); (exact Hd || by destruct cb).
    destruct HH as [Hrun_adp1 Hc_adp1]; simpl in Hc_adp1.
    rewrite -He in Hcb.
    rewrite add0n addnS in Hc_adp1.
    rewrite Hc_adp1 in Hcb.
    contradict Hcb; apply /eqP.
    rewrite neq_ltn; apply /orP; left; exact Hd.
  Qed.

	Lemma FIFO_l_eq_bound ta tb (c : Counter_t) :
    isRUNNING (# ta).(Implementation_State) -> 
    isRUNNING (# tb).(Implementation_State) -> 
    ta < tb ->
    FIFO_counter (# ta).(Implementation_State) = c -> 
    FIFO_counter (# tb).(Implementation_State) = c ->
    ta + WAIT <= tb.
  Proof.
    intros Hrun_a Hrun_b Hlt Hc_a Hc_b.
    apply FIFO_eq_counter with (t' := tb) (d := WAIT.-1 - c) in Hrun_a as Hlt'; try lia.
		2: { 
			rewrite Hc_a subnKC; [ | specialize (FIFO_counter_bounded ta) as H; by rewrite Hc_a in H].
			rewrite -ltnS prednK; [ lia | exact WAIT_pos ].
		}
    apply FIFO_add_counter with (c := c) (d := WAIT.-1 - c) in Hrun_a as H; try done.
    2: {
			rewrite subnKC; [ | specialize (FIFO_counter_bounded ta) as H; by rewrite Hc_a in H].
			rewrite -ltnS prednK; [ lia | exact WAIT_pos ].
		}	
		destruct H as [Hrun_a' Hc_a']; try assumption.
    assert (c + (WAIT.-1 - c) = WAIT.-1); 
		[ rewrite subnKC; [ reflexivity | ]; apply (FIFO_counter_bounded_ c) | ].
    rewrite H in Hc_a'; clear H.

		apply FIFO_l_counter_border in Hc_a' as Hc_a''.
    destruct (FIFO_counter # tb.(Implementation_State) == 0) eqn:HcZ.
    { move : HcZ => /eqP HcZ.
      rewrite HcZ in Hc_b; rewrite -Hc_b subn0 -addnS prednK in Hlt'; exact WAIT_pos || exact Hlt'.
    }
    move : HcZ => /negbT HcZ; rewrite -lt0n in HcZ.
    destruct # ((ta + (WAIT.-1 - c)).+1).(Implementation_State) eqn:Hs.
		{ (* IDLE branch *)
			assert (Haux: FIFO_counter # ((ta + (WAIT.-1 - c)).+1).(Implementation_State) = 0);
			[ rewrite /FIFO_counter Hs; by rewrite /FIFO_counter in Hc_a'' | ].

			clear Hc_a''; rename Haux into Hc_a''; apply isIDLE_fromstate in Hs as Hrun_a''.
			rewrite leq_eqVlt in Hlt'; move : Hlt' => /orP [/eqP Hlt' | Hlt'].
			{ subst tb.
				rewrite Hc_b in Hc_a''.
				rewrite Hc_a'' subn0 -addnS prednK; [ | exact WAIT_pos ].
				apply leqnn.
			}
			apply FIFO_running with (tb := tb) (d := c) in Hrun_a'' as [Hlt'' | Hlt'']; try assumption.
			{ (* Stays IDLE until the counter equals c again *)
				move : Hlt'' => /andP [Hlt'' _].
				rewrite  -[(ta + _).+1]addnS -subSn in Hlt''.
					2: rewrite -ltnS prednK; ( by destruct c) || exact WAIT_pos.
				rewrite prednK in Hlt''.
					2: exact WAIT_pos.
				rewrite addnBA in Hlt''.
					2: apply ltnW; by destruct c.
				rewrite subnK in Hlt''.
					2: destruct c; simpl; lia.
				by apply ltnW.
			}
			move : Hlt'' => [r1 /andP [/andP [/andP [Hlt'' Hr ] Hrun_a'''] /eqP Hc_a''']].
			rewrite leq_eqVlt in Hlt''; move : Hlt'' => /orP [/eqP Hlt'' | Hlt''].
			{ subst tb.
				rewrite Hc_b in Hc_a'''.
				rewrite Hc_a''' subn0 -addnS prednK; [ | exact WAIT_pos ].
				apply leq_addr.
			}
			apply FIFO_running_at_zero_lt_at_c with (t' := tb) (d := (FIFO_counter (# tb).(Implementation_State)).-1) in Hrun_a''' as Hlt''';
			try (by rewrite ltn_predL; exact HcZ); try assumption.
			rewrite Hc_b in Hlt'''.
			rewrite  -[(ta + _).+1]addnS -subSn in Hlt'''.
				2: rewrite -ltnS prednK; (by destruct c) || exact WAIT_pos.
			rewrite prednK in Hlt'''.
				2: exact WAIT_pos.
			rewrite -subn1 addnBA in Hlt'''.
				2: rewrite -Hc_b; exact HcZ.
			rewrite subn1 addnBA in Hlt'''.
				2: apply ltnW; by destruct c.
			rewrite prednK in Hlt'''.
				2: apply ltn_addl; by rewrite -Hc_b.
			rewrite addnBAC in Hlt'''.
				2: destruct c; simpl; lia.
			rewrite subnK in Hlt'''.
				2: destruct c; simpl; lia.
				apply leq_trans with (m := ta + WAIT ) in Hlt'''.
				2: destruct c; simpl; lia.
			exact Hlt'''.
		}
		{ (* RUNNING branch *)
			assert (FIFO_counter (Default_arbitrate (ta + (WAIT.-1 - c)).+1).(Implementation_State) = 0) as Haux.
				{ rewrite /FIFO_counter Hs. by rewrite /FIFO_counter in Hc_a''. }
			clear Hc_a''; rename Haux into Hc_a''.
			apply isRUNNING_fromstate in Hs as Hrun_a''.
			apply FIFO_l_helper_4 with (cb := c) in Hlt' as Hlt''; try assumption.
				2: by rewrite -Hc_b.
			apply FIFO_l_helper_3 with (tb := tb) (d := c.-1) (cb := c) in Hrun_a'' as Hlt'''.
				5: exact Hlt''.
				4: by rewrite ltn_predL -Hc_b.
				3: exact Hc_b.
				2: exact Hc_a''.
			rewrite -addnS prednK in Hlt'''.
				2: by rewrite -Hc_b.
			rewrite -addnS -subSS prednK in Hlt'''.
				2: exact WAIT_pos.
			rewrite subnS prednK in Hlt'''.
				2: rewrite subn_gt0; by destruct c.
			rewrite -addnA subnK in Hlt'''.
				2: { destruct c; simpl; rewrite leq_eqVlt; apply /orP; by right. }
			exact Hlt'''.
		}
  Qed.

  Lemma FIFO_l_eq_bound_or ta tb (c : Counter_t) :
    isRUNNING (Default_arbitrate ta).(Implementation_State) ->
    FIFO_counter (Default_arbitrate ta).(Implementation_State) = c ->
    isRUNNING (Default_arbitrate tb).(Implementation_State) ->
    FIFO_counter (Default_arbitrate tb).(Implementation_State) = c ->
    ta = tb \/ (ta + WAIT <= tb \/ tb + WAIT <= ta). 
  Proof.
    intros Hrun_a Hc_a Hrun_b Hc_b.
    destruct (ta < tb) eqn:Hlt.
    { apply FIFO_l_eq_bound with (ta := ta) (c := c) in Hrun_b; try done;
      right; by left. }
    { rewrite ltnNge in Hlt; apply negbFE in Hlt.
      rewrite leq_eqVlt in Hlt; move: Hlt => /orP [/eqP Heq | Hlt]; [ by left | ].
      apply FIFO_l_eq_bound with (ta := tb) (c := c) in Hrun_a; try done.
      right; by right. }
  Qed.

	Lemma FIFO_date_gt_0 cmd t:
    cmd \in (# t).(Arbiter_Commands) -> cmd.(CDate) > 0.
  Proof.
    induction t; simpl; [ intros H; inversion H | ].
    unfold Next_state; destruct_state t; rewrite in_cons; intros H; move: H => /orP [/eqP H | H];
		try (by rewrite H //=);
		by apply IHt in H.
	Qed.
	
	(* ------------------------------------------------------------------ *)
  (* --------------------- TIMING PROOFS ------------------------------ *)
  (* ------------------------------------------------------------------ *)
  Theorem Cmds_T_RCD_ok t a b:
    a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) ->
    isACT a -> isCAS b -> Same_Bank a b -> Before a b ->
    Apart_at_least a b T_RCD.
  Proof.
    intros Ha Hb iAa iCb SB aBb; rewrite /Apart_at_least.
    apply FIFO_ACT_date in Ha as [Hc_a [Hrun_a _]]; [ | assumption ].
    apply FIFO_CAS_date in Hb as [Hc_b [Hrun_b _]]; [ | assumption ].
    set (d := Next_cycle OCAS_date - Next_cycle OACT_date).
    apply FIFO_add_counter with (c := Next_cycle OACT_date) (d := d) in Hrun_a as [Hrun_a' Hc_a']; try assumption.
    2: { 
			unfold d; rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS.
      rewrite subnKC; [ exact FIFO_WAIT_GT_SCAS | ].
			unfold CAS_date; lia. 
		}
    unfold d in Hc_a'; rewrite subnKC in Hc_a'.
		2 : rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS; unfold CAS_date; lia.
		apply FIFO_l_eq_bound_or with (ta := a.(CDate) + d) (c := Next_cycle OCAS_date) in Hrun_b as H; try done.
    destruct H as [H0 | [H1 | H2]].
		{ rewrite -H0 leq_add2l. unfold d. 
			rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS /CAS_date /ACT_date. 
			rewrite prednK; [ | specialize T_RP_pos; lia ]. 
			rewrite -addnS addnC -addn1 -addnA [1 + T_RP.-1]addnC addn1 prednK; [ | specialize T_RP_pos; lia ]. 
			rewrite -addnBA; [ | lia ].
			apply leq_addr. }
		{ unfold d in H1; rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS in H1.
			apply leq_trans with (m := a.(CDate) + T_RCD) in H1; try assumption.
			rewrite -addnA leq_add2l /CAS_date /ACT_date prednK.
			rewrite [T_RP.-1 + _]addnC -addnS prednK.
				2,3: rewrite lt0n; exact T_RP_pos.
			rewrite addnBAC; [ | lia ].
			rewrite leq_subRL; [ | lia ].
			lia. }
    { unfold d in H2; rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS in H2.
      contradict H2. apply /negP. rewrite -ltnNge addnC -ltn_subRL.
      unfold Before in aBb. apply ltn_trans with (n := b.(CDate)); [ assumption | ].
      rewrite subnBA; [ | rewrite /CAS_date /ACT_date -addnS; lia]. 
			rewrite ltn_subRL addnC -addnA ltn_add2l.
      rewrite /CAS_date /ACT_date prednK; [ | by specialize T_RP_pos; lia].
      rewrite addnC -addnS prednK; [ | by specialize T_RP_pos; lia].
      rewrite ltn_add2r.
      apply nat_add_ltn with (m := T_RP.-1);
			[ specialize T_RP_GT_ONE; lia | ].
      specialize WAIT_CAS; by rewrite /CAS_date /ACT_date. 
		}
  Qed.

	Theorem Cmds_T_RP_ok t a b:
    a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) ->
    isPRE a -> isACT b -> Same_Bank a b -> Before a b ->
    Apart_at_least a b T_RP.
  Proof.
		intros Ha Hb iPa iAb SB aBb; rewrite /Apart_at_least.
    apply FIFO_ACT_date in Hb as [Hc_b [Hrun_b _]]; [ | assumption ].
    apply FIFO_PRE_date in Ha as [Hc_a [Hrun_a _]]; [ | assumption ].
    apply FIFO_running_at_zero_lt_at_c with (t':= b.(CDate)) (d := ACT_date) in Hc_a as H; try assumption.
      2: rewrite Hc_b; by rewrite FIFO_nextcycle_act_eq_actS.
    unfold ACT_date in H.
    rewrite -addnS prednK in H; [ assumption | specialize T_RP_pos; lia ].
  Qed.

	Theorem Cmds_T_RC_ok t a b: 
    a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) ->
    isACT a -> isACT b -> Same_Bank a b -> Before a b ->
    Apart_at_least a b T_RC.
  Proof.
    intros Ha Hb iAa iAb SB aBb; rewrite /Apart_at_least.
    apply FIFO_ACT_date in Ha as [Hc_a [Hrun_a _]]; [ | assumption ].
    apply FIFO_ACT_date in Hb as [Hc_b [Hrun_b _]]; [ | assumption ].
    apply FIFO_l_eq_bound with (ta := a.(CDate)) (tb := b.(CDate)) (c := Next_cycle OACT_date) in Hrun_a as Hlt; try done.
    apply leq_trans with (n := (a.(CDate) + WAIT)); [ | assumption ].
    rewrite leq_add2l; specialize RC_WAIT; lia.
  Qed.

	Theorem Cmds_T_RAS_ok t a b:
    a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) ->
    isACT a -> isPRE b -> Same_Bank a b -> Before a b ->
    Apart_at_least a b T_RAS.
  Proof.
    intros Ha Hb iAa iPb SB aBb; rewrite /Apart_at_least.
    apply FIFO_ACT_date in Ha as [Hc_a [Hrun_a _]]; [ | assumption ].
    apply FIFO_PRE_date in Hb as [Hc_b [Hrun_b _]]; [ | assumption ]; clear iAa iPb.
    apply FIFO_add_counter with (d := Next_cycle OACT_date) in Hc_b as H; try done.
      2: by rewrite /OCycle0 add0n FIFO_nextcycle_act_eq_actS FIFO_WAIT_GT_SACT.
 		destruct H as [Hrun_b' Hc_b'].
    set (d := Next_cycle OACT_date); fold d in Hc_b', Hrun_b'; rewrite /OCycle0 add0n in Hc_b'.
    apply FIFO_l_eq_bound with (ta := a.(CDate)) (c := d) (tb := b.(CDate) + d) in Hrun_a as Hlt; try done.
    2: { 
			rewrite /Before in aBb. apply ltn_trans with (p := b.(CDate) + d) in aBb. exact aBb.
      apply nat_ltn_add; unfold d; by rewrite FIFO_nextcycle_act_eq_actS.
    }
    rewrite [_ + d]addnC -leq_subLR in Hlt; unfold d in Hlt.
    apply leq_trans with (m := a.(CDate) + T_RAS) in Hlt; [ exact Hlt | ].
    rewrite -addnBA.
      2: rewrite FIFO_nextcycle_act_eq_actS; specialize FIFO_WAIT_GT_SACT; apply ltnW. 
    rewrite leq_add2l leq_subRL.
      2: rewrite FIFO_nextcycle_act_eq_actS; specialize FIFO_WAIT_GT_SACT; apply ltnW.
    rewrite FIFO_nextcycle_act_eq_actS /ACT_date prednK.
      2: by rewrite lt0n T_RP_pos.
    by rewrite leq_eqVlt RAS_WAIT orbT.
  Qed.

	Theorem Cmds_T_RTP_ok t a b:
    a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) ->
    isCRD a -> isPRE b -> Same_Bank a b -> Before a b -> Apart_at_least a b T_RTP.
  Proof.
    intros Ha Hb iCa iPb sB aBb; rewrite /Apart_at_least.
    apply FIFO_CAS_date in Ha as [Hc_a [Hrun_a _]].
      2: unfold isCAS; unfold isCRD in iCa; apply /orP; left; exact iCa.
    apply FIFO_PRE_date in Hb as [Hc_b [Hrun_b _]]; [ | assumption ].
    apply FIFO_add_counter with (c := OCycle0) (d := CAS_date.+1) in Hrun_b as [Hrun_b' Hc_b']; try assumption.
      2: simpl; rewrite add0n; exact FIFO_WAIT_GT_SCAS.
    rewrite -{2}FIFO_next_cycle_cas_eq_casS in Hc_b'; simpl in Hc_b'; rewrite add0n in Hc_b'.
    apply FIFO_l_eq_bound with (ta := a.(CDate)) (tb := b.(CDate) + CAS_date.+1) (c := Next_cycle OCAS_date) in Hrun_a as H; try assumption.
      2: unfold Before in aBb; apply ltn_trans with (p := b.(CDate) + CAS_date.+1) in aBb; exact aBb || (by rewrite nat_ltn_add).
    rewrite [b.(CDate) + _]addnC -leq_subLR in H.
    apply leq_trans with (n := a.(CDate) + WAIT - CAS_date.+1); [ | assumption ].
    rewrite -addnBA; [ | exact WAIT_CAS].
    rewrite leq_add2l leq_subRL; [ | exact WAIT_CAS ].
    rewrite addnC addnS addnC; exact WAIT_END_READ.
  Qed.

	Theorem Cmds_T_WTP_ok t a b:
    a \in (# t).(Arbiter_Commands) -> b \in (Default_arbitrate t).(Arbiter_Commands) ->
    isCWR a -> isPRE b -> Same_Bank a b -> Before a b ->
    Apart_at_least a b (T_WR + T_WL + T_BURST).
  Proof.
    intros Ha Hb iCa iPb sB aBb; rewrite /Apart_at_least.
    apply FIFO_CAS_date in Ha as [Hc_a [Hrun_a _]].
      2: unfold isCAS; unfold isCWR in iCa; apply /orP; right; exact iCa.
    apply FIFO_PRE_date in Hb as [Hc_b [Hrun_b _]]; [ | assumption ].
    apply FIFO_add_counter with (c := OCycle0) (d := CAS_date.+1) in Hrun_b as [Hrun_b' Hc_b']; try assumption.
      2: simpl; rewrite add0n; exact FIFO_WAIT_GT_SCAS.
    rewrite -{2}FIFO_next_cycle_cas_eq_casS in Hc_b'; simpl in Hc_b'; rewrite add0n in Hc_b'.
    apply FIFO_l_eq_bound with (ta := a.(CDate)) (tb := b.(CDate) + CAS_date.+1) (c := Next_cycle OCAS_date) in Hrun_a as H; try done.
      2: unfold Before in aBb; apply ltn_trans with (p := b.(CDate) + CAS_date.+1) in aBb; exact aBb || (by rewrite nat_ltn_add).
    rewrite [b.(CDate) + _]addnC -leq_subLR in H.
    apply leq_trans with (n := a.(CDate) + WAIT - CAS_date.+1); [ | assumption ].
    rewrite -addnBA; [ | exact WAIT_CAS ].
    rewrite leq_add2l leq_subRL; [ | exact WAIT_CAS ].
    rewrite addnC addnS addnC !addnA.
    exact WAIT_END_WRITE.
  Qed.

	Theorem Cmds_T_RtoW_ok t a b:
    a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) ->
    isCRD a -> isCWR b -> Before a b -> Apart_at_least a b T_RTW.
  Proof.
    intros Ha Hb iCRa iCWb aBb; rewrite /Apart_at_least.
    apply FIFO_CAS_date in Ha as [Hc_a [Hrun_a _]].
			2 : unfold isCAS; rewrite iCRa; done. 
    apply FIFO_CAS_date in Hb as [Hc_b [Hrun_b _]].
      2 : unfold isCAS; rewrite iCWb orbT; done. 
		apply FIFO_l_eq_bound with (ta := a.(CDate)) (tb := b.(CDate)) (c := Next_cycle OCAS_date) in Hrun_a as H; try done.
    apply leq_trans with (n := a.(CDate) + WAIT); [ | assumption ].
    by rewrite leq_add2l leq_eqVlt RTW_WAIT orbT.
  Qed.

	Theorem Cmds_T_WtoR_SBG_ok t a b:
		a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) ->
		isCWR a -> isCRD b -> Before a b -> Same_Bankgroup a b->
		Apart_at_least a b (T_WTR_l + T_WL + T_BURST).
	Proof.
		intros Ha Hb iCWa iCRb abB SBG; rewrite /Apart_at_least.
		apply FIFO_CAS_date in Ha as [Hc_a [Hrun_a _]]; [ | by unfold isCAS; rewrite iCWa orbT; done ].
		apply FIFO_CAS_date in Hb as [Hc_b [Hrun_b _]]; [ | by unfold isCAS; rewrite iCRb; done ].
		apply FIFO_l_eq_bound with (ta := a.(CDate)) (tb := b.(CDate)) (c := Next_cycle OCAS_date) in Hrun_a as H; try done.
		apply leq_trans with (n := a.(CDate) + WAIT); [ | assumption ].
		by rewrite leq_add2l leq_eqVlt WTR_WAIT orbT.
	Qed.

	(* if they always respect the long then they should also always respect the small *)
	Theorem Cmds_T_WtoR_DBG_ok t a b:
		a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) ->
		isCWR a -> isCRD b -> Before a b -> ~~ Same_Bankgroup a b ->
		Apart_at_least a b (T_WTR_s + T_WL + T_BURST).
	Proof.
		intros Ha Hb iCWa iCRb abB nSBG; rewrite /Apart_at_least.
		apply FIFO_CAS_date in Ha as [Hc_a [Hrun_a _]]; [ | by unfold isCAS; rewrite iCWa orbT; done ].
		apply FIFO_CAS_date in Hb as [Hc_b [Hrun_b _]]; [ | by unfold isCAS; rewrite iCRb; done ].
		apply FIFO_l_eq_bound with (ta := a.(CDate)) (tb := b.(CDate)) (c := Next_cycle OCAS_date) in Hrun_a as H; try done.
		apply leq_trans with (n := a.(CDate) + WAIT); [ | assumption ].
		rewrite leq_add2l; specialize WTR_WAIT; specialize T_WTR_bgs; lia.
	Qed.

	Theorem Cmds_T_CCD_SBG_ok t a b:
		a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) ->
		(isCRD a /\ isCRD b) \/ (isCWR a /\ isCWR b) -> Before a b -> Same_Bankgroup a b ->
		Apart_at_least a b T_CCD_l.
	Proof.
		intros Ha Hb Htype aBb sBG; unfold Apart_at_least.
		destruct Htype as [[iRa iRb] | [iWa iWb]];
		apply FIFO_CAS_date in Ha as [Hc_a [Hrun_a _]];
		apply FIFO_CAS_date in Hb as [Hc_b [Hrun_b _]]; try unfold isCAS;
		try rewrite iRb; try rewrite iRa; try rewrite iWa; try rewrite iWb; try (done || by rewrite orbT);
		apply FIFO_l_eq_bound with (ta := a.(CDate)) (tb := b.(CDate)) (c := Next_cycle OCAS_date) in Hrun_a as H; try done;
		apply leq_trans with (n := a.(CDate) + WAIT); try assumption;
		by rewrite leq_add2l leq_eqVlt CCD_WAIT orbT.
	Qed.

	Theorem Cmds_T_CCD_DBG_ok t a b:
		a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) ->
		(isCRD a /\ isCRD b) \/ (isCWR a /\ isCWR b) -> Before a b -> ~~ Same_Bankgroup a b ->
		Apart_at_least a b T_CCD_s.
	Proof.
		intros Ha Hb Htype aBb sBG; unfold Apart_at_least;
		destruct Htype as [[iRa iRb] | [iWa iWb]];
		apply FIFO_CAS_date in Ha as [Hc_a [Hrun_a _]];
		apply FIFO_CAS_date in Hb as [Hc_b [Hrun_b _]]; try unfold isCAS;
		try rewrite iRb; try rewrite iRa; try rewrite iWa; try rewrite iWb; try (done || by rewrite orbT);
		apply FIFO_l_eq_bound with (ta := a.(CDate)) (tb := b.(CDate)) (c := Next_cycle OCAS_date) in Hrun_a as H; try done;
		apply leq_trans with (n := a.(CDate) + WAIT); try assumption.
		all: rewrite leq_add2l; specialize CCD_WAIT; specialize T_CCD_bgs; lia.
	Qed.

	Theorem Cmds_T_FAW_ok t a b c d:
    a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) -> 
    c \in (# t).(Arbiter_Commands) -> d \in (# t).(Arbiter_Commands) ->
    isACT a -> isACT b -> isACT c -> isACT d -> Diff_Bank [::a;b;c;d] ->
    Before a b -> Before b c -> Before c d ->
    Apart_at_least a d T_FAW.
  Proof.
    intros Ha Hb Hc Hd iAa iAb iAc iAd dB aBb bBc cBd; rewrite /Apart_at_least.
    apply FIFO_date_gt_0 in Ha as Ha_pos, Hb as Hb_pos, Hc as Hc_pos, Hd as Hd_pos.
    apply FIFO_ACT_date in Ha as [Hc_a [Hrun_a _]], Hb as [Hc_b [Hrun_b _]], Hc as [Hc_c [Hrun_c _]], Hd as [Hc_d [Hrun_d _]];
		try assumption; clear iAa iAb iAc iAd.
    apply FIFO_l_eq_bound with (ta := a.(CDate)) (tb := b.(CDate)) (c := Next_cycle OACT_date) in Hrun_a as Hlt_ab; try done.
    apply FIFO_l_eq_bound with (ta := b.(CDate)) (tb := c.(CDate)) (c := Next_cycle OACT_date) in Hrun_b as Hlt_bc; try done.
    apply FIFO_l_eq_bound with (ta := c.(CDate)) (tb := d.(CDate)) (c := Next_cycle OACT_date) in Hrun_c as Hlt_cd; try done.
    apply leq_trans with (p := c.(CDate) - WAIT) in Hlt_ab as Hlt_ac.
      2: rewrite leq_psubRL; (rewrite -addnC; exact Hlt_bc) || exact Hb_pos.
    rewrite leq_psubRL addnC in Hlt_ac.
      2: rewrite addn_gt0; apply /orP; left; exact WAIT_pos.
    apply leq_trans with (p := d.(CDate) - WAIT) in Hlt_ac as Hlt_ad.
      2: rewrite leq_psubRL; (by rewrite -addnC) || exact Hc_pos.
    rewrite leq_psubRL addnC in Hlt_ad.
      2: rewrite addn_gt0; apply /orP; left; exact WAIT_pos.
    apply leq_trans with (n := (a.(CDate) + WAIT + WAIT + WAIT)); [ | assumption ].
    by rewrite -addnA -addnA leq_add2l addnA leq_eqVlt FAW_WAIT orbT.
  Qed.

	Theorem Cmds_T_RRD_SBG_ok t a b: 
    a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) ->
    isACT a -> isACT b -> ~~ Same_Bank a b -> Before a b -> Same_Bankgroup a b ->
    Apart_at_least a b T_RRD_l.
  Proof.
    intros Ha Hb iAa iAb nSb aBb sBG; rewrite /Apart_at_least.
    apply FIFO_ACT_date in Ha as [Hc_a [Hrun_a _]], Hb as [Hc_b [Hrun_b _]]; try assumption; clear iAa iAb.
    apply FIFO_l_eq_bound with (ta := a.(CDate)) (tb := b.(CDate)) (c := Next_cycle OACT_date) in Hrun_a as Hlt; try done.
	  apply leq_trans with (n := (a.(CDate) + WAIT)); [ | assumption ].
    by rewrite leq_add2l leq_eqVlt RRD_WAIT orbT.
  Qed.

  Theorem Cmds_T_RRD_DBG_ok t a b:
    a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) ->
    isACT a -> isACT b -> ~~ Same_Bank a b -> Before a b -> ~~ Same_Bankgroup a b ->
    Apart_at_least a b T_RRD_s.
  Proof.
		intros Ha Hb iAa iAb nSb aBb sBG; rewrite /Apart_at_least.
		apply FIFO_ACT_date in Ha as [Hc_a [Hrun_a _]], Hb as [Hc_b [Hrun_b _]]; try assumption; clear iAa iAb.
		apply FIFO_l_eq_bound with (ta := a.(CDate)) (tb := b.(CDate)) (c := Next_cycle OACT_date) in Hrun_a as Hlt; try done.
		apply leq_trans with (n := (a.(CDate) + WAIT)); [ | assumption ].
		rewrite leq_add2l; specialize RRD_WAIT; specialize T_RRD_bgs; lia.
	Qed.

	(* ------------------------------------------------------------------ *)
  (* -------------------- REQUEST CORRECTNESS ------------------------- *)
  (* ------------------------------------------------------------------ *)

	Lemma FIFO_in_the_past t t' a : t <= t' ->
    a \in (# t).(Arbiter_Commands) -> a \in (# t').(Arbiter_Commands).
  Proof.
    intros; rewrite leq_eqVlt in H; move: H => /orP [/eqP H | H]; [ by rewrite -H | ].
    induction t'; [ inversion H | ].
		rewrite leq_eqVlt in H; move: H => /orP [/eqP H | H].
			{ apply eq_add_S in H.
				simpl; rewrite /Next_state //=.
				destruct (Default_arbitrate t').(Implementation_State), r eqn:HP; simpl.
				all: try (rewrite in_cons; apply /orP; right; subst t; exact H0);
						 try (destruct (nat_of_ord c == ACT_date), (nat_of_ord c == CAS_date), (nat_of_ord c == WAIT.-1); simpl);
				 		 try (rewrite in_cons; apply /orP; right; subst t; exact H0).
			}
			apply ltnSE in H; apply IHt' in H; simpl; rewrite /Next_state //=.
			destruct (Default_arbitrate t').(Implementation_State), r; simpl.
			all: try (rewrite in_cons; apply /orP; right; exact H);
					 try (destruct (nat_of_ord c == ACT_date), (nat_of_ord c == CAS_date), (nat_of_ord c == WAIT.-1); simpl);
					 try (rewrite in_cons; apply /orP; right; exact H).
  Qed.

	Lemma Date_gt_counter t:
    isRUNNING (# t).(Implementation_State) ->
    FIFO_counter (# t).(Implementation_State) < t.
  Proof.
    intros Hrun; induction t; [ done | ].
    simpl in *; unfold Next_state in *.
    destruct (Default_arbitrate t).(Implementation_State) eqn:Hs;
    [ destruct r eqn:Hpp; simpl in *; done | ].
    destruct (nat_of_ord c == ACT_date) eqn:Hact, (nat_of_ord c == CAS_date) eqn:Hcas, (nat_of_ord c == WAIT.-1) eqn:Hwait; simpl in *;
    rewrite Hact //= in Hrun;
    rewrite Hact //=; try (rewrite Hcas //=).
    7: destruct r eqn:Hpp; simpl. 
		all: 
			try done; 
			try (apply IHt in Hrun);
      unfold Next_cycle; set (Hc := c.+1  < WAIT); dependent destruction Hc;
      apply Logic.eq_sym in x; move : Logic.eq_refl; rewrite {2 3} x; simpl; intro; clear e;
      try done; try rewrite Hcas //= in Hrun.
  Qed.

	Lemma Cmd_in_trace cmd t:
    cmd \in (Default_arbitrate t).(Arbiter_Commands) ->
    cmd.(CDate) <= t.
  Proof.
    intros H; induction t; [ by simpl in H | ].
    simpl in H; rewrite /Next_state in H.
    destruct (Default_arbitrate t).(Implementation_State) eqn:Hs.
    { destruct r eqn:Hpp.
      all: simpl; rewrite in_cons in H; move: H => /orP [/eqP H | H].
      all: try (subst cmd; by simpl).
      all: apply IHt in H; by apply ltnW. }
    { destruct (nat_of_ord c == ACT_date) eqn:Hact, (nat_of_ord c == CAS_date) eqn:Hcas, (nat_of_ord c == WAIT.-1) eqn:Hwait; simpl in *.
      all: rewrite Hact //= in H.
      all: try (simpl; rewrite in_cons in H; move: H => /orP [/eqP H | H]).
      all: try (subst cmd; by simpl).
      all: try (apply IHt in H; by apply ltnW).
      all: rewrite Hcas //= in H.
      all: try (simpl; rewrite in_cons in H; move: H => /orP [/eqP H | H]). 
      all: try (subst cmd; by simpl).
      all: try (apply IHt in H; by apply ltnW).
      destruct r; simpl in H.
      all: rewrite in_cons in H; move: H => /orP [/eqP H | H].
      all: try (subst cmd; by simpl).
      all: try (apply IHt in H; by apply ltnW). }
  Qed.

	Lemma Request_starts_idle ra ta:
    let S := (# ta).(Implementation_State) in
    isIDLE S ->
    ra \in FIFO_pending (S) ->
    index ra (FIFO_pending S) = 0 ->
    (FIFO_request (Default_arbitrate ta.+1).(Implementation_State) == Some ra) &&
    (FIFO_counter (Default_arbitrate ta.+1).(Implementation_State) == OCycle0) &&
    (isRUNNING (Default_arbitrate ta.+1).(Implementation_State)).
  Proof.
    intros S Hidle HP Hi.
    unfold S in *; clear S.
    destruct (# ta).(Implementation_State) eqn:Hs; [ | discriminate Hidle ].
    rewrite //= /Next_state Hs.
    destruct r eqn:HPP; [ discriminate HP | ].
    rewrite //= eq_refl !andbT.
    rewrite /FIFO_pending in Hi.
    destruct (r0 == ra) eqn:Heq; [ move: Heq => /eqP Heq; by rewrite Heq | ].
    by rewrite //= Heq in Hi.
  Qed.

	Lemma Request_running_in_slot ra ta d:
    FIFO_request (# ta).(Implementation_State) == Some ra ->
    FIFO_counter (# ta).(Implementation_State) == OCycle0 ->
    d < WAIT ->
    (FIFO_request # (ta + d).(Implementation_State) == Some ra).
  Proof.
    intros Hreq Hc Hd; induction d; [ by rewrite addn0 Hreq | ].
    apply ltn_trans with (m := d) in Hd as Hd'; [ | done ].
    apply IHd in Hd' as IH. clear IHd.
    assert (isRUNNING (# ta).(Implementation_State)) as Hrun.
    { destruct (Default_arbitrate ta).(Implementation_State); [ | done ].
			rewrite /FIFO_request //= in Hreq.
    }
    destruct (Default_arbitrate (ta + d)).(Implementation_State) eqn:Hs;
    [ rewrite /FIFO_request //= in IH | ].
    apply FIFO_add_counter with (c := OCycle0) (d := d) in Hrun as H. destruct H as [Hrun' Hc'].
      3: apply /eqP; exact Hc.
      2: rewrite /OCycle0 //= add0n.
    rewrite /OCycle0 //= add0n in Hc'.
    rewrite addnS; simpl; rewrite /Next_state Hs //=.
    destruct (nat_of_ord c == ACT_date) eqn:Hact, (nat_of_ord c == CAS_date) eqn:Hcas, (nat_of_ord c == WAIT.-1) eqn:Hwait; simpl;
    try by rewrite /FIFO_request in IH;
    destruct r eqn:HPP; simpl.
  	rewrite Hs /FIFO_counter in Hc'; move: Hwait => /eqP Hwait; rewrite Hc' in Hwait; rewrite Hwait prednK in Hd;
    try exact WAIT_pos; by rewrite ltnn in Hd.
  Qed.

	Lemma Request_starts_running ra ta:
		let S := (Default_arbitrate ta).(Implementation_State) in
		isRUNNING (S) ->
		FIFO_counter (S) == WAIT.-1 ->
		ra \in FIFO_pending (S) ->
		index ra (FIFO_pending S) = 0 ->
		(FIFO_request (Default_arbitrate ta.+1).(Implementation_State) == Some ra) &&
		(FIFO_counter (Default_arbitrate ta.+1).(Implementation_State) == OCycle0) &&
		(isRUNNING (Default_arbitrate ta.+1).(Implementation_State)).
	Proof.
		intros S Hrun Hc HP Hi.
		unfold S in *; clear S.
		destruct (Default_arbitrate ta).(Implementation_State) eqn:Hs.
		1: discriminate Hrun.
		simpl; rewrite /Next_state Hs.
		destruct (nat_of_ord c == OACT_date) eqn:Hact, (nat_of_ord c == OCAS_date) eqn:Hcas, (nat_of_ord c == WAIT.-1) eqn:Hwait; simpl.
		all: rewrite /FIFO_counter in Hc.
		all: try by rewrite Hc in Hwait.
		4: destruct r eqn:HPP; simpl.
		4: { discriminate HP. }
		4: {
			destruct (r1 == ra) eqn:Heq.
			{ move: Heq => /eqP Heq. by rewrite Heq eq_refl. }
			{ simpl in Hi. rewrite Heq in Hi. discriminate Hi. }
		}
		1,2: move: Hc => /eqP Hc; rewrite Hc in Hact; by rewrite FIFO_wait_neq_act in Hact.
		move: Hc => /eqP Hc; rewrite Hc in Hcas; by rewrite FIFO_wait_neq_cas in Hcas.
	Qed.

  Lemma Request_PRE_bounded t ra:
    isRUNNING (# t).(Implementation_State) ->
    FIFO_request (# t).(Implementation_State) == Some ra ->
    (PRE_of_req ra (Slot_start t)) \in (Default_arbitrate (Slot_start t)).(Arbiter_Commands).
  Proof.
    intros Hrun Hreq.
  	induction t; [ by simpl in Hrun | ].
    rewrite /Slot_start; simpl in *; unfold Next_state in *; simpl in *.
		destruct (Default_arbitrate t).(Implementation_State) eqn:Hs.
    { destruct r eqn:HPP; simpl in *; [ by simpl in Hrun | ].
      clear Hrun IHt.
      rewrite /Next_state Hs //= subn0 in_cons /PRE_of_req.
			rewrite inj_eq in Hreq ; [ | exact ssrfun.Some_inj ]. 
			move: Hreq => /eqP Hreq; by rewrite Hreq eq_refl orTb. 
		}
    { destruct (nat_of_ord c == ACT_date) eqn:Hact, (nat_of_ord c == CAS_date) eqn:Hcas, (nat_of_ord c == WAIT.-1) eqn:Hwait; simpl in *.
      7: destruct r eqn:HPP.
      7: by simpl in Hrun.
      7: { 
        simpl; rewrite /Next_state Hs Hact Hcas Hwait //=.
        rewrite in_cons subn0; apply /orP; left. 
        simpl in Hreq; rewrite /PRE_of_req.
        rewrite inj_eq in Hreq; [ | exact ssrfun.Some_inj ].
        move: Hreq => /eqP Hreq; by rewrite Hreq.
      }
      all:
				apply IHt in Hreq as IH;
      	try trivial; clear IHt;
      	rewrite /Slot_start Hs in IH;
      	try (rewrite {1}/Next_cycle; set (Hc := c.+1 < WAIT); dependent destruction Hc);
      	apply Logic.eq_sym in x; move : Logic.eq_refl; rewrite {2 3} x; simpl; intro; clear e;
      	try (rewrite {1}/Next_cycle; set (Hc := c.+1 < WAIT); dependent destruction Hc);
      	apply Logic.eq_sym in x; move : Logic.eq_refl; rewrite {2 3} x; simpl; intro; clear e;
      	try (rewrite subSS; by exact IH);
      	try (contradict x; by apply Bool.not_true_iff_false);
      	try (contradict x0; by apply Bool.not_true_iff_false);
      	try (move: Hact => /eqP Hact; by rewrite Hact FIFO_WAIT_GT_SACT in x);
      	try (move: Hcas => /eqP Hcas; by rewrite Hcas FIFO_WAIT_GT_SCAS in x).
      clear Hc0 Hc1 x0.
      rewrite ltnNge in x; apply negbFE in x.
      rewrite leq_eqVlt in x; move: x => /orP [x | x]; [ lia | ].
      destruct c; simpl in *; contradict x; lia.
		}
	Qed.
  
  Lemma Request_ACT_bounded t ra:
    isRUNNING (Default_arbitrate t).(Implementation_State) ->
    FIFO_request (Default_arbitrate t).(Implementation_State) == Some ra ->
    let tact := (Slot_start t + Next_cycle OACT_date) in
    (ACT_of_req ra tact) \in (Default_arbitrate tact).(Arbiter_Commands).
  Proof.
    intros Hrun Hreq; simpl.
    induction t.
      { by simpl in Hrun. }
    rewrite /Slot_start; simpl in *; unfold Next_state in *; simpl in *.
    destruct (Default_arbitrate t).(Implementation_State) eqn:Hs.
    { destruct r eqn:HPP; simpl in *.
      { done. }
      { clear Hrun IHt.
        rewrite /Next_state //=.
        assert (isIDLE (Default_arbitrate t).(Implementation_State)) as Hidle.
          { by rewrite Hs. }
        apply Request_starts_idle with (ra := r0) in Hidle.
          3,2: rewrite Hs /FIFO_pending; (by apply index_head || by apply mem_head).
        rename Hidle into H; move: H => /andP [/andP [Hreq' Hc'] Hrun']; move: Hc' => /eqP Hc'.      
        apply FIFO_add_counter with (d := ACT_date) in Hc' as H. destruct H as [Hrun'' Hc''].
          3: rewrite /OCycle0 add0n //=; specialize FIFO_WAIT_GT_SACT; by apply ltnW.
          2: done.
        apply Request_running_in_slot with (d := ACT_date) in Hreq' as Hreq''.
          3: specialize FIFO_WAIT_GT_SACT; by apply ltnW.
          2: by apply /eqP.
        rewrite -addn1 -addnA [1 + _]addnC addn1 in Hc'',Hrun'', Hreq''.
        rewrite FIFO_nextcycle_act_eq_actS.
        destruct (Default_arbitrate (t + ACT_date.+1)).(Implementation_State) eqn:Hs_act.
          { by simpl in Hrun''. }
        clear Hrun''; rewrite /FIFO_counter /OCycle0 add0n in Hc''.
        assert ((ACT_date == ACT_date) = true).
          { by rewrite eq_refl. }
        rewrite Hc'' H //= in_cons /ACT_of_req subn0.
        rewrite -addn1 -addnA [1 + _]addnC addn1 addnS.   
        rewrite /FIFO_request inj_eq in Hreq''.
          2: exact ssrfun.Some_inj.
        move: Hreq'' => /eqP Hreq''; rewrite -Hreq'' in Hreq.
        rewrite inj_eq in Hreq. move: Hreq => /eqP Hreq.
          2: exact ssrfun.Some_inj.
        by rewrite Hreq eq_refl orTb. }}
    { destruct (nat_of_ord c == ACT_date) eqn:Hact, (nat_of_ord c == CAS_date) eqn:Hcas, (nat_of_ord c == WAIT.-1) eqn:Hwait; simpl in *.
        all: rewrite FIFO_nextcycle_act_eq_actS.
        7: destruct r eqn:HPP; simpl.
        7: discriminate Hrun.
        7: {
          simpl in *; clear IHt Hrun; rewrite /Next_state.
          assert (isRUNNING (Default_arbitrate t).(Implementation_State)).
            { by rewrite Hs. }
          rewrite inj_eq in Hreq. move: Hreq => /eqP Hreq.
            2: exact ssrfun.Some_inj.     
          apply Request_starts_running with (ra := ra) in H. move: H => /andP [/andP [Hreq' Hc']Hrun'].
            4: rewrite Hs /FIFO_pending Hreq; apply index_head.
            3: rewrite /FIFO_pending Hs Hreq; apply mem_head.
            2: move: Hwait => /eqP Hwait; by rewrite /FIFO_counter Hs Hwait eq_refl.
          move: Hc' => /eqP Hc'. 
          apply FIFO_add_counter with (d := ACT_date) in Hc' as H.
            3: rewrite /OCycle0 add0n //=; specialize FIFO_WAIT_GT_SACT; by apply ltnW.
            2: done.
          apply Request_running_in_slot with (d := ACT_date) in Hreq' as Hreq''.
            3: specialize FIFO_WAIT_GT_SACT; by apply ltnW.
            2: by apply /eqP.     
          destruct H as [Hrun'' Hc''].
          rewrite -addn1 -addnA [1 + _]addnC addn1 in Hc'',Hrun'', Hreq''.
          destruct (Default_arbitrate (t + ACT_date.+1)).(Implementation_State) eqn:Hs_act.
            { by simpl in Hrun''. }
          clear Hrun''; rewrite /FIFO_counter /OCycle0 add0n in Hc''.
          assert ((ACT_date == ACT_date) = true).
            { by rewrite eq_refl. }
          rewrite Hc'' H //= in_cons /ACT_of_req subn0.
          rewrite -addn1 -addnA [1 + _]addnC addn1 addnS.   
          rewrite /FIFO_request inj_eq in Hreq''.
            2: exact ssrfun.Some_inj.
          move: Hreq'' => /eqP Hreq''; by rewrite -Hreq'' eq_refl orTb.
        }
        all: rewrite /Slot_start Hs in IHt.
        all: try (rewrite {1}/Next_cycle; set (Hc := c.+1 < WAIT); dependent destruction Hc).
        all: try (apply Logic.eq_sym in x; move : Logic.eq_refl; rewrite {2 3} x; simpl; intro; clear e).
        all: try (apply IHt in Hreq); try done.
        all: try (rewrite {1}/Next_cycle; set (Hc := c.+1 < WAIT); dependent destruction Hc).
        all: try (apply Logic.eq_sym in x; move : Logic.eq_refl; rewrite {2 3} x; simpl; intro; clear e).
        all: try (contradict x; by apply Bool.not_true_iff_false).
        all: try (contradict x0; by apply Bool.not_true_iff_false); try (clear x0 Hc0 Hc1).
        all: assert (t.+1 - c.+1 + ACT_date.+1 = t - c + Next_cycle OACT_date) as H.
        all: try (rewrite -FIFO_nextcycle_act_eq_actS -addn1 -[c.+1]addn1 subnDA -subnAC subn1 addn1 -pred_Sn; reflexivity).
        all: try (by rewrite H); clear H.
        all: try (move: Hact => /eqP Hact; rewrite Hact in x; contradict x; 
                  apply Bool.not_false_iff_true; by rewrite FIFO_WAIT_GT_SACT).
        all: try (move: Hcas => /eqP Hcas; rewrite Hcas in x; contradict x; 
                  apply Bool.not_false_iff_true; by rewrite FIFO_WAIT_GT_SCAS).
        clear Hrun IHt.
        rewrite ltnNge in x; apply negbFE in x.
        rewrite leq_eqVlt in x; move: x => /orP [x | x].
        { move: Hwait => /eqP Hwait; contradict Hwait. apply eq_add_S; rewrite prednK.
            2: exact WAIT_pos.
          move: x => /eqP x.
          by apply Logic.eq_sym. }
        { destruct c; simpl in *; contradict x.
          apply /negP.
          by rewrite -leqNgt. } }
  Qed. 

  Theorem Cmds_ACT_ok t a b: 
    a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) ->
    isACT a \/ isCAS a -> isACT b -> Same_Bank a b -> Before a b ->
    exists c, (c \in (Default_arbitrate t).(Arbiter_Commands)) && isPRE c && Same_Bank b c && Same_Bank a c && After c a && Before c b.
  Proof.
    intros Ha Hb iA iAb SB aBb.
    apply FIFO_ACT_date in Hb as H. destruct H as [Hcb [Hrun_b Hreq_b]].
      all: try done.
    destruct (Default_arbitrate b.(CDate)).(Implementation_State) eqn:Hsb.
      1: by simpl in Hrun_b.
    rewrite -Hsb in Hreq_b.
		unfold get_req in Hreq_b; destruct b.(CKind) eqn:H_kind;
		try (by unfold isACT in iAb; rewrite H_kind in iAb).
    apply Request_PRE_bounded in Hreq_b.
      2: by rewrite -Hsb in Hrun_b.
    rewrite /Slot_start Hsb in Hreq_b.
		exists (PRE_of_req r1 (b.(CDate) - c)).
    rewrite /Same_Bank in SB; move: SB => /eqP SB.
    rewrite /isPRE /Same_Bank //= andbT SB /PRE_of_req /get_bank H_kind //= eq_refl !andbT.
		rewrite /Before /After.
    (* Solving Before c b*)
    apply /andP; split.
    2: { 
      rewrite /FIFO_counter in Hcb; rewrite Hcb.
      rewrite ltn_subLR.
        2: { rewrite -Hsb in Hrun_b; apply Date_gt_counter in Hrun_b; rewrite -Hcb. 
          rewrite /FIFO_counter Hsb in Hrun_b. by rewrite leq_eqVlt Hrun_b orbT.
        }
      rewrite addnC.
      apply nat_ltn_add; by rewrite FIFO_nextcycle_act_eq_actS.
    }

    (* Solving ~~ Before_at a c*)
    apply /andP; split.
    2:{ destruct iA as [iAa | iCa].
        { apply FIFO_ACT_date in Ha as H. destruct H as [Hca [Hrun_a Hreq_a]].
            2: done.
          rewrite -Hsb in Hcb.
          specialize FIFO_l_eq_bound with (ta := a.(CDate)) (tb := b.(CDate)) (c := Next_cycle OACT_date) as HH; apply HH in Hrun_a as Hlt.
            all: try (done || by rewrite -Hsb in Hrun_b).
          clear HH. rewrite leq_eqVlt in Hlt; move: Hlt => /orP [/eqP H0 | H1].
            { rewrite -H0 -addnBA.
                2: by destruct c; simpl; rewrite leq_eqVlt i orbT.
              apply nat_ltn_add; rewrite subn_gt0; destruct c; simpl; exact i. }
            { apply ltn_trans with (n := b.(CDate) - WAIT).
                2: { 
                  apply ltn_sub2l.
                    2: destruct c; simpl; exact i.
                  by rewrite -Hsb in Hrun_b; apply Date_gt_counter in Hrun_b; rewrite /FIFO_counter Hsb in Hrun_b.
                }
              rewrite ltn_subRL addnC; exact H1.
            }
        }
        { apply FIFO_CAS_date in Ha as H. destruct H as [Hca [Hrun_a Hreq_a]].
            2: done.
          rewrite -Hsb in Hcb.
          apply FIFO_add_counter with (d := Next_cycle OCAS_date - Next_cycle OACT_date) in Hcb as H.
            3: { rewrite subnKC. by rewrite FIFO_next_cycle_cas_eq_casS FIFO_WAIT_GT_SCAS.
                 rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS.
                 rewrite /CAS_date /ACT_date -addnS. by apply nat_ltn_add. }
            2: by rewrite -Hsb in Hrun_b.
          destruct H as [Hrun_b' Hcb'].
          set (d := Next_cycle OCAS_date - Next_cycle OACT_date); fold d in Hrun_b',Hcb'.
          assert (Next_cycle OACT_date + d = Next_cycle OCAS_date).
            { unfold d; rewrite subnKC. reflexivity.
              rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS /CAS_date /ACT_date -addnS.
              by apply nat_ltn_add. }
          rewrite H in Hcb'.
          specialize FIFO_l_eq_bound with (ta := a.(CDate)) (tb := b.(CDate) + d) (c := Next_cycle OCAS_date) as HH.
          apply HH in Hrun_a as Hlt; clear HH; try done.
          	2 : { 
							unfold Before in aBb; apply ltn_trans with (n := b.(CDate)).
							exact aBb. apply nat_ltn_add. unfold d.
							rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS /CAS_date /ACT_date prednK.
								2: by rewrite lt0n T_RP_pos.
							rewrite subSn; [ trivial | specialize T_RCD_pos; lia ].
						}
          assert ((a.(CDate) < b.(CDate) - c) == (a.(CDate) + WAIT < b.(CDate) - c + WAIT));
            [ apply /eqP; by rewrite ltn_add2r | ].
          move: H0 => /eqP H0; rewrite H0; clear H0.
          rewrite leq_eqVlt in Hlt; move: Hlt => /orP [/eqP Hlt | Hlt].
          { rewrite Hlt addnBAC.
              2: { rewrite -Hsb in Hrun_b. apply Date_gt_counter in Hrun_b.  
                   rewrite /FIFO_counter Hsb in Hrun_b; by rewrite leq_eqVlt Hrun_b orbT.
              }
            rewrite -addnBA.
              2: by destruct c; simpl; rewrite leq_eqVlt i orbT.
            rewrite ltn_add2l ltn_subRL.
            rewrite /FIFO_counter Hsb in Hcb; rewrite Hcb; unfold d.
            rewrite subnKC.
              2: { rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS.
                   rewrite /CAS_date /ACT_date -addnS. apply nat_ltnn_add. by specialize T_RCD_GT_ONE. 
              }
            by rewrite FIFO_next_cycle_cas_eq_casS FIFO_WAIT_GT_SCAS. }
          { apply ltn_trans with (n := b.(CDate) + d). exact Hlt.
            rewrite addnBAC.
            2: { rewrite -Hsb in Hrun_b. apply Date_gt_counter in Hrun_b.  
                 rewrite /FIFO_counter Hsb in Hrun_b; by rewrite leq_eqVlt Hrun_b orbT.
            }
            rewrite -addnBA.
              2: by destruct c; simpl; rewrite leq_eqVlt i orbT.
            rewrite ltn_add2l ltn_subRL.
            rewrite /FIFO_counter Hsb in Hcb; rewrite Hcb; unfold d.
            rewrite subnKC.
              2: { rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS.
                   rewrite /CAS_date /ACT_date -addnS. apply nat_ltnn_add. by specialize T_RCD_GT_ONE. 
              }
            by rewrite FIFO_next_cycle_cas_eq_casS FIFO_WAIT_GT_SCAS. }
        }
    }
    apply FIFO_in_the_past with (t := b.(CDate)); [ by apply Cmd_in_trace | ].
    apply FIFO_in_the_past with (t := b.(CDate) - c); [ by apply leq_subr | assumption ].
  Qed.

	Lemma Request_exists b c0 r r0 :
		isCAS b ->
		# (b.(CDate)).(Implementation_State) = RUNNING c0 r r0 ->
		FIFO_request # (b.(CDate)).(Implementation_State) == get_req b ->
		FIFO_request # (b.(CDate)).(Implementation_State) == Some r0.
	Proof.
		intros iCb H Hreq_b.
		unfold get_req, FIFO_request in *; rewrite H in Hreq_b.
		destruct b.(CKind) eqn:Hkind;
		try (by unfold isCAS,isCRD,isCWR in iCb; rewrite Hkind in iCb);
		by rewrite H eq_refl.
	Qed.
		
  Theorem Cmds_row_ok t b c: 
    b \in (# t).(Arbiter_Commands) -> c \in (# t).(Arbiter_Commands) ->
    isCAS b -> isPRE c -> Same_Bank b c -> Before c b -> 
    exists a, (a \in (Default_arbitrate t).(Arbiter_Commands))
    && isACT a && Same_Row a b && After a c && Before a b.
  Proof.
    intros Hb Hc iCb iPc SB cBb.

    apply FIFO_CAS_date in Hb as H; [ | done ]. 
		destruct H as [Hcb [Hrun_b Hreq_b]].

    destruct # (b.(CDate)).(Implementation_State) eqn:Hsb; [ done | ].

		apply Request_exists in Hsb as H; try assumption; [ | by rewrite <- Hsb in Hreq_b].

    rewrite -Hsb in Hreq_b.

		apply Request_ACT_bounded in H as H'; try (by rewrite -Hsb in Hrun_b);
    rewrite /Slot_start Hsb in H'.
    
		exists (ACT_of_req r0 (b.(CDate) - c0 + Next_cycle OACT_date)).
		
    rewrite /Same_Bank in SB; move: SB => /eqP SB;
		rewrite /isACT //= andbT /Before /After //= /Same_Row /get_row /ACT_of_req //=.
		apply /andP; split; [ apply /andP; split; [ apply /andP; split | ] | ].
		2: {
			unfold FIFO_request, get_req in *.
			destruct b.(CKind) eqn:Hkind;
			try (by unfold isCAS,isCRD,isCWR in iCb; rewrite Hkind in iCb);
			rewrite Hsb in H,Hreq_b; rewrite inj_eq; try (exact ssrfun.Some_inj);
			rewrite inj_eq in Hreq_b; try (exact ssrfun.Some_inj); move: Hreq_b => /eqP Hreq_b;
			subst r0; by rewrite eq_refl.
		} 

		3: {
      rewrite /FIFO_counter in Hcb; rewrite Hcb FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS;
      rewrite -Hsb in Hrun_b; 
			rewrite -subnA.
			3 : {
				clear H.
				apply Date_gt_counter in Hrun_b as H; rewrite Hsb /FIFO_counter Hcb FIFO_next_cycle_cas_eq_casS in H;
        apply ltn_trans with (m := CAS_date) in H; [ exact H | done ].
			}
			2 : {
    		rewrite /CAS_date /ACT_date -addnS; by apply nat_ltn_add.
			}
			rewrite ltn_subrL;
    	apply FIFO_date_gt_0 in Hb; rewrite subn_gt0 Hb andbT -addn1 -[CAS_date.+1]addn1 leq_add2r;
    	rewrite /CAS_date /ACT_date; apply nat_ltn_add; by rewrite lt0n T_RCD_pos.
		}

    2: {
			clear H.
      apply FIFO_PRE_date in Hc as H; [ | done ]; destruct H as [Hcc [Hrun_c Hreq_c]].
      apply FIFO_add_counter with (d := Next_cycle OCAS_date) in Hcc as H. destruct H as [Hrun_c' Hcc'].
        3: by rewrite /OCycle0 add0n FIFO_next_cycle_cas_eq_casS FIFO_WAIT_GT_SCAS.
        2: done.
      set (d := Next_cycle OCAS_date).
      specialize FIFO_l_eq_bound_or with (ta := c.(CDate) + d) (tb := b.(CDate)) (c := d) as HH.
      apply HH in Hrun_c' as Hlt. clear HH.
        4: unfold d; by rewrite Hsb.
        3: by rewrite Hsb.
        2: by rewrite /OCycle0 add0n in Hcc'; fold d in Hcc'.
      rewrite /FIFO_counter in Hcb; rewrite Hcb.
      destruct Hlt as [H0 | [H1 | H2]]; fold d.
      { rewrite -H0 addnBAC; [ | by rewrite addnC leq_addr ].
        rewrite ltn_subRL addnC -addnA ltn_add2l.
        apply nat_ltn_add; by rewrite FIFO_nextcycle_act_eq_actS. }
      { rewrite leq_eqVlt in H1; move: H1 => /orP [/eqP H1 | H1].
          { rewrite -H1 -addnA -subnA.
              3: rewrite -addnCA; by apply leq_addr.
              2: { unfold d; rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS. 
                   rewrite /CAS_date -addnS; by apply nat_ltn_add. }
            rewrite ltn_subRL addnC ltn_add2l ltn_subLR.
              2: { unfold d; rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS. 
                   rewrite /CAS_date -addnS; by apply nat_ltn_add. }
            rewrite -addnCA; apply nat_ltn_add; by rewrite addn_gt0 WAIT_pos orbT.
          }
        rewrite -subnA.
          3: { rewrite -Hsb in Hrun_b; apply Date_gt_counter in Hrun_b. 
               rewrite /FIFO_counter Hsb Hcb in Hrun_b; fold d in Hrun_b. 
               by rewrite leq_eqVlt Hrun_b orbT. }
          2: { unfold d; rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS. 
               rewrite /CAS_date -addnS; by apply nat_ltn_add. }
        rewrite ltn_subRL addnC.
        apply ltn_trans with (m := c.(CDate) + (d - Next_cycle OACT_date)) in H1. exact H1.
        rewrite -addnA ltn_add2l ltn_subLR.
          2: { unfold d; rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS. 
               rewrite /CAS_date -addnS; by apply nat_ltn_add. }
        rewrite -addnCA; apply nat_ltn_add; by rewrite addn_gt0 WAIT_pos orbT. }
      { contradict H2; apply /negP; rewrite -ltnNge.
        unfold Before in cBb.
        rewrite addnC -ltn_subRL.
        apply ltn_trans with (n := b.(CDate)). exact cBb.
        rewrite ltn_subRL addnC ltn_add2l; unfold d; rewrite FIFO_next_cycle_cas_eq_casS.
        exact FIFO_WAIT_GT_SCAS. } 
		}
    apply FIFO_in_the_past with (t := b.(CDate)); [ by apply Cmd_in_trace | ].
    apply FIFO_in_the_past with (t := b.(CDate) - c0 + Next_cycle OACT_date).
      { rewrite -subnA.
          3: { rewrite -Hsb in Hrun_b; apply Date_gt_counter in Hrun_b. 
               rewrite /FIFO_counter Hsb in Hrun_b; by rewrite leq_eqVlt Hrun_b orbT. }
          2: { rewrite /FIFO_counter in Hcb; rewrite Hcb; 
               rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS; 
               rewrite /CAS_date -addnS; by apply nat_ltn_add. }
          apply leq_subr. }
    exact H'.
  Qed.

	Theorem Cmds_initial t b: 
    b \in (# t).(Arbiter_Commands) -> isCAS b ->
    exists a, (a \in (# t).(Arbiter_Commands)) && (isACT a) && (Same_Row a b) && (Before a b).
  Proof.
    intros Hb iCb.
    apply FIFO_CAS_date in Hb as H; [ | done ]. destruct H as [Hcb [Hrun_b Hreq_b]].
    destruct # (b.(CDate)).(Implementation_State) eqn:Hsb; [ done | ].
   
		apply Request_exists in Hsb as H; try assumption; [ | by rewrite <- Hsb in Hreq_b].
    rewrite -Hsb in Hreq_b.

		apply Request_ACT_bounded in H as H'; try (by rewrite -Hsb in Hrun_b);
    rewrite /Slot_start Hsb in H'.

    exists (ACT_of_req r0 (b.(CDate) - c + Next_cycle OACT_date)).
		rewrite /isACT //= andbT /Before /After //= /Same_Row /get_row /ACT_of_req //=.
		apply /andP; split; [ apply /andP; split | ].
		2: {
			unfold FIFO_request, get_req in *.
			destruct b.(CKind) eqn:Hkind;
			try (by unfold isCAS,isCRD,isCWR in iCb; rewrite Hkind in iCb);
			rewrite Hsb in H,Hreq_b; rewrite inj_eq; try (exact ssrfun.Some_inj);
			rewrite inj_eq in Hreq_b; try (exact ssrfun.Some_inj); move: Hreq_b => /eqP Hreq_b;
			subst r0; by rewrite eq_refl.
		} 
		2: {
      rewrite /FIFO_counter in Hcb; rewrite Hcb FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS.
      rewrite -Hsb in Hrun_b; rewrite -subnA.
        3: { clear H.
          apply Date_gt_counter in Hrun_b as H; rewrite Hsb /FIFO_counter Hcb FIFO_next_cycle_cas_eq_casS in H.
          apply ltn_trans with (m := CAS_date) in H. exact H. done. 
				}
        2: rewrite /CAS_date /ACT_date -addnS; by apply nat_ltn_add.
      rewrite ltn_subrL.
      apply FIFO_date_gt_0 in Hb; rewrite subn_gt0 Hb andbT -addn1 -[CAS_date.+1]addn1 leq_add2r.
      rewrite /CAS_date /ACT_date; apply nat_ltn_add; by rewrite lt0n T_RCD_pos.
    }
    apply (FIFO_in_the_past b.(CDate)); [ by apply Cmd_in_trace | ].
    apply (FIFO_in_the_past (b.(CDate) - c + Next_cycle OACT_date)); [ | assumption ].
  	rewrite -subnA; [ apply leq_subr | |].
			2: { 
				rewrite -Hsb in Hrun_b; apply Date_gt_counter in Hrun_b. 
				rewrite /FIFO_counter Hsb in Hrun_b; by rewrite leq_eqVlt Hrun_b orbT. 
			}	 
		rewrite /FIFO_counter in Hcb; rewrite Hcb; 
		rewrite FIFO_next_cycle_cas_eq_casS FIFO_nextcycle_act_eq_actS; 
		rewrite /CAS_date -addnS; by apply nat_ltn_add. 
	Qed.

	(* ------------------------------------------------------------------ *)
  (* -------------------- REQUEST PROOFS ------------------------------ *)
  (* ------------------------------------------------------------------ *)

	Lemma FIFO_l_counter_border_run t :
    FIFO_pending (# t).(Implementation_State) != [::] ->
    FIFO_counter (# t).(Implementation_State) = WAIT.-1 -> 
    FIFO_counter # (t.+1).(Implementation_State) = 0 /\ isRUNNING  # (t.+1).(Implementation_State).
  Proof.
    intros HP Hc; simpl.
    rewrite /Next_state //=; destruct_state t; simpl; try done;
		destruct c; simpl in *; unfold Next_cycle;  simpl; set (H := m.+1 < WAIT); dependent destruction H;
		apply Logic.eq_sym in x; move : Logic.eq_refl; rewrite {2 3} x; simpl; intro; clear e; try done;
		rewrite Hc prednK in x; lia.
	Qed.

  Lemma Pending_on_arrival ta ra:
    ra \in Arrival_at ta  -> ra \in (FIFO_pending # (ta).(Implementation_State)).
  Proof.
    intros HA; destruct ta; [ by rewrite /FIFO_pending //= /Enqueue mem_cat HA orTb | ].
    rewrite /FIFO_pending //= /Next_state.
    destruct_state ta; simpl; try assumption; rewrite /Enqueue;
		try (by rewrite eq_refl mem_cat HA orbT);
		try (by rewrite in_cons mem_cat HA !orbT).
	Qed.

	Lemma Pending_requestor ta ra:
    ra \in FIFO_pending # ta.(Implementation_State)
    -> isRUNNING # ta.(Implementation_State)
    -> FIFO_counter # ta.(Implementation_State) != WAIT.-1
    -> ra \in FIFO_pending # (ta.+1).(Implementation_State).
  Proof.
    intros HP Hrun HCS.
    simpl; unfold Next_state, FIFO_pending, isRUNNING in *.
    destruct (Default_arbitrate ta).(Implementation_State); simpl; try done.
    destruct c; simpl. destruct (m == ACT_date) eqn:Hact, (m == CAS_date) eqn:Hcas, (m == WAIT.-1) eqn:Hwait, r eqn:HPP; 
		simpl; try discriminate HP;
    try (by rewrite /Enqueue -cat_cons mem_cat HP orTb).
    move: Hwait => /eqP Hwait; simpl in HCS.
    contradict Hwait; by apply /eqP.
	Qed.

	Lemma Running_on_next_cycle ra ta:
    ra \in (FIFO_pending # ta.(Implementation_State)) ->
    isIDLE # ta.(Implementation_State) ->
    (isRUNNING # (ta.+1).(Implementation_State)) && (FIFO_counter # (ta.+1).(Implementation_State) == OCycle0).
  Proof.
    intros HP Hidle.
    destruct (Default_arbitrate ta).(Implementation_State) eqn:Hs; [ | done ]; clear Hidle.
    rewrite /FIFO_pending in HP.
    rewrite //= /Next_state Hs.
    destruct r eqn:HP'; [ discriminate HP | by simpl ].
  Qed.

	(* Index in the pending queue decrements by one, because a request has been select @S *)
	Lemma Request_index_decrements_within_period_idle ta ra:
  	let S := # ta.(Implementation_State) in (ra \in (FIFO_pending S))
    -> isIDLE S
    -> (0 < index ra (FIFO_pending S))
    -> forall tb, ta < tb
      -> tb <= ta + WAIT
      -> let S' := (# tb).(Implementation_State) in
        (ra \in (FIFO_pending S')) && ((index ra (FIFO_pending S)) == (index ra (FIFO_pending S')).+1).
  Proof.
    intros S HP Hidle HI.
    unfold S in *; clear S.
    destruct (# ta.(Implementation_State)) eqn:HSS; simpl; [ | discriminate Hidle ].
    induction tb; intros HL HU; [ contradict HL; by rewrite ltn0 | ].
    rewrite leq_eqVlt in HL; move: HL => /orP [HL | HL].
		{ clear IHtb.
			rewrite eqSS in HL; move: HL => /eqP HL; subst.
			simpl; unfold FIFO_counter, FIFO_pending, Next_state in *.
			rewrite HSS; destruct r eqn:HPP; simpl; [ discriminate HP | ].
			rewrite eq_refl.
			simpl in HI; destruct (r0 == ra) eqn:Heq; [ discriminate HI | ].
			rewrite in_cons in HP; move: HP => /orP [/eqP HP | HP];
			[ contradict Heq; rewrite HP; apply /eqP; by rewrite eq_refl | ].
			by rewrite /Enqueue mem_cat HP orTb index_cat HP; apply /eqP; reflexivity.
		}
    rewrite ltnS in HL; apply ltnW in HU as HU'.
    apply IHtb in HL as IH; clear IHtb; [ | assumption ].
    move: IH => /andP [HP' /eqP IH'].
    rewrite -HSS in HP; apply Running_on_next_cycle in HP as HC; [ | by rewrite HSS ]. 
		move: HC => /andP [Hrun /eqP Hc].
    apply FIFO_add_counter with (c := OCycle0) (d := tb - (ta.+1)) in Hrun as Haux; try assumption.
		2: { 
			rewrite /OCycle0 //= add0n ltn_subLR; [ | assumption ].
			apply ltn_trans with (n := ta + WAIT); [ assumption | ].
			by rewrite ltn_add2r.
		}
		destruct Haux as [Hrun_a' Hca'].
    assert (ta.+1 + (tb - ta.+1) = tb); [ rewrite subnKC; [ reflexivity | exact HL ] | ].
    rewrite H in Hrun_a', Hca'; clear H.
    rewrite /OCycle0 //= add0n in Hca'.
    apply Pending_requestor with (ra := ra) in Hrun_a' as Hpen; try assumption.
    2: {
      rewrite Hca' neq_ltn; apply /orP; left.
			rewrite ltn_subLR; [ | assumption ].
			rewrite addnC -subn1 addnBAC; [ lia | exact WAIT_pos ].
		}
    rewrite Hpen andTb IH'; clear HP HI Hpen;
		rename Hrun_a' into Hrun_b, Hca' into Hcb.
    rewrite eqSS //= /FIFO_pending /Next_state.
    destruct (Default_arbitrate tb).(Implementation_State) eqn:HS; [ discriminate Hrun_b | ].
    destruct c0; simpl.
    destruct (m == ACT_date) eqn:Hact, (m == CAS_date) eqn:Hcas, (m == WAIT.-1) eqn:Hwait; simpl;
    try (by rewrite /Enqueue index_cat HP').
    simpl in Hrun_b, Hcb, IH',HP', HS. 
    assert (Heq : FIFO_counter # tb.(Implementation_State) = WAIT.-1);
		[ rewrite HS //=; move: Hwait => /eqP Hwait; exact Hwait | ].
    apply FIFO_l_counter_border_run in Heq as [Hcb' Hrun_b'];
		[ | rewrite HS //=; by apply Queue_non_empty in HP' ].
    apply FIFO_l_eq_bound with (tb := tb.+1) (c := OCycle0) in Hrun as Hbug; try assumption.
    contradict Hbug; lia.
    (* apply /negP; rewrite leqNgt; apply /negPn. lia.
       rewrite [ta.+1 + _]addnC -[ta.+1]addn1 addnA -[tb.+1]addn1.
      rewrite addnCAC [tb +1 ]addnC -addnA ltn_add2l.  
		*)
  Qed.

	Lemma Request_index_decrements_within_period ta ra:
    let S := (Default_arbitrate ta).(Implementation_State) in
		(ra \in (FIFO_pending S))
		-> (FIFO_counter S) == WAIT.-1 
		-> (0 < index ra (FIFO_pending S))
		-> forall tb, ta < tb 
		-> tb <= ta + WAIT
		-> let S' := (Default_arbitrate tb).(Implementation_State) in
			(ra \in (FIFO_pending S')) && ((index ra (FIFO_pending S)) == (index ra (FIFO_pending S')).+1).
  Proof.
		cbv zeta; intros HP HC HI.
    induction tb; intros HL HU; [ contradict HL; by rewrite ltn0 | ].
    rewrite leq_eqVlt in HL; move: HL => /orP [HL | HL]; [ clear IHtb | ].
		{ rewrite eqSS in HL; move: HL => /eqP HL; subst.
      simpl; unfold FIFO_counter, FIFO_pending, Next_state in *.
      destruct (# tb).(Implementation_State) as [c P | c P rb] eqn:HSS; move: HC => /eqP HC; subst.
      { destruct P eqn:HPP; simpl; [ discriminate HP | ].
				rewrite eq_refl.
        simpl in HI; destruct (r == ra) eqn:He; [ discriminate HI | ].
        rewrite in_cons in HP; move: HP => /orP [/eqP HP | HP];
        [ contradict He; rewrite HP; apply /eqP; by rewrite eq_refl | ].
        by rewrite /Enqueue mem_cat HP orTb index_cat HP andTb eq_refl. 
			}
      { rewrite HC FIFO_wait_neq_act FIFO_wait_neq_cas eq_refl.  (* RUNNING state *)
        destruct P eqn:HPP; simpl; [ discriminate HP | ].
        rewrite eq_refl.
				simpl in HI; destruct (r == ra) eqn:He; [ discriminate HI | ].
        rewrite in_cons in HP; move: HP => /orP [/eqP HP | HP];
        [ contradict He; rewrite HP; apply /eqP; by rewrite eq_refl | ].
        by rewrite /Enqueue mem_cat HP orTb index_cat HP andTb eq_refl.
      }
    }  
    { rewrite ltnS in HL.
      apply IHtb in HL as IH; clear IHtb; [ | by apply ltnW in HU ].
      move: IH => /andP [HP' /eqP HI']; rewrite HI'; move: HC => /eqP HC.
      apply FIFO_l_counter_border_run in HC; [ | by apply Queue_non_empty in HP ].
      destruct HC as [Hca Hrun_a].
      assert (FIFO_counter # (ta.+1).(Implementation_State) = OCycle0); [ rewrite Hca /OCycle0 //= | ].
      apply FIFO_add_counter with (c := OCycle0) (d := tb - (ta.+1)) in Hrun_a as Haux; try assumption.
      	2: rewrite //= /OCycle0 add0n ltn_subLR; [ lia | assumption ].
			destruct Haux as [Hrun_a' Hca'].
      assert (H0 : ta.+1 + (tb - ta.+1) = tb); [ lia | ].
      rewrite H0 in Hrun_a',Hca'.
      rewrite /OCycle0 //= add0n in Hca'.
      apply Pending_requestor with (ra := ra) in Hrun_a' as Hpen; try assumption; [ | lia ].
      rewrite Hpen andTb; clear H H0 HP HI Hpen; rename Hrun_a' into Hrun_b.
      rewrite eqSS //= /FIFO_pending /Next_state.
			destruct (# tb).(Implementation_State) as [c P | c P rb] eqn:HSS; [ discriminate Hrun_b | ];
      destruct c; simpl.
      destruct (m == ACT_date) eqn:Hact, (m == CAS_date) eqn:Hcas, (m == WAIT.-1) eqn:Hwait; simpl;
      try by rewrite /Enqueue index_cat HP'.
      simpl in  Hrun_b, Hca', HI', HP', HSS.
      assert (FIFO_counter (# tb).(Implementation_State) = WAIT.-1) as Heq;
      [ rewrite HSS //=; move: Hwait => /eqP Hwait; exact Hwait | ].
      apply FIFO_l_counter_border_run in Heq as [Hcb Hrun_b'];
    	[ | rewrite HSS //=; by apply Queue_non_empty in HP' ].
      apply FIFO_l_eq_bound with (tb := tb.+1) (c := OCycle0) in Hrun_a as Hbug; try assumption.
      contradict Hbug; lia.
    }
  Qed.

  (* ------------------------------------------------------------------ *)
  (* --------------------- HIGH-LEVEL PROPERTIES ---------------------- *)
  (* ------------------------------------------------------------------ *)

	Lemma Still_pending_on_running_border ra ta:
    let S := # ta.(Implementation_State) in
    let S' := # (ta.+1).(Implementation_State) in
    ra \in (FIFO_pending S) ->
    isRUNNING (S) ->
    0 < index ra (FIFO_pending S) ->
    FIFO_counter (S) == WAIT.-1 ->
    (isRUNNING S') &&
    (FIFO_counter S' == OCycle0) &&
    (ra \in FIFO_pending S') &&
    (index ra (FIFO_pending S) == (index ra (FIFO_pending S')).+1).
  Proof.
    intros S S' HP Hrun Hi_pos Hc.
    unfold S,S' in *; clear S S'.
    destruct (Default_arbitrate ta).(Implementation_State) eqn:Hs.
    { discriminate Hrun. }
    { simpl. rewrite /Next_state Hs //=.
      destruct c; simpl in *. 
      destruct (m == ACT_date) eqn:Hact, (m == CAS_date) eqn:Hcas, (m == WAIT.-1) eqn:Hwait.
      all: try done.
      4: destruct r eqn:HPP.
      all: simpl.
      4: { discriminate HP. }
      4: {
        assert (r1 == r1). done. rewrite H.
        destruct (r1 == ra) eqn:Heq.
        { move: Heq => /eqP Heq. rewrite Heq //= in Hi_pos.
          assert (ra == ra). done. by rewrite H0 in Hi_pos.
        }
        rewrite in_cons in HP. move: HP => /orP [/eqP Hbug | HP].
        { rewrite Hbug in Heq. contradict Heq. apply Bool.not_false_iff_true. rewrite eq_refl. trivial. }
        by rewrite /Enqueue mem_cat HP orTb index_cat HP eq_refl.
      }
      all: try (move: Hwait => /eqP Hwait; rewrite Hwait in Hact; specialize FIFO_wait_neq_act as H;
      rewrite /OACT_date //= in H; by rewrite H in Hact).
      move: Hwait => /eqP Hwait; rewrite Hwait in Hcas; specialize FIFO_wait_neq_cas as H;
      rewrite /OCAS_date //= in H; by rewrite H in Hcas.
    }
  Qed.

  Lemma Still_pending_on_idle_border ra ta:
    let S := # ta.(Implementation_State) in
    let S' := # (ta.+1).(Implementation_State) in
    ra \in (FIFO_pending S) ->
    isIDLE (S) ->
    0 < index ra (FIFO_pending S) ->
    (isRUNNING S') &&
    (FIFO_counter S' == OCycle0) &&
    (ra \in FIFO_pending S') &&
    (index ra (FIFO_pending S) == (index ra (FIFO_pending S')).+1).
  Proof.
    intros S S' HP Hidle Hi.
    unfold S,S' in *; clear S S'.
    destruct (Default_arbitrate ta).(Implementation_State) eqn:Hs.
    2: discriminate Hidle.
    clear Hidle; rewrite /FIFO_pending in HP, Hi.
    simpl; rewrite /Next_state Hs.
    destruct r eqn:HPP; simpl.
    1: discriminate HP.
    { assert (r0 == r0). done.
      rewrite H. 
      destruct (r0 == ra) eqn:Heq.
      { move: Heq => /eqP Heq. by rewrite Heq //= eq_refl in Hi. }
      rewrite in_cons in HP; move: HP => /orP [Hbug | HP].
      { by rewrite eq_sym Heq in Hbug. }
      by rewrite /Enqueue mem_cat HP orTb andTb index_cat HP eq_refl.
    }
  Qed.

  Lemma Still_pending_during_window ra ta (c : Counter_t):
    let S := (Default_arbitrate ta).(Implementation_State) in
    ra \in (FIFO_pending S) -> 
    isRUNNING (S) ->
    FIFO_counter (S) == c ->
    forall d, c + d < WAIT -> let S' := (Default_arbitrate (ta + d)).(Implementation_State) in
    (ra \in (FIFO_pending S')) && 
    (index ra (FIFO_pending S) == index ra (FIFO_pending S')).
  Proof.
    intros S HP Hrun Hc d Hlt; simpl.
    unfold S in *; clear S.
    induction d.
    { rewrite addn0 HP andTb. apply /eqP. reflexivity. }
    { apply ltn_trans with (m := c + d) in Hlt as Hlt'.
        2: rewrite addnS; done.
      apply IHd in Hlt' as IH. clear IHd.
      move: IH => /andP [HP' Hi].
      apply FIFO_add_counter with (c := c) (d := d) in Hrun as H. destruct H as [Hrun' Hc'].
        3: by apply /eqP.
        2: exact Hlt'.
      destruct (Default_arbitrate (ta + d)).(Implementation_State) eqn:Hs.
      { discriminate Hrun'. }
      { clear Hrun'.
        rewrite addnS; simpl; rewrite /Next_state Hs //=.
        rewrite /FIFO_counter in Hc'.
        destruct c0; simpl in *.
        destruct (m == ACT_date) eqn:Hact, (m == CAS_date) eqn:Hcas, (m == WAIT.-1) eqn:Hwait; simpl.
        all: try (rewrite addnS -Hc' in Hlt; move: Hwait => /eqP Hwait; rewrite Hwait prednK in Hlt; (by rewrite ltnn in Hlt || exact WAIT_pos)).
        all: rewrite /Enqueue mem_cat HP' orTb //=.
        all: move: Hi => /eqP Hi; by rewrite Hi index_cat HP'.
      }
    }
  Qed.

  Lemma Request_index_decrements_over_period ta ra:
    let S := (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State) in
    ra \in (FIFO_pending S) ->
    forall i, let S' := (Default_arbitrate ((Requestor_slot_start ta) + i * WAIT)).(Implementation_State) in
    i > 0 -> i <= (index ra (FIFO_pending S)) ->
    (ra \in (FIFO_pending S')) && ((index ra (FIFO_pending S)) == (index ra (FIFO_pending S')) + i).
  Proof.
    intros S HP i S' Hi_pos HU.
    unfold S,S' in *; clear S S'.
    induction i.
    { discriminate Hi_pos. }
    { rewrite leq_eqVlt in Hi_pos. move: Hi_pos => /orP [/eqP Heq | Hi_pos].
      { clear IHi.
        rewrite -Heq mul1n.
        rewrite -[i.+1]add1n addnC -{1}[1]add0n in Heq. move: Heq => /eqP Heq. rewrite eqn_add2r in Heq. move: Heq => /eqP Heq.
        destruct (Default_arbitrate ta).(Implementation_State) eqn:Hs.
        { unfold Requestor_slot_start in *. rewrite Hs in HP, HU. (* IDLE leg *)
          rewrite Hs.
          rewrite -Heq in HU.
          apply Still_pending_on_idle_border in HP as H. move: H => /andP [ /andP [ /andP [Hrun Hc] HP' ] Hi].
            3: exact HU.
            2: by rewrite Hs.
          apply Still_pending_during_window with (c := OCycle0) (d := WAIT.-1) in HP' as H. move: H => /andP [HP'' Hi'].
            4: rewrite ltn_predL; exact WAIT_pos.
            3: exact Hc.
            2: exact Hrun.
          assert (ta.+1 + WAIT.-1 = ta + WAIT); [ specialize WAIT_pos; lia | ].
          rewrite H in HP'' Hi'. clear H.
          rewrite HP'' andTb.
          move: Hi => /eqP Hi. rewrite Hi.
          move: Hi' => /eqP Hi'. rewrite Hi'.
          rewrite addn1 eqSS. apply /eqP. reflexivity.
        }
        { unfold Requestor_slot_start in *. rewrite Hs. rewrite Hs in HP,HU.
          assert (isRUNNING (Default_arbitrate ta).(Implementation_State)) as Hrun.
            { by rewrite Hs. }
          apply FIFO_add_counter with (c:= c) (d := (WAIT.-1 - c)) in Hrun as Hc.
            3: by rewrite /FIFO_counter Hs.
            2: { rewrite subnKC.
              2: destruct c; simpl; clear Hs HP HU; lia.
              1: rewrite ltn_predL; exact WAIT_pos.
            }
          destruct Hc as [Hrun' Hc].
          rewrite subnKC in Hc.
            2: destruct c; simpl; clear Hs HP HU Hrun Hrun'; lia.
          apply Still_pending_on_running_border in HP as H;
						[ | assumption | by rewrite -Heq in HU | by rewrite Hc ].
   				move: H => /andP [ /andP [ /andP [Hrun'' Hc'] HP'] Hi];
          apply Still_pending_during_window with (c := OCycle0) (d := WAIT.-1) in HP' as H; try assumption.
            2: rewrite ltn_predL; exact WAIT_pos.
 					move: H => /andP [HP'' Hi'].
          assert ((ta + (WAIT.-1 -c)).+1 + WAIT.-1 = ta + (WAIT.-1 - c) + WAIT) as H.
            { rewrite -addnS -addn1 addnA -addnA [1 + WAIT.-1]addnC addn1 prednK. reflexivity. exact WAIT_pos. }
          rewrite H in HP'',Hi'.
          rewrite HP'' andTb. 
          move: Hi => /eqP Hi. rewrite Hi.
          move: Hi' => /eqP Hi'. rewrite Hi'.
          rewrite addn1 eqSS. apply /eqP. reflexivity.
        }
      }
      { rewrite -[i.+1]addn1 -{1}[1]add0n  ltn_add2r in Hi_pos.
        apply ltnW in HU as HU'.
        apply IHi in Hi_pos as IH. clear IHi. move: IH => /andP [HP' /eqP Hi].
          2: exact HU'.
        destruct (Default_arbitrate (Requestor_slot_start ta + i * WAIT)).(Implementation_State) eqn:Hs.
        { assert (isIDLE(Default_arbitrate (Requestor_slot_start ta + i * WAIT)).(Implementation_State)) as Hidle.
          { by rewrite Hs. } 
          apply Request_index_decrements_within_period_idle with (ra := ra) (tb := Requestor_slot_start ta + i.+1 * WAIT) in Hidle.
            5: by rewrite mulSn -addnA leq_add2l addnC leqnn.
            4: rewrite mulSn ltn_add2l addnC; apply nat_ltnn_add; exact WAIT_pos.
            3: { rewrite -Hs in Hi. rewrite Hi in HU. rewrite addnC in HU. by apply nat_ltn_add_rev in HU. }
            2: { by rewrite -Hs in HP'. }
          move: Hidle => /andP [HP'' Hi'].
          rewrite HP'' andTb.
          rewrite -Hs in Hi. rewrite Hi. move: Hi' => /eqP Hi'. 
          rewrite Hi' -addn1 -addnA [1 + i]addnC addn1.
          apply /eqP. reflexivity.
        }
        { rewrite -Hs in HP'. 
          assert (c + (WAIT.-1 -c ) = WAIT.-1) as Hcounter.
            { rewrite subnKC. reflexivity. destruct c. simpl in *. clear Hs; lia. }
          assert (isRUNNING (Default_arbitrate (Requestor_slot_start ta + i * WAIT)).(Implementation_State)) as Hrun.
            { by rewrite Hs. }
          apply FIFO_add_counter with (c := c) (d := (WAIT.-1 -c)) in Hrun as Hc. destruct Hc as [Hrun' Hc]. 
            3: by rewrite Hs.
            2: { rewrite Hcounter ltn_predL. exact WAIT_pos. }
          apply Still_pending_during_window with (c := c) (d := (WAIT.-1 -c)) in HP' as H. move: H => /andP [HP'' Hi'].
            4: { rewrite Hcounter ltn_predL. exact WAIT_pos. }
            3: by rewrite Hs.
            2: by rewrite Hs.
          apply Still_pending_on_running_border in HP'' as H.
            4: { rewrite Hcounter in Hc. apply /eqP. exact Hc. }
            3: { move: Hi' => /eqP Hi'. rewrite -Hi'. rewrite -Hs in Hi. rewrite Hi in HU. rewrite addnC in HU. by apply nat_ltn_add_rev in HU. }
            2: exact Hrun'.
          move: H => /andP [/andP [/andP [Hrun'' Hc'] HP'''] Hi''].
          apply Still_pending_during_window with (c := OCycle0) (d := c) in HP''' as H; try assumption.
            2: by destruct c.
         	move: H => /andP [HP'''' Hi'''].
          rewrite mulSn -![WAIT + i * WAIT]addnC addnA.
          assert ((Requestor_slot_start ta + i * WAIT + (WAIT.-1 - c)).+1 + c = Requestor_slot_start ta + i * WAIT + WAIT).
            { rewrite -addnS -addnA.
              apply /eqP.
              rewrite eqn_add2l -subSn; [ | destruct c; simpl in *; clear Hs; lia ];
              rewrite prednK; [ | exact WAIT_pos].
              rewrite subnK;
              [ | destruct c; simpl in *; clear Hs; by apply ltnW in i0 ].
              apply /eqP. reflexivity.
            }
          rewrite H in HP''''; rewrite HP'''' andTb.
          rewrite H in Hi'''. move: Hi''' => /eqP Hi'''. rewrite -Hi'''.
          rewrite -Hs in Hi. rewrite Hi. 
          move: Hi' => /eqP Hi'. rewrite Hi'.
          move: Hi'' => /eqP Hi''. rewrite Hi''.
          rewrite -addn1 -addnA [1 + i]addnC addn1; apply /eqP; reflexivity.
        }
      }
    }
  Qed.

  Lemma Request_index_zero ta ra:
    let S := (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State) in
    let i := (index ra (FIFO_pending S)) in
      ra \in (FIFO_pending S) 
      -> let S' := (Default_arbitrate ((Requestor_slot_start ta) + i * WAIT)).(Implementation_State) in
        (ra \in (FIFO_pending S')) && ((index ra (FIFO_pending S')) == 0).
  Proof.
    intros S i HP; simpl.
    unfold S in *. simpl in *. clear S.
    destruct i eqn:Hi.
    { rewrite mul0n addn0 HP andTb.
      fold i. apply /eqP. exact Hi.
    }
    { apply Request_index_decrements_over_period with (i := i) in HP.
        3: { subst i. by rewrite leqnn. }
        2: { by rewrite Hi. }
      rewrite -Hi.
      move: HP => /andP [HP' Hindex].
      subst i.
      rewrite -{1}[index ra (FIFO_pending (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State))]add0n eqn_add2r eq_sym in Hindex.
      by rewrite HP' Hindex.
    }
  Qed.

  Lemma Pending_requestor_slot_start ta ra:
    ra \in (FIFO_pending (Default_arbitrate ta).(Implementation_State)) ->
    forall tb, ta <= tb -> tb <= (Requestor_slot_start ta) ->
    ra \in (FIFO_pending (Default_arbitrate tb).(Implementation_State)).
  Proof.
    intros HP.
    destruct (Default_arbitrate ta).(Implementation_State) eqn:Hs.
    { intros tb HL HU. (* IDLE *)
      rewrite /Requestor_slot_start Hs in HU.
      assert (ta == tb) as Heq.
      { rewrite leq_eqVlt in HL. move: HL => /orP [HL | HL].
        { exact HL. }
        { contradict HU. apply /negP. by rewrite -ltnNge HL. }
      }
      move: Heq => /eqP Heq; subst.
      unfold FIFO_pending in *. rewrite Hs. exact HP.
    }
    { intros tb HL HU. (* RUNNING*) 
      rewrite /Requestor_slot_start Hs in HU.
      assert (isRUNNING (Default_arbitrate ta).(Implementation_State)) as Hrun_a. { by rewrite Hs. }
      assert (FIFO_counter (Default_arbitrate ta).(Implementation_State) = c) as Hca. { by rewrite Hs. }
      induction tb.
      { rewrite leq_eqVlt in HL. move: HL => /orP [/eqP HL | HL].
          { rewrite HL in Hrun_a. by simpl in Hrun_a. }
          inversion HL.
      }
      rewrite leq_eqVlt in HL. move: HL => /orP [/eqP HL | HL].
      { rewrite -HL. rewrite -Hs in HP. exact HP. }
      rewrite ltnS in HL. apply ltnW in HU as HH.
      apply IHtb in HL as IH. clear IHtb.
        2: exact HH.
      apply FIFO_add_counter with (c := c) (d := (tb - ta)) in Hrun_a as H'. destruct H' as [Hrun_b Hcb].
        3: exact Hca.
        2: { 
          rewrite -ltn_subLR in HU.
            2: exact HL.
          rewrite ltn_subRL in HU. apply ltn_trans with (n := WAIT.-1). exact HU. rewrite ltn_predL WAIT_pos. done. 
        }
      assert (ta + (tb - ta) = tb). { rewrite subnKC. reflexivity. exact HL. }
      rewrite H in Hrun_b Hcb. clear H.
      apply Pending_requestor in IH.
        3: {
          rewrite Hcb neq_ltn. apply /orP. left.
          rewrite -ltn_subLR in HU.
            2: exact HL.
          rewrite ltn_subRL in HU. exact HU.
        }
        2: exact Hrun_b.
      exact IH.
    }
  Qed.

  Lemma Request_processing_starts ta ra:
    ra \in (Arrival_at ta)
    -> let t := (Requestor_slot_start ta) in
       let i := index ra (FIFO_pending (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State)) * WAIT in
       let S := (Default_arbitrate (t + i)).(Implementation_State) in
        (ra \in FIFO_pending S) && (index ra (FIFO_pending S) == 0).
  Proof.
    intros HP.
    apply Pending_on_arrival in HP.
    apply Pending_requestor_slot_start with (tb := (Requestor_slot_start ta)) in HP.
      3: apply leqnn.
      2: {
        destruct (Default_arbitrate ta).(Implementation_State) eqn:Hsa.
        all: rewrite /Requestor_slot_start Hsa.
        2: by rewrite leq_addr.
        1: by rewrite leqnn.
      }
    apply Request_index_zero in HP; simpl in HP.
    exact HP.
  Qed.

  Lemma Running_at_slot_start ta:
    isRUNNING (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State) ->
    FIFO_counter (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State) == WAIT.-1.
  Proof.
    intros H.
    unfold Requestor_slot_start in *.
    destruct (Default_arbitrate ta).(Implementation_State) eqn:Hs.
    { rewrite Hs in H. discriminate H. }
    { assert (c + (WAIT.-1 - c) = WAIT.-1).
        { rewrite subnKC. reflexivity. destruct c. simpl in *. clear Hs. lia. }
      assert (isRUNNING (Default_arbitrate ta).(Implementation_State)) as Hrun.
        { by rewrite Hs. }
      apply FIFO_add_counter with (c := c) (d := (WAIT.-1 -c)) in Hrun as [_ Hc].
        3: by rewrite Hs.
        2: { rewrite H0. rewrite ltn_predL. exact WAIT_pos. }
      rewrite H0 in Hc.
      apply /eqP; rewrite Hc. reflexivity.
    }
  Qed.
    
  Lemma Counter_reaches ra ta:
    let S := (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State) in
    ra \in FIFO_pending (S) ->
    forall i, i > 0 -> i <= index ra (FIFO_pending S) ->
    let S' := (Default_arbitrate (Requestor_slot_start ta + i * WAIT)).(Implementation_State) in
    (isRUNNING S') &&
    (FIFO_counter (S') == WAIT.-1).
  Proof.
    intros S HP i Hi_pos Hi.
    set (S' := (Default_arbitrate (Requestor_slot_start ta + i * WAIT)).(Implementation_State)); simpl.
    unfold S,S' in *; clear S S'.
    induction i.
    { discriminate Hi_pos. }
    { rewrite leq_eqVlt in Hi_pos; move: Hi_pos => /orP [/eqP Hz | Hi_pos].
      { clear IHi. 
        move:Hz => /eqP Hz; rewrite -addn1 -[i.+1]addn1 eqn_add2r in Hz; move:Hz => /eqP Hz.
        rewrite -Hz mul1n.
        destruct (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State) eqn:Hs.
        { rewrite -Hs in HP. 
          apply Still_pending_on_idle_border in HP as H. move: H => /andP [/andP [/andP [Hrun HC] HP'] HI].
            3: by rewrite -Hz -Hs in Hi.
            2: by rewrite Hs.
          apply FIFO_add_counter with (c := OCycle0) (d := WAIT.-1) in Hrun as H. destruct H as [Hrun' HC'].
            3: { apply /eqP. exact HC. }
            2: { rewrite /OCycle0 //= add0n ltn_predL. exact WAIT_pos. }
          assert ((Requestor_slot_start ta).+1 + WAIT.-1 = (Requestor_slot_start ta) + WAIT).
            { rewrite -addn1 -addnA [1 + WAIT.-1]addnC. rewrite addn1 prednK. reflexivity. exact WAIT_pos. }
          rewrite H in HC',Hrun'.
          rewrite /OCycle0 add0n in HC'.
          by rewrite HC' Hrun' eq_refl.
        }
        { rewrite -Hs in HP.
          assert (c + (WAIT.-1 -c) = WAIT.-1) as Hcounter.
            { rewrite subnKC. reflexivity. destruct c; simpl in *. clear Hs. lia. }
          assert (isRUNNING (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State)) as Hrun.
            { by rewrite Hs. }
          apply Running_at_slot_start in Hrun as Hc.
          apply Still_pending_on_running_border in HP as H.
            4: exact Hc.
            3: by rewrite -Hz -Hs in Hi.
            2: exact Hrun.
          move: H => /andP [/andP [/andP [Hrun' Hc']HP']HI'].
          apply FIFO_add_counter with (c := OCycle0) (d := WAIT.-1) in Hrun' as [Hrun'' Hc''].
            3: by apply /eqP.
            2: rewrite /OCycle0 add0n ltn_predL; exact WAIT_pos.
          assert ((Requestor_slot_start ta).+1 + WAIT.-1 = Requestor_slot_start ta + WAIT).
            { apply /eqP. 
              rewrite -addn1 -addnA eqn_add2l addnC addn1 prednK; (by rewrite eq_refl || exact WAIT_pos).
            }
          rewrite H in Hrun'', Hc''.
          rewrite /OCycle0 add0n in Hc''.
          by rewrite Hrun'' Hc'' eq_refl.
        }
      }
      { rename Hi into HU. rewrite -[i.+1]addn1 -{1}[1]add0n  ltn_add2r in Hi_pos.
        apply ltnW in HU as HU'.
        apply IHi in Hi_pos as IH. clear IHi. move: IH => /andP [Hrun /eqP Hc].
          2: exact HU'.
        apply Request_index_decrements_over_period with (i := i) in HP.
          3: exact HU'.
          2: exact Hi_pos.
        move: HP => /andP [HP' Hi].
        apply Still_pending_on_running_border in HP' as H.
          4: by apply /eqP.
          3: { move: Hi => /eqP Hi. rewrite Hi addnC in HU. by apply nat_ltn_add_rev in HU. }
          2: exact Hrun.
        move: H => /andP [/andP [/andP [Hrun' Hc']HP'']Hi'].
        apply FIFO_add_counter with (c := OCycle0) (d := WAIT.-1) in Hrun' as [Hrun'' Hc''].
          3: by apply /eqP.
          2: rewrite /OCycle0 add0n; rewrite ltn_predL; exact WAIT_pos.
        rewrite mulSn. rewrite [WAIT + i * WAIT]addnC addnA.
        rewrite -addn1 -addnA [1 + WAIT.-1]addnC addn1 prednK in Hrun'', Hc''.
          2: exact WAIT_pos.
        rewrite Hrun''.
        rewrite /OCycle0 add0n in Hc''.
        by rewrite Hc'' eq_refl.
      }
    }
  Qed.

  Lemma Request_starts_idle_ ra ta:
    let S := (Default_arbitrate ta).(Implementation_State) in
    isIDLE (S) ->
    ra \in FIFO_pending (S) ->
    index ra (FIFO_pending S) = 0 ->
    (FIFO_request (Default_arbitrate ta.+1).(Implementation_State) == Some ra) &&
    (FIFO_counter (Default_arbitrate ta.+1).(Implementation_State) == OCycle0) &&
    (isRUNNING (Default_arbitrate ta.+1).(Implementation_State)) &&
    (FIFO_pending (Default_arbitrate ta.+1).(Implementation_State) == Enqueue (Arrival_at ta.+1) (Dequeue ra (FIFO_pending S))).
  Proof.
    intros S Hidle HP Hi.
    unfold S in *; clear S.
    destruct (Default_arbitrate ta).(Implementation_State) eqn:Hs.
    2: discriminate Hidle.
    simpl; rewrite /Next_state Hs.
    destruct r eqn:HPP.
    { discriminate HP. }
    simpl; rewrite !eq_refl !andbT.
    rewrite /FIFO_pending in Hi.
    destruct (r0 == ra) eqn:Heq.
    { move: Heq => /eqP Heq. rewrite Heq; by rewrite !eq_refl. }
    simpl in Hi. rewrite Heq in Hi. discriminate Hi.
  Qed.

  Lemma Request_processing ta ra d:
    ra \in (Arrival_at ta)
    -> let t := Requestor_slot_start ta in
       let i := index ra (FIFO_pending (Default_arbitrate t).(Implementation_State)) * WAIT in
       d < WAIT ->
       let S' := (Default_arbitrate ((t + i).+1 + d)).(Implementation_State) in 
       (FIFO_counter (S') == d) && (FIFO_request (S') == Some ra).
  Proof.
    intros HA t i Hd.
    apply Pending_on_arrival in HA as HP.
    apply Pending_requestor_slot_start with (tb := Requestor_slot_start ta) in HP as HP'.
      3: by rewrite leqnn.
      2: { rewrite /Requestor_slot_start. destruct (Default_arbitrate ta).(Implementation_State) eqn:Hs. 
        by rewrite leqnn.
        by rewrite leq_addr. }
    destruct (index ra (FIFO_pending (Default_arbitrate t).(Implementation_State))) eqn:Hz.
      { subst i. 
        rewrite mul0n addn0; subst t.
        unfold Requestor_slot_start in *.
        destruct (Default_arbitrate ta).(Implementation_State) eqn:Hs.
        { rewrite -Hs in HP. apply Request_starts_idle in HP as H. move: H => /andP [/andP [Hreq Hc] Hrun].
            3: exact Hz.
            2: by rewrite Hs.
          apply Request_running_in_slot with (d := d) in Hreq as Hreq'.
            3: exact Hd.
            2: exact Hc.
          rewrite Hreq' andbT.
          apply FIFO_add_counter with (c:=OCycle0) (d:=d) in Hrun. destruct Hrun as [_ H].
            3: by apply /eqP.
            2: rewrite /OCycle0 //= add0n.
          rewrite /OCycle0 add0n in H; by apply /eqP.
        }
        { assert (isRUNNING (Default_arbitrate ta).(Implementation_State)) as Hrun.
            { by rewrite Hs. }
          assert (c + (WAIT.-1 - c ) = WAIT.-1).
            { rewrite subnKC. reflexivity. destruct c; simpl in *. clear Hs. lia. } 
          apply FIFO_add_counter with (c := c) (d := (WAIT.-1 -c)) in Hrun as H'. destruct H' as [Hrun' Hc'].
            3: by rewrite Hs.
            2: rewrite H ltn_predL; exact WAIT_pos.
          rewrite H in Hc'.
          apply Request_starts_running with (ra := ra) in Hrun' as H'. move: H' => /andP [/andP [Hreq Hc''] Hrun''].
            4: exact Hz.
            3: exact HP'.
            2: by rewrite Hc' eq_refl.
          apply FIFO_add_counter with (c := OCycle0) (d:= d) in Hrun'' as H'. destruct H' as [Hrun''' Hc'''].
            3: by apply /eqP.
            2: by rewrite /OCycle0 add0n.
          apply Request_running_in_slot with (d := d) in Hreq.
            3: exact Hd.
            2: exact Hc''.
          rewrite Hreq andbT.
          rewrite /OCycle0 add0n in Hc'''; by rewrite Hc''' eq_refl.
        }
      }
    apply Counter_reaches with (i := index ra (FIFO_pending (Default_arbitrate t).(Implementation_State))) in HP'.
      3: by subst t.
      2: by rewrite Hz.
    move: HP' => /andP [Hrun HC].
    apply Request_processing_starts in HA as H. move: H => /andP [HP' Hiz].
    fold t in Hrun,HC; rewrite Hz in Hrun,HC.
    fold i in Hrun,HC; fold t in HP',Hiz.
    rewrite Hz in HP',Hiz; fold i in HP',Hiz.
    apply Request_starts_running with (ra := ra) in HC as H. move: H => /andP [/andP [Hreq HC'] H].
      4: by apply /eqP.  
      3: exact HP'.
      2: exact Hrun.
    apply FIFO_add_counter with (c := OCycle0) (d := d) in H. destruct H as [Hrun' HC''].
      3: by apply /eqP.
      2: rewrite /OCycle0 add0n; exact Hd.
    rewrite /OCycle0 add0n in HC''.
    apply Request_running_in_slot with (d := d) in Hreq as H.
      3: exact Hd.
      2: exact HC'.
    set S' := (Default_arbitrate ((t + i).+1 + d)).(Implementation_State); simpl.
    by fold S' in HC'',H; rewrite H; rewrite HC'' eq_refl.
  Qed.

  Lemma Request_CAS ta ra: 
    ra \in (Arrival_at ta) 
    -> let tc := (Requestor_slot_start ta + (index ra (FIFO_pending (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State)) * WAIT) + CAS_date).+1 in
       (CAS_of_req ra tc.+1) \in (Default_arbitrate tc.+1).(Arbiter_Commands).
  Proof.
    intros HA.
    apply Request_processing with (d := CAS_date) in HA as H; [ | exact WAIT_CAS].
    move: H => /andP [HC HR]; clear HA.
    rewrite addSn in HC, HR; set (tc := _ + CAS_date); fold tc in HC, HR.
    destruct (Default_arbitrate tc.+1).(Implementation_State) eqn:Hs; simpl.
    { by rewrite /FIFO_request in HR. } 
    simpl in Hs; rewrite Hs.
    rewrite /Next_state.
    rewrite /FIFO_counter in HC.
    assert ((nat_of_ord c == ACT_date) = false) as Hact.
    { move: HC => /eqP HC. by rewrite HC CAS_neq_ACT. }
    rewrite //= Hact HC //= /CAS_of_req in_cons; apply /orP; left.
    rewrite /FIFO_request inj_eq in HR. move: HR => /eqP HR.
      2: exact ssrfun.Some_inj.
    by rewrite HR.
  Qed.

  Lemma Counter_at_running_slot_start ta c:
    FIFO_counter (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State) == c ->
    isRUNNING (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State) ->
    c == WAIT.-1.
  Proof.
    intros Hc Hrun.
    unfold Requestor_slot_start in *.
    destruct (Default_arbitrate ta).(Implementation_State) eqn:Hs.
    { by rewrite Hs in Hrun. }
    { clear Hrun.
      assert (isRUNNING (Default_arbitrate ta).(Implementation_State)) as H.
      { by rewrite Hs. }
      assert (c0 + (WAIT.-1 - c0) = WAIT.-1).
      { rewrite subnKC. reflexivity. destruct c0; simpl in *; clear Hs; lia. }
      apply FIFO_add_counter with (c := c0) (d := (WAIT.-1 - c0)) in H as [Hrun Hc'].
        3: by rewrite Hs.
        2: rewrite H0; rewrite ltn_predL; exact WAIT_pos.
      rewrite H0 in Hc'; by rewrite Hc' eq_sym in Hc.
    }
  Qed.

  Lemma Request_PRE ta ra: 
    ra \in (Arrival_at ta)
    -> let i := index ra (FIFO_pending (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State)) * WAIT in 
       let tc := ((Requestor_slot_start ta) + i).+1 in
        (PRE_of_req ra tc) \in (Default_arbitrate tc).(Arbiter_Commands).
  Proof.
    intros HA.

    apply Request_processing_starts in HA as HP; move: HP => /andP [HP HI].
    set i := index _ _; fold i in HP,HI.
    set t := Requestor_slot_start ta + i * WAIT; fold t in HP,HI.

    destruct i eqn:Hz. (* Case i = 0 *)
    { rewrite mul0n //= addn0; subst t.
      rewrite mul0n addn0 in HP,HI.
      rewrite /Next_state.
      destruct (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State) eqn:Hs.
      { rewrite /FIFO_pending in HP,HI.
        destruct r eqn:HPP.
        { discriminate HP. }
        { destruct (ra == r0) eqn:Heq. 
          { simpl; move: Heq => /eqP Heq; rewrite /PRE_of_req in_cons Heq; apply /orP; left; by rewrite eq_refl. }
          { rewrite eq_sym in Heq; by rewrite //= Heq in HI. }}}
      { rewrite /FIFO_pending in HP,HI.
        assert (FIFO_counter (Default_arbitrate (Requestor_slot_start ta)).(Implementation_State) == c).
        { by rewrite Hs. }
        apply Counter_at_running_slot_start in H.
        2: by rewrite Hs.
        move: H => /eqP H; rewrite H FIFO_wait_neq_act FIFO_wait_neq_cas eq_refl.
        destruct r eqn:HPP.
        { discriminate HP. }
        { destruct (r1 == ra) eqn:Heq.
          { simpl; move: Heq => /eqP Heq; rewrite /PRE_of_req in_cons Heq; apply /orP; left; by rewrite eq_refl. }
          { by rewrite //= Heq in HI. }}}}

    apply Pending_on_arrival in HA as H.
    apply Pending_requestor_slot_start with (tb := Requestor_slot_start ta) in H.
      3: by rewrite leqnn.
      2: { rewrite /Requestor_slot_start; destruct (Default_arbitrate ta).(Implementation_State). 
        by rewrite leqnn. by rewrite leq_addr. }
    apply Counter_reaches with (i := i) in H. move: H => /andP [Hrun HC]; fold t in Hrun,HC.
      3: subst i; by rewrite leqnn.
      2: by rewrite Hz.

    simpl; fold t; rewrite /Next_state.
    destruct (Default_arbitrate t).(Implementation_State) eqn:Hs.
    { destruct r eqn:HPP.
      { simpl in HP; discriminate HP. }
      { destruct (r0 == ra) eqn:Heq.
        { by simpl; move: Heq => /eqP Heq; rewrite in_cons /PRE_of_req //= Heq eq_refl orTb. }
        { by rewrite /FIFO_pending //= Heq in HI. }
      }}
    { unfold FIFO_pending in HP,HI. subst t; rewrite -Hz in Hs; rewrite Hs /FIFO_counter in HC; move: HC => /eqP HC.
      rewrite HC FIFO_wait_neq_act FIFO_wait_neq_cas eq_refl.
      destruct r eqn:HPP.
      { discriminate HP. }
      { destruct (r1 == ra) eqn:Heq.
        { simpl; move: Heq => /eqP Heq; by rewrite in_cons /PRE_of_req //= Heq eq_refl orTb. }
        { by rewrite /FIFO_pending //= Heq in HI. }
      }
    }
  Qed.

  Theorem Requests_handled ta ra:
    ra \in (Arrival_at ta) 
    -> exists tc, (CAS_of_req ra tc) \in ((Default_arbitrate tc).(Arbiter_Commands)).
  Proof.
    intros HA.
    apply Request_CAS in HA as H.
    set tc := _.+1 in H.
    exists (tc.+1).
    exact H.
  Qed.
  
  (* ------------------------------------------------------------------ *)
  (* --------------------- INSTANCES ---------------------------------- *)
  (* ------------------------------------------------------------------ *)

  Lemma Default_arbitrate_notin_cmd t (c : Command_t):
    c.(CDate) > t -> c \notin (Default_arbitrate t).(Arbiter_Commands).
  Proof.
    intros.
    destruct (c \notin (# t).(Arbiter_Commands)) eqn:Hni; [ done | ].
    move : Hni => /negPn Hni; apply Cmd_in_trace in Hni.
    contradict Hni; apply /negP; by rewrite -ltnNge.
  Qed.

  Lemma Default_arbitrate_cmds_uniq t:
    uniq ((# t).(Arbiter_Commands)).
  Proof.
    induction t; simpl; [ done | ].
    rewrite /Next_state //=.
		destruct_state t; simpl; rewrite IHt andbT;
		assert (t < t.+1) as H0; try trivial;
		apply Default_arbitrate_notin_cmd; simpl; lia.
	Qed.
		
  Lemma Default_arbitrate_cmds_date t cmd:
    cmd \in (# t).(Arbiter_Commands) -> 
    cmd.(CDate) <= (# t).(Arbiter_Time).
  Proof.
    rewrite Default_arbitrate_time; apply Cmd_in_trace.
  Qed.

  Program Definition FIFO_arbitrate t :=
    mkTrace 
    (Default_arbitrate t).(Arbiter_Commands) 
    (Default_arbitrate t).(Arbiter_Time)
    (Default_arbitrate_cmds_uniq t)
    (Default_arbitrate_cmds_date t)
    (Cmds_T_RCD_ok t)
    (Cmds_T_RP_ok t)
    (Cmds_T_RC_ok t)
    (Cmds_T_RAS_ok t)
    (Cmds_T_RTP_ok t)
    (Cmds_T_WTP_ok t)
    (Cmds_T_RtoW_ok t)
    (Cmds_T_WtoR_SBG_ok t)
    (Cmds_T_WtoR_DBG_ok t)
    (Cmds_T_CCD_SBG_ok t)
    (Cmds_T_CCD_DBG_ok t)
		_ (* REFRESH *)
		_ (* REFRESH *)
    (Cmds_T_FAW_ok t)
    (Cmds_T_RRD_SBG_ok t)
    (Cmds_T_RRD_DBG_ok t)
    (Cmds_ACT_ok t)
    (Cmds_row_ok t)
		(Cmds_initial t)
		_. (* REFRESH *)
	Admit Obligations.


  Global Instance FIFO_arbiter : Arbiter_t :=
    mkArbiter AF FIFO_arbitrate Requests_handled Default_arbitrate_time_match.

End Proofs.