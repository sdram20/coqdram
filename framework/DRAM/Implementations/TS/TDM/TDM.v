Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.
From DRAM Require Export ImplementationInterface.
From mathcomp Require Export fintype div.
Require Import Program.

Section TDM.

  Context {SYS_CFG : System_configuration}.

  (* #[local] Axiom DDR3: BANKGROUPS = 1. *)

  Definition ACT_date := T_RP.+1.
  Definition CAS_date := ACT_date + T_RCD.+1.

  Class TDM_configuration :=
  {
    SL : nat;
    SN : nat;

    SN_pos  : 0 < SN;
    SN_one  : 1 < SN;
    SL_pos  : 0 < SL;
    SL_ACT  : ACT_date < SL;
    SL_CASS : CAS_date.+1 < SL;

    T_RTP_SN_SL : T_RTP < SN * SL - CAS_date;
    T_WTP_SN_SL : (T_WR + T_WL + T_BURST) < SN * SL - CAS_date;

    T_RAS_SL	: T_RP.+1 + T_RAS < SL;
    T_RC_SL 	: T_RC < SL;
    T_RRD_SL  : T_RRD_l < SL;
    T_RTW_SL	: T_RTW < SL;
    T_CCD_SL	: T_CCD_l < SL;
    WTR_SL		: T_WTR_l + T_WL + T_BURST < SL;
    T_FAW_3SL : T_FAW < SL + SL + SL;
  }.

  Context {TDM_CFG : TDM_configuration}.

	(* -------------------------------------------------------- *)
	(* Helper lemmas *)
  Lemma Last_cycle:
    SL.-1 < SL.
  Proof.
    rewrite ltn_predL. apply SL_pos.
  Qed.

  Lemma SL_CAS: 
    CAS_date < SL.
  Proof.
    specialize SL_CASS as H.
    apply ltn_trans with (m := CAS_date) in H.
    - exact H.
    - apply ltnSn.
  Qed.

  Lemma SL_one:
    1 < SL.
  Proof.
    specialize SL_CAS as H.
    unfold CAS_date, ACT_date in H.
    apply ltn_trans with (m := 1) in H.
    - exact H. clear H.
    - rewrite ltn_addr.
      - trivial.
      - rewrite ltnS.
        specialize T_RP_pos as H.
        by rewrite <- lt0n in H.
  Qed.

  Lemma CAS_pos:
    0 < CAS_date.
  Proof.
    unfold CAS_date, ACT_date.
    rewrite addn_gt0. apply /orP. left.
    apply ltn0Sn.
  Qed.

  Lemma SL_ACTS:
    ACT_date.+1 < SL.
  Proof.
    specialize SL_CAS as H.
    unfold CAS_date, ACT_date in *.
    apply ltn_trans with (m := T_RP.+2) in H.
    - exact H.
    - rewrite <- (addn1 T_RP.+1), ltn_add2l, ltnS, lt0n.
      exact T_RCD_pos.
  Qed.

  Lemma ACT_pos:
    0 < ACT_date.
  Proof.
    by rewrite /ACT_date -[T_RP.+1]addn1 addn_gt0 ltn0Sn orbT.
  Qed.
	(* -------------------------------------------------------- *)

  Definition OZCycle   := Ordinal SL_pos.
  Definition OACT_date := Ordinal SL_ACT.
  Definition OCAS_date := Ordinal SL_CAS.
  Definition OLastCycle := Ordinal Last_cycle.

  Definition OZSlot := Ordinal SN_pos.

  Definition Slot_t := ordinal_eqType SN.

  Instance REQESTOR_CFG : Requestor_configuration := 
  {
    Requestor_t := Slot_t
  }.

  (* Definition Same_Slot (a b : Command_t) :=
    a.(Request).(Requestor) == b.(Request).(Requestor). *)

  Context {AF : Arrival_function_t}.

  Definition Counter_t := ordinal SL.

  (* Track whether a request is processed (RUNNING) or not (IDLE), the owner of 
     the current slot, the cycle offset within the slot, as well as the set of 
     pending  requests. *)
  Variant TDM_state_t :=
    | IDLE    : Slot_t -> Counter_t -> Requests_t -> TDM_state_t 
    | RUNNING : Slot_t -> Counter_t -> Requests_t -> Request_t -> TDM_state_t.

  Definition TDM_state_eqdef (a b : TDM_state_t) :=
    match a, b with
      | IDLE sa ca Pa,       IDLE sb cb Pb       => (sa == sb) && (ca == cb) && (Pa == Pb)
      | RUNNING sa ca Pa ra, IDLE sb cb Pb       => false
      | IDLE sa ca Pa,       RUNNING sb cb Pb rb => false
      | RUNNING sa ca Pa ra, RUNNING sb cb Pb rb => (sa == sb) && (ca == cb) && (Pa == Pb) && (ra == rb)
    end.

  Lemma TDM_state_eqn : Equality.axiom TDM_state_eqdef.
  Proof.
    unfold Equality.axiom. intros. destruct TDM_state_eqdef eqn:H; unfold TDM_state_eqdef in *.
    {
      destruct x, y; apply ReflectT; 
        try (contradict H; discriminate).
      1: move: H => /andP [/andP [/eqP S /eqP C /eqP P]].
      2: move: H => /andP [/andP [/andP [/eqP S /eqP C /eqP P /eqP R]]].
      all: subst; reflexivity.
    }
    destruct x, y; apply negbT in H; apply ReflectF;
      try discriminate.
    all: unfold not; intro BUG.
      1: rewrite 2! negb_and in H.
      2: rewrite 3! negb_and in H.
      2: move: H => /orP [H | /eqP R].
    1,2: move: H => /orP [H | /eqP P].
    1,3: move: H => /orP [/eqP S | /eqP C].
      all: try (by apply S; inversion BUG).
      all: try (by apply C; inversion BUG).
      all: try (by apply P; inversion BUG).
      all: try (by apply R; inversion BUG).
  Qed.

  Canonical TDM_state_eqMixin := EqMixin TDM_state_eqn.
  Canonical TDM_state_eqType := Eval hnf in EqType TDM_state_t TDM_state_eqMixin.

  Global Instance ARBITER_CFG : Arbiter_configuration :=
  {
    State_t := TDM_state_t;
  }.

  (*************************************************************************************************)
  (** ARBITER IMPLEMENTATION ***********************************************************************)
  (*************************************************************************************************)

  (* Increment counter for cycle offset (with wrap-arround). *)
  Definition Next_cycle (c : Counter_t) :=
    let nextc := c.+1 < SL in
      (if nextc as X return (nextc = X -> Counter_t) then 
        fun (P : nextc = true) => Ordinal (P : nextc)
       else
        fun _ => OZCycle) Logic.eq_refl.

  (* Increment the slot counter (with wrap-arround) *)
  Definition Next_slot (s : Slot_t) (c : Counter_t) : Slot_t :=
  if (c.+1 < SL) then
    s
  else
    let nexts := s.+1 < SN in
      (if nexts as X return (nexts = X -> Slot_t) then 
        fun (P : nexts = true) => Ordinal (P : nexts)
       else
        fun _ => OZSlot) Logic.eq_refl.

  (* The set of pending requests of a slot *)
  Definition Pending_of s (P : Requests_t) := [seq r <- P | r.(Requestor) == s].

  Definition Init_state R := IDLE OZSlot OZCycle (Enqueue R [::]).

  Hint Unfold PRE_of_req : core.
  Hint Unfold ACT_of_req : core.
  Hint Unfold CAS_of_req : core.

  (* The next-state function of the arbiter *)
  Definition Next_state R (AS : TDM_state_t) : (TDM_state_t * Command_kind_t) :=
    match AS with
      | IDLE s c P => (* Currently no request is processed *)
        let P' := Enqueue R P in (* enqueue newly arriving requests *)
        let s' := Next_slot s c in (* advance slot counter (if needed) *)
        let c' := Next_cycle c in (* advance slot counter (if needed) *)
          if (c == OZCycle) then
            match Pending_of s P with (* Check if a request is pending *)
              | [::] => (* No? Continue with IDLE *)
                (IDLE s' c' P', NOP)
              | r::_ => (* Yes? Emit PRE, dequeue request, and continue with RUNNING *)
                (RUNNING s' c' (Enqueue R (Dequeue r P)) r, PRE r)
            end
          else (IDLE s' c' P', NOP)
      | RUNNING s c P r => (* Currently a request is processed *)
        let P' := Enqueue R P in (* enqueue newly arriving requests *)
        let s' := Next_slot s c in (* advance slot counter (if needed) *)
        let c' := Next_cycle c in (* advance slot counter (if needed) *)
          if c == OACT_date then (* need to emit the ACT command *)
            (RUNNING s' c' P' r, ACT r)
          else if c == OCAS_date then (* need to emit the CAS command *)
            (RUNNING s' c' P' r, (Kind_of_req r) r)
          else if c == OLastCycle then (* return to IDLE state *)
            (IDLE s' c' P', NOP)
          else 
            (RUNNING s' c' P' r, NOP)
    end.

  Global Instance TDM_implementation : Implementation_t := 
    mkImplementation Init_state Next_state.

	(* -------------------------------------------------------------- *)
	(* Helper functions *)
  Definition TDM_counter (AS : State_t) :=
    match AS with
        | IDLE _ c _      => c 
        | RUNNING _ c _ _ => c
    end.

  Definition TDM_pending (AS : State_t) :=
    match AS with
        | IDLE _ _ P      => P
        | RUNNING _ _ P _ => P
    end.

  Definition TDM_request (AS : State_t) :=
    match AS with
        | IDLE _ _ _      => None
        | RUNNING _ _ _ r => Some r
    end.

  Definition TDM_slot (AS : State_t) :=
    match AS with
        | IDLE s _ _      => s
        | RUNNING s _ _ _ => s
    end.

  Definition isRUNNING (AS : State_t) :=
    match AS with
      | IDLE _ _ _ => false
      | RUNNING _ _ _ _ => true
    end.

  #[local] Axiom Private_Mapping :
		forall t a b,
		a \in (Default_arbitrate t).(Arbiter_Commands) -> 
		b \in (Default_arbitrate t).(Arbiter_Commands) ->
		Same_Bank a b <->
    nat_of_ord (TDM_slot (Default_arbitrate b.(CDate)).(Implementation_State)) =
    nat_of_ord (TDM_slot (Default_arbitrate a.(CDate)).(Implementation_State)).

	Locate not_iff_compat.

	Lemma Private_Mapping_ : 
		forall t a b,
		a \in (Default_arbitrate t).(Arbiter_Commands) -> 
		b \in (Default_arbitrate t).(Arbiter_Commands) ->
		~ Same_Bank a b ->
		nat_of_ord (TDM_slot (Default_arbitrate b.(CDate)).(Implementation_State)) !=
    nat_of_ord (TDM_slot (Default_arbitrate a.(CDate)).(Implementation_State)).
	Proof.
		specialize Private_Mapping as PM; unfold Same_Bank in *.
		intros t a b Ha Hb; specialize (PM t a b Ha Hb).
		apply not_iff_compat in PM.
		intros; apply /eqP; apply PM; exact H.
	Qed.

	Locate not_iff_compat.

End TDM.