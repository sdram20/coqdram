module App where

import qualified TDM
import qualified Arbiter
import qualified Bank
import qualified Commands
import qualified Datatypes
import qualified Requests
import qualified Trace
import qualified Eqtype
import qualified Seq
import qualified Ssrbool
import qualified Ssrnat
import qualified Bool

import qualified ForeignRequest
import qualified ForeignCommand

import Foreign
import Foreign.Storable
import Foreign.Ptr
import Foreign.C.Types

cint2bool :: CInt -> CInt -> Datatypes.Coq_bool
cint2bool a b =
    if (a == b) then Datatypes.Coq_true
    else Datatypes.Coq_false

cint2reflect :: CInt -> CInt -> Bool.Coq_reflect
cint2reflect a b =
    if (a == b) then Bool.ReflectT
    else Bool.ReflectF

convertReq :: ForeignRequest.ForeignRequest_t -> IO (Requests.Request_t)
convertReq r = do
    case (ForeignRequest.requestType r) of
        0 -> return (Requests.Coq_mkReq
            (TDM.unsafeCoerce(ForeignRequest.requestorID r))
            (fromIntegral $ ForeignRequest.arriveTime r) 
            Requests.RD
            (fromIntegral $ ForeignRequest.bankGroup r)
            (fromIntegral $ ForeignRequest.bank r) 
            (fromIntegral $ ForeignRequest.row r))
        otherwise ->
            return (Requests.Coq_mkReq -- write case
            (TDM.unsafeCoerce(ForeignRequest.requestorID r))
            (fromIntegral $ ForeignRequest.arriveTime r) 
            Requests.WR 
            (fromIntegral $ ForeignRequest.bankGroup r)
            (fromIntegral $ ForeignRequest.bank r) 
            (fromIntegral $ ForeignRequest.row r))

convertReqList :: [ForeignRequest.ForeignRequest_t] -> IO (Datatypes.Coq_list Requests.Request_t)
convertReqList [] = do return (Datatypes.Coq_nil)
convertReqList (r:rs) = do
    coq_req <- convertReq r
    coq_req_tail <- convertReqList rs
    return (Datatypes.Coq_cons coq_req coq_req_tail)

tdm_init_state_genstate :: [ForeignRequest.ForeignRequest_t] -> IO (TDM.TDM_state_t)
tdm_init_state_genstate reqlist = do
    coq_list <- convertReqList reqlist
    -- this should be arguments eventually
    -- using values From DRAM 800E
    let bkcfg = Bank.Build_Bank_configuration 2 4 5 4 20 21 6 6 15 4 6 7 4 4 10 10 10 10 10 10
    -- let reqcfg = (TDM.unsafeCoerce CInt)
    let tdmcfg = TDM.Build_TDM_configuration 24 2
    -- calling coq
    return (TDM.coq_Init_state bkcfg tdmcfg coq_list)

foreign export ccall tdm_init_state_wrapper :: CInt -> ForeignRequest.PtrRequest -> IO (StablePtr TDM.TDM_state_t)
tdm_init_state_wrapper :: CInt -> ForeignRequest.PtrRequest -> IO (StablePtr TDM.TDM_state_t)
tdm_init_state_wrapper size ptr = 
    if (size == 0) then do
        reqlist <- return []
        state <- tdm_init_state_genstate reqlist
        newStablePtr state
    else do
        reqlist <- peekArray (fromIntegral size) ptr
        state <- tdm_init_state_genstate reqlist
        newStablePtr state

tdm_next_state_genstate :: [ForeignRequest.ForeignRequest_t] -> TDM.TDM_state_t -> IO (TDM.TDM_state_t)
tdm_next_state_genstate reqlist as = do
    coq_list <- convertReqList reqlist
    -- this should be arguments eventually, 
    let bkcfg = Bank.Build_Bank_configuration 2 4 5 4 20 21 6 6 15 4 6 7 4 4 10 10 10 10 10 10
    -- let reqcfg = TDM.unsafeCoerce ((Eqtype.Equality__Mixin (cint2bool) cint2reflect))
    let tdmcfg = TDM.Build_TDM_configuration 24 2
    -- calling coq
    return (TDM.coq_Next_state bkcfg tdmcfg coq_list as)

foreign export ccall tdm_next_state_wrapper :: CInt -> ForeignRequest.PtrRequest -> StablePtr TDM.TDM_state_t -> IO (StablePtr TDM.TDM_state_t)
tdm_next_state_wrapper :: CInt -> ForeignRequest.PtrRequest -> StablePtr TDM.TDM_state_t -> IO (StablePtr TDM.TDM_state_t)
tdm_next_state_wrapper size ptr as = do
    case size of
        0 -> do
            reqlist <- return []
            oldstate <- deRefStablePtr as
            newstate <- tdm_next_state_genstate reqlist oldstate
            newStablePtr newstate
        otherwise -> do
            reqlist <- peekArray (fromIntegral size) ptr
            oldstate <- deRefStablePtr as
            newstate <- tdm_next_state_genstate reqlist oldstate
            newStablePtr newstate
---------------------------------------------------------------

--------------------- command interface -----------------------
coqcmd2foreigncmd :: Commands.Command_t -> ForeignCommand.ForeignCommand_t
coqcmd2foreigncmd (Commands.Coq_mkCmd cdate kind (Requests.Coq_mkReq requestor rdate typ bank row)) =
    let cmd_type = case kind of
            Commands.ACT -> case typ of
                    Requests.RD -> 4
                    Requests.WR -> 5
            Commands.PRE -> 7
            Commands.CRD -> 0
            Commands.CWR -> 2 in
    ForeignCommand.ForeignCommand_t 
        cmd_type                            -- busPacketType
        (TDM.unsafeCoerce(requestor))      -- requestorID
        0                                   -- @
        0                                   -- column
        (fromIntegral row)                  -- row
        0                                   -- subArray
        (fromIntegral bank)                 -- bank
        0                                   -- bankGroup
        0                                   -- rank
        (castPtrToStablePtr nullPtr)        -- ddata
        (fromIntegral rdate)                -- arriveTime
        0                                   -- postCommand
        0                                   -- postCounter
        0                                   -- addressMap

nullcmd :: Commands.Command_t
nullcmd = 
    let req = Requests.Coq_mkReq (TDM.unsafeCoerce(10)) (-1) (Requests.RD) 0 0 in
    Commands.Coq_mkCmd 0 Commands.ACT req

foreign export ccall getcommand :: StablePtr Arbiter.Arbiter_state_t -> IO (ForeignCommand.PtrCommand)
getcommand :: StablePtr Arbiter.Arbiter_state_t -> IO (ForeignCommand.PtrCommand)
getcommand state_ptr = do
    (Arbiter.Coq_mkArbiterState cmds time internal_state) <- deRefStablePtr state_ptr
    let last_cmd = case cmds of
            Datatypes.Coq_nil -> coqcmd2foreigncmd nullcmd
            Datatypes.Coq_cons (Commands.Coq_mkCmd cdate kind req) tl -> do
                if (cdate == time) then coqcmd2foreigncmd (Commands.Coq_mkCmd cdate kind req)
                else coqcmd2foreigncmd nullcmd
    ptr <- malloc :: IO(Ptr ForeignCommand.ForeignCommand_t)
    poke ptr last_cmd
    return ptr
---------------------------------------------------------------

---------------------------------------------------------------
-- output functions
---------------------------------------------------------------
req2string :: Requests.Request_t -> String
req2string (Requests.Coq_mkReq requestor date typ bank row) =
    let reqtype = case typ of
            Requests.RD -> "RD"
            Requests.WR -> "WR" in
    "requestor: " ++ show(TDM.unsafeCoerce(requestor) :: CInt) ++ 
    ", date: " ++ show(date) ++ 
    ", type: " ++ reqtype ++ 
    ", bank: " ++ show(bank) ++ 
    ", row:  " ++ show(row)

print_command :: Commands.Command_t -> IO()
print_command (Commands.Coq_mkCmd date kind req) = do
    case kind of
        Commands.ACT -> 
            putStr $ show(date) ++ ", ACT. Request: " ++
            req2string (req)
        Commands.PRE ->
            putStr $ show(date) ++ ", PRE. Request: " ++
            req2string (req)
        Commands.CRD ->
            putStr $ show(date) ++ ", RD. Request: " ++
            req2string (req)
        Commands.CWR ->
            putStr $ show(date) ++ ", WR. Request: " ++
            req2string (req)

print_cmd_list :: Datatypes.Coq_list Commands.Command_t -> IO()
print_cmd_list Datatypes.Coq_nil = putStrLn "[::]"
print_cmd_list (Datatypes.Coq_cons hd tail) = do
    putStr "["
    print_command hd
    putStr "],"
    print_cmd_list tail

print_req_list :: Datatypes.Coq_list Requests.Request_t -> IO()
print_req_list Datatypes.Coq_nil = putStrLn "[::]"
print_req_list (Datatypes.Coq_cons hd tail) = do
    putStr "["
    putStr $ req2string hd
    putStr "],"
    print_req_list tail

foreign export ccall print_state :: StablePtr Arbiter.Arbiter_state_t -> IO()
print_state :: StablePtr Arbiter.Arbiter_state_t -> IO()
print_state ptr = do
    (Arbiter.Coq_mkArbiterState cmds time internal_state) <- deRefStablePtr ptr
    putStrLn "------------------- printing state -------------------"
    print_cmd_list (cmds)
    print $ "Time: " ++ show (time)
    let fifo_state = (TDM.unsafeCoerce(internal_state) :: TDM.TDM_state_t)
    case fifo_state of
        TDM.IDLE s c p -> do
            putStrLn $ "IDLE, slot: " ++ show(TDM.unsafeCoerce(s) :: Int)
            putStrLn $ "Counter: " ++ show(c)
            putStrLn $ "Pending List: "
            print_req_list p
        TDM.RUNNING s c p r -> do
            putStrLn $ "IDLE, slot: " ++ show(TDM.unsafeCoerce(s) :: Int)
            putStrLn $ "Counter: " ++ show(c)
            putStrLn $ "Request being currently served: " ++ req2string r           
            putStrLn $ "Pending List: "
            print_req_list p
    putStrLn "------------------------------------------------------"
---------------------------------------------------------------


    

