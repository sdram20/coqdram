#ifndef FOREIGNCOMMAND_H
#define FOREIGNCOMMAND_H

#include <stdint.h>
#include <stdbool.h>

typedef enum 
{
	RD,
	RDA,
	WR,
	WRA, // CAS access
	ACT_R,
	ACT_W,
	ACT,
	PRE,
	PREA, // Open/Close operation - ACT_R and ACT_W are basically same but it was differentiated since it could be useful in some controllers.
	REF,
	REFPB, // Refresh
	PDE,
	PDX,
	SRE,
	SRX, // Power Related Command
	DATA
} CoqBusPacketType;

typedef struct{
	unsigned busPacketType;
	unsigned requestorID;
	unsigned address;
	unsigned column;
	unsigned row;
	unsigned subArray;
	unsigned bank;
	unsigned bankGroup;
	unsigned rank;
	void *ddata;
	unsigned arriveTime;
	// had to convert from postCommnad
	bool postCommand;
	unsigned postCounter;
	unsigned int addressMap[4];
} ForeignCommand_t;

#endif /* REQUEST_H */
