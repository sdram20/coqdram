#include <iostream>
#include <stdlib.h>
#include <errno.h>   // for errno
#include <limits.h>  // for INT_MAX, INT_MIN

#include "MCsimRequest.h"

#include "App_stub.h"

#include "ForeignRequest_t.h"
#include "ForeignCommand_t.h"

void * state = nullptr;

using namespace std;

#define PRINT(str) std::cerr << str << std::endl;

void printcommand(ForeignCommand_t * cmd){
	PRINT ("busPacketType: " << cmd->busPacketType);
	PRINT ("requestorID: " << cmd->requestorID);
	PRINT ("address: " << cmd->address);
	PRINT ("column: " << cmd->column);
	PRINT ("row: " << cmd->row);
	PRINT ("subArray: " << cmd->subArray);
	PRINT ("bank: " << cmd->bank);
	PRINT ("bankGroup: " << cmd->bankGroup);
	PRINT ("ddata: " << cmd->ddata); // this is also a null pointer
	PRINT ("arriveTime: " << cmd->arriveTime);
	PRINT ("postCommand: " << cmd->postCommand);
	PRINT ("postCounter: " << cmd->postCounter);
	PRINT ("addressMap: " << cmd->addressMap); // this is also a null pointer
}

int main(int argc, char *argv[]){

	if(argc != 2){
		PRINT ("Please provide the number of states to generate!");
		return 0;
	}
	// Getting the number of states to generate as a program arguments
	char *p;
	int cycles;
	errno = 0;
	long num = strtol(argv[1], &p, 10);
	if (errno != 0 || *p != '\0' || num > INT_MAX || num < INT_MIN) {
    } else {
        cycles = num;
    }

	ForeignCommand_t * cmd;
	unsigned long long * data = (unsigned long long *) malloc(sizeof(unsigned long long));
	* data = 0xDEADBEEF;

	/* This is the C-equivalent request representation */
	ForeignRequest_t req1 = {
		DATA_WRITE,	// type
		0,			// id
		10,			// size
		0x0004,		// @
		data,		// data
		0,			// arriveTime
		1,			// returnTime
		0,			// rank
		0,			// bankgroup
		0,			// bank
		0,			// subArray
		0,			// row
		0,			// col
		{0,1,0,1}	// @ map
	};

	ForeignRequest_t req2 = {DATA_READ,1,20,0x0008,data,10,20,0,0,1,0,50,0,{1,0,1,0}};
	ForeignRequest_t req3 = {DATA_READ,0,40,0x000c,data,30,40,0,0,5,0,10,0,{1,0,1,0}};
	ForeignRequest_t req4 = {DATA_WRITE,1,10,0x0010,data,40,50,0,0,5,0,10,0,{1,0,1,0}};

	ForeignRequest_t reqlist[] = {req1,req2,req3,req4};
	// ForeignRequest_t reqlist2[] = {req3,req4};

	PRINT ("Initializing haskell runtime");
	hs_init(&argc,&argv);
	PRINT ("Calling haskell function");

	// Calling the init state function
	state = tdm_init_state_wrapper(4,&reqlist[0]);
	print_state(state);
	// cmd = (ForeignCommand_t *) getcommand(state);
	// if (cmd->arriveTime == -1){
	// 	PRINT("This is a nop!");
	// } else {
	// 	printcommand(cmd);
	// }

	// Calling the next state function
	for (size_t i = 1; i < cycles; i++)
	{
		state = tdm_next_state_wrapper(0,&reqlist[0],state);
		print_state(state);
		// cmd = (ForeignCommand_t *) getcommand(state);
		// if (cmd->arriveTime == -1){
		// 	PRINT("This is a nop!");
		// } else {
		// 	printcommand(cmd);
		// }
	}
	PRINT ("Return from haskell function");
	hs_exit();
	PRINT ("Finishing haskell runtime");
}