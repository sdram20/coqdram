Set Warnings "-notation-overridden,-parsing".
From sdram Require Export TDM.
From Coq Require Extraction.
Require Import Arith.
Require Import ExtrHaskellNatNum.

Extraction Language Haskell.

Extract Inductive nat => "Prelude.Int" [ "0" "Prelude.succ" ]
  "(\fO fS n -> if n Prelude.== 0 then fO () else fS (n Prelude.- 1))".

Cd "haskell_gencode_tdm".
Separate Extraction TDM.
Cd "..".
