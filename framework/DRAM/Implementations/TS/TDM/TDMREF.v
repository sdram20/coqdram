Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From DRAM Require Export ImplementationInterface.
From mathcomp Require Export fintype div ssrZ zify.
From Coq Require Import Program.

Section TDMREF.

	(* Refresh strategy 			 : Halting with minimum slots *)
	(* Page-policy 						 : Closed (PRE-ACT-CAS) *)
	(* Bank-mapping assumption : Adjacent slots target different banks *)
	(* Refresh cycle					 : non-optimised (with PREA) *)

  Context {SYS_CFG : System_configuration}.

	Definition PRE_date := 0.
  Definition ACT_date := PRE_date + T_RP.
	Definition CAS_date := ACT_date + T_RCD.
	
	Definition REF_slot 		:= T_REFI.
	Definition REF_date 		:= T_REFI.-1.
	Definition PREA_date 		:= REF_date - T_RP.
	Definition ENDREF_date  := T_RFC.-1.

	(* Specific constraints for this algorithm *)
  Class TDMREF_configuration :=
  {
		SN : nat;
    SN_one  : 1 < SN;
	
		SL : nat;
    SL_pos  : 0 < SL;

		(* Enough time to issue PRE-ACT-CAS sequence, plus one extra slot *)
		SL_CASS : CAS_date.+1 < SL;

		(* T_RCD is respected within the slot *)
		(* T_RP is respected within the slot *)
		(* T_RC is respected through an axiom *)
		(* T_RC_ACT_to_ACT_SB  		: T_RC < SL; *)
		(* T_RAS is respected through an axiom *)
		(* T_RAS_ACT_to_PRE_SB			: T_RAS <= SL + T_RCD + (SL - SL_min) + 1; *)
		(* T_WL can't be modelled since we don't model the bus *)
		(* T_WL can't be modelled since we don't model the bus *)
		(* T_RTP is respected through an axiom *)
		(* T_RTP_RD_to_PRE_SB			: T_RTP <= SL + (SL - SL_min) + 1; *)
		(* T_WR (actually WR to PRE) can be derived from SL_min (see below) *)

	}.

	Context {TDMREF_CFG : TDMREF_configuration}.

	(* -------------------- Additional Lemmas about SN ------------------------ *)
	Lemma SN_pos : SN > 0.
		specialize SN_one; lia.
	Qed.

	(* --------------------- Additional Lemmas about PREA_date ---------------- *)
	
	Lemma REF_slot_GT_PREA_date : PREA_date < REF_slot.
		unfold PREA_date, REF_date, REF_slot; specialize T_RP_pos; specialize T_REFI_pos; lia.
	Qed.

	Lemma REF_slot_GT_PREA_date_SL : PREA_date - SL < REF_slot.
		unfold PREA_date, REF_date, REF_slot; specialize T_REFI_pos; specialize T_RP_pos; lia.
	Qed.

	Lemma REF_slot_GT_ENDREF_date	: ENDREF_date < REF_slot.
		unfold ENDREF_date, REF_slot; specialize T_REFI_GT_T_RFC; lia.
	Qed.

	Lemma REF_slot_GT_REF_date : REF_date < REF_slot.
		unfold REF_slot, REF_date; specialize T_REFI_pos; lia.
	Qed.

	(* ---------------------- Additional Lemmas about SL ---------------------- *)
	Lemma SL_CAS : CAS_date < SL.
		specialize SL_CASS; lia.
	Qed.

	Lemma SL_PRE : PRE_date < SL.
		specialize SL_CASS; unfold PRE_date, CAS_date, ACT_date, PRE_date, REF_date; lia.
	Qed.

	Lemma SL_ACT : ACT_date < SL.
		specialize SL_CASS; unfold CAS_date, ACT_date; lia.
	Qed.
  
  Lemma Last_cycle: SL.-1 < SL.
    rewrite ltn_predL; apply SL_pos.
  Qed.
  
  Lemma SL_one: 1 < SL.
		specialize SL_CASS; lia.
	Qed.

  Lemma SL_ACTS: ACT_date.+1 < SL.
    specialize SL_CASS; unfold CAS_date, ACT_date; lia.
	Qed.

	(* ------------------------ Additional Lemmas about ACT_date & CAS_date ---- *)
  Lemma ACT_pos: 0 < ACT_date.
		unfold ACT_date; specialize T_RP_pos; lia.
	Qed.

	Lemma CAS_pos: 0 < CAS_date.
		unfold CAS_date; specialize T_RCD_pos; lia.
	Qed.

	(* ------------------------ Controller implementation ---------------------- *)

	(* Normal counter *)
	Definition Counter_t := ordinal SL.
  Definition OZCycle   := Ordinal SL_pos.
	Definition OPRE_date := Ordinal SL_PRE.
  Definition OACT_date := Ordinal SL_ACT.
  Definition OCAS_date := Ordinal SL_CAS.
  Definition OLastCycle := Ordinal Last_cycle.

	Definition Next_cycle (c : Counter_t) :=
		let nextc := c.+1 < SL in
			(if nextc as X return (nextc = X -> Counter_t) then 
				fun (P : nextc = true) => Ordinal (P : nextc)
				else
				fun _ => OZCycle) Logic.eq_refl.

	(* Slot counter *)
  Definition Slot_t := ordinal_eqType SN.
	Definition OZSlot := Ordinal SN_pos.

	Definition Next_slot (s : Slot_t) (c : Counter_t) : Slot_t :=
		if (c.+1 < SL) then s
		else
			let nexts := s.+1 < SN in
				(if nexts as X return (nexts = X -> Slot_t) then 
					fun (P : nexts = true) => Ordinal (P : nexts)
					else
					fun _ => OZSlot) Logic.eq_refl.

	(* Refresh counter *)
	Definition Counter_ref_t := ordinal REF_slot.

	Definition OCycle0REF := Ordinal T_REFI_pos.
	Definition OEnough		:= Ordinal REF_slot_GT_PREA_date_SL.
	Definition OPREA_date := Ordinal REF_slot_GT_PREA_date.
	Definition OREF_date  := Ordinal REF_slot_GT_REF_date.
	Definition OENDREF_date := Ordinal REF_slot_GT_ENDREF_date.
	
	Definition Next_cycle_ref (c : Counter_ref_t) : Counter_ref_t := 
		let nextcref := c.+1 < REF_slot in
			(if nextcref as X return (nextcref = X -> Counter_ref_t) then
				(fun (P : nextcref = true) => Ordinal (P : nextcref))
			else (fun _ => OCycle0REF)) Logic.eq_refl.

  (* Requestor configuration *)
	Instance REQUESTOR_CFG : Requestor_configuration := {
    Requestor_t := Slot_t
  }.

  Context {AF : Arrival_function_t}.

  Inductive TDM_state_t :=
    | IDLE    : Slot_t -> Counter_t -> Counter_ref_t -> Requests_t -> TDM_state_t 
    | RUNNING : Slot_t -> Counter_t -> Counter_ref_t -> Requests_t -> Request_t -> TDM_state_t
		| REFRESH : Slot_t -> Counter_ref_t -> Requests_t -> TDM_state_t.

  Global Instance ARBITER_CFG : Arbiter_configuration := {
    State_t := TDM_state_t;
  }.

	(* Helper functions *)
  Definition Enqueue (R P : Requests_t) := P ++ R.
  Definition Dequeue r (P : Requests_t) := rem r P.
  Definition Pending_of s (P : Requests_t) := [seq r <- P | r.(Requestor) == s].
	
  Definition nullreq := 
    mkReq OZSlot 0 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.

	(* State machine definition *)
	Definition Init_state R := IDLE OZSlot OZCycle OCycle0REF (Enqueue R [::]).

  Definition Next_state R (AS : TDM_state_t) : (TDM_state_t * (Command_kind_t * Request_t)) :=
    match AS return (TDM_state_t * (Command_kind_t * Request_t)) with
      | IDLE s c cref P =>
        let P' := Enqueue R P in
        let s' := Next_slot s c in
        let c' := Next_cycle c in
				let cref' := Next_cycle_ref cref in
				if (c == OZCycle) then ( (* only scheduel at the beginning of the slot *)
					if (cref < OEnough) then ( (* there is enough time to process a request *)
						match Pending_of s P with
						| [::] => (IDLE s' c' cref' P', (NOP,nullreq))
						| r :: _ => (RUNNING s' c' cref' (Enqueue R (Dequeue r P)) r, (PRE,nullreq))
						end
					) else ( (* at 0 but not enough time to start a new request, go to REFRESH,
						block counters *)
						(REFRESH s cref' P', (NOP,nullreq))
					)
				) else (IDLE s' c' cref' P', (NOP,nullreq))
			| RUNNING s c cref P r =>
				let P' := Enqueue R P in
				let s' := Next_slot s c in
				let c' := Next_cycle c in
				let cref' := Next_cycle_ref cref in
				if (c == OACT_date) then (RUNNING s' c' cref' P' r, (ACT, r))
        else if (c == OCAS_date) then (RUNNING s' c' cref' P' r, (Kind_of_req r, r))
        else if (c == OLastCycle) then (IDLE s' c' cref' P', (NOP, nullreq))
        else (RUNNING s' c' cref' P' r, (NOP, nullreq))
			| REFRESH s cref P =>
				let P' := Enqueue R P in
				let cref' := Next_cycle_ref cref in
				if (cref == OPREA_date) then (REFRESH s cref' P', (PREA,nullreq))
				else if (cref == OREF_date) then (* the counter resets to 0 at this location *)
					(REFRESH s cref' P', (REF,nullreq))
				else if (cref == OENDREF_date) then
					(IDLE s OZCycle cref' P', (NOP,nullreq))
				else (REFRESH s cref' P', (NOP,nullreq))
		end.

  Global Instance TDMREF_implementation : Implementation_t := 
    mkImplementation Init_state Next_state.

	Program Definition TDMREF_arbitrate t :=
		mkTrace 
		(Default_arbitrate t).(Arbiter_Commands)
		(Default_arbitrate t).(Arbiter_Time)
		_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _.
	Admit Obligations.

	Program Instance TDMREF_arbiter : Arbiter_t :=
		mkArbiter AF TDMREF_arbitrate _ _.
	Admit Obligations.

End TDMREF.

Section TDMREF_sim.

	(* DDR4-1600J *)
	(* Density : 8Gb *)
	(* Page Size : 1K (1G8 adressing) *)
	(* 1tCK write preamble mode *)
	(* 1tCK read preamble mode *)
	(* AL = 0 *)
	(* 1X Refresh mode *)
	Program Instance SYS_CFG : System_configuration :=
	{
		BANKGROUPS := 4;
		BANKS := 16;

		(* [Figure 105] always the same *)
		T_BURST := 4; (* INTRA/INTER *)

		(* [Figure 105] 1tCK write preamble mode, AL = 0
			-- independent of BC4 or BL8 
			-- depends on write preamble mode (1tCK or 2tCK)
			-- depends on AL (what is AL ?) *)
		T_WL    := 9;
		
		(* ACT to ACT, same and different bank groups, exclusevely inter-bank
			-- depends on page size, which depends on device density (total size) and configuration
			see (https://www.micron.com/-/media/client/global/documents/products/data-sheet/dram/ddr4/16gb_ddr4_sdram.pdf)
		*)
		T_RRD_s := 4; (* [Page 189] Max (4nCK, 5 ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 5 -> 4 cycles at 800MHz*)
		T_RRD_l := 5; (* [Page 189] Max (4nCK, 6 ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 6 -> 4.8 ~ 5 cyles at 800MHz *)

		(* Intra/inter-bank: Four ACT delay window *)
		T_FAW := 20; (* [Page 189] 25ns *)

		(* ACT to ACT, intra bank *) 
		T_RC  := 38; (* [Page 163] 47.5 ns *)

		(* PRE to ACT, intra bank *)
		T_RP  := 10; (* [Page 163] 12.5 ns *)

		(* ACT to CAS, intra bank *)
		T_RCD := 10; (* [Page 163] 12.5 ns *)

		(* ACT to PRE, intra bank *)
		T_RAS := 28; (* [Page 163] 35.0 ns *)

		(* RD to PRE, intra bank *)
		T_RTP := 6; (* [Page 189] Max (4nCK, 7.5ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 7 -> 5.6 ~ 6 cycles at 800MHz *)

		(* Write recovery time: R data to PRE *)
		T_WR  := 12; (* [Page 189] 15 ns *)

		(* Read to write, inter and intra bank *)
		T_RTW := 8; (* [Page 94] T_RTW = RL + BL/2 - WL + 2tCK = 11 + 8/2 - 9 + 2 = 8 *)

		(* End of write operation to read *)
		T_WTR_s := 2; (* [Page 189] Max (2nCK,2.5ns) -> 2nCK = 1/800MHz * 2 = 2.5ns -> max is 2.5ns -> 2 cycles at 800MHz *)
		T_WTR_l := 6; (* [Page 189] Nax (4nCK,7.5ns) -> 4nCK = 5ns -> max is 7.5ns -> 6 cycles at 800MHz *)

		T_CCD_s := 4;
		T_CCD_l := 5;
		
		T_REFI := 6240; (* 6240 [Page 36] Refresh average time*)
		T_RFC := 280; (* 280 [Page 36] *)
	}.

	Program Instance TDMREF_CFG : TDMREF_configuration :=
	{
		SN := 2;
		SL := 24
	}.
	
	(* Program Instance SYS_CFG : System_configuration :=
	{
		BANKGROUPS := 1;
		BANKS := 8;

		T_BURST := 1;
		T_WL    := 1;
		T_RRD_s := 1;
		T_RRD_l := 2;
		T_FAW := 10;
		T_RC  := 3;
		T_RP  := 3;
		T_RCD := 2;
		T_RAS := 1;
		T_RTP := 1;
		T_WR  := 1;
		T_RTW := 1;
		T_WTR_s := 1;
		T_WTR_l := 2;
		T_CCD_s := 1;
		T_CCD_l := 2;
		
		T_REFI := 30;
		T_RFC := 8;
	}.

	Program Instance TDMREF_CFG : TDMREF_configuration :=
	{
		SN := 2;
		SL := 8;
	}.
	 *)
	Existing Instance REQUESTOR_CFG.
	Existing Instance TDMREF_implementation.
	Existing Instance TDMREF_arbiter.

	Definition Slot1 := Ordinal SN_one.
	
	Program Definition ReqA0 := mkReq 
		OZSlot 2 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 5.

	Program Definition ReqB0 := mkReq (* 3 instead of 14 *)
		Slot1 4 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 10.

	Program Definition ReqA1 := mkReq 
		OZSlot 20 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 5.

	Program Definition ReqB1 := mkReq (* 3 instead of 14 *)
		Slot1 22 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 10.

	Definition test_req_seq := [:: ReqA0;ReqB0;ReqA1;ReqB1].

	Program Instance AF : Arrival_function_t := Default_arrival_function_t test_req_seq.

	(* Compute (Default_arbitrate 39).(Implementation_State). *)
	Compute seq.map (fun x => (x.(CDate),x.(CKind))) (Arbitrate 30).(Commands).

End TDMREF_sim.