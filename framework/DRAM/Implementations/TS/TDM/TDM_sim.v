Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From mathcomp Require Export fintype div.
Require Import Program.
From DRAM Require Export TDM_proofs.

Section Test.

	Program Instance SYS_CFG : System_configuration := {
		BANKGROUPS := 1;
		BANKS := 3;

		T_BURST := 1;
		T_WL    := 1;

		T_RRD_s := 1;
		T_RRD_l := 3;

		T_FAW := 20;

		T_RC  := 3;
		T_RP  := 4;
		T_RCD := 2;
		T_RAS := 4;
		T_RTP := 4;
		T_WR  := 1;

		T_RTW := 10;
		T_WTR_s := 1;
		T_WTR_l := 10;
		T_CCD_s := 1;
		T_CCD_l := 12;
	}.

  Program Instance TDM_CFG : TDM_configuration :=
  {
    SL := 59;
    SN := 3
  }.

  Existing Instance REQESTOR_CFG.
	Existing Instance TDM_implementation.
	Existing Instance TDM_arbiter.

  Program Definition Req1 := mkReq (Ordinal (_ : 0 < SN)) 0 RD 0 0 0.
  Program Definition Req2 := mkReq (Ordinal (_ : 1 < SN)) 4 WR 0 1 0.

  Definition Input := [:: Req2; Req1].

  Program Instance AF : Arrival_function_t := 
    Default_arrival_function_t Input.
  
  (* Compute Arbitrate 300. *)
End Test.