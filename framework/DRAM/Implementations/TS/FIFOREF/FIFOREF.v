Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.
Set Printing Coercions.

From mathcomp Require Import ssrnat fintype div ssrZ zify ring.
From DRAM Require Export ImplementationInterface.
From Coq Require Import Program NArith.

Section FIFOREF.

  Context {SYS_CFG : System_configuration}.

  Instance REQESTOR_CFG : Requestor_configuration := {
    Requestor_t := unit_eqType
  }.

  Definition ACT_date := T_RP.-1.
  Definition CAS_date := ACT_date + T_RCD.

	Definition PREA_date := (T_REFI - T_RP).
	Definition REF_date := T_RP.-1.
	Definition END_REF_date := REF_date + T_RFC.
	(* Just an alias for PRE_date *)
	(* Definition WAIT_REF := PREA_date. *)

	(* axiom should be here *)
  Class FIFO_configuration :=
  {
		(* The slot length *)
    WAIT : nat;
		(* Axioms for hardware compatibility *)
		WAIT_PW_2 :  Nat.pow 2 (Nat.log2 WAIT) = WAIT;
		WAIT_PW_2_N : N.pow 2 (N.of_nat (Nat.log2 WAIT)) = N.of_nat WAIT;

		(* Basic constraints *)
    T_RP_GT_ONE     : 1 < T_RP;
    T_RCD_GT_ONE    : 1 < T_RCD;
    T_RTP_GT_ONE    : 1 < T_RTP;
    WAIT_gt_one     : 1 < WAIT;
    WAIT_pos        : 0 < WAIT;
    WAIT_ACT        : ACT_date < WAIT;
    WAIT_CAS        : CAS_date < WAIT;

    (* Length of the minimum slot (write request): T_RP + T_RCD + T_WL + T_BURST + T_WR *)
    WAIT_END_WRITE  : CAS_date + T_WR + T_WL + T_BURST < WAIT;

    (* Length of the minimum slot (read request): T_RP + T_RCD + T_RTP *)
    WAIT_END_READ   : CAS_date + T_RTP < WAIT;

		(* WAIT constraints for fitting the slot *)
    RC_WAIT         : T_RC  < WAIT;
    CCD_WAIT        : T_CCD_l < WAIT;
    RRD_WAIT        : T_RRD_l < WAIT;
    RTW_WAIT        : T_RTW < WAIT;
    RAS_WAIT        : T_RP + T_RAS < WAIT;
    WTP_WAIT        : T_WR + T_WL + T_BURST < WAIT;
    WTR_WAIT        : T_WTR_l + T_WL + T_BURST < WAIT;
    FAW_WAIT        : T_FAW < WAIT + WAIT + WAIT;

		(* WAIT_REF is the next power of two greather than T_REFI - T_RP *)
		(* Not optimal but necessary for hardware compatibility *)
		(* WAIT_REF 						: nat; *)
		(* WAIT_REF_val				: WAIT_REF = PREA_date; *)
		(* PREA_date_pos 				: 0 < PREA_date ; *)
		PREA_date_gt_one 			: 1 < PREA_date ;
		PREA_date_REF_date		: REF_date < PREA_date ;
		PREA_date_ENDREF_date	: END_REF_date < PREA_date ;
		(* PREA_date_TRP_TRFC 	  : T_RP.-1 + T_RFC < PREA_date ; *)
		
		(* Hardware compatibility : Workaround *)
		PREA_date_PW_2 : Nat.pow 2 (Nat.log2 PREA_date) = PREA_date;
		PREA_date_PW_2_N : N.pow 2 (N.of_nat (Nat.log2 PREA_date)) = N.of_nat PREA_date
  }.

  Context {FIFOREF_CFG : FIFO_configuration}.
	
	Lemma PREA_date_pos : 0 < PREA_date.
	Proof.
		specialize PREA_date_gt_one; lia.
	Qed.
  
	Context {AF : Arrival_function_t}.

  Definition Counter_t := ordinal WAIT.
	Definition Counter_ref_t := ordinal PREA_date.

  Inductive FIFOREF_state_t :=
    | IDLE    : Counter_t -> Counter_ref_t -> Requests_t -> FIFOREF_state_t
    | RUNNING : Counter_t -> Counter_ref_t -> Requests_t -> Request_t -> FIFOREF_state_t
		| REFRESHING : Counter_ref_t -> Requests_t -> FIFOREF_state_t.

  Instance ARBITER_CFG : Arbiter_configuration := {
    State_t := FIFOREF_state_t;
  }.

  Definition OCycle0 := Ordinal WAIT_pos.
  Definition OACT_date := Ordinal WAIT_ACT.
  Definition OCAS_date := Ordinal WAIT_CAS.
	
  (* Increment counter for cycle ofset (with wrap-arround). *)
  Definition Next_cycle (c : Counter_t) : Counter_t :=
    let nextc := c.+1 < WAIT in
      (if nextc as X return (nextc = X -> Counter_t) then 
        fun (P : nextc = true) => Ordinal (P : nextc)
       else
        fun _ => OCycle0) Logic.eq_refl.

	Definition OCycle0REF := Ordinal PREA_date_pos.

	(* Lemma REF_Ord :
		REF_date < PREA_date.
	Proof.
		unfold REF_date, PREA_date.
		specialize WAIT_REF_TRP_TRFC as H.
		unfold REF_date, WAIT_REF in *.
		apply ltn_trans with (n := T_RP.-1 + T_RFC); [ | done].
		apply nat_ltn_add; by rewrite T_RFC_pos.
	Qed. *)

	(* Lemma ENDREF_Ord :
		END_REF_date < WAIT_REF.
	Proof.
		unfold END_REF_date,WAIT_REF,REF_date.
		exact WAIT_REF_TRP_TRFC.
	Qed. *)

	Definition OREF_date := Ordinal PREA_date_REF_date. 
	Definition OENDREF_date := Ordinal PREA_date_ENDREF_date.

	Definition Next_cycle_ref (c : Counter_ref_t) : Counter_ref_t := 
		let nextcref := c.+1 < PREA_date in
			(if nextcref as X return (nextcref = X -> Counter_ref_t) then
				(fun (P : nextcref = true) => Ordinal (P : nextcref))
			else (fun _ => OCycle0REF)) Logic.eq_refl.

  (* Definition Enqueue (R : Request_t) (P : Requests_t) := P ++ [:: R]. *)
  (* Definition Dequeue r (P : Requests_t) := rem r P. *)

	Definition Init_state R := IDLE OCycle0 OCycle0REF R.

  Definition nullreq := mkReq tt 0 RD 
		(mkAddress (Nat_to_bankgroup 0) (Nat_to_bank 0) 0).

	(* check if there's enough time to proc another req *)
	Definition Next_state (R : Requests_t) (AS : FIFOREF_state_t) 
		: (FIFOREF_state_t * Command_kind_t ) :=
    match AS return FIFOREF_state_t * Command_kind_t with
      | IDLE c cref P =>	
				let P' := Enqueue R P in
				let c' := Next_cycle c in
				let cref' := Next_cycle_ref cref in
				if (nat_of_ord cref == (PREA_date - 1)) then (REFRESHING OCycle0REF P', PREA)
				else if (cref + WAIT < PREA_date) then
					match P with
						| [::]    => (IDLE c' cref' P', NOP)
						| r :: PP => (RUNNING OCycle0 cref' (Enqueue R (Dequeue r P)) r, (PRE r))
					end
				else (IDLE c' cref' P', NOP) (* Cases 3a/3b *)
      | RUNNING c cref P r =>
        let P' := Enqueue R P in
        let c' := Next_cycle c in
				let cref' := Next_cycle_ref cref in
        if nat_of_ord c == OACT_date then (RUNNING c' cref' P' r, (ACT r))
        else if nat_of_ord c == OCAS_date then (RUNNING c' cref' P' r, ((Kind_of_req r) r))
        else if nat_of_ord c == WAIT.-1 then (IDLE OCycle0 cref' P', NOP)
        else (RUNNING c' cref' P' r, NOP)
			| REFRESHING cref P =>
				let P' := Enqueue R P in
				let cref' := Next_cycle_ref cref in
				if (nat_of_ord cref == OREF_date) then 
					(REFRESHING cref' P', REF)
				else if (cref == OENDREF_date) then 
					(IDLE OCycle0 OCycle0REF P', NOP)
				else 
					(REFRESHING cref' P', NOP)
    end.

  #[global] Instance FIFOREF_implementation : Implementation_t := 
    mkImplementation Init_state Next_state.

End FIFOREF.