Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From Coq Require Export Program.
From DRAM Require Export ImplementationInterface2.

(* Should choose which implementation to do *)

Section FIFOImp2.

	Context {SYS_CFG : System_configuration}.
	 
	Instance REQESTOR_CFG : Requestor_configuration := 
  {
    Requestor_t := unit_eqType
  }.

	Context {AF : Arrival_function_t}.

	(* first one that is ready ? if command not ready do not choose (commit) yet *)
	Definition FIFOSelect (P : Requests_t) (SS : SystemState_t) := 
		ohead P.
	
	Global Instance FIFOScheduler : Scheduler_t := {	
		Select := FIFOSelect
	}.

End FIFOImp2.

