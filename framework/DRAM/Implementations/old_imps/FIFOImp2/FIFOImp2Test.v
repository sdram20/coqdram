Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From DRAM Require Import FIFOImp2.
From Coq Require Import VectorDef.

Import VectorNotations.

Section FIFOImp2test.

Program Instance SYS_CFG : System_configuration :=
  {
    BANKGROUPS := 1;
    BANKS := 4;

    T_BURST := 4;
    T_WL    := 2;

    T_RRD_s := 2;
    T_RRD_l := 4;

    T_FAW := 20;

    T_RC  := 3;
    T_RP  := 3;
    T_RCD := 3;
    T_RAS := 3;
    T_RTP := 3;
    T_WR  := 3;

    T_RTW := 5;
    T_WTR_s := 2;
    T_WTR_l := 4;
    T_CCD_s := 2;
    T_CCD_l := 4;

		T_REFI := 100;
		T_RFC := 20
  }.

	(* Defined in FIFOImp2, tt in this case, irrelevant for the scheduling *)
	Existing Instance REQESTOR_CFG.

	Definition Req1 := mkReq tt 1 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.
	Definition Req2 := mkReq tt 2 WR (Nat_to_bankgroup 0) (Nat_to_bank 1) 0.
	Definition Req3 := mkReq tt 4 WR (Nat_to_bankgroup 0) (Nat_to_bank 0) 2.
	Definition Req4 := mkReq tt 8 RD (Nat_to_bankgroup 0) (Nat_to_bank 2) 10.

	Definition Input := [::Req1;Req2;Req3;Req4].

	Program Instance AF : Arrival_function_t :=  Default_arrival_function_t Input.

	Definition FIFOImp2_arbitrate t :=
		mkTestTrace
		(Default_arbitrate t).(Arbiter_Commands)
		(Default_arbitrate t).(Arbiter_Time).

	Instance FIFOArbiter : TestArbiter_t := 
		mkTestArbiter AF FIFOImp2_arbitrate.

	Compute (Default_arbitrate 1).
	Compute TestArbitrate 1.
	
End FIFOImp2test.