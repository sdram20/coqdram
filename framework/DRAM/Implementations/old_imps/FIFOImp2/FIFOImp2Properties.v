Local Notation "# t" := (Default_arbitrate t) (at level 0).

Check nth.
Check is_true.
Check False_rect.

(* Try to do some sanity checks before proving stuff *)
Lemma test t : forall a, a \in (# t).(Arbiter_Commands) -> isACT a -> get_req a == None -> False.
	intros a Ha; unfold get_req, isACT; destruct a.(CKind); done.
Qed.

(* Has to be the request selected by the FIFO scheduler *)

(* how to link r, a's request to the chosen request ? *)
Lemma FIFO_ACT_date t : forall a, a \in (# t).(Arbiter_Commands) -> isACT a ->
	exists r, get_req a == Some r -> let SS := # (a.(CDate)).(SystemState) in
	let aBankNat := Bank_to_nat r.(Bank) in
	let aBankState := Vector.nth SS.(Banks) (@Fin.of_nat_lt aBankNat BANKS (bank_bounded r)) in
	is_true (snd (isOK r aBankState # (a.(CDate)).(SystemState))).
Proof.
	intros a Ha; unfold isACT; destruct a.(CKind) eqn:Hact; try done.
	intros iAa; clear iAa; exists r; intros Hr; clear Hr.

	move: Ha; induction t; [ simpl; done | ].
	simpl; unfold UpdateSystem; intros Ha;
	rewrite in_cons in Ha; move: Ha => /orP [ /eqP Ha | Ha]; [ | by apply IHt in Ha ].

	set newreq := FIFOSelect # (t).(ReqQueue) # (t).(SystemState); fold newreq in Ha. 
	destruct newreq eqn:Hnew.
	{ (* A new request has been selected and an ACT needs to be issued *)
		move: Ha.
		set HisOK := isOK r0 _ _.
		set idx_r0 := Fin.of_nat_lt (bank_bounded r0); fold idx_r0.
		set (SS' := (RecordSet.set SysCounters _ _)); fold SS'; fold SS' in HisOK.
		(* is snd HisOK is not really ok, then a will be a NOP, which is a contradiction *)
		(* if snd HisOK is true, then a is a result of fstHisOK, will have to unfold it *)
		set (b := snd HisOK).


		generalize dependent b.
		case b. dependent destruction b.
		unfold HisOK, isOK.
		set (v := Vector.nth _ _). dependent destruction v.

		(fun _ : GlobalCounters =>
		 IncrementGlobalCounters
			 # (t).(SystemState).(SysCounters))
		# (t).(SystemState))); fold SS'; fold SS' in HisOK; fold idx_r0 in HisOK.

		set (bb := snd HisOK).
		dependent induction bb.
		
		(* dependent destruction (snd HisOK0). *)
		
		rewrite H0.
		
		
		assert (fst)
		rewrite (surjective_pairing HisOK0).
		destruct 
		set sndHisOK := snd HisOK.
		destruct sndHisOK.
		dependent destruction sndHisOK. 
		set BUP := BankUpdate r0 _.
		(* intros. *)
		unfold BankUpdate in Ha.
		cbn in Ha.
		set test := Vector.nth
		(RecordSet.set SysCounters
			 (fun _ : GlobalCounters =>
				IncrementGlobalCounters
					# (t).(SystemState).(SysCounters))
			 # (t).(SystemState)).(
		Banks) (Fin.of_nat_lt (bank_bounded r0)); fold test in Ha.
		cbv [snd] in Ha.
		destruct 
		set cond := snd HisOK; fold cond in Ha; move: Ha.
		rewrite surjective_pairing

		(* rewrite surjective_pairing *)
		(* dependent destruction cond. *)
		move: Ha; destruct cond. dependent destruction cond.
		rewrite (surjective_pairing (HisOK)).

		frdytivy 
		progress cbv zeta in Ha.
		destruct HisOK. as [newcmd cond].
		cbn in Ha.

	}
	intros Ha; simpl in *.

	unfold isOK; set BS := Vector.nth # (a.(CDate)).(SystemState).(Banks) (Fin.of_nat_lt (bank_bounded r)).
	destruct BS eqn:HBS.
	{ cbv [snd].


	}

	
	eapply ex_intro; intro Hr;
	unfold get_req in Hr; unfold isACT in iAa; destruct a.(CKind); try done.
	assert (r == ?r). evar (req : Request_eqType).

	
(* Think about how to do this proof *)
Lemma FIFO_T_RCD_ok t : forall a b, 
	a \in (# t).(Arbiter_Commands) -> b \in (# t).(Arbiter_Commands) ->
	isACT a -> isCAS b -> (get_bank a == get_bank b) ->
	Before a b -> Apart_at_least a b T_RCD.
Proof.
	intros a b Ha Hb iAa iCb SB aBb; unfold Apart_at_least, Before, get_bank, isACT, isCAS in *;
	destruct a.(CKind) eqn:akind; try done;
	move: iCb => /orP [b_isCRD | b_isCWR]; [ unfold isCRD in b_isCRD | unfold isCWR in b_isCWR ];
	destruct b.(CKind) eqn:bkind; try done.
	assert (r.(Bank) == r0.(Bank)) as SB_; [ auto | ]; clear SB; move: SB_ => /eqP SB.
	{

	}
Admitted.

End FIFOImp2.