Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From DRAM Require Export ImplementationInterface.
From mathcomp Require Import fintype div ssrZ zify.
From mathcomp Require Import ssreflect seq.

From Coq Require Import Program FMapList OrderedTypeEx.

(* FMapList.Make expects an instance of the OrderedType module, which we define as Nat_as_OT *)
Module Import NatMap := FMapList.Make(Nat_as_OT).

Section TDMesREF.

  Context {SYS_CFG : System_configuration}.

	Definition REF_start_date := T_REFI.
	Definition PREA_date  := T_REFI - T_RP.

	Definition REF_date 	:= T_RP.
	Definition PRE_date  	:= REF_date + T_RFC.
	Definition ACT_date		:= PRE_date + T_RP.
	Definition CAS_date		:= ACT_date + T_RCD.

  Class TDMesREF_configuration :=
  {
		SN : nat;
    SN_one  : 1 < SN;
	
		SL : nat;
    SL_pos  : 0 < SL;

		(* There has to be enough time to complete a full PRE - ACT - CAS sequence *)
    SL_CASS : CAS_date.+1 < SL;

		(* SL has to be large enough to acomodate bus changing directions *)
		SL_BUS_RD_TO_WR 	: T_RTW < SL;
		SL_BUS_WR_TO_RD		: T_WTR_l + T_WL + T_BURST < SL;

		(* Other axioms about SL *)
		T_RTP_SN_SL : T_RTP < SN * SL - CAS_date;
    T_WTP_SN_SL : (T_WR + T_WL + T_BURST) < SN * SL - CAS_date;

    T_RAS_SL	: T_RP.+1 + T_RAS < SL;
    T_RC_SL 	: T_RC < SL;
    T_RRD_SL  : T_RRD_l < SL;
    T_CCD_SL	: T_CCD_l < SL;
    T_FAW_3SL : T_FAW < SL + SL + SL;
		T_RCD_SL	: T_RCD < SL;

		(* Axiom A1: SL needs to be a divider of T_REFI *)
		SL_REF_aligned : T_REFI %% SL == 0;
		
		(* No need for disjoint banks if this is satisfied -- initial REF delay is big enough
			to acomodate the write recovery time. This should always be the case, since T_RFC is big enough  *)
		SL_REF_cycle	: (T_RP + T_RFC).+1 > (T_WL + T_BURST + T_WR);
  }.

  Context {TDMesREF_CFG : TDMesREF_configuration}.

	(* -------------------- Additional Lemmas about SN ------------------------ *)
	Lemma SN_pos : SN > 0.
		specialize SN_one; lia.
	Qed.

	(* --------------------- Additional Lemmas about PREA_date ---------------- *)
	(* Lemma PREA_date_GT_REF_date : REF_date < PREA_date.
		specialize PREA_date_GT_ENDREF_date; unfold ENDREF_date; lia.
	Qed. *)
	
	Lemma REF_start_date_pos : 0 < REF_start_date.
		unfold REF_start_date; specialize T_REFI_GT_T_RP; lia.
	Qed.

	(* ---------------------- Additional Lemmas about SL ---------------------- *)
	Lemma SL_PRE : PRE_date < SL.
		specialize SL_CASS; unfold PRE_date, CAS_date, ACT_date, PRE_date, REF_date; lia.
	Qed.

	Lemma SL_REF : REF_date < SL.
		unfold REF_date; specialize T_RAS_SL; lia.
	Qed.

	Lemma SL_ACT : ACT_date < SL.
		specialize SL_CASS; unfold CAS_date, ACT_date; lia.
	Qed.
  
  Lemma Last_cycle: SL.-1 < SL.
    rewrite ltn_predL; apply SL_pos.
  Qed.

  Lemma SL_CAS: CAS_date < SL.
    specialize SL_CASS as H; lia.
	Qed.
    
  Lemma SL_one: 1 < SL.
		specialize SL_CASS; lia.
	Qed.

  Lemma SL_ACTS: ACT_date.+1 < SL.
    specialize SL_CASS; unfold CAS_date, ACT_date; lia.
	Qed.

	Lemma CAS_ACT_SL : CAS_date - ACT_date < SL.
		unfold CAS_date; specialize T_RCD_SL; lia.
	Qed.

	(* ------------------------ Additional Lemmas about ACT_date & CAS_date ---- *)
  Lemma ACT_pos: 0 < ACT_date.
		unfold ACT_date; specialize T_RP_pos; lia.
	Qed.

	Lemma CAS_pos: 0 < CAS_date.
		unfold CAS_date; specialize T_RCD_pos; lia.
	Qed.

	(* ------------------------ Requestor  ------------------------------------- *)
	Record Requestor_t_def := mkRequestorSlack 
	{
		nb : nat; 		(* the id of the requestor : no longer bounded by slot number *)
		crit : bool; 	(* criticality *)

		(* critical requests are bounded by the number of slots *)
		crit_req : crit == true <-> nb < SN;
	}.

	Definition Same_Requestor (a b : Requestor_t_def) :=
		a.(nb) == b.(nb).

	Definition Requestor_t_def_eqdef (a b : Requestor_t_def) :=
		(a.(nb) == b.(nb)) && (a.(crit) == b.(crit)).

	Lemma Requestor_t_def_eqn : Equality.axiom Requestor_t_def_eqdef.
	Admitted.

	Canonical Requestor_t_def_eqMixin := EqMixin Requestor_t_def_eqn.
	Canonical Requestor_t_def_eqType := Eval hnf in EqType Requestor_t_def Requestor_t_def_eqMixin.

	Instance REQUESTOR_CFG : Requestor_configuration := {
    Requestor_t := Requestor_t_def_eqType
  }.

	Context {AF : Arrival_function_t}.

	(* ------------------------ Counter Types  -------------------------------- *)
	Definition Counter_t := ordinal SL.
  Definition OZCycle   := Ordinal SL_pos.
	Definition OSlot1		 := Ordinal SL_one.
	Definition OREF_date := Ordinal SL_REF.
	Definition OPRE_date := Ordinal SL_PRE.
  Definition OACT_date := Ordinal SL_ACT.
  Definition OCAS_date := Ordinal SL_CAS.
  Definition OLastCycle := Ordinal Last_cycle.

	Definition Next_cycle (c : Counter_t) :=
		let nextc := c.+1 < SL in
			(if nextc as X return (nextc = X -> Counter_t) then 
				fun (P : nextc = true) => Ordinal (P : nextc)
				else
				fun _ => OZCycle) Logic.eq_refl.

	(* Slot counter *)
  Definition Slot_t := ordinal_eqType SN.
	Definition OZSlot := Ordinal SN_pos.

	Definition Next_slot (s : Slot_t) (c : Counter_t) : Slot_t :=
		if (c.+1 < SL) then s
		else
			let nexts := s.+1 < SN in
				(if nexts as X return (nexts = X -> Slot_t) then 
					fun (P : nexts = true) => Ordinal (P : nexts)
					else
					fun _ => OZSlot) Logic.eq_refl.

	(* Refresh counter *)
	Definition Counter_ref_t := ordinal REF_start_date.

	Lemma REF_start_date_GT_PREA_date : PREA_date < REF_start_date.
		unfold PREA_date, REF_start_date; specialize T_RP_pos; specialize T_REFI_pos; lia.
	Qed.

	Definition OPREA_date := Ordinal REF_start_date_GT_PREA_date.
	Definition OCycle0REF := Ordinal T_REFI_pos.
	
	Definition Next_cycle_ref (c : Counter_ref_t) : Counter_ref_t := 
		let nextcref := c.+1 < REF_start_date in
			(if nextcref as X return (nextcref = X -> Counter_ref_t) then
				(fun (P : nextcref = true) => Ordinal (P : nextcref))
			else (fun _ => OCycle0REF)) Logic.eq_refl.

	Definition Slack_t := NatMap.t nat.
	Definition empty_nat_map := NatMap.empty nat.

	(* ------------------------ State -------------------------------------- *)
  Inductive TDM_state_t :=
    | IDLE    : Slot_t -> Slack_t -> Counter_t -> Counter_ref_t -> 
			Requests_t -> TDM_state_t 
    | RUNNING : Slot_t -> Slack_t -> Counter_t -> Counter_ref_t -> 
			Counter_t -> Requests_t -> Request_t-> TDM_state_t
		| REFRESH : Slot_t -> Slack_t -> Counter_t -> Counter_ref_t ->
			Requests_t -> Request_t -> TDM_state_t.

  Global Instance ARBITER_CFG : Arbiter_configuration := {
    State_t := TDM_state_t;
  }.

  (* ------------------------ Arbitration functions ----------------------- *)
	Definition p_transform (x cs : nat) : nat :=
		match (cs > 0) as X with
		| true => if (x < cs) then (cs - 1) - x else (SN - 1 + cs) - x
		| _ => (SN - 1) - x
		end. 

	Definition egt (cs a b : nat) :=
		gtn (p_transform a cs) (p_transform b cs).

	Definition lp (cs : Slot_t) := if (nat_of_ord cs == 0) then (SN.-1) else cs.-1. 

	Definition dist (max a b : nat) : nat :=
		if (b < a) then (a - b) else (if (b == a) then 0 else (max - (b - a))). 
	
	(* This function iterates over a list of request to choose the next one to be serviced *)
	Program Definition edf_slack_crit (cs : Slot_t) (smap : Slack_t) := foldr (fun r_it r_saved => 
		let cs_nat				:= nat_of_ord cs in
		let r_it_crit 		:= r_it.(Requestor).(crit) in
		let r_it_id  			:= r_it.(Requestor).(nb) in
		let r_saved_crit 	:= r_saved.(Requestor).(crit) in 
		let r_saved_id		:= r_saved.(Requestor).(nb) in
		(* If request is non-critical *)
		if ~~ (r_it_crit) then 
			(* safe to choose the non-critical request when the saved 
			request is not the upcoming one, which has the most priority *)
			(if (r_saved_id != cs_nat) then r_it else r_saved)
		else (
				 (* choose the request if: 
				 	  a) r_it[i] > r_saved[i] , i.e., r_it has more priority than r_saved
						b) r_it[i] is the due for the upcoming slot (most priority), this condition
								is useful to overwrite previously selected non-critical requests
						c) overwrite the default request, which has special date 0 *)
				 let cond 	:= (egt cs_nat r_it_id r_saved_id) || (r_it_id == cs_nat) || (r_saved.(Date) == 0) in
				 let slack 	:= find r_it.(Requestor).(nb) smap in
				 match slack with
					| None => (* if there is no slack, then only consider cond *)
						if cond then r_it else r_saved
					| Some sl => (* if there is enough slack, then don't choose that request *)
						let t := (r_it.(Date) %% (SL * SN)) in
						if (sl > t) then r_saved else
						(* if there is enough slack, then the
						associated request is not priority, don't update variables *)
						(if cond then r_it else r_saved)
					end
		)) (mkReq (mkRequestorSlack (lp cs) true _) 0 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0).
	Next Obligation.
		split; [ intros H; clear H | trivial ]; unfold lp.
		destruct (nat_of_ord cs == 0); [ rewrite ltn_predL; exact SN_pos | ].
		destruct cs; simpl; lia.
	Qed.

	Check edf_slack_crit.

	Definition early_start cs cnt cref smap req :=
		let ncs := nat_of_ord (Next_slot cs OLastCycle) in
		(* if a refresh is scheduled, then early start is not allowed *)
		if (OPREA_date - cref < SL) then false else (
		if (ncs == req.(Requestor).(nb)) then true
		else (let slack := find ncs smap in
			match slack with
			| None => false
			| Some sl => SL.-1 - cnt < sl (* if next slot owner has enough slack, it is safe to early start *)
			end
		)).

	Definition Enqueue (R P : Requests_t) := P ++ R.
	Definition Dequeue r (P : Requests_t) := rem r P.
	
	Program Definition nullreq := mkReq
		(mkRequestorSlack 0 true _) 0 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.
	Next Obligation.
		split; [ intros _; exact SN_pos | trivial].
	Qed.

	Program Definition Next_state (R : Requests_t) (AS : TDM_state_t) 
		: (TDM_state_t * (Command_kind_t * Request_t)) :=
    match AS return (TDM_state_t * (Command_kind_t * Request_t)) with
      | IDLE s slmap c cref P =>
				let P' := Enqueue R P in
        let s' := Next_slot s c in
        let c' := Next_cycle c in
				let cref' := Next_cycle_ref cref in
				if (c == OZCycle) then (
					(* can only happen at OZCycle, because refresh cycles are aligned with slots *)
					if (cref == OPREA_date) then (
						match P with
						| [::] => 
							(REFRESH s' slmap c' cref' P' nullreq, (PREA,nullreq))
						| _ => let req := edf_slack_crit s slmap P in
							(REFRESH s' slmap c' cref' P' req, (PREA,nullreq))
						end
					) else (
						match P withL
						| [::] => (IDLE s' slmap c' cref' P',(NOP,nullreq))
						| _ => let req := edf_slack_crit s slmap P in
							(* both counters will be equal in this case -> no offset *)
							(RUNNING s' slmap c' cref' OZCycle (Enqueue R (Dequeue req P)) req, (NOP,req))
						end
					)
				) else ( (* early start *)
					(* if arbitrating in the middle of slot, then priority rules chance, hence
								 the different argument sent to the scheduling function *)
					let req := edf_slack_crit (Next_slot s OLastCycle) slmap P in
					if (early_start s c cref slmap req) then
					(* if early start will happen, then the counter offset should be the counter value *)
					(RUNNING s' slmap c' cref' c  (Enqueue R (Dequeue req P)) req, (NOP,req))
					else (IDLE s' slmap c' cref' P', (NOP,nullreq))
				)
			| RUNNING s slmap c cref cnt_offset P r =>
				let P' := Enqueue R P in
				let s' := Next_slot s c in
				let c' := Next_cycle c in
				let cref' := Next_cycle_ref cref in
				let pre_cond := Ordinal (@ltn_pmod (cnt_offset + PRE_date) SL SL_pos) in
				let act_cond := Ordinal (@ltn_pmod (cnt_offset + ACT_date) SL SL_pos) in
				let cas_cond := Ordinal (@ltn_pmod (cnt_offset + CAS_date) SL SL_pos) in
				let end_cond := Ordinal (@ltn_pmod (cnt_offset + SL.-1) SL SL_pos) in
				(* let cnt_cmd := nat_of_ord c + nat_of_ord cnt_offset in *)
				if (c == pre_cond) then (RUNNING s' slmap c' cref' cnt_offset P' r, (PRE,r))
				else if (c == act_cond) then (RUNNING s' slmap c' cref' cnt_offset P' r, (ACT, r))
				else if (c == cas_cond) then (RUNNING s' slmap c' cref' cnt_offset P' r, (Kind_of_req r, r))
				else if (c == end_cond) then ( (* calculate new slack *)
					let req_id := r.(Requestor).(nb) in
					let slack' := if r.(Requestor).(crit) then
					(SL.-1 - c) + ((dist SN (nat_of_ord s) req_id) * SL) else 0 in
					let slmap' := NatMap.add req_id slack' slmap in
					(IDLE s' slmap' c' cref' P', (NOP, nullreq))
				) else (RUNNING s' slmap c' cref' cnt_offset P' r, (NOP, nullreq))
			| REFRESH s slmap c cref P r =>
				let P' := Enqueue R P in
				let s' := Next_slot s c in
				let c' := Next_cycle c in
				let cref' := Next_cycle_ref cref in
				if (c == OREF_date) then (REFRESH s' slmap c' cref' P' r, (REF,nullreq))
				else if (c == OPRE_date) then (
					if (r == nullreq) then (IDLE s' slmap c' cref' P', (NOP,nullreq)) 
					(* IN this case the two running counters will be equal *)
					else (RUNNING s' slmap c' cref' c' P' r, (PRE,r)) 
				) else (REFRESH s' slmap c' cref' P' r, (NOP,nullreq))
		end.

	Definition Init_state R :=
    IDLE OZSlot empty_nat_map OZCycle OPREA_date (Enqueue R [::]).

  Instance TDMesREF_implementation : Implementation_t := 
    mkImplementation Init_state Next_state.

	(* Start proving *)
	Program Definition TDMesREF_arbitrate t :=
		mkTrace 
		(Default_arbitrate t).(Arbiter_Commands)
		(Default_arbitrate t).(Arbiter_Time)
		_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _.
	Admit Obligations.

	Program Instance TDMesREF_arbiter : Arbiter_t :=
		mkArbiter AF TDMesREF_arbitrate _ _.
	Admit Obligations.

End TDMesREF.

Section TDMesREF_sim.

	Program Instance SYS_CFG : System_configuration :=
	{
		BANKGROUPS := 1;
		BANKS := 8;

		T_BURST := 1;
		T_WL    := 1;
		T_RRD_s := 1;
		T_RRD_l := 2;
		T_FAW := 20;
		T_RC  := 3;
		T_RP  := 2;
		T_RCD := 2;
		T_RAS := 1;
		T_RTP := 1;
		T_WR  := 1;
		T_RTW := 1;
		T_WTR_s := 1;
		T_WTR_l := 2;
		T_CCD_s := 1;
		T_CCD_l := 2;
		
		T_REFI := 48;
		T_RFC := 4;
	}.

	Program Instance TDMdsREF_CFG : TDMesREF_configuration :=
	{
		SN := 3;
		SL := 12;
	}.
	
	Existing Instance REQUESTOR_CFG.
	Existing Instance TDMesREF_implementation.
	Existing Instance TDMesREF_arbiter.

	Hint Extern 1 => split; [intro H; lia | trivial] : core.

	Program Definition ReqA0 := mkReq 
		(mkRequestorSlack 0 true _) 6 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 5.
	
	Program Definition ReqB0 := mkReq (* 3 instead of 14 *)
		(mkRequestorSlack 1 true _) 15 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 10.
	
	Program Definition ReqC0 := mkReq 
		(mkRequestorSlack 2 true _) 39 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.
	
	Program Definition ReqB1 := mkReq 
		(mkRequestorSlack 1 true _) 28 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.
	
	Program Definition ReqA1 := mkReq 
		(mkRequestorSlack 0 true _) 40 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.
	
	Program Definition ReqC1 := mkReq 
		(mkRequestorSlack 2 true _) 40 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.

	Program Definition ReqB2  := mkReq 
		(mkRequestorSlack 1 true _) 44 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.

	Program Definition ReqA2  := mkReq 
		(mkRequestorSlack 0 true _) 62 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.

	Definition test_req_seq := [:: ReqA0;ReqB0;ReqC0].

	Program Instance AF : Arrival_function_t := Default_arrival_function_t test_req_seq.

	(* Compute (Default_arbitrate 36).(Implementation_State). *)
	Compute seq.map (fun cmd => (cmd.(CKind),cmd.(CDate),cmd.(Request).(Requestor).(nb))) (Arbitrate 64).(Commands).

	(* Next: introduce the concepts of page open / closed *)
	(* Do proofs about TDMes ? *)

End TDMesREF_sim.