Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.
From DRAM Require Export ImplementationInterface.
From mathcomp Require Export fintype div ssrZ zify.
From Coq Require Import Program.

Section TDMREF.

  Context {SYS_CFG : System_configuration}.

  (* Implementing everything with just one counter *)
	(* Definition REF_cycle		:= T_REFI. *)
	Definition REF_start_date := T_REFI.
	Definition PREA_date  := T_REFI - T_RP.

	Definition REF_date 	:= T_RP.
	Definition PRE_date  	:= REF_date + T_RFC.
	Definition ACT_date		:= PRE_date + T_RP.
	Definition CAS_date		:= ACT_date + T_RCD.

	(* Definition ENDREF_date 	:= REF_date + T_RFC. *)

	(* Need an axiom saying that:
		 T_RP + T_RFC > T_RTP 
		 T_RP + T_RFC > T_WL + T_BURST + T_WR 
		 Then, in this case, I won't need an hypothesis of disjoint banks *)

  Class TDMREF_configuration :=
  {
		SN : nat;
    SN_one  : 1 < SN;
	
		SL : nat;
    SL_pos  : 0 < SL;

		(* There has to be enough time to complete a full PRE - ACT - CAS sequence, plus one extra 
				IDLE cycle  *)
    SL_CASS : CAS_date.+1 < SL;

		(* SL has to be large enough to acomodate bus changing directions *)
		SL_BUS_RD_TO_WR 	: T_RTW < SL;
		SL_BUS_WR_TO_RD		: T_WTR_l + T_WL + T_BURST < SL;

		(* Other axioms about SL *)
		T_RTP_SN_SL : T_RTP < SN * SL - CAS_date;
    T_WTP_SN_SL : (T_WR + T_WL + T_BURST) < SN * SL - CAS_date;

    T_RAS_SL	: T_RP.+1 + T_RAS < SL;
    T_RC_SL 	: T_RC < SL;
    T_RRD_SL  : T_RRD_l < SL;
    T_CCD_SL	: T_CCD_l < SL;
    T_FAW_3SL : T_FAW < SL + SL + SL;

		(* Axiom A1: SL needs to be a divider of T_REFI *)
		SL_REF_aligned : T_REFI %% SL == 0;
		
		(* No need for disjoint banks if this is satisfied -- initial REF delay is big enough
			to acomodate the write recovery time. This should always be the case, since T_RFC is big enough  *)
		SL_REF_cycle	: (T_RP + T_RFC).+1 > (T_WL + T_BURST + T_WR);
  }.

	Context {TDMREF_CFG : TDMREF_configuration}.

	(* -------------------- Additional Lemmas about SN ------------------------ *)
	Lemma SN_pos : SN > 0.
		specialize SN_one; lia.
	Qed.

	(* --------------------- Additional Lemmas about PREA_date ---------------- *)
	(* Lemma PREA_date_GT_REF_date : REF_date < PREA_date.
		specialize PREA_date_GT_ENDREF_date; unfold ENDREF_date; lia.
	Qed. *)
	
	Lemma REF_start_date_pos : 0 < REF_start_date.
		unfold REF_start_date; specialize T_REFI_GT_T_RP; lia.
	Qed.

	(* ---------------------- Additional Lemmas about SL ---------------------- *)
	Lemma SL_PRE : PRE_date < SL.
		specialize SL_CASS; unfold PRE_date, CAS_date, ACT_date, PRE_date, REF_date; lia.
	Qed.

	Lemma SL_REF : REF_date < SL.
		unfold REF_date; specialize T_RAS_SL; lia.
	Qed.

	Lemma SL_ACT : ACT_date < SL.
		specialize SL_CASS; unfold CAS_date, ACT_date; lia.
	Qed.
  
  Lemma Last_cycle: SL.-1 < SL.
    rewrite ltn_predL; apply SL_pos.
  Qed.

  Lemma SL_CAS: CAS_date < SL.
    specialize SL_CASS as H; lia.
	Qed.
    
  Lemma SL_one: 1 < SL.
		specialize SL_CASS; lia.
	Qed.

  Lemma SL_ACTS: ACT_date.+1 < SL.
    specialize SL_CASS; unfold CAS_date, ACT_date; lia.
	Qed.

	(* ------------------------ Additional Lemmas about ACT_date & CAS_date ---- *)
  Lemma ACT_pos: 0 < ACT_date.
		unfold ACT_date; specialize T_RP_pos; lia.
	Qed.

	Lemma CAS_pos: 0 < CAS_date.
		unfold CAS_date; specialize T_RCD_pos; lia.
	Qed.

	(* ------------------------ Controller implementation ---------------------- *)

	(* Normal counter *)
	Definition Counter_t := ordinal SL.
  Definition OZCycle   := Ordinal SL_pos.
	Definition OREF_date := Ordinal SL_REF.
	Definition OPRE_date := Ordinal SL_PRE.
  Definition OACT_date := Ordinal SL_ACT.
  Definition OCAS_date := Ordinal SL_CAS.
  Definition OLastCycle := Ordinal Last_cycle.

	Definition Next_cycle (c : Counter_t) :=
		let nextc := c.+1 < SL in
			(if nextc as X return (nextc = X -> Counter_t) then 
				fun (P : nextc = true) => Ordinal (P : nextc)
				else
				fun _ => OZCycle) Logic.eq_refl.

	(* Slot counter *)
  Definition Slot_t := ordinal_eqType SN.
	Definition OZSlot := Ordinal SN_pos.

	Definition Next_slot (s : Slot_t) (c : Counter_t) : Slot_t :=
		if (c.+1 < SL) then s
		else
			let nexts := s.+1 < SN in
				(if nexts as X return (nexts = X -> Slot_t) then 
					fun (P : nexts = true) => Ordinal (P : nexts)
					else
					fun _ => OZSlot) Logic.eq_refl.

	(* Refresh counter *)
	Definition Counter_ref_t := ordinal REF_start_date.

	Lemma REF_start_date_GT_PREA_date : PREA_date < REF_start_date.
		unfold PREA_date, REF_start_date; specialize T_RP_pos; specialize T_REFI_pos; lia.
	Qed.

	Definition OPREA_date := Ordinal REF_start_date_GT_PREA_date.
	Definition OCycle0REF := Ordinal T_REFI_pos.
	
	Definition Next_cycle_ref (c : Counter_ref_t) : Counter_ref_t := 
		let nextcref := c.+1 < REF_start_date in
			(if nextcref as X return (nextcref = X -> Counter_ref_t) then
				(fun (P : nextcref = true) => Ordinal (P : nextcref))
			else (fun _ => OCycle0REF)) Logic.eq_refl.

  Instance REQUESTOR_CFG : Requestor_configuration := 
	{
    Requestor_t := Slot_t
  }.

  Context {AF : Arrival_function_t}.

  Inductive TDM_state_t :=
    | IDLE    : Slot_t -> Counter_t -> Counter_ref_t -> Requests_t -> TDM_state_t 
    | RUNNING : Slot_t -> Counter_t -> Counter_ref_t -> Requests_t -> Request_t -> TDM_state_t
		| REFRESH : Slot_t -> Counter_t -> Counter_ref_t -> Requests_t -> Request_t -> TDM_state_t.

  Global Instance ARBITER_CFG : Arbiter_configuration :=
  {
    State_t := TDM_state_t;
  }.

  Definition Enqueue (R P : Requests_t) := P ++ R.

  Definition Dequeue r (P : Requests_t) := rem r P.

  (* The set of pending requests of a slot *)
  Definition Pending_of s (P : Requests_t) := [seq r <- P | r.(Requestor) == s].
	
	(* Definition OREF_TRP := Ordinal T_REFI_GT_T_RP. *)

	(* Start the refresh counter at T_RP, so that the first refresh you happen at
		(T_REFI - T_RP) + T_RP cycles *)
  Definition Init_state R := IDLE OZSlot OZCycle OPREA_date (Enqueue R [::]).

  Definition nullreq := 
    mkReq OZSlot 0 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.

  (* The next-state function of the arbiter *)
  Definition Next_state R (AS : TDM_state_t) : (TDM_state_t * (Command_kind_t * Request_t)) :=
    match AS return (TDM_state_t * (Command_kind_t * Request_t)) with
      | IDLE s c cref P =>
        let P' := Enqueue R P in
        let s' := Next_slot s c in
        let c' := Next_cycle c in
				let cref' := Next_cycle_ref cref in
        if (c == OZCycle) then (
					(* Only need to test for refresh date here, since it can only happen here -- alignment *)
					(* Will start with a REFRESH cycle *)
					if (cref == OPREA_date) then (
						match Pending_of s P with
						| [::] => (REFRESH s' c' cref' P' nullreq, (PREA,nullreq))
						| r :: _ => (REFRESH s' c' cref' P' r, (PREA,nullreq))
						end
					)
					else (
						match Pending_of s P with
						| [::] => (IDLE s' c' cref' P', (NOP,nullreq))
						| r :: _ => (RUNNING s' c' cref' (Enqueue R (Dequeue r P)) r, (NOP,r))
						end 
					)
				) else (IDLE s' c' cref' P', (NOP, nullreq))
			| RUNNING s c cref P r =>
				let P' := Enqueue R P in
				let s' := Next_slot s c in
				let c' := Next_cycle c in
				let cref' := Next_cycle_ref cref in
				if (c == OPRE_date) then (RUNNING s' c' cref' P' r, (PRE,r))
				else if (c == OACT_date) then (RUNNING s' c' cref' P' r, (ACT, r))
        else if (c == OCAS_date) then (RUNNING s' c' cref' P' r, (Kind_of_req r, r))
        else if (c == OLastCycle) then (IDLE s' c' cref' P', (NOP, nullreq))
        else (RUNNING s' c' cref' P' r, (NOP, nullreq))
			| REFRESH s c cref P r =>
				let P' := Enqueue R P in
				let s' := Next_slot s c in
				let c' := Next_cycle c in
				let cref' := Next_cycle_ref cref in
				(* Could use cref as well to issue *)
				if (c == OREF_date) then (REFRESH s' c' cref' P' r, (REF,nullreq))
				(* No scheduling decisions while REFRESHING *)
				else if (c == OPRE_date) then 
					(if (r == nullreq) then (IDLE s' c' cref' P', (NOP,nullreq)) else (RUNNING s' c' cref' P' r, (PRE,r))) 
				else (REFRESH s' c' cref' P' r, (NOP,nullreq))
		end.

  Global Instance TDMREF_implementation : Implementation_t := 
    mkImplementation Init_state Next_state.

	Program Definition TDMREF_arbitrate t :=
		mkTrace 
		(Default_arbitrate t).(Arbiter_Commands)
		(Default_arbitrate t).(Arbiter_Time)
		_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _.
	Admit Obligations.

	Program Instance TDMREF_arbiter : Arbiter_t :=
		mkArbiter AF TDMREF_arbitrate _ _.
	Admit Obligations.

End TDMREF.

Section TDMREF_sim.

	(* DDR4-1600J *)
	(* Density : 8Gb *)
	(* Page Size : 1K (1G8 adressing) *)
	(* 1tCK write preamble mode *)
	(* 1tCK read preamble mode *)
	(* AL = 0 *)
	(* 1X Refresh mode *)
	Program Instance SYS_CFG : System_configuration :=
	{
		BANKGROUPS := 4;
		BANKS := 16;

		(* [Figure 105] always the same *)
		T_BURST := 4; (* INTRA/INTER *)

		(* [Figure 105] 1tCK write preamble mode, AL = 0
			-- independent of BC4 or BL8 
			-- depends on write preamble mode (1tCK or 2tCK)
			-- depends on AL (what is AL ?) *)
		T_WL    := 9;
		
		(* ACT to ACT, same and different bank groups, exclusevely inter-bank
			-- depends on page size, which depends on device density (total size) and configuration
			see (https://www.micron.com/-/media/client/global/documents/products/data-sheet/dram/ddr4/16gb_ddr4_sdram.pdf)
		*)
		T_RRD_s := 4; (* [Page 189] Max (4nCK, 5 ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 5 -> 4 cycles at 800MHz*)
		T_RRD_l := 5; (* [Page 189] Max (4nCK, 6 ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 6 -> 4.8 ~ 5 cyles at 800MHz *)

		(* Intra/inter-bank: Four ACT delay window *)
		T_FAW := 20; (* [Page 189] 25ns *)

		(* ACT to ACT, intra bank *) 
		T_RC  := 38; (* [Page 163] 47.5 ns *)

		(* PRE to ACT, intra bank *)
		T_RP  := 10; (* [Page 163] 12.5 ns *)

		(* ACT to CAS, intra bank *)
		T_RCD := 10; (* [Page 163] 12.5 ns *)

		(* ACT to PRE, intra bank *)
		T_RAS := 28; (* [Page 163] 35.0 ns *)

		(* RD to PRE, intra bank *)
		T_RTP := 6; (* [Page 189] Max (4nCK, 7.5ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 7 -> 5.6 ~ 6 cycles at 800MHz *)

		(* Write recovery time: R data to PRE *)
		T_WR  := 12; (* [Page 189] 15 ns *)

		(* Read to write, inter and intra bank *)
		T_RTW := 8; (* [Page 94] T_RTW = RL + BL/2 - WL + 2tCK = 11 + 8/2 - 9 + 2 = 8 *)

		(* End of write operation to read *)
		T_WTR_s := 2; (* [Page 189] Max (2nCK,2.5ns) -> 2nCK = 1/800MHz * 2 = 2.5ns -> max is 2.5ns -> 2 cycles at 800MHz *)
		T_WTR_l := 6; (* [Page 189] Nax (4nCK,7.5ns) -> 4nCK = 5ns -> max is 7.5ns -> 6 cycles at 800MHz *)

		T_CCD_s := 4;
		T_CCD_l := 5;
		
		T_REFI := 100; (* 6240 [Page 36] Refresh average time*)
		T_RFC := 15; (* 280 [Page 36] *)
	}.

	Program Instance TDMREF_CFG : TDMREF_configuration :=
	{
		SN := 2;
		SL := 50
	}.
	
	Existing Instance REQUESTOR_CFG.
	Existing Instance TDMREF_implementation.
	Existing Instance TDMREF_arbiter.

	Axiom SN1 : 1 < SN.
	Definition Slot1 := Ordinal SN1.
	
	Program Definition ReqA0 := mkReq 
		OZSlot 10 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 5.

	Program Definition ReqB0 := mkReq (* 3 instead of 14 *)
		Slot1 20 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 10.

	Definition test_req_seq := [:: ReqA0;ReqB0].

	Program Instance AF : Arrival_function_t := Default_arrival_function_t test_req_seq.

	(* Compute (Default_arbitrate 100).(Implementation_State). *)
	Compute (Arbitrate 150).(Commands).

	(* Next: introduce the concepts of page open / closed *)
	(* Do proofs about TDMes ? *)

End TDMes_sim.