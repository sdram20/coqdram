Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From DRAM Require Export ImplementationInterface.
From mathcomp Require Import fintype div ssrZ zify.
From mathcomp Require Import ssreflect seq.

From Coq Require Import Program FMapList OrderedTypeEx.

(* keys have to be ordered types for strong sets *)
Module Import NatMap := FMapList.Make Nat_as_OT.

Section TDMesREFpg.

  Context {SYS_CFG : System_configuration}.

	Definition PRE_date := 0.
  Definition ACT_date := PRE_date + T_RP.
	Definition CAS_date := ACT_date + T_RCD.
	
	Definition REF_slot 		:= T_REFI.
	Definition REF_date 		:= T_REFI.-1.
	Definition PREA_date 		:= REF_date - T_RP.
	Definition ENDREF_date  := T_RFC.-1.

  Class TDMesREFpg_configuration :=
  {
		SN : nat;
    SN_one  : 1 < SN;
	
		SL : nat;
    SL_pos  : 0 < SL;

		(* There has to be enough time to complete a full PRE - ACT - CAS sequence, plus
			 one extra IDLE cycle *)
    SL_CASS : CAS_date.+1 < SL;
	}.

  Context {TDMesREFpg_CFG : TDMesREFpg_configuration}.

	(* -------------------- Additional Lemmas about SN ------------------------ *)
	Lemma SN_pos : SN > 0.
		specialize SN_one; lia.
	Qed.

	(* --------------------- Additional Lemmas about PREA_date ---------------- *)
	
	Lemma REF_slot_GT_PREA_date : PREA_date < REF_slot.
		unfold PREA_date, REF_date, REF_slot; specialize T_RP_pos; specialize T_REFI_pos; lia.
	Qed.

	Lemma REF_slot_GT_PREA_date_SL : PREA_date - SL < REF_slot.
		unfold PREA_date, REF_date, REF_slot; specialize T_REFI_pos; specialize T_RP_pos; lia.
	Qed.

	Lemma REF_slot_GT_ENDREF_date	: ENDREF_date < REF_slot.
		unfold ENDREF_date, REF_slot; specialize T_REFI_GT_T_RFC; lia.
	Qed.

	Lemma REF_slot_GT_REF_date : REF_date < REF_slot.
		unfold REF_slot, REF_date; specialize T_REFI_pos; lia.
	Qed.

	(* ---------------------- Additional Lemmas about SL ---------------------- *)
	Lemma SL_PRE : PRE_date < SL.
		specialize SL_CASS; unfold PRE_date, CAS_date, ACT_date, PRE_date, REF_date; lia.
	Qed.

	Lemma SL_ACT : ACT_date < SL.
		specialize SL_CASS; unfold CAS_date, ACT_date; lia.
	Qed.
  
  Lemma Last_cycle: SL.-1 < SL.
    rewrite ltn_predL; apply SL_pos.
  Qed.

  Lemma SL_CAS: CAS_date < SL.
    specialize SL_CASS as H; lia.
	Qed.
    
  Lemma SL_one: 1 < SL.
		specialize SL_CASS; lia.
	Qed.

  Lemma SL_ACTS: ACT_date.+1 < SL.
    specialize SL_CASS; unfold CAS_date, ACT_date; lia.
	Qed.

	Lemma CAS_ACT_SL : CAS_date - ACT_date < SL.
		specialize SL_CAS; unfold CAS_date; lia.
	Qed.

	(* ------------------------ Additional Lemmas about ACT_date & CAS_date ---- *)
  Lemma ACT_pos: 0 < ACT_date.
		unfold ACT_date; specialize T_RP_pos; lia.
	Qed.

	Lemma CAS_pos: 0 < CAS_date.
		unfold CAS_date; specialize T_RCD_pos; lia.
	Qed.

	(* ------------------------ Requestor  ------------------------------------- *)
	Record Requestor_t_def := mkRequestorSlack 
	{
		nb : nat; 		(* the id of the requestor : no longer bounded by slot number *)
		crit : bool; 	(* criticality *)

		(* critical requests are bounded by the number of slots *)
		(* how to add that into the equality axiom ? *)
		crit_req : crit == true <-> nb < SN;
	}.

	Definition Same_Requestor (a b : Requestor_t_def) :=
		a.(nb) == b.(nb).

	Definition Requestor_t_def_eqdef (a b : Requestor_t_def) :=
		(a.(nb) == b.(nb)) && (a.(crit) == b.(crit)).

	Lemma Requestor_t_def_eqn : Equality.axiom Requestor_t_def_eqdef.
	Proof.
		unfold Equality.axiom; intros.
		destruct (Requestor_t_def_eqdef x y) eqn:H; unfold Requestor_t_def_eqdef in *.
		{ apply ReflectT. destruct x, y; inversion H; move: H => /andP [/eqP H /eqP H0]; simpl in H,H0; subst nb0 crit0.
			admit. 
		}
	Admitted.

	Canonical Requestor_t_def_eqMixin := EqMixin Requestor_t_def_eqn.
	Canonical Requestor_t_def_eqType := Eval hnf in EqType Requestor_t_def Requestor_t_def_eqMixin.

	Instance REQUESTOR_CFG : Requestor_configuration := 
	{
    Requestor_t := Requestor_t_def_eqType
  }.

	Context {AF : Arrival_function_t}.

	(* ------------------------ Counter Types  -------------------------------- *)
	Definition Counter_t := ordinal SL.
  Definition OZCycle   := Ordinal SL_pos.
	Definition OSlot1		 := Ordinal SL_one.
	Definition OACT_date := Ordinal SL_ACT.
  Definition OCAS_date := Ordinal SL_CAS.
  Definition OLastCycle := Ordinal Last_cycle.

	Definition Next_cycle (c : Counter_t) :=
		let nextc := c.+1 < SL in
			(if nextc as X return (nextc = X -> Counter_t) then 
				fun (P : nextc = true) => Ordinal (P : nextc)
				else
				fun _ => OZCycle) Logic.eq_refl.

	(* Slot counter *)
  Definition Slot_t := ordinal_eqType SN.
	Definition OZSlot := Ordinal SN_pos.

	Definition Next_slot (s : Slot_t) (c : Counter_t) : Slot_t :=
		if (c.+1 < SL) then s
		else
			let nexts := s.+1 < SN in
				(if nexts as X return (nexts = X -> Slot_t) then 
					fun (P : nexts = true) => Ordinal (P : nexts)
					else
					fun _ => OZSlot) Logic.eq_refl.

	(* Refresh counter *)
	Definition Counter_ref_t := ordinal REF_slot.
	Definition OCycle0REF := Ordinal T_REFI_pos.
	Definition OEnough		:= Ordinal REF_slot_GT_PREA_date_SL.
	Definition OPREA_date := Ordinal REF_slot_GT_PREA_date.
	Definition OREF_date  := Ordinal REF_slot_GT_REF_date.
	Definition OENDREF_date := Ordinal REF_slot_GT_ENDREF_date.
	
	Definition Next_cycle_ref (c : Counter_ref_t) : Counter_ref_t := 
		let nextcref := c.+1 < REF_slot in
			(if nextcref as X return (nextcref = X -> Counter_ref_t) then
				(fun (P : nextcref = true) => Ordinal (P : nextcref))
			else (fun _ => OCycle0REF)) Logic.eq_refl.

	(* ------------------------ Slack & Page Map  -------------------------------- *)
	(* Requestor -> nat *)
	Definition Slack_t := NatMap.t nat.
	Definition empty_nat_map := NatMap.empty nat.

	(* Bank_t -> Row_t mapping *)
	Definition Pages_t := NatMap.t Row_t.
	Definition empty_pg_map := NatMap.empty Row_t.

	(* ------------------------ Cathegorising Latencies  -------------------------------- *)
	(* On a miss following an acess to the same bank bank, needs a PRE - ACT - CAS *)
	Definition MISS_SB_latency := SL + (T_WL + T_BURST + T_WR).

	(* On a miss following an acess to a different bank, needs a PRE - ACT - CAS, without intra-bank PRE to CAS delay *)
	Definition MISS_DB_latency := SL.

	(* On a hit with WR to RD bus change, same bank group *)
	Definition HIT_WR_to_RD_SBG_latency := T_WL + T_BURST + T_WTR_l.

	(* On a hit with WR to RD bus change, different bank group *)
	Definition HIT_WR_to_RD_DBG_latency := T_WL + T_BURST + T_WTR_s.

	(* On a hit with RD to WR bus change *)
	Definition HIT_RD_to_WR_latency := T_RTW.

	(* ON a hit with no bus changes, same bank group *)
	Definition HIT_nobuschg_SBG_latency := T_CCD_l.

	(* ON a hit with no bus changes, same bank group *)
	Definition HIT_nobuschg_DBG_latency := T_CCD_s.

	Definition buschg_delay_t := ordinal (T_WL + T_BURST + T_WTR_l).

	Lemma buschg_delay_pos : T_WL + T_BURST + T_WTR_l > 0.
		specialize T_WTR_l_pos; lia.
	Qed.

	Definition OBCHdelay := Ordinal buschg_delay_pos.

	Definition caspre_delay_t := ordinal (T_WL + T_BURST + T_WR).

	Lemma caspre_delay_pos : T_WL + T_BURST + T_WR > 0.
		specialize T_WR_pos; lia.
	Qed.

	Definition OCPdelay := Ordinal caspre_delay_pos.

	Inductive Busdir_t := BWR | BRD.

	(* Want to have a total function, something that maps every slot to a set of banks
		this set is a partial funciton only, as not every slot is bounded 
		but NatMap won't be the way, because there are infinite naturals ... 
		but there can also be infinite slots 
		SLOTS NEED to be mapped into a set of Banks *)
	Class SlotMap_t := { 
		
		(* Ideally I'd have to create a module Slot_t_as_OT, but NatMap will do *)
		BankMap : NatMap.t Banks_t;

		(* as many elements in the map as there are slots in a TDM period *)
		EverySlopHasABinding : cardinal BankMap == SN;

		(* Consecutive slots have to target different bank sets*)
		ConsecutiveSlots : forall sa sb : Slot_t, 
			nat_of_ord sa = nat_of_ord sb + 1 \/ nat_of_ord sb = nat_of_ord sa + 1 ->
			let bsa := find (nat_of_ord sa) BankMap in
			let bsb := find (nat_of_ord sb) BankMap in
			match bsa,bsb with
			| Some bs_a, Some bs_b =>
			(* No intersection between sets, there must be a better way to write this *)
			((all (fun el => el \notin bs_b) bs_a) /\ (all (fun el => el \notin bs_a) bs_b))
			| _,_ => False
			end
	}.

	Context {SlotMap : SlotMap_t}.
	
	(* How to associate a requestor to a slot ? *)
	(* Will a certain slot always target a specific bank ?
		 might prefer referring to a set of banks 
		 slot -> Bank_t *)

	(* Always assume a MISS on speculative scenarios *)
	Definition Speculative_WC_Latency (slot : Slot_t) (req : Request_t) := 
		let bank_set := find (nat_of_ord slot) BankMap in
		match bank_set with
		| None => 0 (* Impossible to happen because of EverySlopHasABinding *)
		| Some bank_set =>
			(* won't happen if they are adjacent slots ... that still has to play a role in the algorithm *)
			if (req.(Bank) \in bank_set) 
			(* a bank colision might ocurr *) then MISS_SB_latency
			(* no chance of bank collision *) else MISS_DB_latency
		end.

	(* Now the next slot has an associated request, can actually analyse bank collision instead of speculate *)
	(* The actual latency can be calculated with the normal Latency function *)
	Record StateVars := mkSV {
		Slot 				: Slot_t;
		SlackMap 		: Slack_t;
		Counter 		: Counter_t;
		CounterREF 	: Counter_ref_t;
		Bus					: Busdir_t;
		Pages 			: Pages_t;
		ReqQueue		: Requests_t;
	}.

	Record LastAccess := mkLastAcess {
		LastBank 	: Bank_t;
		LastBG		: Bankgroup_t;
		CP_delay 	: caspre_delay_t;
		BC_delay 	: buschg_delay_t;
	}.

	Definition Urgent_t := bool.

	Definition isHIT req (PG : Pages_t) :=
		let active_row := find (Bank_to_nat req.(Bank)) PG in (req.(Row) == active_row).

	Definition Latency
		(* the req which will have it's latency calculated *)
	(req 			: Request_t)
		(* the bank status map *)
	(PG 			: Pages_t)
		(* current direction of the bus *)
	(bus 			: Busdir_t) 
		(* remaining delays from last CAS *)
	(LA 			: LastAccess) :=
	if (isHIT req PG) then (
		let hit_delay := (* always needs just a CAS, the latency can be as low as 0, if the latency is respected before *)
		(match bus,req.(Kind) with
		| BWR,RD => if (req.(Bankgroup) == LA.(LastBG)) then HIT_WR_to_RD_SBG_latency else HIT_WR_to_RD_DBG_latency
		| BRD,WR => HIT_RD_to_WR_latency
		| _,_ => if (req.(Bankgroup) == LA.(LastBG)) then HIT_nobuschg_SBG_latency else HIT_nobuschg_DBG_latency
		end) in hit_delay - LA.(BC_delay)
	) else (if (req.(Bank) == LA.(LastBank)) 
		then (MISS_SB_latency - LA.(CP_delay)) (* delay bounded by (T_WL + T_BURST + T_WR), will be at least SL *)
		else MISS_DB_latency
	).

	(* ------------------------ State -------------------------------------- *)
  Inductive TDM_state_t :=
		(* IDLE state carries the the bank and bankgroup of the last access *)
    | IDLE    : StateVars -> LastAccess -> Urgent_t -> TDM_state_t 
		(* RUNNING carries an extra counter to account for the offset, and the request currently being processed *)
    | RUNNING_HIT : StateVars -> Counter_t -> Request_t-> Urgent_t -> TDM_state_t
		| RUNNING_MIS : StateVars -> Counter_t -> Request_t-> Urgent_t -> TDM_state_t
		| REFRESH : StateVars -> TDM_state_t.

  Global Instance ARBITER_CFG : Arbiter_configuration := 
	{
    State_t := TDM_state_t;
  }.

  (* ------------------------ Arbitration functions ----------------------- *)
	Definition p_transform (x cs : nat) : nat :=
		match (cs > 0) as X with
		| true => if (x < cs) then (cs - 1) - x else (SN - 1 + cs) - x
		| _ => (SN - 1) - x
		end.

	Definition egt (cs a b : nat) :=
		gtn (p_transform a cs) (p_transform b cs).

	Definition lp (cs : Slot_t) := if (nat_of_ord cs == 0) then (SN.-1) else cs.-1. 

	Definition dist (max a b : nat) : nat :=
		if (b < a) then (a - b) else (if (b == a) then 0 else (max - (b - a))). 
	
	(* The set of pending requests of a slot *)
	Definition Pending_of s (P : Requests_t) := [seq r <- P | r.(Requestor).(nb) == s].
	Definition Enqueue (R P : Requests_t) := P ++ R.
	Definition Dequeue r (P : Requests_t) := rem r P.

	Definition Request_kind_t_BUS (RK : Request_kind_t) :=
		match RK with
		| RD => BRD
		| WR => BWR
		end.

	Program Definition edf_slack_crit_ (SV : StateVars) (LA : LastAccess) := 
		foldr (fun r_it '(r_saved,next_urgent) =>
			let r_it_slack := find r_it.(Requestor).(nb) SV.(SlackMap) in
			let ncs := Next_slot SV.(Slot) OLastCycle in
			if (r_it.(Requestor).(nb) == nat_of_ord SV.(Slot)) then (
				if next_urgent then (r_it,false) else (r_saved,true)
			) else (
				if ((r_saved.(Requestor).(nb) != nat_of_ord SV.(Slot)) || (r_saved.(Date) == 0)) then (
					if (r_it.(Requestor).(nb) == nat_of_ord ncs) then ( (* next slow is its own *)
						match r_it_slack with
						| None => (r_it,false)
						| Some sl => let t := (SL - (r_it.(Date) %% (SL * SN))) in 
							(if (sl > t) then (r_saved,false) else (r_it,false))
						end
					) else ( (* the latency calculation case *)
						let req_latency := Latency r_it SV.(Pages) SV.(Bus) LA in
						let ncs_slack		:= find (nat_of_ord ncs) SV.(SlackMap) in
						let priority		:= egt (nat_of_ord SV.(Slot)) r_it.(Requestor).(nb) r_saved.(Requestor).(nb) in
						match Pending_of (nat_of_ord ncs) SV.(ReqQueue) with
						| [::]	=>
							let ncs_latency := Speculative_WC_Latency ncs r_it in
							let cond := priority && ((req_latency + ncs_latency) <= SL - SV.(Counter) + SL) in
							match ncs_slack with
							| None => if cond then (r_it,true) else (r_saved,false)
							| Some sl => let t := (SL - SV.(Counter)) in
								if (sl > t) then (r_it,false) else if cond then (r_it,true) else (r_saved,false)
							end
						| r :: _ =>	
							let pmap' := NatMap.add (Bank_to_nat r_it.(Bank)) r_it.(Row) SV.(Pages) in
							let ncs_latency := Latency r pmap' (Request_kind_t_BUS r_it.(Kind)) 
								(mkLastAcess r_it.(Bank) r_it.(Bankgroup) LA.(CP_delay) LA.(BC_delay)) in 
							let cond := priority && ((req_latency + ncs_latency) <= SL - SV.(Counter) + SL) in
							match ncs_slack with
							| None => if cond then (r_it,true) else (r_saved,false)
							| Some sl => let t := (SL - (r.(Date) %% (SL * SN))) in
								if (sl > t) then (r_it,false) else if cond then (r_it,true) else (r_saved,false)
							end
						end
					)
				) else (r_saved,false) (* if the selected request is urgent, then it cannot be overwritten *)
			)
		) ((mkReq (mkRequestorSlack (lp SV.(Slot)) true _) 0 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0),false).
		Next Obligation.
			split; [ intros H; clear H | trivial ]; unfold lp.
			destruct (nat_of_ord SV.(Slot) == 0); [ rewrite ltn_predL; exact SN_pos | ].
			destruct SV.(Slot); simpl; lia.
		Defined.
	
	Program Definition nullreq := mkReq
		(mkRequestorSlack 0 true _) 0 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.
	Next Obligation.
		split; [ intros _; exact SN_pos | trivial].
	Qed.

	Definition Init_state R :=
    IDLE 
		(mkSV OZSlot empty_nat_map OZCycle OCycle0REF BRD empty_pg_map (Enqueue R [::]))
		(mkLastAcess (Nat_to_bank 0) (Nat_to_bankgroup 0) OCPdelay OBCHdelay)
		false.

	Check edf_slack_crit_.

	Definition Next_state (R : Requests_t) (AS : TDM_state_t) 
		: (TDM_state_t * (Command_kind_t * Request_t)) :=
    match AS return (TDM_state_t * (Command_kind_t * Request_t)) with
    | IDLE SV LA u =>
			let P' 		:= Enqueue R SV.(ReqQueue) in
			let s' 		:= Next_slot SV.(Slot) SV.(Counter) in
			let c' 		:= Next_cycle SV.(Counter) in
			let cref' := Next_cycle_ref SV.(CounterREF) in
			if (SV.(CounterREF) < OEnough) then ( (* there is enough time to process a request before request cycle beings *)
				match SV.(ReqQueue) with
				| [::] => ((IDLE (mkSV s' SV.(SlackMap) c' cref' SV.(Bus) SV.(Pages) P') LA false,(NOP,nullreq))
				| _ => let '(req,urgent) := edf_slack_crit_ SV LA SV.(ReqQueue) in
					if (req.(Date) == 0) then (* no request was chosen *) 
						((IDLE (mkSV s' SV.(SlackMap) c' cref' SV.(Bus) SV.(Pages) P') LA false,(NOP,nullreq))
					else (
						if (isHIT req SV.(Pages)) (* when to update page and bus ? *)
							(* don't start already issuing any commands, because waiting times might apply *)
							then ((RUNNING_HIT (mkSV s' SV.(SlackMap) c' cref' SV.(Bus) SV.(Pages) P') LA urgent),(NOP,req))
							else ((RUNNING_MIS (mkSV s' SV.(SlackMap) c' cref' SV.(Bus) SV.(Pages) P') LA urgent),(NOP,req))
					)
			) else ((REFRESH (mkSV s SV.(SlackMap) c' cref' SV.(Bus) SV.(Pages) P')), (NOP,nullreq))
		| RUNNING_HIT SV LA req urgent =>

				)




				let P' := Enqueue R P in
        let s' := Next_slot s c in
        let c' := Next_cycle c in
				let cref' := Next_cycle_ref cref in
				if (cref < OEnough) then ( (* there is enough time to process a request before request cycle beings *)
					match P with
					| [::] => (IDLE s' slmap c' cref' P', (NOP,nullreq))
					| _ =>
						if (c == OZCycle) then (
							let req := edf_slack_crit s slmap P in
							(RUNNING s' slmap c' cref' c (Enqueue R (Dequeue req P)) req, (PRE,req))
						) else (
							let req := edf_slack_crit (Next_slot s OLastCycle) slmap P in
							if (early_start s c slmap req) then
								(RUNNING s' slmap c' cref' c (Enqueue R (Dequeue req P)) req, (PRE,req))
							else (IDLE s' slmap c' cref' P', (NOP,nullreq)))
					end
				) else ( (* there is not enough time to start a new request before the start of the refresh cycle *)
					(REFRESH s slmap cref' P', (NOP,nullreq))
				)
			| RUNNING s slmap c cref cnt_offset P r =>
				let P' := Enqueue R P in
				let s' := Next_slot s c in
				let c' := Next_cycle c in
				let cref' := Next_cycle_ref cref in
				let act_cond := Ordinal (@ltn_pmod (cnt_offset + ACT_date) SL SL_pos) in
				let cas_cond := Ordinal (@ltn_pmod (cnt_offset + CAS_date) SL SL_pos) in
				let end_cond := Ordinal (@ltn_pmod (cnt_offset + SL.-1) SL SL_pos) in
				if (c == act_cond) then (RUNNING s' slmap c' cref' cnt_offset P' r, (ACT, r))
				else if (c == cas_cond) then (RUNNING s' slmap c' cref' cnt_offset P' r, (Kind_of_req r, r))
				else if (c == end_cond) then ( (* calculate new slack *)
					let req_id := r.(Requestor).(nb) in
					let slack' := if (r.(Requestor).(crit)) then
						(SL.-1 - c) + ((dist SN (nat_of_ord s) req_id) * SL) else 0 in
					let slmap' := NatMap.add req_id slack' slmap in
					(* gotta have enough time to refresh to go to IDLE before refreshing *)
					(IDLE s' slmap' c' cref' P', (NOP, nullreq))
			) else (RUNNING s' slmap c' cref' cnt_offset P' r, (NOP, nullreq))
		| REFRESH s slmap cref P =>
			let P' := Enqueue R P in
			let cref' := Next_cycle_ref cref in
			if (cref == OPREA_date) then (REFRESH s slmap cref' P', (PREA,nullreq))
			else if (cref == OREF_date) then (* the counter resets to 0 at this location *)
				(REFRESH s slmap cref' P', (REF,nullreq))
			else if (cref == OENDREF_date) then
				(IDLE s slmap OZCycle cref' P', (NOP,nullreq))
			else (REFRESH s slmap cref' P', (NOP,nullreq))
		end.

  Instance TDMesREFpg_implementation : Implementation_t := 
    mkImplementation Init_state Next_state.

	(* Start proving *)
	Program Definition TDMesREFpg_arbitrate t :=
		mkTrace 
		(Default_arbitrate t).(Arbiter_Commands)
		(Default_arbitrate t).(Arbiter_Time)
		_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _.
	Admit Obligations.

	Program Instance TDMesREFpg_arbiter : Arbiter_t :=
		mkArbiter AF TDMesREFpg_arbitrate _ _.
	Admit Obligations. *)

End TDMesREFpg.

Section TDMesREFpg_sim.

	(* DDR4-1600J *)
	(* Density : 8Gb *)
	(* Page Size : 1K (1G8 adressing) *)
	(* 1tCK write preamble mode *)
	(* 1tCK read preamble mode *)
	(* AL = 0 *)
	(* 1X Refresh mode *)
	Program Instance SYS_CFG : System_configuration :=
	{
		BANKGROUPS := 4;
		BANKS := 16;

		(* [Figure 105] always the same *)
		T_BURST := 4; (* INTRA/INTER *)

		(* [Figure 105] 1tCK write preamble mode, AL = 0
			-- independent of BC4 or BL8 
			-- depends on write preamble mode (1tCK or 2tCK)
			-- depends on AL (what is AL ?) *)
		T_WL    := 9;
		
		(* ACT to ACT, same and different bank groups, exclusevely inter-bank
			-- depends on page size, which depends on device density (total size) and configuration
			see (https://www.micron.com/-/media/client/global/documents/products/data-sheet/dram/ddr4/16gb_ddr4_sdram.pdf)
		*)
		T_RRD_s := 4; (* [Page 189] Max (4nCK, 5 ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 5 -> 4 cycles at 800MHz*)
		T_RRD_l := 5; (* [Page 189] Max (4nCK, 6 ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 6 -> 4.8 ~ 5 cyles at 800MHz *)

		(* Intra/inter-bank: Four ACT delay window *)
		T_FAW := 20; (* [Page 189] 25ns *)

		(* ACT to ACT, intra bank *) 
		T_RC  := 38; (* [Page 163] 47.5 ns *)

		(* PRE to ACT, intra bank *)
		T_RP  := 10; (* [Page 163] 12.5 ns *)

		(* ACT to CAS, intra bank *)
		T_RCD := 10; (* [Page 163] 12.5 ns *)

		(* ACT to PRE, intra bank *)
		T_RAS := 28; (* [Page 163] 35.0 ns *)

		(* RD to PRE, intra bank *)
		T_RTP := 6; (* [Page 189] Max (4nCK, 7.5ns) -> 4nCK = 1/800MHz * 4 = 5ns -> max is 7 -> 5.6 ~ 6 cycles at 800MHz *)

		(* Write recovery time: R data to PRE *)
		T_WR  := 12; (* [Page 189] 15 ns *)

		(* Read to write, inter and intra bank *)
		T_RTW := 8; (* [Page 94] T_RTW = RL + BL/2 - WL + 2tCK = 11 + 8/2 - 9 + 2 = 8 *)

		(* End of write operation to read *)
		T_WTR_s := 2; (* [Page 189] Max (2nCK,2.5ns) -> 2nCK = 1/800MHz * 2 = 2.5ns -> max is 2.5ns -> 2 cycles at 800MHz *)
		T_WTR_l := 6; (* [Page 189] Nax (4nCK,7.5ns) -> 4nCK = 5ns -> max is 7.5ns -> 6 cycles at 800MHz *)

		T_CCD_s := 4;
		T_CCD_l := 5;
		
		T_REFI := 6240; (* 6240 [Page 36] Refresh average time*)
		T_RFC := 280; (* 280 [Page 36] *)
	}.

	Program Instance TDMesREFpg_CFG : TDMesREFpg_configuration := {
		SN := 2;
		SL := 22 (* 1 + T_RP + T_RCD = 21 *)
	}.

	Compute MISS_SB_latency. (* 47 more than two slots ... *)
	Compute MISS_DB_latency. (* 22 one slot *)
	Compute HIT_WR_to_RD_SBG_latency. (* 19 *)
	Compute HIT_WR_to_RD_DBG_latency. (* 15 *)
	Compute HIT_RD_to_WR_latency. (* 8 *)
	Compute HIT_nobuschg_SBG_latency. (* 5 *)
	Compute HIT_nobuschg_DBG_latency. (* 4 *)

	Program Instance SYS_CFG : System_configuration :=
	{
		BANKGROUPS := 1;
		BANKS := 8;

		T_BURST := 1;
		T_WL    := 1;
		T_RRD_s := 1;
		T_RRD_l := 2;
		T_FAW := 10;
		T_RC  := 3;
		T_RP  := 3;
		T_RCD := 2;
		T_RAS := 1;
		T_RTP := 1;
		T_WR  := 1;
		T_RTW := 1;
		T_WTR_s := 1;
		T_WTR_l := 2;
		T_CCD_s := 1;
		T_CCD_l := 2;
		
		T_REFI := 30;
		T_RFC := 8;
	}.
	

	Existing Instance REQUESTOR_CFG.
	Existing Instance TDMesREFpg_implementation.
	Existing Instance TDMesREFpg_arbiter.

	Hint Extern 1 => split; [intro H; lia | trivial] : core.

	Definition ReqA0 := mkReq 
		(mkRequestorSlack 0 true) 2 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 5.
	
	Definition ReqB0 := mkReq (* 3 instead of 14 *)
		(mkRequestorSlack 1 true) 4 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 10.

	Definition ReqC0 := mkReq 
		(mkRequestorSlack 2 false) 10 WR (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.
	
		(*
	Program Definition ReqB1 := mkReq 
		(mkRequestorSlack 1 true _) 28 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.
	
	Program Definition ReqA1 := mkReq 
		(mkRequestorSlack 0 true _) 40 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.
	
	Program Definition ReqC1 := mkReq 
		(mkRequestorSlack 2 true _) 40 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.

	Program Definition ReqB2  := mkReq 
		(mkRequestorSlack 1 true _) 44 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.

	Program Definition ReqA2  := mkReq 
		(mkRequestorSlack 0 true _) 62 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.
	*)

	Definition test_req_seq := [:: ReqA0;ReqB0;ReqC0].

	Program Instance AF : Arrival_function_t := Default_arrival_function_t test_req_seq.

	Compute (Default_arbitrate 12).(Implementation_State).
	Compute seq.map (fun cmd => (cmd.(CKind),cmd.(CDate),cmd.(Request).(Requestor).(nb))) 
		(Arbitrate 15).(Commands).

	(* Next: introduce the concepts of page open / closed *)
	(* Do proofs about TDMes ? *)

End TDMesREFpg_sim.