Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From DRAM Require Export ImplementationInterface.
From mathcomp Require Import fintype div ssrZ zify.
From mathcomp Require Import ssreflect seq.

From Coq Require Import Program FMapList OrderedTypeEx.

(* FMapList.Make expects an instance of the OrderedType module, which we define as Nat_as_OT *)
Module Import NatMap := FMapList.Make(Nat_as_OT).

Section TDMes.

  Context {SYS_CFG : System_configuration}.

  #[local] Axiom DDR3: BANKGROUPS = 1.

  Definition ACT_date := T_RP.+1.
  Definition CAS_date := ACT_date + T_RCD.+1.

  Class TDMds_configuration :=
  {
    SL : nat; (* still exists *)
    SN : nat; (* number of critical tasks / TDM slots *)

    SN_one  : 1 < SN;

    SL_pos  : 0 < SL;
		SL_ACT  : ACT_date < SL;
    SL_CASS  : CAS_date.+1 < SL;
  }.

  Context {TDMds_CFG : TDMds_configuration}.

	Lemma SN_pos : 0 < SN.
		specialize SN_one; lia. 
	Qed.

	Lemma SL_CAS: CAS_date < SL.
    specialize SL_CASS as H; lia.
  Qed.

	Lemma Last_cycle: SL.-1 < SL.
		specialize SL_pos; lia.
  Qed.

	(* Have to introduce the concept of criticality as well *)
	Record Requestor_t_def := mkRequestorSlack 
	{
		nb : nat; 		(* the id of the requestor : no longer bounded by slot number *)
		crit : bool; 	(* criticality *)

		(* critical requests are bounded by the number of slots *)
		crit_req : crit == true -> nb < SN;
	}.

	Definition Same_Requestor (a b : Requestor_t_def) :=
		a.(nb) == b.(nb).

	Definition Requestor_t_def_eqdef (a b : Requestor_t_def) :=
		(a.(nb) == b.(nb)) && (a.(crit) == b.(crit)).

	Lemma Requestor_t_def_eqn : Equality.axiom Requestor_t_def_eqdef.
	Admitted.

	Canonical Requestor_t_def_eqMixin := EqMixin Requestor_t_def_eqn.
	Canonical Requestor_t_def_eqType := Eval hnf in EqType Requestor_t_def Requestor_t_def_eqMixin.

	Instance REQUESTOR_CFG : Requestor_configuration := 
	{
    Requestor_t := Requestor_t_def_eqType
  }.

	Context {AF : Arrival_function_t}.

	Definition Slot_t := ordinal_eqType SN.
  Definition Counter_t := ordinal SL.

	(* Weak maps : maps with no ordering on the key type nor the data type, WSfun and WS *)
	(* Sfun and S: the key type is ordered *)
	(* Check NatMap.t. *)

	(* Slack is an ordered map from nat (requestor id) to nat (slack amount) *)
	Definition Slack_t := NatMap.t nat.

  (* Track whether a request is processed (RUNNING) or not (IDLE), the owner of 
     the current slot, the cycle offset within the slot, as well as the set of 
     pending  requests. *)
  Inductive TDM_state_t :=
    | IDLE    : Slot_t -> Slack_t -> Counter_t -> Requests_t -> TDM_state_t 
    | RUNNING : Slot_t -> Slack_t -> Counter_t -> Counter_t -> Requests_t -> Request_t-> TDM_state_t.

  Global Instance ARBITER_CFG : Arbiter_configuration :=
  {
    State_t := TDM_state_t;
  }.

	Definition OZCycle   	:= Ordinal SL_pos.

	Lemma SL_one : 1 < SL.
		specialize SL_ACT; specialize T_RP_pos; unfold ACT_date; lia.
	Qed.
	Definition OSlot1 		:= Ordinal SL_one.

	Definition OACT_date 	:= Ordinal SL_ACT.
	Definition OCAS_date 	:= Ordinal SL_CAS.
	Definition OLastCycle := Ordinal Last_cycle.
	Definition OZSlot 		:= Ordinal SN_pos.

  (*************************************************************************************************)
  (** ARBITER IMPLEMENTATION ***********************************************************************)
  (*************************************************************************************************)

	Definition p_transform (x cs : nat) : nat :=
		match (cs > 0) as X with
		| true => if (x < cs) then (cs - 1) - x else (SN - 1 + cs) - x
		| _ => (SN - 1) - x
		end. 

	Definition egt (cs a b : nat) :=
		gtn (p_transform a cs) (p_transform b cs).

	Definition lp (cs : Slot_t) := if (nat_of_ord cs == 0) then (SN.-1) else cs.-1. 

	Definition dist (max a b : nat) : nat :=
		if (b < a) then (a - b) else (if (b == a) then 0 else (max - (b - a))). 

	(* Increment counter for cycle offset (with wrap-arround). *)
	Definition Next_cycle (c : Counter_t) :=
		let nextc := c.+1 < SL in
			(if nextc as X return (nextc = X -> Counter_t) then 
				fun (P : nextc = true) => Ordinal (P : nextc)
				else
				fun _ => OZCycle) Logic.eq_refl.
	
		(* Increment the slot counter (with wrap-arround) *)
	Definition Next_slot (s : Slot_t) (c : Counter_t) : Slot_t :=
		if (c.+1 < SL) then s
		else
			let nexts := s.+1 < SN in
				(if nexts as X return (nexts = X -> Slot_t) then 
					fun (P : nexts = true) => Ordinal (P : nexts)
				 else
					fun _ => OZSlot) Logic.eq_refl.

	Definition Enqueue (R P : Requests_t) := P ++ R.
	Definition Dequeue r (P : Requests_t) := rem r P.
	
	Program Definition nullreq := mkReq
		(mkRequestorSlack 0 true _) 0 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.
	Next Obligation.
		exact SN_pos.
	Qed.
	
	Program Definition edf_slack_crit (cs : Slot_t) (smap : Slack_t) := foldr (fun r_it r_saved => 
		let cs_nat				:= nat_of_ord cs in
		let r_it_crit 		:= r_it.(Requestor).(crit) in
		let r_it_id  			:= r_it.(Requestor).(nb) in
		let r_saved_crit 	:= r_saved.(Requestor).(crit) in 
		let r_saved_id		:= r_saved.(Requestor).(nb) in
		if ~~ (r_it_crit) then 
			(* safe to choose the non-critical request when the saved 
			request is not the upcoming one, which has the most priority *)
			(if (r_saved_id != cs_nat) then r_it else r_saved)
		else (
				 (* choose the request if: 
				 	  a) r_it[i] > r_saved[i] , i.e., r_it has more priority than r_saved
						b) r_it[i] is the due for the upcoming slot (most priority), this condition
								is useful to overwrite previously selected non-critical requests
						c) overwrite the default request, which has special date 0 *)
				 let cond 	:= (egt cs_nat r_it_id r_saved_id) || (r_it_id == cs_nat) || (r_saved.(Date) == 0) in
				 let slack 	:= find r_it.(Requestor).(nb) smap in
				 match slack with
					| None => if cond then r_it else r_saved
					| Some sl => 
						let t := (r_it.(Date) %% (SL * SN)) in
						if (sl > t) then r_saved else
						(* if there is enough slack, then the
						associated request is not priority, don't update variables *)
						(if cond then r_it else r_saved)
					end(* how to make sure that the initial request will never be picked ? *)
		)) (mkReq (mkRequestorSlack (lp cs) true _) 0 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0).
	Next Obligation.
		unfold lp; clear H.
		destruct (nat_of_ord cs == 0); [ rewrite ltn_predL; exact SN_pos | ].
		destruct cs; simpl; lia.
	Qed.

	Definition empty_nat_map := NatMap.empty nat.

	Definition early_start cs cnt smap req :=
		let ncs := nat_of_ord (Next_slot cs OLastCycle) in
		if (ncs == req.(Requestor).(nb)) then true
		else (let slack := find ncs smap in
			match slack with
			| None => false
			| Some sl => SL.-1 - cnt  < sl
			end
		).

	Definition Next_state (R : Requests_t) (AS : TDM_state_t) 
		: (TDM_state_t * (Command_kind_t * Request_t)) :=
    match AS return (TDM_state_t * (Command_kind_t * Request_t)) with
      | IDLE s slmap c P =>
				let P' := Enqueue R P in
        let s' := Next_slot s c in
        let c' := Next_cycle c in
				match P with
					| [::] => (IDLE s' slmap c' P', (NOP,nullreq))
					| _ => 
						(* normal start at beginning of slot *)
						if (c == OZCycle) then (
							let req := edf_slack_crit s slmap P in
							(RUNNING s' slmap c' OSlot1 (Enqueue R (Dequeue req P)) req, (PRE, req)))
						(* early-start optimisation *)
						else (
							(* if arbitrating in the middle of slot, then priority rules chance, hence
								 the different argument sent to the scheduling function *)
							let req := edf_slack_crit (Next_slot s OLastCycle) slmap P in 
						 	if (early_start s c slmap req) then
							(RUNNING s' slmap c' OSlot1 (Enqueue R (Dequeue req P)) req, (PRE, req))
							else (IDLE s' slmap c' P', (NOP,req)))						
				end
			| RUNNING s slmap c cnt_cmd P r =>
				let P' := Enqueue R P in
				let s' := Next_slot s c in
				let c' := Next_cycle c in
				let cnt_cmd' := Next_cycle cnt_cmd in
				if (cnt_cmd == OACT_date) then
					(RUNNING s' slmap c' cnt_cmd' P' r, (ACT, r))
				else if (cnt_cmd == OCAS_date) then
					(RUNNING s' slmap c' cnt_cmd' P' r, (Kind_of_req r, r))
				else if (cnt_cmd == OLastCycle) then
				  let req_id := r.Requestor).(nb) in
					(* slack can be gained in two ways:
						either by request finishing in the middle of the slot or at the end *)
					let slack' := if r.(Requestor).(crit) then 
					(SL.-1 - c) + ((dist SN (nat_of_ord s) req_id) * SL) else 0 in
					(* replaces existing slack with new slack -- won't accumulate *)
					let slmap' := NatMap.add req_id slack' slmap in
					(IDLE s' slmap' c' P', (NOP, nullreq))
				else 
					(RUNNING s' slmap c' cnt_cmd' P' r, (NOP, nullreq))
		end.

	Definition Init_state R :=
    IDLE OZSlot empty_nat_map OZCycle (Enqueue R [::]).

  Instance TDMes_implementation : Implementation_t := 
    mkImplementation Init_state Next_state.

	Program Definition TDMes_arbitrate t :=
		mkTrace 
		(Default_arbitrate t).(Arbiter_Commands)
		(Default_arbitrate t).(Arbiter_Time)
		_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _.
	Admit Obligations.

	Program Instance TDMes_arbiter : Arbiter_t :=
		mkArbiter AF TDMes_arbitrate _ _.
	Admit Obligations.

End TDMes.

Section TDMes_sim.

	Program Instance SYS_CFG : System_configuration :=
	{
		BANKGROUPS := 1;
		BANKS := 8;

		T_BURST := 1;
		T_WL    := 1;
		T_RRD_s := 1;
		T_RRD_l := 3;
		T_FAW := 20;
		T_RC  := 3;
		T_RP  := 1;
		T_RCD := 1;
		T_RAS := 4;
		T_RTP := 4;
		T_WR  := 1;
		T_RTW := 10;
		T_WTR_s := 1;
		T_WTR_l := 10;
		T_CCD_s := 1;
		T_CCD_l := 12;
		
		T_REFI := 10;
		T_RFC := 10;
	}.

	Program Instance TDMds_CFG : TDMds_configuration :=
	{
		SN := 2;
		SL := 8;
	}.

	Existing Instance REQUESTOR_CFG.
	Existing Instance TDMes_implementation.
	Existing Instance TDMes_arbiter.

	Program Definition ReqA0 := mkReq 
		(mkRequestorSlack 0 true _) 2 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 5.

	Program Definition ReqB0 := mkReq (* 3 instead of 14 *)
		(mkRequestorSlack 1 true _) 14 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 10.

	Program Definition ReqC0 := mkReq 
		(mkRequestorSlack 2 false _) 26 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.

	Program Definition ReqB1 := mkReq 
		(mkRequestorSlack 1 true _) 28 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.

	Program Definition ReqA1 := mkReq 
		(mkRequestorSlack 0 true _) 40 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.

	Program Definition ReqC1 := mkReq 
		(mkRequestorSlack 2 false _) 40 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.

	Program Definition ReqB2  := mkReq 
		(mkRequestorSlack 1 true _) 44 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.

	Program Definition ReqA2  := mkReq 
		(mkRequestorSlack 0 true _) 62 RD (Nat_to_bankgroup 0) (Nat_to_bank 0) 0.

	Definition test_req_seq := [:: ReqA0;ReqB0;ReqC0;ReqB1;ReqA1;ReqC1;ReqB2;ReqA2].

	Program Instance AF : Arrival_function_t := Default_arrival_function_t test_req_seq.

	Compute (Arbitrate 80).(Commands).

	(* Next: introduce the concepts of page open / closed *)
	(* Do proofs about TDMes ? *)

End TDMes_sim.