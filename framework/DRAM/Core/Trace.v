Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From mathcomp Require Export ssreflect ssrnat ssrbool seq eqtype.
From DRAM Require Export Commands.

Section Trace.

	Context {SYS_CFG      : System_configuration}.
  Context {REQESTOR_CFG : Requestor_configuration}.

  Definition isACT_or_PRE cmd :=
    (isACT cmd || isPRE cmd).

  Definition Before (a b : Command_t) :=
    a.(CDate) < b.(CDate).

  Definition Before_at (a b : Command_t) :=
    a.(CDate) <= b.(CDate).

  Definition After (a b : Command_t) := 
    a.(CDate) > b.(CDate).
  
  Definition Apart (a b : Command_t) t :=
    a.(CDate) + t < b.(CDate).

  Definition Apart_at_least (a b : Command_t) t :=
    a.(CDate) + t <= b.(CDate).

  Definition Between (l : Commands_t) (a b : Command_t) : Commands_t :=
    [seq cmds <- l | (cmds.(CDate) > a.(CDate)) && (cmds.(CDate) < b.(CDate))].

	Fixpoint Diff_Bank_ a (S : seq Command_t) :=
    match S with
      | [::]  => true
      | b::S' => (a != b) && (Diff_Bank_ a S')
    end.

  Fixpoint Diff_Bank (S : seq Command_t) :=
    match S with
      | [::]  => true
      | a::S' => Diff_Bank_ a S' && Diff_Bank S'
    end.

	Definition Same_Row a b := (get_row a == get_row b).
	Definition Same_Bank a b := (get_bank a == get_bank b).
	Definition Same_Bankgroup a b := (get_bankgroup a == get_bankgroup b).

	Record TestTrace_t := mkTestTrace
	{
		TestCommands : Commands_t;
		TestTime     : nat;
	}.

  Record Trace_t := mkTrace
  {
    Commands : Commands_t;
    Time     : nat;

    (* All commands must be uniq *)
    Cmds_uniq    : uniq Commands;

    (* All commands have to occur before the current time instant *)
    Cmds_time_ok : forall cmd, cmd \in Commands -> cmd.(CDate) <= Time;

    (* ------------------------------------------------------------------------- *)
    (* -------------------Timing constraints ----------------------------------- *)
    (* ------------------------------------------------------------------------- *)

		(* ------------------ Intra-bank constraints ------------------------------- *)

    (* Ensure that the time between an ACT and a CAS commands respects T_RCD *)
    Cmds_T_RCD_ok : forall a b, a \in Commands -> b \in Commands ->
										 isACT a -> isCAS b -> Same_Bank a b ->
										 Before a b -> Apart_at_least a b T_RCD;

    (* Ensure that the time between a PRE and an ACT commands respects T_RP *)
    Cmds_T_RP_ok : forall a b, a \in Commands -> b \in Commands ->
										 isPRE a -> isACT b -> Same_Bank a b ->
                     Before a b -> Apart_at_least a b T_RP;

    (* Ensure that the time between two ACT commands respects T_RC *)
    Cmds_T_RC_ok : forall a b, a \in Commands -> b \in Commands ->
										 isACT a -> isACT b -> Same_Bank a b ->
                     Before a b -> Apart_at_least a b T_RC;

    (* Maximum interval between ACT and PRE is T_RAS *)       
    Cmds_T_RAS_ok : forall a b, a \in Commands -> b \in Commands ->
                     isACT a -> isPRE b -> Same_Bank a b -> 
										 Before a b -> Apart_at_least a b T_RAS;
      
    (* RL *)
    (* WL *)

    (* Ensure that the time between a CRD and a PRE commands respects T_RTP *)
    Cmds_T_RTP_ok : forall a b, a \in Commands -> b \in Commands ->
                     isCRD a -> isPRE b -> Same_Bank a b -> 
										 Before a b -> Apart_at_least a b T_RTP;

    (* Ensure that the time between a CWR and a PRE commands respects T_WR + T_WL + T_BURST *)
    Cmds_T_WTP_ok : forall a b, a \in Commands -> b \in Commands ->
                     isCWR a -> isPRE b -> Same_Bank a b -> 
										 Before a b -> Apart_at_least a b (T_WR + T_WL + T_BURST);

    (* ------------------ Intra and Inter-bank constraints ----------------------- *)

    Cmds_T_RtoW_ok : forall a b, a \in Commands -> b \in Commands ->
                     isCRD a -> isCWR b -> Before a b ->
                     Apart_at_least a b T_RTW;

    Cmds_T_WtoR_SBG_ok : forall a b, a \in Commands -> b \in Commands ->
                     isCWR a -> isCRD b -> Before a b -> Same_Bankgroup a b ->
                     Apart_at_least a b (T_WTR_l + T_WL + T_BURST);

    Cmds_T_WtoR_DBG_ok : forall a b, a \in Commands -> b \in Commands ->
                     isCWR a -> isCRD b -> Before a b -> ~~ Same_Bankgroup a b ->
                     Apart_at_least a b (T_WTR_s + T_WL + T_BURST);

    Cmds_T_CCD_SBG_ok : forall a b, a \in Commands -> b \in Commands ->
                        (isCRD a /\ isCRD b) \/ (isCWR a /\ isCWR b) -> Before a b -> Same_Bankgroup a b ->
                        Apart_at_least a b T_CCD_l;

    Cmds_T_CCD_DBG_ok : forall a b, a \in Commands -> b \in Commands ->
                        (isCRD a /\ isCRD b) \/ (isCWR a /\ isCWR b)-> Before a b -> ~~ Same_Bankgroup a b ->
                        Apart_at_least a b T_CCD_s;

		(* ------------------ Refresh-related constraints -------------------------- *)
		Cmds_T_RFC_ok : forall a b, a \in Commands -> b \in Commands ->
										isREF a -> ~~ isNOP b -> Before a b -> Apart_at_least a b T_RFC;

		Cmds_T_REFI_max_dist_ok : forall a, a \in Commands -> isREF a -> 
			exists b, b \in Commands /\ isREF b /\ b.(CDate) <= a.(CDate) + 9 * T_REFI;

		Cmds_T_REFI_density1_ok : forall t, 
			size ([seq cmd <- Commands | isREF cmd && (t <= cmd.(CDate)) && (cmd.(CDate) <= t + 2 * T_REFI)]) <= 16;

		Cmds_T_REFI_density2_ok : forall x, 
			(x+1) - 8 < size ([seq cmd <- Commands | isREF cmd && (cmd.(CDate) <= x * T_REFI)]);
  
    (* ------------------ Exclusively inter-bank constraints ------------------- *)
    Cmds_T_FAW_ok : forall a b c d,
                     a \in Commands -> b \in Commands -> c \in Commands -> d \in Commands ->
                     isACT a -> isACT b -> isACT c -> isACT d -> Diff_Bank [::a;b;c;d] ->
                     Before a b -> Before b c -> Before c d ->
                     Apart_at_least a d T_FAW;
										 
    Cmds_T_RRD_SBG_ok : forall a b, a \in Commands -> b \in Commands ->
                     isACT a -> isACT b -> ~~ Same_Bank a b -> Same_Bankgroup a b ->
                     Before a b -> Apart_at_least a b T_RRD_l;

    Cmds_T_RRD_DBG_ok : forall a b, a \in Commands -> b \in Commands ->
                     isACT a -> isACT b -> ~~ Same_Bank a b -> ~~ Same_Bankgroup a b -> 
                     Before a b -> Apart_at_least a b T_RRD_s;

    (* ------------------------------------------------------------------------- *)
    (* ----------------------- FUNCTIONAL CORRECTNESS -------------------------- *)
    (* ------------------------------------------------------------------------- *)
    
    Cmds_ACT_ok: forall a b, a \in Commands -> b \in Commands ->
                 isACT a \/ isCAS a -> isACT b -> Same_Bank a b -> Before a b ->
                 exists c, (c \in Commands) && (isPRE c) && Same_Bank b c && Same_Bank a c
								 && (After c a) && (Before c b);

    (* Every CAS command is preceded by a matching ACT without another ACT or PRE in between *)
    Cmds_row_ok : forall b c, b \in Commands -> c \in Commands ->
                  isCAS b -> isPRE c -> Same_Bank b c -> Before c b -> 
                  exists a, (a \in Commands) && (isACT a) && Same_Row a b && (After a c) && (Before a b);

		(* implies the init state of the memory *)
    Cmds_initial : forall b, b \in Commands -> isCAS b ->
                   exists a, (a \in Commands) && (isACT a) && Same_Row a b && (Before a b);

		(* Banks must be idle for at least T_RP cycles before a REF is issued *)
		Cmds_REF_ok : forall ref, ref \in Commands -> 
									(exists prea, (prea \in Commands) && 
									(isPREA prea) && (Before prea ref) && (prea.(CDate) + T_RP <= ref.(CDate)))
						 			\/
									(forall bank, bank \in All_banks -> exists pre, (pre \in Commands) &&
									(Before pre ref) && (pre.(CDate) + T_RP <= ref.(CDate)))
  }.

End Trace.