
Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From mathcomp Require Import ssreflect ssrnat ssrbool eqtype seq zify.
From Coq Require Import DecidableType DecidableTypeEx.
From DRAM Require Export System Address.

Class Requestor_configuration := {
	Requestor_t : eqType;
}.

Section Requests.

	Context {SYS_CFG : System_configuration}. 
	Context {REQ_CFG : Requestor_configuration}.

	(* This will be All_banks for most algorithms *)
	(* Class BankMap := mkBankMap {
		Mapping : Requestor_t -> Banks_t 
	}.

	Context {BK_MAP : BankMap}. *)

	Inductive Request_kind_t : Set := RD | WR.

	#[local] Definition Request_kind_eqdef (a b : Request_kind_t) :=
		match a, b with
			| RD, RD
			| WR, WR => true
			| _, _   => false
		end.

	Lemma Request_kind_eqn : Equality.axiom Request_kind_eqdef.
	Proof.
		unfold Equality.axiom. intros.
		destruct (Request_kind_eqdef x y) eqn:H; unfold Request_kind_eqdef in *.
		- apply ReflectT. destruct x, y; inversion H; auto.
		- apply ReflectF; destruct x, y; inversion H; unfold not; intros; inversion H0.
	Qed.

	Canonical Request_kind_eqMixin := EqMixin Request_kind_eqn.
	Canonical Request_kind_eqType:= Eval hnf in EqType Request_kind_t Request_kind_eqMixin.
		
	Record Request_t := mkReq
	{
		Requestor : Requestor_t;
		Date : nat;
		Kind : Request_kind_t;
		Address : Address_t;

		(* The bank has to be included in the mapping for such requestor *)
		(* Address_matches_BKMAP : Address.(Bank) \in Mapping Requestor *)
	}.

	Local Definition Request_eqdef (a b : Request_t) :=
		(a.(Requestor) == b.(Requestor)) &&
		(a.(Date) == b.(Date)) &&
		(a.(Kind) == b.(Kind)) &&
		(a.(Address) == b.(Address)).

	Local Definition Request_eqdef_ (a b : Request_t) :=
		(a.(Requestor) == b.(Requestor)) &&
		(a.(Date) == b.(Date)) &&
		(a.(Kind) == b.(Kind)) &&
		(a.(Address) == b.(Address)).

	Lemma Request_eqn : Equality.axiom Request_eqdef.
	Proof.
		unfold Equality.axiom. intros. destruct Request_eqdef eqn:H.
		{
			apply ReflectT; unfold Request_eqdef in *.
			move: H => /andP [/andP [/andP [/eqP H0 /eqP H1 /eqP H2 /eqP H3]]].
			destruct x,y; simpl in *; by subst.
			(* by rewrite (ProofIrrelevance.proof_irrelevance (Address1.(Bank) \in BK_MAP.(Mapping) Requestor1)  *)
				(* Address_matches_BKMAP0 Address_matches_BKMAP1). *)
		}
		apply ReflectF; unfold Request_eqdef, not in *.
		intro BUG; apply negbT in H; rewrite negb_and in H.
		destruct x, y.
			move: H => /orP [H | /eqP Address].
			rewrite negb_and in H.
			move: H => /orP [H | /eqP Kind].
			rewrite negb_and in H.
			move: H => /orP [/eqP Req | /eqP Date].
			by apply Req; inversion BUG.
			by apply Date; inversion BUG.
			by apply Kind; inversion BUG.
			by apply Address; inversion BUG.
	Qed.

	Canonical Request_eqMixin := EqMixin Request_eqn.
	Canonical Request_eqType := Eval hnf in EqType Request_t Request_eqMixin.

	Definition Requests_t := seq.seq Request_t.

End Requests.