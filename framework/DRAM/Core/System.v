
Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From mathcomp Require Import eqtype ssrnat ssrbool.

Class System_configuration :=
{
  BANKGROUPS : nat;
  BANKS      : nat;

  T_BURST : nat;   (* delay of a burst transfer RD/WR *)
  T_WL    : nat;   (* delay between a WR and its bus transfer *)
  T_RRD_s : nat;   (* ACT to ACT delay inter-bank : different bank groups *)
  T_RRD_l : nat; 	 (* ACT to ACT delay inter-bank : same bank groups*)
  T_FAW  : nat;    (* Four ACT window inter-bank *)
  T_RC   : nat;    (* ACT to ACT delay intra-bank *)
  T_RP   : nat;    (* PRE to ACT delay intra-bank *)
  T_RCD  : nat;    (* ACT to CAS delay intra-bank *)
  T_RAS  : nat;    (* ACT to PRE delay intra-bank *)
  T_RTP  : nat;    (* RD to PRE delay intra-bank *)
  T_WR   : nat;    (* WR end to PRE delay intra-bank *)
  T_RTW  : nat;    (* RD to WR delay intra- + inter-bank *)
  T_WTR_s : nat;	 (* WR to RD delay intra- + inter-bank : different bank groups *)
  T_WTR_l : nat;	 (* WR to RD delay intra- + inter-bank : same bank groups *)
  T_CCD_s : nat;	 (* RD/WR to RD/WR delay intra- + inter-bank : different bank groups *)
  T_CCD_l : nat;	 (* RD/WR to RD/WR delay intra- + inter-bank : same bank groups *)

	(* Refresh related properties *)
	(* Assuming 1X refresh mode *)

	T_REFI 	: nat;	 
	(* All banks must be pre-charged and idle for a minimum time tRP before the REF command can be applied *)
	(* Address bits should be don't care during a refresh command *)
	T_RFC 	: nat; (* Depends on memory density *)
	(* Delay to be respected between REF and the next valid command  (except DES) *)
	(* Maximum 8/16/32 REF can be postponed -> 1X/2X/4X refresh mode *)

  (* POs *)
  BANKGROUPS_pos : BANKGROUPS != 0;
  BANKS_pos      : BANKS != 0;
  
	T_REFI_pos : T_REFI > 0;
	T_REFI_GT_T_RFC : T_REFI > T_RFC;

  T_RRD_s_pos  : T_RRD_s != 0;
  T_RRD_l_pos  : T_RRD_l != 0;
  T_FAW_pos  : T_FAW != 0;
  T_RC_pos   : T_RC != 0;
  T_RP_pos   : T_RP != 0;
  T_RCD_pos  : T_RCD != 0;
  T_RAS_pos  : T_RAS != 0;
  T_RTP_pos  : T_RTP != 0;
  T_WR_pos   : T_WR != 0;
  T_RTW_pos  : T_RTW != 0;
  T_WTR_s_pos : T_WTR_s != 0;
  T_WTR_l_pos : T_WTR_l != 0;
  T_CCD_s_pos  : T_CCD_s != 0;
  T_CCD_l_pos  : T_CCD_l != 0;
	T_RFC_pos			: T_RFC > 0;

  T_RRD_bgs : T_RRD_s < T_RRD_l;
  T_WTR_bgs : T_WTR_s < T_WTR_l;
  T_CCD_bgs : T_CCD_s < T_CCD_l;
}.