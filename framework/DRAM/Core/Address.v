Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From mathcomp Require Import ssreflect ssrnat ssrbool seq eqtype fintype div ssrZ zify.
From Coq Require Import Program.
From DRAM Require Export System.

Section Address.

	Context {SYS : System_configuration}.

	(* ------------ Definition of Bankgroup ---------------- *)
  Definition Bankgroup_t := { bg : nat | bg < BANKGROUPS }.


  Program Definition Nat_to_bankgroup a : Bankgroup_t := 
    match a < BANKGROUPS with
      | true => (exist (fun x : nat => x < BANKGROUPS) a _) (* blank here is the proof that a satisfies P a *)
      | false => (exist _ (BANKGROUPS - 1) _)
    end.
  Next Obligation.
    rewrite subn1 ltn_predL lt0n; by specialize BANKGROUPS_pos.
  Qed. 

  Definition Bankgroup_to_nat (a : Bankgroup_t) : nat :=
    proj1_sig a.

	Lemma Bankgroup_to_nat_eq (bg0 bg1 : Bankgroup_t) :
		(Bankgroup_to_nat bg0 = Bankgroup_to_nat bg1) -> bg0 = bg1.
	Proof.
		intro H; unfold Bankgroup_to_nat in H.
		destruct bg0, bg1; cbv in H; subst x.
		specialize proof_irrelevance with (p1 := i) (p2 := i0) as H_pir.
		subst; reflexivity.
	Qed.

	(* ------------ Definition of Bank -------------------- *)
  Definition Bank_t := { a : nat | a < BANKS }.

  Program Definition Nat_to_bank a : Bank_t := 
    match a < BANKS with
    | true  => (exist _ a _)
    | false => (exist _ (BANKS - 1) _)
    end.
  Next Obligation.
    rewrite subn1 ltn_predL lt0n; by specialize BANKS_pos.
  Qed.

  Definition Bank_to_nat (a : Bank_t) : nat :=
    proj1_sig a.

	Lemma Bank_to_nat_eq (bk0 bk1 : Bank_t) :
		(Bank_to_nat bk0 = Bank_to_nat bk1) ->
		bk0 = bk1.
	Proof.
		intro H; unfold Bank_to_nat in H.
		destruct bk0, bk1; cbv in H; subst x.
		specialize proof_irrelevance with (p1 := i) (p2 := i0) as H_pir.
		subst; reflexivity.
	Qed.
	
  Definition Banks_t := seq.seq Bank_t.

  Definition All_banks : Banks_t := 
    map Nat_to_bank (iota 0 BANKS).

	(* ------------ Definition of Row --------------------- *)
  Definition Row_t := nat.

	(* ------------ Definition of Address --------------------- *)
	Record Address_t := mkAddress {
		Bankgroup : Bankgroup_t;
		Bank : Bank_t;
		Row : Row_t;
	}.

	Local Definition Address_eqdef (a b : Address_t) :=
    (a.(Bankgroup) == b.(Bankgroup)) &&
    (a.(Bank) == b.(Bank)) &&
    (a.(Row) == b.(Row)).

	Lemma Address_eqn : Equality.axiom Address_eqdef.
	Proof.
		unfold Equality.axiom. intros. destruct Address_eqdef eqn:H.
		{
			apply ReflectT. unfold Address_eqdef in *.
			move: H => /andP [/andP [/eqP BG /eqP BK /eqP RW]].
			destruct x,y; simpl in *; by subst.
		}
		apply ReflectF; unfold Address_eqdef, not in *.
		intro BUG.
		apply negbT in H; rewrite negb_and in H.
		destruct x, y.
			move: H => /orP [H | /eqP Row].
			2: { apply Row; inversion BUG; reflexivity. }
			rewrite negb_and in H.
			move: H => /orP [/eqP BG | /eqP Bank].
			2: { apply Bank; inversion BUG; reflexivity. }
			apply BG; inversion BUG; reflexivity.
	Qed.
	
	Canonical Address_eqMixin := EqMixin Address_eqn.
  Canonical Address_eqType := Eval hnf in EqType Address_t Address_eqMixin.

End Address.
