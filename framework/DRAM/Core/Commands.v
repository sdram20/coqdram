Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From mathcomp Require Import ssreflect eqtype ssrbool ssrnat fintype div ssrZ zify.

From Coq Require Import ProofIrrelevance PeanoNat.

From DRAM Require Export Requests.	

Section Commands.

	Context {SYS_CFG      : System_configuration}.
  Context {REQESTOR_CFG : Requestor_configuration}.
	(* Context {BK_MAP 			: BankMap}. *)

	Inductive Command_kind_t :=
		| CRD  : Request_t -> Command_kind_t
		| CRDA : Request_t -> Command_kind_t
		| CWR  : Request_t -> Command_kind_t
		| CWRA : Request_t -> Command_kind_t
		| ACT  : Request_t -> Command_kind_t
		| PRE  : Request_t -> Command_kind_t
		| REF  : Command_kind_t
		| PREA : Command_kind_t
		| NOP  : Command_kind_t.

	Local Definition Command_kind_eqdef (a b : Command_kind_t) :=
    match a, b with
      | ACT r0, ACT r1 => (r0 == r1)
			| PRE r0, PRE r1 => (r0 == r1)
			| PREA,PREA => true
			| CRD r0, CRD r1 => (r0 == r1)
			| CRDA r0, CRDA r1 => (r0 == r1)
			| CWR r0, CWR r1 => (r0 == r1)
			| CWRA r0, CWRA r1 => (r0 == r1)
      | REF, REF => true
      | NOP, NOP => true
      | _, _     => false
    end.
	
  Lemma Command_kind_eqn : Equality.axiom Command_kind_eqdef.
  Proof.
    unfold Equality.axiom. intros.
    destruct (Command_kind_eqdef x y) eqn:H; unfold Command_kind_eqdef in *.
    { apply ReflectT. destruct x, y; inversion H; try done;
			move: H => /eqP H; try (rewrite H; reflexivity). 
		}
  	{ apply ReflectF; destruct x, y; inversion H; unfold not; intros; inversion H0;
			move: H => /eqP H; try (contradict H; exact H3). 
		}
  Qed.

  Canonical Command_kind_eqMixin := EqMixin Command_kind_eqn.
  Canonical Command_kind_eqType := Eval hnf in EqType Command_kind_t Command_kind_eqMixin.

  Record Command_t := mkCmd {
    CDate	: nat;
    CKind	: Command_kind_t;
  }.

  Local Definition Command_eqdef (a b : Command_t) :=
    (a.(CDate) == b.(CDate)) && (a.(CKind) == b.(CKind)).

  Lemma Command_eqn : Equality.axiom Command_eqdef.
  Proof.
    unfold Equality.axiom. intros. destruct Command_eqdef eqn:H; unfold Command_eqdef in *.
    { apply ReflectT; move: H => /andP [/eqP CD /eqP R].
      destruct x,y; simpl in *; subst; done.
    }
    apply ReflectF. unfold not in *. intro BUG.
    apply negbT in H; rewrite negb_and in H.
    destruct x, y.
      move: H => /orP [H | /eqP CK].
      move: H => /eqP CD.
      by apply CD; inversion BUG.
      by apply CK; inversion BUG.
  Qed.

  Canonical Command_eqMixin := EqMixin Command_eqn.
  Canonical Command_eqType := Eval hnf in EqType Command_t Command_eqMixin.

  Definition Command_lt a b :=
    a.(CDate) > b.(CDate).

  Definition Kind_of_req req :=
    match req.(Kind) with
      | RD => CRD
      | WR => CWR
    end.

  Definition isACT (cmd : Command_t) :=
    match cmd.(CKind) with
		| ACT _ => true
		| _ => false
		end.

  Definition isPRE (cmd : Command_t) :=
		match cmd.(CKind) with
		| PRE _ => true
		| _ => false
		end.
    
  Definition isPRE_or_PREA (cmd : Command_t):= 
    match cmd.(CKind) with
      | PRE _ 
      | PREA => true
      | _ => false
    end.

  Definition isNOP cmd := cmd.(CKind) == NOP.

  Definition isPREA (cmd : Command_t) := cmd.(CKind) == PREA.

  Definition isCRD (cmd : Command_t) :=
		match cmd.(CKind) with
		| CRD _
		| CRDA _ => true
		| _ => false
		end.
  
  Definition isCWR (cmd : Command_t) :=
    match cmd.(CKind) with
		| CWR _
		| CWRA _ => true
		| _ => false
		end.

  Definition isCAS (cmd : Command_t) := isCRD cmd || isCWR cmd.

	Definition isCAS_cmdkind (cmd : Command_kind_t) :=
		match cmd with
		| CRD _ | CRDA _ | CWR _ | CWRA _ => true
		| _ => false
		end.

  Definition isREF (cmd : Command_t) := (cmd.(CKind) == REF).

	Definition isREF_cmdkind (cmd : Command_kind_t) :=
		match cmd with
		| REF => true
		| _ => false
		end.

  Definition ACT_of_req req t := mkCmd t (ACT req).

  Definition CAS_of_req req t := mkCmd t ((Kind_of_req req) req).
  
  Definition issueREF t := mkCmd t REF.

	Definition PRE_of_req req t := mkCmd t (PRE req).

  Definition Commands_t := seq.seq Command_t.

	Definition get_req (cmd : Command_t) : option Request_t :=
		match cmd.(CKind) with
		| ACT r
		| CRD r 
		| CRDA r
		| CWR r
		| CWRA r
		| PRE r => Some r
		| _ => None
		end.

	Definition get_req_from_cmdkind (cmd : Command_kind_t) :=
		match cmd with
			| CRD r | CRDA r | CWR r | CWRA r | ACT r | PRE r => Some r
			| _ => None
		end.

	Definition get_bank (cmd : Command_t) : option Bank_t :=
		match cmd.(CKind) with
		| ACT r => Some r.(Address).(Bank)
		| CRD r => Some r.(Address).(Bank)
		| CRDA r => Some r.(Address).(Bank)
		| CWR r => Some r.(Address).(Bank)
		| CWRA r => Some r.(Address).(Bank)
		| PRE r => Some r.(Address).(Bank)
		| _ => None
		end.

	Lemma get_bank_get_req cmd bk req :
		get_bank cmd = Some bk -> get_req cmd = Some req ->
		bk = req.(Address).(Bank).
	Proof.
		unfold get_bank, get_req; destruct cmd.(CKind); intros;
		try (
		move: H0 => /eqP H0; rewrite inj_eq in H0; [ | exact ssrfun.Some_inj]; move: H0 => /eqP H0; subst;
		move: H => /eqP H; rewrite inj_eq in H; [ | exact ssrfun.Some_inj]; move: H => /eqP H; subst; reflexivity
		);
		discriminate H.
	Qed.

	Definition get_bankgroup (cmd : Command_t) : option Bankgroup_t :=
		match cmd.(CKind) with
		| ACT r => Some r.(Address).(Bankgroup)
		| CRD r => Some r.(Address).(Bankgroup)
		| CRDA r => Some r.(Address).(Bankgroup)
		| CWR r => Some r.(Address).(Bankgroup)
		| CWRA r => Some r.(Address).(Bankgroup)
		| PRE r  => Some r.(Address).(Bankgroup)
		| _ => None
		end.

	Definition get_row (cmd : Command_t) : option Row_t :=
		match cmd.(CKind) with
		| ACT r => Some r.(Address).(Row)
		| CRD r => Some r.(Address).(Row)
		| CRDA r => Some r.(Address).(Row)
		| CWR r => Some r.(Address).(Row)
		| CWRA r => Some r.(Address).(Row)
		| _ => None
		end.
		
End Commands.
