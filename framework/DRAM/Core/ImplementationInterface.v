Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From DRAM Require Export Arbiter.

Section ImplementationInterface.

	Context {SYS_CFG      	: System_configuration}.
  Context {REQUESTOR_CFG 	: Requestor_configuration}.
	(* Context {BK_MAP					: BankMap}. *)

	Class Arbiter_configuration := {
    State_t : Type;
  }.

  Context {ARBITER_CFG  	: Arbiter_configuration}.

  Class Implementation_t := mkImplementation
  {
    Init : Requests_t -> State_t;
    Next : Requests_t -> State_t -> State_t * Command_kind_t;
  }.

	Record Arbiter_state_t := mkArbiterState
  {
    Arbiter_Commands        : Commands_t;
    Arbiter_Time            : nat;
    Implementation_State    : State_t;
  }.

	(* Computes the t_th state *)
  Fixpoint Default_arbitrate {AF : Arrival_function_t} {IM : Implementation_t} t {struct t}: Arbiter_state_t :=
    let R := Arrival_at t in 
    match t with
      | 0     => mkArbiterState [::] t (Init R)
      | S(t') => let old_state := Default_arbitrate t' in
                 let (new_state,new_cmd_kind) := Next R old_state.(Implementation_State) in
                 let new_cmd := mkCmd t new_cmd_kind in
                 let cmd_list := (new_cmd :: old_state.(Arbiter_Commands)) in
                 mkArbiterState cmd_list t new_state
    end.

  Lemma Default_arbitrate_time_match {AF: Arrival_function_t} {IM : Implementation_t} t:
    (Default_arbitrate t).(Arbiter_Time) == t.
  Proof.
    induction t; [ done | ].
    simpl; destruct (Next (Arrival_at t.+1) (Default_arbitrate t).(Implementation_State)); simpl; by apply /eqP.
  Qed.

  Lemma Default_arbitrate_time {AF: Arrival_function_t} {IM : Implementation_t} t:
    (Default_arbitrate t).(Arbiter_Time) = t.
  Proof.
    apply /eqP; apply Default_arbitrate_time_match.
  Qed.

	Definition Enqueue (R P : Requests_t) := P ++ R.

  Definition Dequeue r (P : Requests_t) := rem r P.

	(* ------------ Hardware constrained versions -------------------- *)

	(* Class HW_Implementation_t := mkHWImplementation
  {
    HWInit : Request_t -> State_t;
    HWNext : Request_t -> State_t -> State_t * Command_kind_t;
  }.

	Fixpoint HW_Default_arbitrate 
		{HAF : HW_Arrival_function_t} 
		{HIM : HW_Implementation_t} t {struct t} : Arbiter_state_t :=
    let R := HW_Arrival_at t in 
    match t with
      | 0     => mkArbiterState [::] t (HWInit R)
      | S(t') => let old_state := HW_Default_arbitrate t' in
                 let (new_state,new_cmd_kind) := HWNext R old_state.(Implementation_State) in
                 let new_cmd := mkCmd t new_cmd_kind in
                 let cmd_list := (new_cmd :: old_state.(Arbiter_Commands)) in
                 mkArbiterState cmd_list t new_state
    end. *)

End ImplementationInterface.

