Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From mathcomp Require Import ssreflect ssrnat ssrbool seq eqtype div zify.

Lemma nat_ltn_leq_pred x y :
	y > 0 -> x <= (x + y).-1.
Proof. lia. Qed.

Lemma nat_cancel_terms a:
  a - a = 0.
Proof.
  apply /eqP.
  by rewrite subn_eq0 leqnn.
Qed.

Lemma nat_ltn_add_rev m p:
  m < m + p -> p > 0.
Proof.
  intros.
  induction p.
  { by rewrite addn0 ltnn in H. }
  { rewrite addnS leq_eqVlt in H.
    move: H => /orP [/eqP H | H].
    { move: H => /eqP H. rewrite eqSS in H.
      rewrite -{1}[m]add0n addnC eqn_add2l in H.
      move: H => /eqP H.
      by rewrite -H.
    }
    rewrite -[m.+1]addn1 -[(m + p).+1]addn1 ltn_add2r in H.
    apply IHp in H.
    apply ltn_trans with (n := p).
    exact H.
    done.
  }
Qed. 

Lemma Queue_non_empty [T : eqType] (ra : T) (s : seq T):
  ra \in s -> s != [::].
Proof.
  intros H.
  induction s.
  { discriminate H. }
  auto.
Qed.

Lemma nat_ltn_add n m:
  m > 0 -> n < n + m.
Proof.
  intros.
  induction n.
    { by rewrite add0n. }
    rewrite -addnC !addnS.
    rewrite -[(m + n).+1]addn1 -[n.+1]addn1.
    rewrite ltn_add2r.
    by rewrite addnC.
Qed.

Lemma nat_add_ltn m n p:
  m > 0 -> m + n < p -> n < p.
Proof.
  intros.
  induction n.
    { rewrite addn0 in H0; apply ltn_trans with (m := 0) in H0; (exact H0 || exact H). }
    apply ltn_trans with (m := n.+1) in H0.
      2: { 
        rewrite addnC.
        apply nat_ltn_add.
        exact H.
      }
    exact H0.
Qed.

Lemma nat_leq_addl m n p:
  m <= n -> m <= p + n.
Proof.
  intros H.
  induction p.
  - by rewrite add0n.
  - rewrite addSn.
    apply ltnW.
    by rewrite ltnS.
Qed.

Lemma nat_leq_addr m n p:
  m <= n -> m <= n + p.
Proof.
  intros H.
  induction p.
  - by rewrite addn0.
  - rewrite addnS.
    apply ltnW.
    by rewrite ltnS.
Qed.

Lemma nat_add_modn_sub n d s:
  0 < d -> s < d -> n %% d <= s -> (n + (s - n %% d)) %% d = s.
Proof.
  intros Hd Hs Hl.
  apply /eqP.
  rewrite -{2}(modn_small (m := s) (d := d)).
    2: exact Hs.
  rewrite addnBCA.
    3: exact Hl.
    2: apply leq_mod.
  rewrite -{2}(addn0 s) eqn_modDl -(eqn_modDr (n %% d)) subnK.
    2: apply leq_mod.
  rewrite add0n modn_mod eq_refl. exact isT.
Qed.

Lemma nat_add_modd_sub n d:
  0 < d -> (n + (d - n %% d)) %% d = 0.
Proof.
  intros H.
  apply /eqP.
  rewrite -(mod0n d) addnBCA.
    3: apply ltnW; rewrite ltn_mod; exact H.
    2: apply leq_mod.
  rewrite modnDl -(eqn_modDr (n %% d)) subnK.
    2: apply leq_mod.
  rewrite add0n modn_mod eq_refl. exact isT.
Qed.

Lemma nat_add_modd_sub_div n d:
  0 < d -> (n + (d - n %% d)) %/ d = (n %/ d).+1.
Proof.
  intros H.
  apply /eqP.
  rewrite eq_sym eqn_div.
    3: rewrite /dvdn nat_add_modd_sub;
        (exact H || rewrite eq_refl; exact isT).
    2: exact H.
  rewrite addnBA.
    2: apply ltnW; rewrite ltn_mod; exact H.
  rewrite -(eqn_add2l (n %% d)) addnBCA.
    3: apply leq_trans with (n := n);
        (rewrite leq_mod || rewrite leq_addr); exact isT.
    2: rewrite leqnn; exact isT.
rewrite subnn addn0 mulSn -addnACl -divn_eq eq_refl. exact isT.
Qed.

Lemma nat_add_gt_1 m n:
  m > 0 -> n > 0 -> 1 < m + n.
Proof.
  intros Hm Hn.
  induction (m).
    { discriminate Hm. }
    induction (n).
      { discriminate Hn. }
      rewrite addnS addnC addnS -ltn_predRL.
      by simpl.
Qed.

Lemma nat_ltlt_empty m n:
  0 < m -> m.-1 < n -> n < m -> false.
Proof.
  intros Hz Ha Hb.
  contradict Hb.
  apply /negP. 
  rewrite <- leqNgt.
  by rewrite <- (prednK (n := m)).
Qed.

Lemma nat_eq_ltS m n :
  m = n -> m < n.+1.
Proof.
  intros.
  induction n.
    { by rewrite H. }
    rewrite H; simpl. rewrite <- ltn_predRL; by simpl.
Qed.

Lemma nat_maxn_add m n x: ((maxn m n) + x) = (maxn (m + x) (n + x)).
Proof.
  unfold maxn.
  destruct (m < n) eqn:Hlt, (m + x < n + x) eqn:Hltp; auto;
  contradict Hltp; by rewrite ltn_add2r Hlt.
Qed.

Lemma nat_ltnn_add n: forall x, 0 < x -> is_true (n < n + x).
Proof.
  intros.
  induction n. 
    - apply /ltP. apply PeanoNat.Nat.lt_lt_add_l. by apply /ltP.
    - auto.
Qed.

Lemma nat_add_lt_add m1 m2 n1 n2: is_true ((m1 < m2) && (n1 < n2)) -> is_true (m1 + n1 < m2 + n2).
Proof.
  intros.
  rewrite <- addn1. rewrite addnACl addnC.
  apply leq_add.
  - rewrite addnC addn1. by move : H => /andP /proj1 H. 
  - apply ltnW. by move : H => /andP /proj2 H. 
Qed.

Lemma nat_ltmaxn_l_add a b: forall x, 0 < x -> is_true (a < (maxn a b) + x).
Proof.
  intros.
  unfold maxn.
  destruct (a < b) eqn:Hlt.
    - by apply /ltn_addr.
    - by move : H => /nat_ltnn_add H.
Qed.

Lemma nat_lt_ltmaxn_add a b c x: 0 < x -> is_true (maxn a b + x < c) -> is_true (a < c).
Proof.
  unfold maxn.
  intros Hz H.
  destruct (a < b) eqn:Hlt.
    - apply (ltn_addr x) in Hlt.
      by apply (ltn_trans (m := a) (n := b+x)) in H.
    - move : Hz => /(nat_ltnn_add a x) Hz.
      by apply (ltn_trans (m := a) (n := a+x)) in H.
Qed.

Lemma nat_lt_add_lemaxn_add n m o: forall x, (n < m + x) -> is_true (n < (maxn m o) + x).
Proof.
  intros.
  unfold maxn.
  destruct (m < o) eqn:Hlt. 
  - rewrite <- (ltn_add2r x m o) in Hlt.
    by apply ltn_trans with (n := m + x) (m := n) (p := o + x) in Hlt.
  - done.
Qed.

Lemma nat_le_lemaxn n m o: (n <= m) -> (n <= (maxn m o)).
Proof.
  intros.
  unfold maxn.
  destruct (m < o) eqn:Hlt. 
  - move : H  => /leP /(PeanoNat.Nat.le_trans n m o) H.
    by move : Hlt  => /ltP /PeanoNat.Nat.lt_le_incl /H /leP.
  - auto.
Qed.

Lemma nat_le_add_lemaxn_add n m o: forall x, (n <= m + x) -> is_true (n <= (maxn m o) + x).
Proof.
  intros.
  unfold maxn.
  destruct (m < o) eqn:Hlt. 
  - rewrite <- (ltn_add2r x m o) in Hlt.
    apply ltnW in Hlt.
    by apply leq_trans with (n := m + x) (m := n) (p := o + x) in Hlt.
  - auto.
Qed.

Lemma nat_le_lemaxn_add n m o: forall x, (n <= m) -> (n <= (maxn m o) + x).
Proof.
  intros.
  apply nat_le_lemaxn with (o := o) in H.
  apply leq_trans with (p := (maxn m o) + x) in H.
  - auto.
  - apply leq_addr.
Qed.

Lemma seq_rem_id {T : eqType} (x y : T) S:
  x \in S -> x != y
  -> x \in rem y S.
Proof.
  induction S; intros Hi He.
  - contradict Hi; by rewrite in_nil.
  - rewrite in_cons in Hi; move : Hi => /orP [/eqP Hi | Hi].
    - subst.
      move : He => /negPf He.
      by rewrite //= He in_cons eq_refl.
    - rewrite //=.
      destruct (a == y).
      - exact Hi.
      - apply IHS in Hi as IH.
        - by rewrite in_cons IH orbT.
        - exact He.
Qed.

Lemma seq_eq_cons_in {T : eqType} (x :T) S S':
  S = x::S' -> x \in S.
Proof.
  intros He.
  rewrite He.
  by rewrite in_cons eq_refl orTb.
Qed.

Lemma seq_in_filer_in {T : eqType} (x : T) S p:
  is_true (p x) -> x \in S -> x \in (filter p S).
Proof.
  intros Hp Hi.
  by rewrite mem_filter Hp Hi.
Qed.

Lemma seq_filter_eq_cons_p {T : eqType} (x :T) S S' p:
  filter p S = x::S' -> p x.
Proof.
  induction S; intros Hf.
  - contradict Hf; discriminate.
  - destruct (a == x) eqn:He, (p a) eqn:Hp'.
    - move : He => /eqP He; by subst.
    - rewrite //= Hp' in Hf; apply IHS in Hf.
      contradict Hf.
      move : He => /eqP He; subst; by rewrite Hp'.
    - rewrite //= Hp' in Hf.
      move : Hf => /eqP Hf; rewrite eqseq_cons in Hf; move : Hf => /andP [He' _].
      contradict He'; by rewrite He.
    - rewrite //= Hp' in Hf; apply IHS in Hf; exact Hf.
Qed.

Lemma seq_filter_rem_id {T : eqType} (x : T) S p :
  p x = false -> filter p (rem x S) = filter p S.
Proof.
  induction S; intros Hp.
  - done.
  - simpl.
    destruct (a == x) eqn:He, (p a) eqn:Hp'.
    - contradict Hp'; move : He => /eqP He; subst; by rewrite Hp.
    - move : He => /eqP He; subst; reflexivity.
    all: by rewrite //= Hp' IHS.
Qed.

Lemma seq_filter_rem {T : eqType} (x : T) S p:
  filter p (rem x S) = rem x (filter p S).
Proof.
  induction S.
  - simpl. reflexivity.
  - simpl.
    destruct (a == x) eqn:He, (p a) eqn:Hp.
    - rewrite //= He. reflexivity.
    - rewrite -IHS seq_filter_rem_id //=; move : He => /eqP He; subst; exact Hp.
    - rewrite //= He Hp IHS. reflexivity.
    - rewrite //= Hp IHS. reflexivity.
Qed.

Lemma seq_index_zero_head {T : eqType} (x : T) S:
  x \in S -> index x S == 0 -> exists S', S = x :: S'.
Proof.
  intros Hi HI.
  induction S.
  - contradict Hi. rewrite in_nil. exact notF.
  - rewrite in_cons in Hi. move : Hi => /orP [/eqP Hi | Hi]; exists S.
    - subst. reflexivity.
    - simpl in Hi.
      destruct (a == x) eqn:He.
      - move : He => /eqP He. subst. reflexivity.
      - contradict HI.
        simpl. rewrite He eqE. exact notF.
Qed.

Lemma eq_seq_consr {T : eqType} (s : seq T):
  forall x, (s != x::s).
Proof.
  induction s; intros.
  - done.
  - rewrite eqseq_cons.
    specialize IHs with (x := a).
    move : IHs => /negbTE IHs.
    by rewrite IHs Bool.andb_false_r.
Qed.

Definition seq_sorted_pred T := T -> T -> bool.

Local Fixpoint seq_sorted_rec {T : Type} (P : seq_sorted_pred T) x s :=
  match s with
    | [::]   => true
    | x'::s' => if (P x x') then seq_sorted_rec P x' s'
                else false
  end.

Definition seq_sorted {T : Type} (P : seq_sorted_pred T) s :=
  match s with
    | [::]   => true
    | x'::s' => seq_sorted_rec P x' s'
  end.

Lemma seq_sorted_tail {T : eqType} (P : seq_sorted_pred T) s x: 
  (seq_sorted P (x::s)) -> (seq_sorted P s).
Proof.
  intros Hs. simpl in Hs.
  induction s.
  - done.
  - simpl in *.
    destruct (P x a).
    - done.
    - by contradict Hs.
Qed.

Lemma seq_sorted_cons {T : eqType} (P : seq_sorted_pred T) s x: 
  (seq_sorted P s) -> (forall y, y \in s -> P x y) -> (seq_sorted P (x::s)).
Proof.
  induction s; intros Hs Hip.
  - done.
  - specialize Hip with (y :=  a).
    rewrite in_cons eq_refl orTb in Hip.
    by rewrite /= Hip //=.
Qed.

Lemma all_filter [T : eqType] a (p : pred T) S: 
  all a S -> all a [seq x <- S | p x]. 
Proof.
  intros.
  induction S.
    auto.
    simpl in *.
    destruct (p a0).
      simpl. apply /andP.
      split. 
        move : H => /andP /proj1 H. auto.
        move : H => /andP /proj2 H. apply IHS in H. auto.
      move : H => /andP /proj2 H. apply IHS in H. auto.
Qed.

(* TODO replace by generic definition *)
(* Definition pred2_t (T : eqType) := T -> T -> bool.

Definition all_pred2 [T : eqType] (p : pred2_t T) S x :=
  all (p x) S.

Lemma all_pred2_filter [T : eqType] (p2 : pred2_t T) (p : pred T) S (x : T): 
  all_pred2 p2 S x -> all_pred2 p2 [seq y <- S | p y] x.
Proof.
  unfold pred2_t.
  intros.
  by apply all_filter.
Qed.

Fixpoint all_pred2_recursive [T : eqType] (p2 : pred2_t T) S :=
  match S with
    | [::]  => true
    | x::S' => all_pred2 p2 S' x && 
               all_pred2_recursive p2 S'
  end.

Lemma all_pred2_recursive_filter [T : eqType] (p2 : pred2_t T) (p : pred T) S: 
  all_pred2_recursive p2 S -> all_pred2_recursive p2 [seq x <- S | p x].
Proof.
  induction S; intros; auto.
    simpl in *. move : H => /andP H.
     destruct (p a); simpl.
      apply /andP. split.
        apply proj1 in H. by apply all_filter.
        apply proj2 in H. by apply IHS in H.
      apply proj2 in H. by apply IHS in H.
Qed. *)

(* Lemma filter_forall_pred [T : eqType] S (p : pred T): forall z , z \in [seq y <- S | p y ] -> p z.
  intros.
  rewrite (mem_filter p z S) in H. by move : H => /andP /proj1.
Qed.
 *)
