Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From mathcomp Require Import ssreflect eqtype ssrbool ssrnat fintype div ssrZ zify tuple.
From Coq Require Import List Program.
From RecordUpdate Require Import RecordSet.

From DRAM Require Export ImplementationInterface BankMachine.

Import ListNotations RecordSetNotations.

Section InterfaceSubLayer.

	Context {SYS_CFG      	: System_configuration}.
	Context {REQUESTOR_CFG 	: Requestor_configuration}.
	(* Context {BK_MAP					: BankMap}. *)

	Context {SCH_OPT 	: SchedulingOptions_t}.
	Context {AF : Arrival_function_t}.

	Class SchedulerInternalState := mkSIS 
	{
		SchState_t : Type
	}.

	Context {SCH_ST 	: SchedulerInternalState}.

	Record ImplSubLayerState_t := mkImplSubLayerState
	{
		SystemState	: SystemState_t;
		CMap				: ReqCmdMap_t;
		SchState	  : SchState_t;
	}.

	Lemma size_length : forall {A} (l : list A), size l = length l.
	Proof.
		intros.
		unfold size, length.
		reflexivity.
	Qed.

	Class Scheduler_t := mkScheduler {
		
		Schedule : 
			ReqCmdMap_t -> 		(* Filtered list *)
			SystemState_t -> 	(* System State *) 
			SchState_t -> 		(* Internal schedular state *)
			Command_kind_t;

		Schedule_Empty : forall m SYS_ST SCH_ST, m = [] -> Schedule m SYS_ST SCH_ST = NOP;

		Schedule_Cons : forall hd tl SYS_ST SCH_ST,
			Schedule (hd :: tl) SYS_ST SCH_ST = hd \/
			Schedule (hd :: tl) SYS_ST SCH_ST = Schedule tl SYS_ST SCH_ST;

		Schedule_In : forall m SYS_ST SCH_ST, let sch_cmd := Schedule m SYS_ST SCH_ST in
			(sch_cmd = NOP) \/ (sch_cmd \in m);
			
		InitSchState : ReqCmdMap_t -> SchState_t;
		
		UpdateSchState : 
			Command_kind_t -> (* Scheduled command *) 
			ReqCmdMap_t -> 		(* Unfiltered command list *) 
			SystemState_t -> 	(* System State *)
			SchState_t -> 		(* Current scheduler state *)
			SchState_t;
	}.
	
	Context {SCH : Scheduler_t}.

	#[global] Instance ARBITER_CFG : Arbiter_configuration :=
  {
    State_t := option ImplSubLayerState_t;
  }.
	

	(* --------------------------------------------------------------------------- *)
	(* ------------------------------ Init function ------------------------------ *)
	(* --------------------------------------------------------------------------- *)
	(* Change this to start with the nominal value of constraints *)
	Definition init_val := 1000.

	(* Check ACTIVE. *)

	Definition InitBank : BankState_t := 
		ACTIVE 100 (mkLocalCounters init_val init_val init_val init_val init_val init_val).

	Lemma repeat_size {A} (x : A) (N : nat) :
		size (repeat x N) == N.
	Proof.
		rewrite size_length repeat_length; apply /eqP; reflexivity.
	Qed.

	Definition InitSS := (mkSystemState 
		(exist _ (repeat InitBank BANKS) (repeat_size InitBank BANKS))
		(mkGlobalCounters
		 (exist _ (repeat init_val 3) (repeat_size 0 3))
		 (exist _ (repeat init_val BANKGROUPS) (repeat_size init_val BANKGROUPS))
		 init_val
		 (exist _ (repeat init_val BANKGROUPS) (repeat_size init_val BANKGROUPS))
		 init_val
		 (exist _ (repeat init_val BANKGROUPS) (repeat_size init_val BANKGROUPS))
		 T_RFC) BRD).

	Definition empty_rcmdmap : ReqCmdMap_t := [].

	Definition Init_SL_state (R : Requests_t) : option ImplSubLayerState_t :=
		let cmdmap := map_arriving_req_to_cmd R empty_rcmdmap InitSS in
		Some (mkImplSubLayerState InitSS cmdmap (InitSchState cmdmap)).

	(* --------------------------------------------------------------------------- *)
	(* ------------------------------ Next function ------------------------------ *)
	(* --------------------------------------------------------------------------- *)

	Definition replace_nonrdy_cmds (map : ReqCmdMap_t) SS : ReqCmdMap_t :=
		seq.map (fun cmd => if (iscmdOK cmd SS) then cmd else NOP) map.

	Definition Next_SL_state 
		(R : Requests_t) (ST : option ImplSubLayerState_t) 
		: (option ImplSubLayerState_t) * Command_kind_t :=
		match ST with
		| None => (None,NOP)
		| Some ST => 
			let SS 					:= ST.(SystemState) in
			(* The internal scheduler state *)
			let SchS				:= ST.(SchState) in
			(* ------------------------ Mapping phase ------------------------------- --*)
			(* Updates the mapping for existing requests *)
			let cmdmap' 			:= map_running_req_to_cmd ST.(CMap) SS in
			(* (Possibly) adds REFRESH-related commands*)
			let cmdmap'' 		:= IssueREF cmdmap' SS in
			(* ------------------------ Scheduling phase ---------------------------- --*)
			(* Filters the cmdmap to only consider ready commands *)
			let rdy_cmds  	:= replace_nonrdy_cmds cmdmap'' SS in
			let sch_cmd 	  := Schedule rdy_cmds SS SchS in
			(* ------------------------ Updating phase ---------------------------- ----*)
			(* If the scheduled command is CRD or CWR, remove it from the map *)
			let updated_map := (match sch_cmd with
													| CRD _ | CRDA _ | CWR _ | CWRA _ => seq.rem sch_cmd cmdmap'' 
													| _ => cmdmap''
												 end) in
			(* Updates the mapping for arriving requests *)
			let updated_map' := map_arriving_req_to_cmd R updated_map SS in
			(* Update the system *)
			let SS'					:= SystemUpdate sch_cmd (Some SS) in
			match SS' with
			| None => (None,NOP)
			| Some SS'valid => 
				let new_SchState := UpdateSchState sch_cmd cmdmap'' SS SchS in
				let new_ImplSubLayerState := mkImplSubLayerState SS'valid updated_map' new_SchState in
				(Some new_ImplSubLayerState, sch_cmd)
			end
		end.
		
	#[global] Instance ImplementationSubLayer : Implementation_t :=
		mkImplementation Init_SL_state Next_SL_state.

	Definition get_cACTsb_ (SS : option SystemState_t) (bk : Bank_t) : option Counter_t :=
		match SS with
		| None => None
		| Some SS' => let BS := seq.nth def_BKS (proj1_sig SS'.(Banks)) (Bank_to_nat bk) in 
			match BS with
			| IDLE lc
			| ACTIVE _ lc => Some lc.(cACTsb)
			end
		end.

	(* ------------------------------------ General properties about traces --------------------- *)

	Lemma cmd_date_lt : forall cmd t,
		cmd \in (Default_arbitrate t).(Arbiter_Commands) ->
		cmd.(CDate) <= t.
	Proof.
		intros cmd t.
		induction t; simpl; [done|].
		rewrite (surjective_pairing (Next_SL_state _ _)).
		intros H; rewrite in_cons in H; move : H => /orP [/eqP H | H]; [ rewrite H; simpl ; lia | ].
		apply IHt in H; lia.
	Qed.
		
	Lemma cmd_in_trace_at_CDate : forall cmd t,
		cmd \in (Default_arbitrate t).(Arbiter_Commands) ->
		cmd \in (Default_arbitrate cmd.(CDate)).(Arbiter_Commands).
	Proof.
		intros cmd t.
		induction t; simpl; [done|].
		rewrite (surjective_pairing (Next_SL_state _ _));
		intros H; rewrite in_cons in H; move : H => /orP [/eqP H | H].
		{ rewrite H; simpl.
			rewrite (surjective_pairing (Next_SL_state _ _)); simpl.
			rewrite in_cons; apply /orP; left; done.
		}
		apply IHt in H; assumption.
	Qed.

	Lemma cmd_date_gt_0 cmd t:
    cmd \in (Default_arbitrate t).(Arbiter_Commands) -> cmd.(CDate) > 0.
  Proof.
    induction t; [ done | ].
    simpl; unfold Next_SL_state.
    destruct (Default_arbitrate t).(Implementation_State) eqn:HS.	(* destruct (DecidableTyp) *)
		2: { simpl; rewrite in_cons; intro H; move: H => /orP [/eqP H | H]; [by rewrite H | by apply IHt]. }
		set SS := SystemUpdate _ _; destruct SS eqn:HSS.
		2: { simpl; rewrite in_cons; intro H; move: H => /orP [/eqP H | H]; [by rewrite H | by apply IHt]. }
		set sch := SCH.(Schedule) _ _ _; simpl.
		simpl; rewrite in_cons; intro H; move: H => /orP [/eqP H | H]; [by rewrite H | by apply IHt].
	Qed.

	Lemma previous_state_is_valid n IS:
		(Default_arbitrate n.+1).(Implementation_State) = Some IS ->
		exists IS', (Default_arbitrate n).(Implementation_State) = Some IS'.
	Proof.
		intros H. 
		move: H; simpl; unfold Next_SL_state.
		destruct (Default_arbitrate n).(Implementation_State) eqn:H_IS;	[|simpl; intros H; discriminate H].
		set SS := SystemUpdate _ _.
		destruct SS eqn:H_SS; [|simpl; intros H; discriminate H].
		set sch := SCH.(Schedule) _ _ _; simpl.
		intros H; injection H as H.
		exists i; reflexivity.
	Qed.

	Lemma SS_Banks_not_nil SS : 
		proj1_sig SS.(Banks) <> [].
	Proof.
		destruct SS.(Banks); simpl in *; specialize BANKS_pos as Hbanks.
		rewrite size_length in i.
		destruct x; [ simpl in i; lia | done ].
	Qed.

	Lemma ScheduleCmdOK map SS SCHS :
		let sch_cmd := SCH.(Schedule) (replace_nonrdy_cmds map SS) SS SCHS in
		iscmdOK sch_cmd SS.
	Proof.
	  cbv zeta; unfold replace_nonrdy_cmds; induction map; simpl;
		[ rewrite (Schedule_Empty [] SS SCHS); [ by simpl | reflexivity] | ].
		set hd := if _ then _ else _; set tl := seq.map _ _.
		specialize (Schedule_Cons hd tl SS SCHS) as [H | H]; rewrite H.
		{ unfold hd; destruct (iscmdOK a SS) eqn:HSS;
			[ by rewrite HSS | by simpl ]. }
		unfold tl; exact IHmap.
	Qed.

	Lemma SystenUpdate_valid n IS map Sys_S Sch_S : 
		(Default_arbitrate n).(Implementation_State) = Some IS ->
		let filtered_map := replace_nonrdy_cmds map Sys_S in
		isSome (SystemUpdate (SCH.(Schedule) filtered_map Sys_S Sch_S) (Some Sys_S)).
	Proof.
		intros H; cbv zeta.
		unfold replace_nonrdy_cmds; induction map.
		{ cbn [map]; specialize (Schedule_Empty [] Sys_S Sch_S) as H_empty; rewrite H_empty; [ | reflexivity];
			clear H_empty; unfold SystemUpdate; done. } 
		rewrite map_cons;
		set hd := if _ then _ else _; set tl := seq.map _ _.
		specialize (Schedule_Cons hd tl Sys_S Sch_S) as SCH_cons; destruct SCH_cons as [H_sch_hd | H_sch_tl];
		[ | rewrite H_sch_tl; unfold tl; exact IHmap ].
		rewrite H_sch_hd; unfold hd.
		destruct (iscmdOK _ _ ) eqn:H_cmd_ok; [ | unfold SystemUpdate; done ].
		unfold SystemUpdate; destruct a; try done;
		set bk := nth _ _ _; destruct bk eqn:H_bk; try done; simpl in H_cmd_ok; move: H_cmd_ok; set bk_prev := nth _ _ _;
		destruct bk_prev eqn:H_bk_prev; try done.
		5 : { 
			specialize (stillIdle_after_increment Sys_S (Bank_to_nat r.(Address).(Bank)) def_BKS) as H_idle.
			cbv zeta in H_idle; fold bk_prev in H_idle.
			assert (isIDLE bk_prev); [ by rewrite H_bk_prev | ];
			apply H_idle in H0; [ clear H_idle | apply SS_Banks_not_nil];
			contradict H0; fold bk; rewrite H_bk; by simpl.
		} all: 
		specialize (stillActive_after_increment Sys_S (Bank_to_nat r.(Address).(Bank)) def_BKS) as H_active;
		cbv zeta in H_active; fold bk_prev in H_active;
		assert (isACTIVE bk_prev); try (by rewrite H_bk_prev);
		apply H_active in H0; try (by apply SS_Banks_not_nil);
		contradict H0; fold bk; rewrite H_bk; by simpl.
	Qed.

	Lemma valid_state_when_cmd_in_trace cmd t :
		cmd \in (Default_arbitrate t).(Arbiter_Commands) -> ~~ isNOP cmd ->
		isSome (Default_arbitrate cmd.(CDate)).(Implementation_State).
	Proof.
		intros H Hnop.
		apply cmd_in_trace_at_CDate in H; induction cmd.(CDate); [ done | ].
		move: H; simpl; unfold Next_SL_state.
		destruct (Default_arbitrate n).(Implementation_State) eqn:HS.
		2: { 
			simpl; rewrite in_cons; intros H_; move: H_ => /orP [/eqP H_ | H_];
			[ rewrite H_ /isNOP //= in Hnop | apply IHn in H_; discriminate H_].
		}
		set SS := SystemUpdate _ _; destruct SS eqn:HSS; simpl.
		2: {
			contradict HSS; unfold SS; set map := IssueREF _ _.
			apply (SystenUpdate_valid) with (map := map) (Sys_S := i.(SystemState)) (Sch_S := i.(SchState)) in HS;
			cbv zeta in HS; unfold isSome in *; fold map in SS; fold SS in HS; fold SS; destruct SS; done.
		}
		done.
	Qed.

	Lemma iscmdOK_true_between_valid_states n IS_prev IS_cur map cmd: 
		(Default_arbitrate n).(Implementation_State) = Some IS_prev ->
		(Default_arbitrate n.+1).(Implementation_State) = Some IS_cur ->
		let SS := IS_prev.(SystemState) in
		let SCH_ST := IS_prev.(SchState) in
		cmd = (mkCmd n.+1 (SCH.(Schedule) (replace_nonrdy_cmds map SS) SS SCH_ST)) ->
		~~ isNOP cmd -> iscmdOK cmd.(CKind) IS_prev.(SystemState).
	Proof.
		simpl; unfold Next_SL_state; intros H.
		rewrite H; set SS := SystemUpdate _ _;
		destruct SS; [ | intros Hcontradiction; discriminate Hcontradiction ].
		set map_ := IssueREF _ _.
		set sch := SCH.(Schedule) _ _ _; simpl.
		intros _  Hcmd Hnop. 
		specialize (ScheduleCmdOK map IS_prev.(SystemState) IS_prev.(SchState)); cbv zeta; intros HH.
		subst; simpl.
		rewrite /isNOP //= in Hnop; move: HH => /eqP HH; rewrite HH in Hnop; done.
	Qed.

	Lemma command_matches_between_valid_states n IS_prev IS_cur cmd: 
		cmd.(CDate) = n.+1 -> ~~ isNOP cmd ->
		cmd \in (Default_arbitrate n.+1).(Arbiter_Commands) ->
		(Default_arbitrate n).(Implementation_State) = Some IS_prev ->
		(Default_arbitrate n.+1).(Implementation_State) = Some IS_cur ->
		let SS := IS_prev.(SystemState) in
		let SCH_ST := IS_prev.(SchState) in
		let map := (IssueREF
			(map_running_req_to_cmd IS_prev.(CMap) IS_prev.(SystemState))
			IS_prev.(SystemState)) in
		cmd = (mkCmd n.+1 (SCH.(Schedule) (replace_nonrdy_cmds map SS) SS SCH_ST)).
	Proof.
		intros Hdate Hnop Hcmd HS_prev HS_cur; cbv zeta; set map := IssueREF _ _.
		move: Hcmd; simpl; unfold Next_SL_state.
		rewrite HS_prev; set SS := SystemUpdate _ _; destruct SS eqn:HSS.
		2: { simpl; intros H; rewrite in_cons in H; move: H => /orP [/eqP H | H]; [ rewrite H /isNOP //= in Hnop | ].
			apply cmd_date_lt in H; rewrite Hdate in H; lia. }
		set sch := SCH.(Schedule) _ _ _; simpl.
		intros H; rewrite in_cons in H; move: H => /orP [/eqP H | H ]; [ assumption | ].
		apply cmd_date_lt in H; rewrite Hdate in H; lia.
	Qed.

	Lemma exists_command_in_trace t IS_past IS_future :
		(Default_arbitrate t).(Implementation_State) = Some IS_past ->
		(Default_arbitrate t.+1).(Implementation_State) = Some IS_future ->
		exists cmd, cmd \in (Default_arbitrate t.+1).(Arbiter_Commands) 
		/\ cmd.(CDate) = t.+1 
		/\ (iscmdOK cmd.(CKind) IS_past.(SystemState))
		/\ cmd.(CKind) = (SCH.(Schedule) (replace_nonrdy_cmds
			(IssueREF (map_running_req_to_cmd IS_past.(CMap) IS_past.(SystemState)) IS_past.(SystemState))
			IS_past.(SystemState)) IS_past.(SystemState) IS_past.(SchState))
		/\ exists (bk : option Bank_t), get_bank cmd = bk.
	Proof.
		intros H_cur.
		simpl; unfold Next_SL_state; rewrite H_cur.
		set sch_cmd := SCH.(Schedule) _ _ _.
		set SS := SystemUpdate _ _; destruct SS eqn:HSS; simpl; intro H; [ | discriminate H].
		exists (mkCmd t.+1 sch_cmd).
		split; [ rewrite in_cons; apply /orP; left; apply /eqP; reflexivity | ].
		split; [ simpl; reflexivity | ].
		split; [ unfold sch_cmd; apply ScheduleCmdOK | ].
		split; [ simpl; reflexivity | ].
		unfold get_bank; simpl; destruct sch_cmd; eapply ex_intro; reflexivity.
	Qed.

	Lemma different_banks bk0 bk1 :
		(Some bk0 == Some bk1) = false ->
		(Bank_to_nat bk1 == Bank_to_nat bk0) = false.
	Proof.
		intro H_diff; rewrite inj_eq in H_diff; [ | exact ssrfun.Some_inj ].
		apply /negP; apply contra_not with (P := (bk0 == bk1));
		[ | unfold not; intro H; move: H => /eqP H; rewrite H eq_refl in H_diff; done ].
		clear H_diff; intros H_eq; unfold Bank_to_nat in H_eq; move: H_eq => /eqP H_eq; apply Logic.eq_sym in H_eq.
		apply /eqP.
		destruct bk0 as [bk0_ p0], bk1 as [bk1_ p1]; simpl in *.
		subst bk0_.
		f_equal.
		apply proof_irrelevance.
	Qed.

	Lemma Next_state_matches cmd IS_prev IS n :
		iscmdOK cmd.(CKind) IS_prev.(SystemState) ->
		cmd.(CKind) = (SCH.(Schedule)
		(replace_nonrdy_cmds
			(IssueREF (map_running_req_to_cmd IS_prev.(CMap) IS_prev.(SystemState)) IS_prev.(SystemState))
			IS_prev.(SystemState)) IS_prev.(SystemState)
		IS_prev.(SchState)) ->
		(Default_arbitrate n).(Implementation_State) = Some IS_prev ->
		(Default_arbitrate n.+1).(Implementation_State) = Some IS ->
		SystemUpdate cmd.(CKind) (Some IS_prev.(SystemState)) = Some IS.(SystemState).
	Proof.
		intros H_is_cmd_ok H_cmd_eq H.
		cbn -[SystemUpdate]; unfold Next_SL_state; rewrite H.
		set SS := SystemUpdate _ _.
		destruct SS eqn:HSS; cbn -[SystemUpdate];
		[ | intro H_; discriminate H_ ].
		intro H_eq; injection H_eq as H_eq.
		assert (H_s : IS.(SystemState) = s); [ rewrite - H_eq //= | ].
		rewrite H_s; clear H_s.
		rewrite -HSS; clear HSS; unfold SS in *; clear SS.
		by rewrite H_cmd_eq.
	Qed.

	(* ------------------------------------ PROOFS ---------------------------------------------- *)
	(* ------------------------ specific to cACTsb ---------------------------------------------- *)

	(* Prove that a.(CDate) + T_RCD <= b.(CDate)  
	
		1) if (b) CAS in trace at time t -> iscmdOK at b.(CDate).-1

		(stated lemma) 
			2) if iscmdOK at b.(CDate), then T_RCD <= lc.(ACTsb) at b.(CDate) 
		
		3) if (a) ACT is trace at time t -> iscmdOK at a.(CDate).-1
		
			(ok but relies on unproved lemma)
			4) if iscmdOK at a.(CDate) lc.(cACTsb) = 0 at a.(Cdate).+1 (or a.(CDate) ?)

		(ok) 
			5) if lc.(cACTsb) = 0 at a.(CDate) and
					T_RCD <= lc.(cACTsb) at b.(CDate) then
					a.(CDate) + T_RCD <= b.(CDate)
	*)
	
	Lemma cACTsb_gt_TRCD : forall (t : nat) (IS : ImplSubLayerState_t) cmd,
		(Default_arbitrate t).(Implementation_State) = Some IS -> 
		isCAS cmd -> let r := get_req cmd in
		match r with
		| None => False
		| Some r => 
			iscmdOK cmd.(CKind) (IS.(SystemState)) ->
			let bk := Bank_to_nat r.(Address).(Bank) in
			let BS := seq.nth def_BKS (proj1_sig IS.(SystemState).(Banks)) bk in
			match BS with
			| IDLE _ => False
			| ACTIVE _ lc => is_true (T_RCD <= lc.(cACTsb))
			end
		end.
	Proof.
		cbv zeta. intros t IS cmd H iCAS. 
		destruct (get_req cmd) eqn:Hreq.
		2: { 
			destruct cmd; rewrite /isCAS /isCWR /isCWR //= in iCAS.
			rewrite /get_req //= in Hreq; destruct CKind; done. 
		}
		intro cmdOK.
		set bs := nth _ _ _; destruct bs eqn:Hbs; destruct cmd as [CDate CKind]; destruct CKind;
		unfold isCAS in iCAS; simpl in iCAS; try done; unfold get_req in Hreq; simpl in Hreq; injection Hreq as Hreq; subst;
		revert cmdOK; unfold iscmdOK; destruct IS.(SystemState).(Banks); simpl in *; fold bs; rewrite Hbs; try done;
		destruct (_ == r0); destruct (IS.(SystemState).(Busdir)); try done; lia.
	Qed.
	
	Lemma cACTsb_reset : forall (r : Request_t) (SS : SystemState_t),
		iscmdOK (ACT r) SS -> 
		let SS' := SystemUpdate (ACT r) (Some SS) in (* this being None will contradict the hypothesis before *)
		get_cACTsb_ SS' r.(Address).(Bank) = Some 0.
	Proof.
		intros r SS Hvalid; cbv zeta.
		unfold SystemUpdate.
		set l := nth _ _ _.
		destruct l eqn:Hl.
		2: {
			contradict Hvalid.
			unfold iscmdOK.
			set v := seq.nth _ _ _.
			destruct v eqn:Hv; [ | trivial ].
			assert (isIDLE v); [ by rewrite Hv | ].
			apply stillIdle_after_increment in H.
			2: { 
				destruct SS.(Banks); simpl in *; specialize BANKS_pos as Hbanks.
				rewrite size_length in i.
				destruct x; [ simpl in i; lia | done ].
			}
			unfold l in *; clear l. 
			rewrite Hl in H; discriminate H.
		}
		simpl; set idx := Bank_to_nat _.
		set ll := [seq Dummy_BankUpdate x | x <- `SS.(Banks)].
		rewrite (@nth_set_nth BankState_t def_BKS ll idx (ACTIVE r.(Address).(Row) (l0 <| cACTsb := 0|>))).
		simpl; rewrite eqtype.eq_refl; simpl; reflexivity.
	Qed.

	Lemma cACTsb_inc cmd (SS : SystemState_t) x : 
		~~ isACT cmd -> iscmdOK cmd.(CKind) SS ->
		forall bk,
		get_cACTsb_ (Some SS) bk = Some x ->
		get_cACTsb_ (SystemUpdate cmd.(CKind) (Some SS)) bk = Some (x + 1). 
	Proof.
		rewrite /isACT; destruct cmd; cbn -[addn iscmdOK SystemUpdate];
		destruct CKind; try done; intros _ H_is_cmd_ok bank; cbn -[SystemUpdate addn];
		set nth_rand := nth _ _ _; unfold SystemUpdate; simpl; destruct nth_rand eqn:H_bk_rand; intro Hc;
		try (set nth_update := nth _ _ _; destruct nth_update eqn:H_bk_update); simpl;
		move: H_is_cmd_ok; simpl; destruct SS.(Banks) as [Banks_ p]; simpl; intro H_is_cmd_ok; simpl in nth_rand, nth_update.

		all: try (by (* contracition to iscmdOK *)
			contradict H_is_cmd_ok;
			set nth_bk := nth _ _ _; destruct nth_bk eqn:H_bk; [ done | ];
			assert (Banks_ <> []) as H_banks_not_empty; 
			[ specialize BANKS_pos as Hbanks; rewrite size_length in p; destruct Banks_; [ simpl in p; lia | done ] | ];
			apply stillActive_after_simple_increment with (idx := Bank_to_nat r.(Address).(Bank)) (def := def_BKS) in H_banks_not_empty;
			[ | by fold nth_bk; rewrite H_bk //= ];
			contradict H_banks_not_empty; fold nth_update; rewrite H_bk_update /isACTIVE //=
		).

		all: try (
			rewrite nth_set_nth //=; destruct (Bank_to_nat bank == _) eqn:H_idx_eq; simpl
		).

		all: try ( (* cmd is to same bank as bk *)
			clear H_is_cmd_ok; move: H_idx_eq => /eqP H_idx_eq; apply Logic.eq_sym in H_idx_eq;
			unfold nth_rand, nth_update in *; clear nth_rand nth_update;
			rewrite H_idx_eq in H_bk_update
		).

		all: try (
			clear H_bk_update nth_update H_idx_eq H_is_cmd_ok;
			set nth_update := nth _ _ _; destruct nth_update eqn:H_bk_update
		).

		all: try (unfold nth_rand, nth_update in *; clear nth_rand nth_update).

		all: try (by (* correct increment cases *)
			assert (Bank_to_nat bank < size Banks_); [ destruct bank; move: p => /eqP p; simpl; by rewrite p | ];
			apply (@nth_map BankState_t def_BKS BankState_t def_BKS Dummy_BankUpdate (Bank_to_nat bank) Banks_) in H;
			rewrite H in H_bk_update; clear H; rewrite H_bk_rand in H_bk_update;
			rewrite /Dummy_BankUpdate /IncrementLocalCounters in H_bk_update;
			try (injection H_bk_update as H_eq_rows H_eq_cnt || injection H_bk_update as H_eq_rows); subst; simpl;
			move: Hc => /eqP Hc; rewrite inj_eq in Hc; [ | exact ssrfun.Some_inj ]; move: Hc => /eqP Hc;
			rewrite Hc -addn1; reflexivity
		).

		all: try (by (* contradictions to stillIdle *)
			assert (Banks_ <> []) as H_banks_not_empty;
			[ specialize BANKS_pos as Hbanks; rewrite size_length in p; destruct Banks_; [ simpl in p; lia | done ] | ];

			apply (stillIdle_after_simple_increment Banks_ (Bank_to_nat bank) def_BKS) in H_banks_not_empty;
			[ | by rewrite H_bk_rand ]; by rewrite H_bk_update //= in H_banks_not_empty
		).

		all: try (by (* contradictions to stillActive *)
			assert (Banks_ <> []) as H_banks_not_empty; 
			[ specialize BANKS_pos as Hbanks; rewrite size_length in p; destruct Banks_; [ simpl in p; lia | done ] | ];

			apply stillActive_after_simple_increment with (idx := Bank_to_nat bank) (def := def_BKS) in H_banks_not_empty;
			[ | by rewrite H_bk_rand //= ]; by rewrite H_bk_update //= in H_banks_not_empty
		).

		all: try (by (* PREA case -> update function is composite, requires different step *)
			move: H_bk_update; 
			rewrite - map_comp //=; set fun_ := ssrfun.comp _ _;
			specialize (@nth_map BankState_t def_BKS BankState_t def_BKS fun_ (Bank_to_nat bank) Banks_) as H_nth_map;
			rewrite H_nth_map; clear H_nth_map;
			[ | clear H_is_cmd_ok; destruct bank; simpl; move: p => /eqP p; by rewrite p ];
			unfold fun_; rewrite H_bk_rand /Dummy_BankUpdate //=; intro H_eq; injection H_eq as H_eq_cnts; subst;
			unfold IncrementLocalCounters; simpl; rewrite -addn1; move:Hc => /eqP Hc;
			rewrite inj_eq in Hc; [ | exact ssrfun.Some_inj ]; move: Hc => /eqP Hc;
			rewrite Hc; reflexivity
		).

		all: try (by
			move: H_bk_update; 
			rewrite - map_comp //=; set fun_ := ssrfun.comp _ _;
			specialize (@nth_map BankState_t def_BKS BankState_t def_BKS fun_ (Bank_to_nat bank) Banks_) as H_nth_map;
			rewrite H_nth_map; clear H_nth_map;
			[ | clear H_is_cmd_ok; destruct bank; simpl; move: p => /eqP p; by rewrite p ];
			unfold fun_; rewrite H_bk_rand /Dummy_BankUpdate //=; intro H_eq; injection H_eq as H_eq_cnts; subst
		).
	Qed.

	Lemma cACTsb_inc_diff_bank cmd SS x : 
		isACT cmd -> iscmdOK cmd.(CKind) SS ->
		forall bk, (get_bank cmd == Some bk) = false ->
		get_cACTsb_ (Some SS) bk = Some x ->
		get_cACTsb_ (SystemUpdate cmd.(CKind) (Some SS)) bk = Some (x + 1).
	Proof.
		rewrite /isACT; destruct cmd.
		cbn -[addn iscmdOK SystemUpdate sig_eqType eq_op].
		destruct CKind eqn:H_kind; try done; intros _ H_is_cmd_ok bk H_diff_bank.
		rewrite /get_bank //= in H_diff_bank; move: H_is_cmd_ok; simpl.
		destruct SS.(Banks) as [Banks_ p]; simpl; intros H_is_cmd_ok.
		set nth_rand := nth _ Banks_ _; unfold SystemUpdate; simpl. 

		set nth_up := nth _ _ _; destruct nth_up eqn:H_bk_up; simpl.
		2: {
			move: H_is_cmd_ok; set nth_bk := nth _ _ _; destruct nth_bk	eqn:H_bk; try done.
			assert (isIDLE nth_bk); [ by rewrite H_bk | ].
			apply stillIdle_after_simple_increment in H; fold nth_up in H;
			[ | specialize BANKS_pos as Hbanks; rewrite size_length in p; destruct Banks_; [ simpl in p; lia | done ] ].
			rewrite H_bk_up //= in H.
		}

		destruct nth_rand eqn:H_bk_rand; intros Hc; clear H_is_cmd_ok; rewrite nth_set_nth //=;
		apply different_banks in H_diff_bank; rewrite H_diff_bank;
		set nth_rand_up := nth _ _ _; destruct nth_rand_up eqn:H_nth_rand_up;

		try (
			unfold nth_rand, nth_rand_up, nth_up in *; clear nth_rand nth_rand_up nth_up;
			assert (H : Bank_to_nat bk < size Banks_); [ destruct bk; move: p => /eqP p; simpl; by rewrite p | ];
			apply (@nth_map BankState_t def_BKS BankState_t def_BKS Dummy_BankUpdate (Bank_to_nat bk) Banks_) in H;
			rewrite H in H_nth_rand_up; clear H; rewrite H_bk_rand /Dummy_BankUpdate /IncrementLocalCounters in H_nth_rand_up;
			injection H_nth_rand_up as H_eq_cnts; subst; simpl;
			apply /eqP; move: Hc => /eqP;
			rewrite !inj_eq; try (exact ssrfun.Some_inj); lia
		);

		try (
			assert (isIDLE nth_rand); [ by rewrite H_bk_rand | ];
			apply stillIdle_after_simple_increment in H; fold nth_rand_up in H;
			[ rewrite H_nth_rand_up //= in H |
			specialize BANKS_pos as Hbanks; rewrite size_length in p; destruct Banks_; [ simpl in p; lia | done ] ]
		);

		try (
			assert (isACTIVE nth_rand); [ by rewrite H_bk_rand | ];
			apply stillActive_after_simple_increment in H; fold nth_rand_up in H;
			[ rewrite H_nth_rand_up //= in H |
			specialize BANKS_pos as Hbanks; rewrite size_length in p; destruct Banks_; [ simpl in p; lia | done ] ]
		).
	Qed.

	Lemma cACTsb_gt_TRCD_after_update : forall (n : nat) (IS IS_prev: ImplSubLayerState_t) (cmd_ : Command_kind_t),
		let cmd := mkCmd n.+1 cmd_ in
		cmd \in (Default_arbitrate n.+1).(Arbiter_Commands) -> isCAS cmd ->
		(Default_arbitrate n.+1).(Implementation_State) = Some IS -> 
		(Default_arbitrate n).(Implementation_State) = Some IS_prev ->
		let SS := SystemUpdate cmd_ (Some IS_prev.(SystemState)) in
		let r := get_req cmd in
		match r with
		| None => False
		| Some r_v => 
			let cACTsb := get_cACTsb_ SS r_v.(Address).(Bank) in
			match cACTsb with
			| None => False
			| Some cACTsb_v => is_true (T_RCD <= cACTsb_v)
			end
		end.
	Proof.
		intros n IS IS_prev cmd_ cmd Hin iCAS H H_prev; cbv zeta.
		set req := get_req _; destruct req eqn:Hreq;
		[ | unfold req in *; clear req;
			rewrite /get_req in Hreq; rewrite /isCAS /isCRD /isCWR in iCAS; destruct cmd.(CKind); done ].
		
		set map := (IssueREF
			 (map_running_req_to_cmd IS_prev.(CMap) IS_prev.(SystemState))
			 IS_prev.(SystemState)).

		assert (~~ isNOP cmd) as H_cmd_not_NOP; [ move: iCAS; rewrite /isCAS /isNOP /isCRD /isCWR; destruct cmd.(CKind); done | ].

		apply (iscmdOK_true_between_valid_states n IS_prev IS map cmd) in H as H'; try done.
		2: {
			assert (cmd.(CDate) = n.+1) as H_; [ by unfold cmd | ].
			specialize (command_matches_between_valid_states n IS_prev IS cmd H_ H_cmd_not_NOP Hin H_prev H) as H';
			cbv zeta in H'; fold map in H'; exact H'.
		} rename H' into H_is_cmd_ok.

		specialize (cACTsb_gt_TRCD n IS_prev cmd H_prev iCAS) as H'; cbv zeta in H'.

		destruct (get_bank cmd) eqn:H_bank;
		[ | unfold get_bank in H_bank; rewrite /isCAS /isCRD /isCWR in iCAS; destruct cmd.(CKind); done ].
		rename b into bk.

		destruct (get_req cmd) eqn:H_req; [ | done ]; unfold req in *; move: Hreq => /eqP Hreq.	
		
		rewrite inj_eq in Hreq; [ | exact ssrfun.Some_inj ]; move: Hreq => /eqP Hreq; subst r0.
		apply H' in H_is_cmd_ok as H_; rename H_ into H_lc_geq_TRCD; clear H'.

		set bs := nth def_BKS (` IS_prev.(SystemState).(Banks)) (Bank_to_nat r.(Address).(Bank)).
		fold bs in H_lc_geq_TRCD; destruct bs eqn:Hbs; [ done | ]; rename r0 into row, l into lc.
		
		assert (get_cACTsb_ (Some IS_prev.(SystemState)) r.(Address).(Bank) = Some lc.(cACTsb)) as H_;
		[ unfold get_cACTsb_; fold bs; rewrite Hbs; reflexivity | ].

		assert (~~ isACT cmd) as H_not_act; [ move: iCAS; rewrite /isCAS /isCRD /isCWR /isACT; destruct cmd.(CKind); done | ].
		specialize (cACTsb_inc cmd IS_prev.(SystemState) lc.(cACTsb) H_not_act H_is_cmd_ok r.(Address).(Bank)) as H0.
		apply H0 in H_; rewrite H_; lia.
	Qed.

	(* Don't need the hypotheis that ACT don't exist in the sub trace ! *)
	Lemma cACTsb_add t d x bk IS IS_fut : d > 0 ->
		(Default_arbitrate t).(Implementation_State) = Some IS ->
		(Default_arbitrate (t + d)).(Implementation_State) = Some IS_fut ->
		get_cACTsb_ (Some IS.(SystemState)) bk = Some x ->
		get_cACTsb_ (Some IS_fut.(SystemState)) bk = Some (x + d) \/
		(exists cnt_val, cnt_val < d /\ get_cACTsb_ (Some IS_fut.(SystemState)) bk = Some cnt_val).
	Proof.
		intros Hlt Ht Ht' Hc.
		generalize dependent IS_fut; induction d; [ done | ].

		rewrite leq_eqVlt in Hlt; move: Hlt => /orP [/eqP Hd_0 | Hd_pos].
		{ (* Induction base case *)
			intros IS_future; assert (d = 0); [lia | ]; subst; clear IHd Hd_0.
			rewrite addn1; intros Ht'.

			(* A command must have been issued at t.+1 *)
			apply exists_command_in_trace with (IS_future := IS_future) in Ht as H; [ | done ];
			destruct H as [cmd [H_in [H_CDate [H_is_cmd_ok [H_sch_cmd H_bank_]]]]];
			destruct H_bank_ as [cmd_bank H_bank_].

			destruct ((isACT cmd) && (get_bank cmd == Some bk)) eqn:H. (* it resets in this case *)
			{ (* Case where the counter resets *) 
				right; exists 0; split; [ lia | ].
				move: H => /andP [H_is_act /eqP H_cmd_to_same_bank].
				move: Ht'; simpl; unfold Next_SL_state; rewrite Ht - H_sch_cmd.
				rewrite /isACT in H_is_act.
				destruct cmd.(CKind) eqn:H_kind; try done; clear H_is_act.
				assert (get_req cmd = Some r) as H_req; [ unfold get_req; rewrite H_kind; reflexivity | ].
				apply (get_bank_get_req cmd bk r) in H_cmd_to_same_bank; [ | exact H_req ]; subst bk; simpl. 
				set nth_bk := nth _ _ _; destruct nth_bk eqn:H_nth_bk; simpl; [ | done ].

				(* a simple simplification would work, but how to get rid of Some ? *)
				intro H_; injection H_ as H_; rewrite - H_ //=; clear H_.
				rewrite nth_set_nth //= eq_refl //=.
			}

			{ left.
				(* There are actualy two sub-cases :
						1. (isACT = true) && ((get_banc_cmd == Some bk) = true) : impossible
						2. (isACT = true) && ((get_bank cmd == Some bk) = fals) : ACT to different bank, counter increments
						3. (isACT = fals) && ((get_bank cmd == Some bk) = true) : trivial, command is not ACT, cACTdb_inc
						4. (isACT = fals) && ((get_bank cmd == Some bk) = fals) : trivial, command is not ACT, cACTdb_inc 
				*)
				destruct (isACT cmd) eqn:H_is_act, (get_bank cmd == Some bk) eqn:H_bank; try (by rewrite Bool.andb_true_r in H).
				2-3:
					assert (~~ isACT cmd) as H0; [ by rewrite H_is_act | ]; clear H_is_act; rename H0 into H_is_act;
					specialize (cACTsb_inc cmd IS.(SystemState) x H_is_act H_is_cmd_ok bk Hc) as H_inc;
					rewrite - H_inc H_sch_cmd;

					move: Ht'; cbn -[get_cACTsb_ SystemUpdate]; unfold Next_SL_state;
					rewrite Ht; set SS := SystemUpdate _ _; destruct SS eqn:HSS; cbn -[get_cACTsb_ SystemUpdate]; try done;
					
					intro H_future;
					assert (Some IS_future.(SystemState) = Some s); try (injection H_future as H_; rewrite -H_; simpl; reflexivity);
					rewrite H0 -HSS; fold SS; reflexivity.

				{ (* ACT but to a different bank, counter still increments *)
					assert (H_is_act_ : isACT cmd); [ by rewrite H_is_act | ].
					specialize (cACTsb_inc_diff_bank cmd IS.(SystemState) x H_is_act_ H_is_cmd_ok bk H_bank Hc) as H_inc.
					rewrite - H_inc H_sch_cmd.

					move: Ht'; cbn -[get_cACTsb_ SystemUpdate]; unfold Next_SL_state;
					rewrite Ht; set SS := SystemUpdate _ _; destruct SS eqn:HSS; cbn -[get_cACTsb_ SystemUpdate]; try done;
					
					intro H_future;
					assert (Some IS_future.(SystemState) = Some s); try (injection H_future as H_; rewrite -H_; simpl; reflexivity);
					rewrite H0 -HSS; fold SS; reflexivity.
				}
			}
		}

		(* Induction step *)
		assert (0 < d) as H_; [ lia | ]; clear Hd_pos; rename H_ into Hd_pos.
		intros IS_future Ht'; rewrite addnS in Ht'.

		(* specialize (IHd Hd_pos IS_future Ht'). *)
		apply previous_state_is_valid in Ht' as H_; destruct H_ as [IS_prev Ht'_prev].
		specialize (IHd Hd_pos IS_prev Ht'_prev).

		apply exists_command_in_trace with (IS_future := IS_future) in Ht'_prev as H; [ | done ];
		destruct H as [cmd [H_in [H_CDate [H_is_cmd_ok [H_sch_cmd H_bank_]]]]];
		destruct H_bank_ as [cmd_bank H_bank_].

		destruct ((isACT cmd) && (get_bank cmd == Some bk)) eqn:H.
		{
			right. (* counter gets reset at (t + d).+1 *) 
			exists 0; split; [ lia | ].
			move: H => /andP [H_is_act /eqP H_cmd_to_same_bank].
			move: Ht'; simpl; unfold Next_SL_state; rewrite Ht'_prev - H_sch_cmd.
			rewrite /isACT in H_is_act.
			destruct cmd.(CKind) eqn:H_kind; try done; clear H_is_act.
			assert (get_req cmd = Some r) as H_req; [ unfold get_req; rewrite H_kind; reflexivity | ].
			apply (get_bank_get_req cmd bk r) in H_cmd_to_same_bank; [ | exact H_req ]; subst bk; simpl. 
			set nth_bk := nth _ _ _; destruct nth_bk eqn:H_nth_bk; simpl; [ | done ].
			intro H_; injection H_ as H_; rewrite - H_ //=; clear H_.
			rewrite nth_set_nth //= eq_refl //=.
		}
		
		destruct IHd as [H_cnt_doesnt_reset | H_cnt_resets].
		{ left.
			destruct (isACT cmd) eqn:H_is_act, (get_bank cmd == Some bk) eqn:H_bank; try (by rewrite Bool.andb_true_r in H).
			2-3:
				assert (~~ isACT cmd) as H0; [ by rewrite H_is_act | ]; clear H_is_act; rename H0 into H_is_act;
				specialize (cACTsb_inc cmd IS_prev.(SystemState) (x + d) H_is_act H_is_cmd_ok bk H_cnt_doesnt_reset) as H_inc;
				rewrite addn1 - addnS in H_inc;
				rewrite - H_inc H_sch_cmd;

				move: Ht'; cbn -[get_cACTsb_ SystemUpdate]; unfold Next_SL_state;
				rewrite Ht'_prev; set SS := SystemUpdate _ _; destruct SS eqn:HSS; cbn -[get_cACTsb_ SystemUpdate]; try done;
				
				intro H_future;
				assert (Some IS_future.(SystemState) = Some s); try (injection H_future as H_; rewrite -H_; simpl; reflexivity);
				rewrite H0 -HSS; fold SS; reflexivity.
			{
				assert (H_is_act_ : isACT cmd); [ by rewrite H_is_act | ].
				specialize (cACTsb_inc_diff_bank cmd IS_prev.(SystemState) (x + d) H_is_act_ H_is_cmd_ok bk H_bank H_cnt_doesnt_reset) as H_inc.
				rewrite addn1 -addnS in H_inc.
				rewrite -H_inc H_sch_cmd.

				move: Ht'; cbn -[get_cACTsb_ SystemUpdate]; unfold Next_SL_state.
				rewrite Ht'_prev; set SS := SystemUpdate _ _; destruct SS eqn:HSS; cbn -[get_cACTsb_ SystemUpdate]; try done;
				
				intro H_future;
				assert (Some IS_future.(SystemState) = Some s); try (injection H_future as H_; rewrite -H_; simpl; reflexivity);
				rewrite H0 -HSS; fold SS; reflexivity.
			}
		}

		{ (* Case with resets: counter has been reset before (t + d) and is now at a new value *)
			destruct H_cnt_resets as [cnt_new_val [H_new_val_lt_d H_cnt_resets]].
			right; exists (cnt_new_val.+1); split; [ lia | ].

			destruct (isACT cmd) eqn:H_is_act, (get_bank cmd == Some bk) eqn:H_bank; try (by rewrite Bool.andb_true_r in H).

			2-3:
				assert (~~ isACT cmd) as H0; [ by rewrite H_is_act | ]; clear H_is_act; rename H0 into H_is_act;
				specialize (cACTsb_inc cmd IS_prev.(SystemState) cnt_new_val H_is_act H_is_cmd_ok bk H_cnt_resets) as H_inc;
				rewrite addn1 in H_inc;
				rewrite - H_inc H_sch_cmd;

				move: Ht'; cbn -[get_cACTsb_ SystemUpdate]; unfold Next_SL_state;
				rewrite Ht'_prev; set SS := SystemUpdate _ _; destruct SS eqn:HSS; cbn -[get_cACTsb_ SystemUpdate]; try done;
				
				intro H_future;
				assert (Some IS_future.(SystemState) = Some s); try (injection H_future as H_; rewrite -H_; simpl; reflexivity);
				rewrite H0 -HSS; fold SS; reflexivity.
			{ assert (H_is_act_ : isACT cmd); [ by rewrite H_is_act | ].
				specialize (cACTsb_inc_diff_bank cmd IS_prev.(SystemState) cnt_new_val H_is_act_ H_is_cmd_ok bk H_bank H_cnt_resets) as H_inc.
				rewrite addn1 in H_inc.
				rewrite -H_inc H_sch_cmd.

				move: Ht'; cbn -[get_cACTsb_ SystemUpdate]; unfold Next_SL_state.
				rewrite Ht'_prev; set SS := SystemUpdate _ _; destruct SS eqn:HSS; cbn -[get_cACTsb_ SystemUpdate]; try done;
				
				intro H_future;
				assert (Some IS_future.(SystemState) = Some s); try (injection H_future as H_; rewrite -H_; simpl; reflexivity);
				rewrite H0 -HSS; fold SS; reflexivity.
			}
		}
	Qed.

	Lemma counter_monotonic_ (bk : Bank_t) (t t' : nat) (x d : Counter_t) IS IS' : 
		t < t' -> d > 0 ->
		(Default_arbitrate t).(Implementation_State) = Some IS ->
		(Default_arbitrate t').(Implementation_State) = Some IS' ->
		let SS := IS.(SystemState) in
		let SS' := IS'.(SystemState) in
		(match get_cACTsb_ (Some SS) bk with
			| None => False
			| Some z => z = x
		end) ->
		(match get_cACTsb_ (Some SS') bk with
		| None => False 
		| Some y => is_true (y >= (x + d))
		end) -> 
		t' >= t + d.
	Proof.
		intros Hlt Hdpos Ht Ht'; cbv zeta; intros Hc Hgeq.
		induction d; [ lia | ].
		rewrite leq_eqVlt in Hdpos; move: Hdpos => /orP [/eqP Hd0 | Hdpos];
		[ assert (d = 0) as H; [ lia | ]; clear Hd0; rename H into Hd0; subst; clear IHd; lia | ].
		assert (d > 0) as Hdpos_; [ lia | ]; clear Hdpos; rename Hdpos_ into Hdpos.
		apply IHd in Hdpos as IH; clear IHd;
		[ | move: Hgeq; unfold get_cACTsb_;
			destruct IS'.(SystemState).(Banks); simpl; set bs := nth _ _ _; destruct bs eqn:HBS; lia ].
		rewrite leq_eqVlt in IH; move: IH => /orP [/eqP Heq | IH]; [ | lia ]; subst.
		destruct (get_cACTsb_ (Some IS.(SystemState)) bk) eqn:Hc_; [ | done ];
		destruct (get_cACTsb_ (Some IS'.(SystemState)) bk) eqn:Hc'_; [ | done ]; subst c.
		specialize (cACTsb_add t d x bk IS IS' Hdpos Ht Ht' Hc_) as [H_ | H_].
		{
			rewrite H_ in Hc'_; injection Hc'_ as Hc'_.
			rewrite addnS in Hgeq; rewrite Hc'_ ltnn in Hgeq; done.
		}
		destruct H_ as [cnt_val [H_bound H_]].
		rewrite H_ in Hc'_; injection Hc'_ as Hc'_.
		rewrite addnS in Hgeq; rewrite - Hc'_ in Hgeq; lia.
	Qed.
	
	(* ------------------------------------------- Timing proofs -------------------------------------- *)
	Theorem Cmds_T_RCD_ok t a b:
    a \in (Default_arbitrate t).(Arbiter_Commands) -> 
		b \in (Default_arbitrate t).(Arbiter_Commands) ->
    isACT a -> isCAS b -> (get_bank a == get_bank b) ->
		Before a b -> Apart_at_least a b T_RCD.
  Proof.
	  (* --------------------proof prelude ------------------------------- *)
		intros Ha Hb iAa iCb SB aBb;
		apply cmd_in_trace_at_CDate in Ha as Ha', Hb as Hb'.

		apply valid_state_when_cmd_in_trace in Hb as Hb_valid;
		[ | move: iCb; rewrite /isCAS /isCRD /isCWR /isNOP; destruct b.(CKind); done ].

		apply valid_state_when_cmd_in_trace in Ha as Ha_valid;
		[ | move: iAa; rewrite /isACT /isNOP; destruct a.(CKind); done ].

		destruct (Default_arbitrate a.(CDate)) as [a_Cmds a_Time a_State] eqn:HA.
		destruct (Default_arbitrate b.(CDate)) as [b_Cmds b_Time b_State] eqn:HB; simpl in *. 

		destruct a_State eqn:Ha_IS_State, b_State eqn:Hb_IS_State; try done.
		rename i into a_IS, i0 into b_IS.

		(* ------------------------ get the lc.(cACTsb) counter -------------------------- *)
		assert ((Default_arbitrate a.(CDate)).(Implementation_State) = Some a_IS) as Haux; [ by rewrite HA | ].

		specialize (previous_state_is_valid a.(CDate).-1 a_IS) as a_IS_prev.
		apply (cmd_date_gt_0 a t) in Ha as aCdate_pos; destruct a.(CDate) eqn:Ha_CDate; [ done | ].

		cbn -[Default_arbitrate] in a_IS_prev; rewrite -Ha_CDate in a_IS_prev Haux; apply a_IS_prev in Haux as Haux_; clear a_IS_prev.
		destruct Haux_ as [a_IS_prev Ha_State_prev]; rewrite Ha_CDate in Haux; rename Haux into Ha_State; clear aCdate_pos.

		set a_map := (IssueREF
			 	(map_running_req_to_cmd a_IS_prev.(CMap) a_IS_prev.(SystemState)) a_IS_prev.(SystemState)).
		
		assert (~~ isNOP a) as H_a_not_NOP; [ rewrite /isNOP; rewrite /isACT in iAa; destruct a.(CKind); done | ].

		apply (command_matches_between_valid_states n a_IS_prev a_IS a) in Ha_CDate as Ha_eq; try done;
		[ | rewrite - Ha_CDate; by apply cmd_in_trace_at_CDate in Ha ].

		(* now I know that iscmdOK at a.(CDate).-1 *)
		apply (iscmdOK_true_between_valid_states n a_IS_prev a_IS a_map a) in H_a_not_NOP as H_a_cmd_ok; try done.

		unfold isACT in iAa; unfold get_bank in SB; destruct a.(CKind) eqn:Ha_; try done; rename r into a_req.

		(* Counter resets *)
		apply (cACTsb_reset a_req a_IS_prev.(SystemState)) in H_a_cmd_ok as H_cACTsb_reset.

		(* Clear the context *)
		clear a_map Ha_IS_State a_State H_a_not_NOP iAa Ha.
		
		(* ----------------------------counter information for b ----------------------- *)
		assert ((Default_arbitrate b.(CDate)).(Implementation_State) = Some b_IS) as Haux; [ by rewrite HB | ].

		specialize (previous_state_is_valid b.(CDate).-1 b_IS) as b_IS_prev.
		apply (cmd_date_gt_0 b t) in Hb as bCdate_pos; destruct b.(CDate) eqn:Hb_CDate; [ done | ].

		cbn -[Default_arbitrate] in b_IS_prev; rewrite -Hb_CDate in b_IS_prev Haux; apply b_IS_prev in Haux as Haux_; clear b_IS_prev.
		destruct Haux_ as [b_IS_prev Hb_State_prev]; rewrite Hb_CDate in Haux; rename Haux into Hb_State; clear bCdate_pos.

		set b_map := (IssueREF
			 	(map_running_req_to_cmd b_IS_prev.(CMap) b_IS_prev.(SystemState)) b_IS_prev.(SystemState)).
		
		assert (~~ isNOP b) as H_b_not_NOP; [ rewrite /isNOP; rewrite /isCAS /isCRD /isCWR in iCb; destruct b.(CKind); done | ].
		
		apply (command_matches_between_valid_states n0 b_IS_prev b_IS b) in Hb_CDate as Hb_eq; try done;
		[ | rewrite - Hb_CDate; by apply cmd_in_trace_at_CDate in Hb ].
		
		apply (iscmdOK_true_between_valid_states n0 b_IS_prev b_IS b_map b) in H_b_not_NOP as H_b_cmd_ok; try done.

		apply (cACTsb_gt_TRCD n0 b_IS_prev b) in Hb_State_prev as H_cACTsb_TRC_; try done.
		destruct (get_req b) eqn:Hreq_b; [ | done ]; rename r into b_req.
		apply H_cACTsb_TRC_ in H_b_cmd_ok as H_cACTsb_TRC; clear H_cACTsb_TRC_.
		destruct (nth def_BKS (` b_IS_prev.(SystemState).(Banks)) (Bank_to_nat b_req.(Address).(Bank))) eqn:Hb_bank; [ done | ].

		(* some bookeeping *)
		rename r into b_row, l into b_localcounters; clear H_b_not_NOP b_map Hb.

		(* --------------------------------monotonic counter ------------------------------ *)
		assert (a_req.(Address).(Bank) = b_req.(Address).(Bank)) as SB_.
		{ unfold get_req in Hreq_b; destruct b.(CKind) eqn:Hb_r; simpl in SB;
			try (by rewrite /isCAS /isCRD /isCWR Hb_r in iCb);
			move: Hreq_b => /eqP Hreq_b; 
			rewrite inj_eq in Hreq_b; try (exact ssrfun.Some_inj); 
			move: Hreq_b => /eqP Hreq_b; subst r;
			rewrite inj_eq in SB; try (exact ssrfun.Some_inj); apply /eqP; done.
		} clear SB; rename SB_ into SB.

		set a_SS := (SystemUpdate (ACT a_req) (Some a_IS_prev.(SystemState))).
		fold a_SS in H_cACTsb_reset.
		set b_SS := (SystemUpdate b.(CKind) (Some b_IS_prev.(SystemState))).
		assert (n.+1 < n0.+1) as H; [ rewrite - Ha_CDate - Hb_CDate; by rewrite /Before in aBb | ].
		assert (0 < T_RCD) as H_; [ by specialize T_RCD_pos; lia | ].

		specialize (counter_monotonic_ a_req.(Address).(Bank) n.+1 n0.+1 0 T_RCD a_IS b_IS H H_) as H_counter. 
		cbv zeta in H_counter; clear H H_.

		rewrite /Apart_at_least Ha_CDate Hb_CDate.
		apply H_counter; try assumption.
		{ (* follows by H_cACTsb_reset *)
			unfold a_SS in *; clear a_SS; move: H_cACTsb_reset.
			rewrite - Ha_ in H_a_cmd_ok.
			specialize (Next_state_matches a a_IS_prev a_IS n H_a_cmd_ok) as H_; rewrite Ha_ in H_; rewrite H_; try done;
			[ | rewrite -Ha_ Ha_eq //= ]; clear H_.
			intros H_rew; rewrite H_rew; reflexivity.
		}

		clear H_counter.

		(* side effect : now got to prove that the counter at b is bigger than T_RCD *)
		specialize (cACTsb_gt_TRCD_after_update n0 b_IS b_IS_prev b.(CKind)) as H_; cbv zeta in H_.

		assert (b = {| CDate := n0.+1; CKind := b.(CKind) |}); [ rewrite Hb_eq; reflexivity | ].
		rewrite - H in H_; clear H.
		assert ((Default_arbitrate n0.+1).(Arbiter_Commands) = b_Cmds); [ rewrite HB; simpl; reflexivity | ].
		rewrite - H in Hb'; clear H.
		
		apply H_ in Hb'; clear H_; try done.
		rewrite Hreq_b in Hb'; rewrite SB add0n; unfold b_SS.

		specialize (Next_state_matches b b_IS_prev b_IS n0 H_b_cmd_ok) as H_; 
		rewrite H_ in Hb'; try done; by rewrite Hb_eq.
	Qed.
	
End InterfaceSubLayer.