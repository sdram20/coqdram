Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From DRAM Require Export Trace.

Section Arbiter.

	Context {SYS_CFG      : System_configuration}.
  Context {REQESTOR_CFG : Requestor_configuration}.
	(* Context {BK_MAP				: BankMap}. *)
  
  Class Arrival_function_t := mkArrivalFunction
  {
    Arrival_at : nat -> Requests_t;

    Arrival_date : forall ta x,
            (x \in (Arrival_at ta)) -> x.(Date) == ta;

    Arrival_uniq : forall t, 
            uniq (Arrival_at t);
  }.

	Class TestArbiter_t {AF : Arrival_function_t} := mkTestArbiter
  {
    TestArbitrate  : nat -> TestTrace_t;
  }.

	Class Arbiter_t {AF : Arrival_function_t} := mkArbiter
  {
    Arbitrate  : nat -> Trace_t;

    (* All requests must handled *)
    Requests_handled : 
      forall ta req, req \in (Arrival_at ta) -> 
                    exists tc, (CAS_of_req req tc) \in ((Arbitrate tc).(Commands));

    (* Time has to match *)
    Time_match : 
      forall t, (Arbitrate t).(Time) == t
  }.

	Class SequentialConsistent_Arbiter {AF : Arrival_function_t} {AR : Arbiter_t} := mkSeqArbiter {

    R2 : forall ta reqa tb reqb, 
    reqa \in (Arrival_at ta) -> (* reqa arrives at ta *) 
    reqb \in (Arrival_at tb) -> (* reqb arrives at tb *)
    (* either reqa arrived before reqb OR 
       they arrived at the same instant, but there
       is an arbitrary order between reqa and reqb, 
       and reqa is to be serviced before *)
    (ta < tb) \/ (ta = tb /\ index reqa (Arrival_at ta) < index reqb (Arrival_at ta))
    (* txa, the completion date of reqa must happen before txb, 
       the completion date of reqb *)
    -> exists txa txb, (CAS_of_req reqa txa \in (Arbitrate txa).(Commands)) 
    && (CAS_of_req reqb txb \in (Arbitrate txb).(Commands)) && (txa < txb)
	}.

	Class W_SequentialConsistent_Arbiter {AF : Arrival_function_t} {AR : Arbiter_t} := mkWSeqARbiter {  
    R2_relaxed : forall ta reqa tb reqb, 
    reqa \in (Arrival_at ta) -> (* reqa arrives at ta *) 
    reqb \in (Arrival_at tb) -> (* reqb arrives at tb *)
    (ta < tb) \/ 
    (ta = tb /\ index reqa (Arrival_at ta) < index reqb (Arrival_at ta))
    (* Here, an additional pre-condition: reqa and reqb target the same row *) ->
		 reqa.(Address).(Row) = reqb.(Address).(Row) ->
    (* txa, the completion date of reqa must happen before txb, 
       the completion date of reqb *)
    exists txa txb, 
    (CAS_of_req reqa txa \in (Arbitrate txa).(Commands)) && 
    (CAS_of_req reqb txb \in (Arbitrate txb).(Commands)) && (txa < txb)
	}.

  Definition Default_arrival_at Input t :=
    [seq x <- Input | x.(Date) == t].

  Program Instance Default_arrival_function_t (R : Requests_t) `{uniq R} : Arrival_function_t :=
    mkArrivalFunction (Default_arrival_at R) _ _.
  Next Obligation.
    induction R.
    - contradict H. simpl. by rewrite in_nil.
    - simpl in H.
      rewrite cons_uniq in uniq0. move : uniq0 => /andP [_ Hu].
      destruct (a.(Date) == ta) eqn:Hd.
      - rewrite in_cons in H.
        move : H => /orP [/eqP H | H].
        - by subst.
        - by apply IHR in Hu.
      - by apply IHR in Hu.
  Qed.
  Next Obligation.
    unfold Default_arrival_at.
    by rewrite filter_uniq.
  Qed.

	(* --------------------- Definitions that ensure hardware compatibility ---------------- *)
	(* Class HW_Arrival_function_t {AF : Arrival_function_t} := mkHWArrivalFunction
  {
    (* Assumption 1 *)
    HW_single : forall t, size (Arrival_at t) <= 1; 
    
    (* Assumption 2 consisting of two POs  *)
    pending_i : nat -> signal Bit; (* pending input for controller circuit *)
    request_i : nat -> signal request_t; (* request as input *)
		ack_o 		: signal Bit;

    HW_arrived : forall t, size (Arrival_at t) = 1 <-> (ack_o t) /\ (pending_i t);
    HW_request : forall t, size (Arrival_at t) = 1 -> 
        EqReq (HW_Arrival_at t) (request_i t);
	}. *)

	(* 
	Definition HW_Default_arrival_at (Input : Request_t) (t : nat) := Input. 
	*)
	
	(* 
	Instance HW_Default_arrival_function_t (R: Request_t) : HW_Arrival_function_t := 
		mkHWArrivalFunction (HW_Default_arrival_at R). 
	*)

End Arbiter.  