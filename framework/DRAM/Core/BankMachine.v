Set Warnings "-notation-overridden,-parsing".
Set Printing Projections.

From mathcomp Require Import ssreflect ssrbool ssrnat eqtype zify tuple fintype.
From Coq Require Import List Program.
From RecordUpdate Require Import RecordSet.

From DRAM Require Export Commands.

Import ListNotations RecordSetNotations.

Inductive page_policy := opage | cpage.

Class SchedulingOptions_t := mkSCH_OPT {
	pP : page_policy;
}.

Section BankMachine.

	Context {SYS					: System_configuration}.
	Context {REQ_CFG 			: Requestor_configuration}.
	(* Context {BK_MAP					: BankMap}. *)
	
	Context {SCH_CFG 			: SchedulingOptions_t}.

	(* -------------------------------------------------------------------------------------------------------- *)
	(* ---------------------------------PRELIMINATIES AND DEFINITIONS ----------------------------------------- *)
	(* -------------------------------------------------------------------------------------------------------- *)

	Definition Counter_t := nat.

	Inductive Busdir_t : Type := BWR | BRD.

	(* Intra-bank counters *)
	Record LocalCounters := mkLocalCounters {
			(* LOCAL  : intra bank counter for ACTs *)
		cACTsb 	: Counter_t;
			(* LOCAL  : intra bank counter for PREs*)
		cPRE 		: Counter_t;
			(* LOCAL  : intra bank reads *)
		cRDsb  	: Counter_t;
			(* LOCAL  : intra bank read with auto-precharge *)
		cRDA  	: Counter_t;
			(* LOCAL  : intra bank writes *)
		cWRsb   : Counter_t;
			(* LOCAL  : intra bank write with auto-precharge *)
		cWRA	  : Counter_t;
	}.

	Global Instance etaX : Settable _ := settable! mkLocalCounters <cACTsb; cPRE; cRDsb; cRDA; cWRsb; cWRA>.

	(* Inter-bank counters *)
	Record GlobalCounters := mkGlobalCounters {
		(* GLOBAL : inter bank counter [same and different bank group] counter for ACTs, stores timing for three last inter-bank ACTs *)
		cACTdb 	: { cnts : seq.seq Counter_t | seq.size cnts == 3 };
		(* GLOBAL : inter bank counter per bankgroup *)
		cACTbgs	: { cnts : seq.seq Counter_t | seq.size cnts == BANKGROUPS };
		(* GLOBAL : inter bank reads (RDA also changes this, and RDA does not need a inter-bank counter) *)
		cRDdb  	: Counter_t;
		(* GLOBAL : inter bank writes (for different bank groups) *)
		cRDbgs  : { cnts : seq.seq Counter_t | seq.size cnts == BANKGROUPS };
		(* GLOBAL : inter bank writes *)
		cWRdb   : Counter_t;
		(* GLOBAL : inter bank writes (for different bank groups) *)
		cWRbgs  : { cnts : seq.seq Counter_t | seq.size cnts == BANKGROUPS };
		(* GLOBAL : refresh counter *)
		cREF		: Counter_t;
	}.

	Global Instance etaY : Settable _ := settable! mkGlobalCounters 
		<cACTdb; cACTbgs; cRDdb; cRDbgs; cWRdb; cWRbgs; cREF>.

	Inductive BankState_t :=
		| IDLE				: LocalCounters -> BankState_t
		| ACTIVE 			: Row_t -> LocalCounters -> BankState_t.

	Definition BankStates_t : Set := {l : seq.seq BankState_t | seq.size l == BANKS }.

	Record SystemState_t := mkSystemState {
		Banks : BankStates_t;
		SysCounters : GlobalCounters;
		Busdir : Busdir_t
	}.

	Global Instance etaZ : Settable _ := settable! mkSystemState <Banks; SysCounters; Busdir>.

	Lemma bankgroup_bounded_ : forall (r : Request_t), (Bankgroup_to_nat r.(Address).(Bankgroup) < BANKGROUPS).
		intros r; destruct (r.(Address).(Bankgroup)); simpl; lia.
	Defined.

	Definition bankgroup_bounded (req : Request_t) := @Ordinal BANKGROUPS 
		(Bankgroup_to_nat req.(Address).(Bankgroup)) (bankgroup_bounded_ req).

	Lemma bank_bounded_ : forall r, (Bank_to_nat r.(Address).(Bank) < BANKS).
		intros r; destruct (r.(Address).(Bank)); simpl; lia.
	Defined.
	
	Definition bank_bounded (req : Request_t) := @Ordinal BANKS 
		(Bank_to_nat req.(Address).(Bank)) (bank_bounded_ req).

	(* -------------------------------------------------------------------------------------------------------- *)
	(* --------------------------------------- CMD GENERATION / MAPPING --------------------------------------- *)
	(* -------------------------------------------------------------------------------------------------------- *)

	Definition def_BKS := IDLE (mkLocalCounters 0 0 0 0 0 0).
	
	(* Possibly unsafe indexing -> add hypothesis whenever proving things *)
	Definition cmdGEN_cpage (req : Request_t) (SS : SystemState_t) : Command_kind_t :=
		let BS := seq.nth def_BKS (proj1_sig SS.(Banks)) (Bank_to_nat req.(Address).(Bank)) in
		match BS with
		| IDLE _ => (ACT req)
		|	ACTIVE row lc => 
			match req.(Kind) with
				| WR => (CWRA req)
				| RD => (CRDA req)
			end 
		end.

	Definition cmdGEN_opage (req : Request_t) (SS : SystemState_t) : Command_kind_t :=
		let BS := seq.nth def_BKS (proj1_sig SS.(Banks)) (Bank_to_nat req.(Address).(Bank)) in
		match BS with
		| IDLE _ => (ACT req)
		| ACTIVE row lc => 
			if (req.(Address).(Row) == row) then ( (* row HIT, needs a CAS *)
				match req.(Kind) with
					| WR => (CWR req)
					| RD => (CRD req)
				end 
			) else ( (* row MISS, a PRE is needed *)
				(PRE req))
		end.

	Definition cmdGEN :=
		match pP with
		| cpage => cmdGEN_cpage
		| opage => cmdGEN_opage
		end.

	Definition ReqCmdMap_t := seq.seq Command_kind_t.

	Fixpoint map_arriving_req_to_cmd (Q : Requests_t) (m : ReqCmdMap_t) SS : ReqCmdMap_t :=
		match Q with
		| Datatypes.nil => m
		| req :: tl => (map_arriving_req_to_cmd tl m SS) ++ [cmdGEN req SS]
		end.

	Fixpoint map_running_req_to_cmd (m : ReqCmdMap_t) SS : ReqCmdMap_t :=
		match m with
		| nil => m
		| hd :: tl =>
			match hd with
			| CRD r | CRDA r | CWR r | CWRA r | ACT r | PRE r =>
				(cmdGEN r SS) :: (map_running_req_to_cmd tl SS)
			| _ => (map_running_req_to_cmd tl SS)
			end
		end.

	Definition IssueREF (m : ReqCmdMap_t) SS :=
		match pP with
		| opage =>
			if (SS.(SysCounters).(cREF) == T_REFI - T_RP) then PREA :: m
			else if (SS.(SysCounters).(cREF) == T_REFI) then REF :: m
			else m
		| cpage =>
			if (SS.(SysCounters).(cREF) == T_REFI) then REF :: m
			else m
		end.
		
	(* -------------------------------------------------------------------------------------------------------- *)
	(* -------------------------------------------- CHECK ----------------------------------------------------- *)
	(* -------------------------------------------------------------------------------------------------------- *)
	
	(* check if all banks have been IDLE for at least T_RP cycles *)
	Definition checkBanksAreIdle SS :=
		@fold_right bool BankState_t
		(fun bs r => if r then 
			(match bs with
			| IDLE lc => 
				(T_RP <= lc.(cPRE)) || 
				(T_RTP + T_RP <= lc.(cRDA)) || 
				(T_WL + T_BURST + T_WR + T_RP <= lc.(cWRA))
			| ACTIVE _ _ => false
			end) else false
		) true (proj1_sig SS.(Banks)).

	(* check if all banks are OK to be pre-charged *)
	(* If alreadu IDLE ok ? *)
	(* Some logic to not allow commands to be issued if PREA or REF are due -> already there ! *)
	Definition checkPREA SS :=
		@fold_right bool BankState_t 
		(fun bs r => if r then 
			(match bs with
			| IDLE lc
			| ACTIVE _ lc =>
				(* ACT-to-PRE same bank *)
				(T_RAS <= lc.(cACTsb)) && 
				(* RD-to-PRE, same bank *)
				(T_RTP <= lc.(cRDsb)) &&
				(* WR-to-PRE, same bank *) 
				(T_WL + T_BURST + T_WR <= lc.(cWRsb))
			end) else false
		) true (proj1_sig SS.(Banks)).
	
	Definition iscmdOK
		(cmd : Command_kind_t) (SS : SystemState_t) : bool :=
		match cmd with
		| CRD req
		| CRDA req => 
			let bk := (Bank_to_nat req.(Address).(Bank)) in
			let bg := Bankgroup_to_nat req.(Address).(Bankgroup) in
			let BS := seq.nth def_BKS (proj1_sig SS.(Banks)) bk in
			let REF_cond := 
				(* Can't issue a RD if there's an incoming REFRESH cycle. Only issuing RD in the open page case *)
				(SS.(SysCounters).(cREF) <= T_REFI - T_RP - T_RTP) &&
				(* Can't issue a RD after a REF for T_RFC cycles *)
				(T_RFC <= SS.(SysCounters).(cREF)) in
			match BS with
			| IDLE _ => false (* RD not ok on an IDLE bank *)
			| ACTIVE row lc => 
				if (req.(Address).(Row) == row) then ( (* row hit *)
				match SS.(Busdir) with
				| BRD => 
					(* could be 0 by default, actually, have to use a dependent type *)
					let lastRD_sbg := seq.nth 0 (proj1_sig SS.(SysCounters).(cRDbgs)) bg in
					let lastRD := SS.(SysCounters).(cRDdb) in
					(* RD-to-RD same bank (also same bankgroup) *)
					(T_CCD_l <= lc.(cRDsb)) && 
					(* RD-to-RD different banks (but same bankgroups) *)
					(T_CCD_l <= lastRD_sbg) &&
					(* RD-to_RD different bankgroups *)
					(T_CCD_s <= lastRD) &&
					(* ACT to RD *)
					(T_RCD <= lc.(cACTsb)) && REF_cond
				| BWR => 
					let lastWR_sbg := seq.nth 0 (proj1_sig SS.(SysCounters).(cWRbgs)) bg in
					let lastWR := SS.(SysCounters).(cWRdb) in
					(* RD-to-WR same bank (also same bankgroup) *)
					(T_WL + T_BURST + T_WTR_l <= lc.(cWRsb)) && 
					(* RD-to-WR different banks (but same bankgroup) *)
					(T_WL + T_BURST + T_WTR_l <= lastWR_sbg) &&
					(* RD-to-WR different bankgroups *)
					(T_WL + T_BURST + T_WTR_s <= lastWR) &&
					(* ACT to WR *)
					(T_RCD <= lc.(cACTsb)) && REF_cond
				end) else false (* CRD not ok when the active row is not the request's row *)
			end
		| CWR req
		| CWRA req => 
			let bk := (Bank_to_nat req.(Address).(Bank)) in
			let bg := Bankgroup_to_nat req.(Address).(Bankgroup) in
			let BS := seq.nth def_BKS (proj1_sig SS.(Banks)) bk in
			let REF_cond := 
				(* Can't issue a WR if there's an incoming REFRESH cycle. Only issuing WR in the open page case *)
				(SS.(SysCounters).(cREF) <= T_REFI - T_RP - (T_WL + T_BURST + T_WR)) &&
				(* Can't issue a RD after a REF for T_RFC cycles *)
				(T_RFC <= SS.(SysCounters).(cREF)) in
			match BS with
			| IDLE _ => false
			| ACTIVE row lc => if (req.(Address).(Row) == row) then ( (* row hit *)
				match SS.(Busdir) with
				| BRD =>
					(* RD-to-WR same bank *)
					(T_RTW <= lc.(cRDsb)) &&
					(* RD-to-WR different banks *)
					(T_RTW <= SS.(SysCounters).(cRDdb)) &&
					(* ACT-to-WR *)
					(T_RCD <= lc.(cACTsb)) && REF_cond
				| BWR => 
					let lastWR_sbg := seq.nth 0 (proj1_sig SS.(SysCounters).(cWRbgs)) bg in
					let lastWR := SS.(SysCounters).(cWRdb) in
					(* WR-to-WR same bank *)
					(T_CCD_l <= lc.(cWRsb)) &&
					(* WR-to-WR different banks (but same bank-group) *)
					(T_CCD_l <= lastWR_sbg) &&
					(* WR-to-WR different bankgroups *)
					(T_CCD_s <= lastWR) &&
					(* ACT-to-WR *)
					(T_RCD <= lc.(cACTsb)) && REF_cond
				end) else false
			end
		| ACT req =>
			let bk := (Bank_to_nat req.(Address).(Bank)) in
			let bg := Bankgroup_to_nat req.(Address).(Bankgroup) in
			let BS := seq.nth def_BKS (proj1_sig SS.(Banks)) bk in
			let REF_cond :=
				(* Can't issue an ACT if the REFRESH cycle is about to start *)
				(SS.(SysCounters).(cREF) <= T_REFI - T_RP - T_RAS) &&
				(* Can't issue an ACT T_RFC cycles after a REF has been issued *)
				(T_RFC <= SS.(SysCounters).(cREF)) in
			match BS with
			| IDLE lc =>
				let lACT_sbg := seq.nth 0 (proj1_sig SS.(SysCounters).(cACTbgs)) bg in
				let lACT := nth 0 (proj1_sig SS.(SysCounters).(cACTdb)) 0 in
				let lACT_shift := seq.nth 0 (proj1_sig SS.(SysCounters).(cACTdb)) 2 in
				(* PRE-to-ACT same bank (but PRE comming from RDA or WRA) *)
				(T_RTP + T_RP <= lc.(cRDA)) && (T_RTP + T_RP <= lc.(cWRA)) &&
				(* PRE-to-ACT same bank (normal PRE) *)
				(T_RP <= lc.(cPRE)) && 
				(* ACT-to-ACT same bank *)
				(T_RC <= lc.(cACTsb)) &&
				(* ACT-to-ACT different banks (same bank group) *)
				(T_RRD_l <= lACT_sbg) &&
				(* ACT-to-ACT d ifferent banks (different bankgroups) *)
				(T_RRD_s <= lACT) &&
				(* 4-ACT window *)
				(T_FAW <= lACT_shift) && REF_cond
			| ACTIVE _ _ => false
			end
		| PRE req =>
			let BS := seq.nth def_BKS (proj1_sig SS.(Banks)) (Bank_to_nat req.(Address).(Bank)) in
			let REF_cond :=
				(* Can't issue a PRE if the REFRESH cycle is about to start *)
				(SS.(SysCounters).(cREF) <= T_REFI - T_RP) &&
				(* Can't issue an ACT T_RFC cycles after a REF has been issued *)
				(T_RFC <= SS.(SysCounters).(cREF)) in
			match BS with
			| IDLE lc (* PREs when banks are already IDLE are allowed *)
			| ACTIVE _ lc =>
				(* ACT-to-PRE same bank *)
				(T_RAS <= lc.(cACTsb)) && 
				(* RD-to-PRE, same bank *)
				(T_RTP <= lc.(cRDsb)) &&
				(* WR-to-PRE, same bank *) 
				(T_WL + T_BURST + T_WR <= lc.(cWRsb)) && REF_cond
			end
		| REF => 
			(* All banks must be IDLE for at least T_RP cycles *)
			(checkBanksAreIdle SS) &&
			(* Counter has reached its value *)
			(SS.(SysCounters).(cREF) == T_REFI)
		| PREA => 
			(* All banks must be ready to be pre-charged (i.e., satisfy all intra-bank [something] -> PRE related consraints) *)
			(checkPREA SS) &&
			(* Counter has reached its value *)
			(SS.(SysCounters).(cREF) == T_REFI - T_RP)
		| NOP => true (* NOPs are always allowed *)
		end.

	(* -------------------------------------------------------------------------------------------------------- *)
	(* ------------------------------------------- UPDATE SYSTEM ---------------------------------------------- *)
	(* -------------------------------------------------------------------------------------------------------- *)
	
	Definition IncrementLocalCounters (c : LocalCounters) := mkLocalCounters
		(c.(cACTsb)).+1
		(c.(cPRE)).+1
		(c.(cRDsb)).+1
		(c.(cRDA)).+1
		(c.(cWRsb)).+1
		(c.(cWRA)).+1.

	Definition list_to_BankState_t (l : seq.seq BankState_t) (H : seq.size l == BANKS) :=
		@exist (list BankState_t) (fun l => length l == BANKS) l H.

	Definition list_to_BoundedCounter N (l : seq.seq Counter_t) (H : seq.size l == N) :=
		@exist (list Counter_t) (fun l => length l == N) l H.

	Lemma increment_safe N : forall (l : { l : seq.seq Counter_t | seq.size l == N}),
		seq.size (seq.map (fun x => x + 1) (proj1_sig l)) == N.
	Proof.
		intros l; induction l; simpl; rewrite seq.size_map; assumption.
	Defined.

	Definition inc x := x + 1.

	Definition IncrementGlobalCounters (c : GlobalCounters) := mkGlobalCounters
		(list_to_BoundedCounter 3 (seq.map inc (` c.(cACTdb))) (increment_safe 3 c.(cACTdb)))
		(list_to_BoundedCounter BANKGROUPS (seq.map inc (` c.(cACTbgs))) (increment_safe BANKGROUPS c.(cACTbgs)))
		(c.(cRDdb)).+1
		(list_to_BoundedCounter BANKGROUPS (seq.map inc (` c.(cRDbgs))) (increment_safe BANKGROUPS c.(cRDbgs)))
		(c.(cWRdb)).+1
		(list_to_BoundedCounter BANKGROUPS (seq.map inc (` c.(cWRbgs))) (increment_safe BANKGROUPS c.(cWRbgs)))
		(c.(cREF)).+1.

	Definition Request_kind_t_BUS (RK : Request_kind_t) :=
		match RK with
		| RD => BRD
		| WR => BWR
		end.

	Definition Dummy_BankUpdate (BS : BankState_t) : BankState_t :=
		match BS with
		| IDLE c => let ic := IncrementLocalCounters c in (IDLE ic)
		| ACTIVE row c => let ic := IncrementLocalCounters c in (ACTIVE row ic)
		end.

	Lemma Dummy_BankUpdate_safe : forall (l : {l : seq.seq BankState_t | seq.size l == BANKS}),
		seq.size (seq.map Dummy_BankUpdate (proj1_sig l)) == BANKS.
	Proof.
		intros l; induction l; simpl; rewrite seq.size_map; assumption.
	Defined.

	Definition isIDLE (BS : BankState_t) :=
		match BS with
		| IDLE _ => true
		| ACTIVE _ _ => false
		end.

	Definition isACTIVE (BS : BankState_t) :=
		match BS with
		| IDLE _ => false
		| ACTIVE _ _ => true
		end.

	Lemma stillIdle_after_simple_increment (Banks : seq.seq BankState_t) idx def:
		Banks <> nil ->
		is_true (isIDLE (seq.nth def Banks idx)) ->
		is_true (isIDLE (seq.nth def (seq.map Dummy_BankUpdate Banks) idx)).
	Proof.
		intros Hnempty.
		set t := seq.nth def Banks idx; intros H.
		generalize dependent idx;
		induction Banks; [ done | ].
		simpl; intros idx H.
		destruct idx; simpl;
		[ destruct a eqn:Ha; [ by simpl | simpl in H; discriminate H] | ].
		destruct Banks0; simpl in *; [ exact H | ].
		assert ((b :: Banks0) <> []) as aux; [ done | ]; apply (IHBanks) with (idx := idx) in aux; assumption.
	Qed.

	Lemma stillActive_after_simple_increment (Banks : seq.seq BankState_t) idx def:
		Banks <> nil ->
		is_true (isACTIVE (seq.nth def Banks idx)) ->
		is_true (isACTIVE (seq.nth def (seq.map Dummy_BankUpdate Banks) idx)).
	Proof.
		intros Hnempty.
		set t := seq.nth def Banks idx; intros H.
		generalize dependent idx;
		induction Banks; [ done | ].
		simpl; intros idx H.
		destruct idx; simpl;
		[ destruct a eqn:Ha; [ by simpl in H; discriminate H | by simpl ] | ].
		destruct Banks0; simpl in *; [ exact H | ].
		assert ((b :: Banks0) <> []) as aux; [ done | ]; apply (IHBanks) with (idx := idx) in aux; assumption.
	Qed.

	Lemma stillIdle_after_increment SS idx def:
		(` SS.(Banks)) <> nil ->
		is_true (isIDLE (seq.nth def (proj1_sig SS.(Banks)) idx)) ->
		let BS' := list_to_BankState_t (seq.map Dummy_BankUpdate (proj1_sig SS.(Banks))) (Dummy_BankUpdate_safe SS.(Banks)) in
		is_true (isIDLE (seq.nth def
		(proj1_sig (SS <|Banks := BS' |> <| SysCounters := IncrementGlobalCounters SS.(SysCounters) |>).(Banks)) idx)).
	Proof.
		intros Hnempty; destruct SS as [Banks SysCounters Busdir]; simpl;
		destruct Banks as [Banks p]; simpl in *; clear p.
		set t := seq.nth def Banks idx; intros H.
		generalize dependent idx;
		induction Banks; [ done | ].
		simpl; intros idx H.
		destruct idx; simpl.
		{ destruct a eqn:Ha; [ by simpl | simpl in H; discriminate H]. }
		destruct Banks0; simpl in *; [ exact H | ].
		assert ((b :: Banks0) <> []) as aux; [ done | ]; apply (IHBanks) with (idx := idx) in aux; assumption.
	Qed.

	Lemma stillActive_after_increment SS idx def:
		(` SS.(Banks)) <> nil ->
		is_true (isACTIVE (seq.nth def (proj1_sig SS.(Banks)) idx)) ->
		let BS' := list_to_BankState_t (seq.map Dummy_BankUpdate (proj1_sig SS.(Banks))) (Dummy_BankUpdate_safe SS.(Banks)) in
		is_true (isACTIVE (seq.nth def
		(proj1_sig (SS <|Banks := BS' |> <| SysCounters := IncrementGlobalCounters SS.(SysCounters) |>).(Banks)) idx)).
	Proof.
		intros Hactive; destruct SS as [Banks SysCounters Busdir]; simpl;
		destruct Banks as [Banks p]; simpl in *; clear p.
		set t := seq.nth def Banks idx; intros H.
		generalize dependent idx;
		induction Banks; [ done | ].
		simpl; intros idx H.
		destruct idx; simpl; [ destruct a eqn:Ha; by simpl | ].
		destruct Banks0; simpl in *; [ exact H | ].
		assert ((b :: Banks0) <> []) as aux; [ done | ]; apply (IHBanks) with (idx := idx) in aux; assumption.
	Qed.

	Definition rotate {A} (sA : list A) (x : A) : list A :=
		cons x (removelast sA).

	Lemma size_removelast {A} (l : list A) :
		seq.size l > 0 -> seq.size (removelast l) = (seq.size l).-1.
	Proof.
		intros H_l_not_empty; induction l; [ done | ]; simpl.
		destruct l; [ done | ].
		assert (0 < seq.size (a0 :: l)); [ by simpl | ]; apply IHl in H; clear IHl.
		cbn [seq.size]; rewrite H; cbn; reflexivity.
	Qed.

	Lemma rotate_safe {A} (new_el : A) N (l : {l : seq.seq A | seq.size l == N}) :
		N > 0 -> seq.size (rotate (proj1_sig l) new_el) == N.
	Proof.
		intros Hnpos; destruct l; simpl in *.
		move: i => /eqP i; rewrite - i in Hnpos.
		apply size_removelast in Hnpos as H; rewrite H prednK; [ by apply /eqP | done ].
	Qed.

	Lemma set_nth_safe_gen {A} (default new_val : A) (N : nat)
		(idx : {idx : nat | idx < N })
		(l : {l : seq.seq A | seq.size l == N}) :
		seq.size (seq.set_nth default (proj1_sig l) (proj1_sig idx) new_val) == N.
	Proof.
		rewrite seq.size_set_nth.
		destruct idx; simpl.
		destruct l; unfold maxn; simpl.
		destruct (_ < seq.size x0) eqn:H; [ assumption | lia ].
	Qed.

	(* Lemma set_nth_safe : forall (l : BankStates_t) (idx : Bank_t) nbs,
		seq.size (seq.set_nth def_BKS (proj1_sig l) (Bank_to_nat idx) nbs) == BANKS.
	Proof.
		intros l idx nbs.
		rewrite seq.size_set_nth.
		destruct l; unfold maxn; simpl.
		destruct (_ < seq.size x) eqn:H; [ assumption | ].
		assert ((Bank_to_nat idx).+1 >= seq.size x); [ lia | ]; clear H.
		move: i => /eqP i; rewrite i in H0.
		destruct idx; simpl in H0; simpl.
		rewrite leq_eqVlt in H0; move: H0 => /orP [/eqP H0 | H0]; lia.
	Qed. *)

	Lemma act_cPRE_safe SS : 
		seq.size (seq.map (fun bs => 
			match bs with
			| IDLE lc => IDLE (lc <| cPRE := 0 |>) 
			| ACTIVE row lc => IDLE (lc <| cPRE := 0 |>)
			end) (proj1_sig SS.(Banks))) == BANKS.
	Proof.
		rewrite seq.size_map; destruct SS.(Banks); simpl; assumption.
	Qed.

	Lemma _3_gt_0 : 3 > 0.
		by simpl.
	Qed.

	Definition SystemUpdate 
		(cmd	: Command_kind_t) (* The incoming command, chosen by the scheduler function *)
		(SS	 	: option SystemState_t)  (* The current System State *) 
		: option SystemState_t :=
		match SS with
		| None => None
		| Some SS =>
			let banks' 		:= list_to_BankState_t (seq.map Dummy_BankUpdate (proj1_sig SS.(Banks))) (Dummy_BankUpdate_safe SS.(Banks)) in
			let gbl_cnt' 	:= IncrementGlobalCounters SS.(SysCounters) in
			let SS' 			:= (SS <| Banks := banks' |> <| SysCounters := gbl_cnt' |>) in
			match cmd with
			| CRD req => let bk_idx := req.(Address).(Bank) in
									 let bg_idx := req.(Address).(Bankgroup) in
									 let BS := seq.nth def_BKS (proj1_sig SS'.(Banks)) (Bank_to_nat bk_idx) in
				(match BS with
				| IDLE cnt => None
				| ACTIVE row cnt => 
					let cnt' 					:= (cnt <| cRDsb := 0 |>) in
					let new_bk_state 	:= ACTIVE row cnt' in
					let	BS' 					:= seq.set_nth def_BKS (proj1_sig SS'.(Banks)) (Bank_to_nat bk_idx) new_bk_state in
					let BS'_proof 		:= set_nth_safe_gen def_BKS new_bk_state BANKS bk_idx SS'.(Banks) in
					let BS_sigma 			:= list_to_BankState_t BS' BS'_proof in
					(* Update inter-bank counters *)
					let v' 						:= seq.set_nth 0 (proj1_sig gbl_cnt'.(cRDbgs)) (Bankgroup_to_nat bg_idx) 0 in
					let v'_proof 			:= set_nth_safe_gen 0 0 BANKGROUPS bg_idx gbl_cnt'.(cRDbgs) in
					let v_sigma 			:= list_to_BoundedCounter BANKGROUPS v' v'_proof in
					let gcnt' 				:= (gbl_cnt' <|cRDdb := 0 |> <|cRDbgs := v_sigma|>) in
					(* Build new system state *)
					Some (SS' <| Banks := BS_sigma |> <| SysCounters := gcnt'|> <| Busdir := Request_kind_t_BUS req.(Kind) |>)
				end)
			| CRDA req => let bk_idx := req.(Address).(Bank) in
										let bg_idx := req.(Address).(Bankgroup) in
										let BS := seq.nth def_BKS (proj1_sig SS'.(Banks)) (Bank_to_nat bk_idx) in
				(match BS with
				| IDLE _ => None
				| ACTIVE row cnt =>
					(* Update intra-bank counters *)
					let cnt' 					:= (cnt <| cRDsb := 0 |> <| cRDA := 0 |>) in
					let new_bk_state 	:= IDLE cnt' in
					let	BS' 					:= seq.set_nth def_BKS (proj1_sig SS'.(Banks)) (Bank_to_nat bk_idx) new_bk_state in
					let BS'_proof 		:= set_nth_safe_gen def_BKS new_bk_state BANKS bk_idx SS'.(Banks) in
					let BS_sigma 			:= list_to_BankState_t BS' BS'_proof in
					(* Update inter-bank counters *)
					let v' 						:= seq.set_nth 0 (proj1_sig gbl_cnt'.(cRDbgs)) (Bankgroup_to_nat bg_idx) 0 in
					let v'_proof 			:= set_nth_safe_gen 0 0 BANKGROUPS bg_idx gbl_cnt'.(cRDbgs) in
					let v_sigma 			:= list_to_BoundedCounter BANKGROUPS v' v'_proof in
					let gcnt' 				:= (gbl_cnt' <|cRDdb := 0 |> <|cRDbgs := v_sigma|>) in
					Some (SS' <| Banks := BS_sigma |> <| SysCounters := gcnt'|> <| Busdir := Request_kind_t_BUS req.(Kind) |> )
				end)
			| CWR req => let bk_idx := req.(Address).(Bank) in
									 let bg_idx := req.(Address).(Bankgroup) in
									 let BS := seq.nth def_BKS (proj1_sig SS'.(Banks)) (Bank_to_nat bk_idx) in
				match BS with
				| IDLE _ => None
				| ACTIVE row cnt =>
					(* Update intra-bank counters *)
					let cnt' 					:= (cnt <| cWRsb := 0 |>) in
					let new_bk_state 	:= ACTIVE row cnt' in
					let	BS' 					:= seq.set_nth def_BKS (proj1_sig SS'.(Banks)) (Bank_to_nat bk_idx) new_bk_state in
					let BS'_proof 		:= set_nth_safe_gen def_BKS new_bk_state BANKS bk_idx SS'.(Banks) in
					let BS_sigma 			:= list_to_BankState_t BS' BS'_proof in
					(* Update inter-bank counters *)
					let v' 						:= seq.set_nth 0 (proj1_sig gbl_cnt'.(cWRbgs)) (Bankgroup_to_nat bg_idx) 0 in
					let v'_proof 			:= set_nth_safe_gen 0 0 BANKGROUPS bg_idx gbl_cnt'.(cWRbgs) in
					let v_sigma 			:= list_to_BoundedCounter BANKGROUPS v' v'_proof in
					let gcnt' 				:= (gbl_cnt' <|cWRdb := 0 |> <|cWRbgs := v_sigma|>) in
					(* Build new system state *)
					Some (SS' <| Banks := BS_sigma |> <| SysCounters := gcnt'|> <| Busdir := Request_kind_t_BUS req.(Kind) |> )
				end
			| CWRA req => let bk_idx := req.(Address).(Bank) in
										let bg_idx := req.(Address).(Bankgroup) in
										let BS := seq.nth def_BKS (proj1_sig SS'.(Banks)) (Bank_to_nat bk_idx) in
				match BS with
				| IDLE _ => None
				| ACTIVE row cnt =>
					(* Update intra-bank counters *)
					let cnt' 					:= (cnt <| cRDsb := 0 |> <| cWRA := 0 |>) in
					let new_bk_state 	:= IDLE cnt' in
					let	BS' 					:= seq.set_nth def_BKS (proj1_sig SS'.(Banks)) (Bank_to_nat bk_idx) new_bk_state in
					let BS'_proof 		:= set_nth_safe_gen def_BKS new_bk_state BANKS bk_idx SS'.(Banks) in
					let BS_sigma 			:= list_to_BankState_t BS' BS'_proof in
					(* Update inter-bank counters *)
					let v' 						:= seq.set_nth 0 (proj1_sig gbl_cnt'.(cWRbgs)) (Bankgroup_to_nat bg_idx) 0 in
					let v'_proof 			:= set_nth_safe_gen 0 0 BANKGROUPS bg_idx gbl_cnt'.(cWRbgs) in
					let v_sigma 			:= list_to_BoundedCounter BANKGROUPS v' v'_proof in
					let gcnt' 				:= (gbl_cnt' <|cWRdb := 0 |> <|cWRbgs := v_sigma|>) in
					(* Build new system state *)
					Some (SS' <| Banks := BS_sigma |> <| SysCounters := gcnt'|> <| Busdir := Request_kind_t_BUS req.(Kind) |> )
				end
			|	ACT req => let bk_idx := req.(Address).(Bank) in
									 let bg_idx := req.(Address).(Bankgroup) in
									 let BS := seq.nth def_BKS (proj1_sig SS'.(Banks)) (Bank_to_nat bk_idx) in
				match BS with
				| IDLE cnt =>
					(* Update intra-bank counters *)
					let cnt' 					:= (cnt <| cACTsb := 0 |>) in
					let new_bk_state 	:= ACTIVE req.(Address).(Row) cnt' in
					let	BS' 					:= seq.set_nth def_BKS (proj1_sig SS'.(Banks)) (Bank_to_nat bk_idx) new_bk_state in
					let BS'_proof 		:= set_nth_safe_gen def_BKS new_bk_state BANKS bk_idx SS'.(Banks) in
					let BS_sigma 			:= list_to_BankState_t BS' BS'_proof in
					(* Update inter-bank counters *)
					let v' 						:= seq.set_nth 0 (proj1_sig gbl_cnt'.(cACTbgs)) (Bankgroup_to_nat bg_idx) 0 in
					let v'_proof 			:= set_nth_safe_gen 0 0 BANKGROUPS bg_idx gbl_cnt'.(cACTbgs) in
					let v_sigma 			:= list_to_BoundedCounter BANKGROUPS v' v'_proof in
					let rot' 					:= rotate (proj1_sig gbl_cnt'.(cACTdb)) 0 in
					let rot'_proof 		:= rotate_safe 0 3 gbl_cnt'.(cACTdb) _3_gt_0 in
					let rot_sigma 		:= list_to_BoundedCounter 3 rot' rot'_proof in
					let gcnt' 				:= (gbl_cnt' <| cACTdb := rot_sigma |> <|cACTbgs := v_sigma|>) in
					(* Build new system state *)
					Some (SS' <| Banks := BS_sigma |> <| SysCounters := gcnt' |>)
				| ACTIVE _ _ => None
				end
			| PRE req => let bk_idx := req.(Address).(Bank) in
									 let bg_idx := req.(Address).(Bankgroup) in
									 let BS := seq.nth def_BKS (proj1_sig SS'.(Banks)) (Bank_to_nat bk_idx) in
				match BS with
				| IDLE cnt
				| ACTIVE _ cnt =>
					(* Update local counters *)
					let cnt' 					:= (cnt <| cPRE := 0 |>) in
					let new_bk_state 	:= IDLE cnt' in
					let	BS' 					:= seq.set_nth def_BKS (proj1_sig SS'.(Banks)) (Bank_to_nat bk_idx) new_bk_state in
					let BS'_proof 		:= set_nth_safe_gen def_BKS new_bk_state BANKS bk_idx SS'.(Banks) in
					let BS_sigma 			:= list_to_BankState_t BS' BS'_proof in
					(* Build new system state *)
					Some (SS' <| Banks := BS_sigma |>)
				end
			| REF => (* Update refresh counter, no need to change bank states *)
				let cnt' := (SS'.(SysCounters) <| cREF := 0 |>) in Some (SS' <| SysCounters := cnt' |>)
			| PREA =>
				(* Update all banks  *)
				let banks' := list_to_BankState_t (seq.map (fun bs => 
					match bs with
					| IDLE lc => IDLE (lc <| cPRE := 0 |>) 
					| ACTIVE row lc => IDLE (lc <| cPRE := 0 |>)
					end) (proj1_sig SS'.(Banks))) (act_cPRE_safe SS') in
				Some (SS' <| Banks := banks' |>)
			| NOP => Some SS'
			end
		end.

End BankMachine.
